program Project1;

{$APPTYPE CONSOLE}

procedure TForm1.Button1Click(Sender: TObject);
var reg:TRegExpr;
begin
reg:=TRegExpr.Create;
reg.Expression:='(thread_title_)[\d]+["][>]([^<]+)';
if reg.Exec(memo1.text) then
repeat
  memo2.Lines.Add(reg.Match[2]);
until not reg.ExecNext;
 
FreeAndNil(reg);
end;
