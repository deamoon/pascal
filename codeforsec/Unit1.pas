unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    btn1: TButton;
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
var reg:TRegExpr;
begin
reg:=TRegExpr.Create;
reg.Expression:='(thread_title_)[\d]+["][>]([^<]+)';
if reg.Exec(memo1.text) then
repeat
  memo2.Lines.Add(reg.Match[2]);
until not reg.ExecNext;
 
FreeAndNil(reg);
end;

end.
 