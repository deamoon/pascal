{$APPTYPE CONSOLE}

var
  p,s:longint;

procedure sum(n:longint);
var
  m:longint;
begin
  s:=0; p:=0;
  while n<>0 do begin
    m:=n mod 10;
    s:=s+m;
    if m=5 then Inc(p);
    n:=n div 10;
  end;
end;

var
  i:longint; d:Int64;
begin
  Assign(Output,'2.txt');
  d:=1;
  for i:=1000000000 to 1000110000{1999999999} do begin
    sum(i);
    if ((s>=30) and (s<40) and (p<=1)) then begin
      if i mod 29 =0 then Writeln(i);
    end;
  end;
end.
