program Project1;

{$APPTYPE CONSOLE}

uses
  SysUtils;

type point=record
  x:int64;
  y:int64;
end;

function vc(x1,y1,x2,y2:int64):int64;
begin
  vc:=x1*y2-x2*y1;
end;

function pl(a,b,c,d,e,f:int64):Extended;
var
  p:Extended;
begin
  p:=(a*d-b*c+c*f-d*e+e*b-a*f)/2;
  if p<0 then p:=-p;
  pl:=p;
end;


var
  f,f1:text;
  x,y,x1,y1:array[0..5001] of int64;
  i,n,m,j:longint;  s,s1,r,p:extended; a,b:point; t:Boolean;

begin
  assign(f,'input.txt');
  assign(f1,'output.txt');
  reset(f);
  rewrite(f1);

  readln(f,n);
  for i:=1 to n do begin
    Readln(f,x[i],y[i])
  end;
  readln(f,m);
  for i:=1 to m do begin
    Readln(f,x1[i],y1[i])
  end;

s:=0;
  for i := 1 to n-1 do begin
    s:=s+(x[i]*y[i+1]-x[i+1]*y[i])/2;
  end;
  s:=s+(x[n]*y[1]-x[1]*y[n])/2;
  if s<0 then s:=-s;

  for i:=1 to m do begin
   s1:=0;
    for j:=1 to n-1 do begin
      p:=pl(x[j],y[j],x[j+1],y[j+1],x1[i],y1[i]);
      s1:=s1+p;
    end;
    p:=pl(x[n],y[n],x[1],y[1],x1[i],y1[i]);
    s1:=s1+p; if s1<0 then s1:=-s;
    r:=s1-s; if r<0 then r:=-r;

    if r<1e-8 then begin
      t:=true;
      for j:=1 to n-1 do begin
        a.x:=x[j]; a.y:=y[j]; b.x:=x[j+1]; b.y:=y[j+1];
        if vc(b.x-a.x,b.y-a.y,x1[i]-a.x,y1[i]-a.y)=0 then begin t:=false; Break; end;
      end;
      a.x:=x[n]; a.y:=y[n]; b.x:=x[1]; b.y:=y[1];
      if vc(b.x-a.x,b.y-a.y,x1[i]-a.x,y1[i]-a.y)=0 then begin t:=false; end;

      if t then writeln(f1,'inside') else writeln(f1,'border');
    end else writeln(f1,'outside');

   end;

  close(f);
  close(f1);
end.
