program Project1;

{$APPTYPE CONSOLE}

uses
  SysUtils;


type point=record
  x:int64;
  y:int64;
end;

type pr=record
  a:int64;
  b:int64;
  c:Int64;
end;

function sc(x1,y1:Real; x2,y2:int64):real;
begin
  sc:=x1*x2+y1*y2;
end;
function vc(x1,y1,x2,y2:int64):int64;
begin
  vc:=x1*y2-x2*y1;
end;
function vek(n,k:int64):int64;
begin
  vek:=k-n;
end;

function prin(a,b:point;x,y:extended):Boolean;
begin
 if ( (sc(x-a.x,y-a.y,b.x-a.x,b.y-a.y)<0) or (sc(x-b.x,y-b.y,a.x-b.x,a.y-b.y)<0) ) then prin:=false else prin:=true;

end;

var
  f,f1:text;
  x,y,x1,y1,a:array[0..200001] of int64;
  i,n:longint;  tt,t,vp,pol:boolean; s:int64;

begin
  assign(f,'input.txt');
  assign(f1,'output.txt');
  reset(f);
  rewrite(f1);

  readln(f,n);
  for i := 1 to n do begin
    readln(f,x[i],y[i]);
  end;
  s:=0;
  for i := 1 to n-1 do begin
    x1[i]:=x[i+1]-x[i]; y1[i]:=y[i+1]-y[i];
    s:=s+(x[i]*y[i+1]-x[i+1]*y[i]);
    //s1:=s1+(x[i+1]+x[i])*(y[i]-y[i+1])/2;
  end;
  x1[n]:=x[1]-x[n]; y1[n]:=y[1]-y[n];
  s:=s+(x[n]*y[1]-x[1]*y[n]);

  tt:=true;
  for i := 1 to n-1 do begin
    a[i]:=vc(x1[i],y1[i],x1[i+1],y1[i+1]);
    if a[i]=0 then tt:=False;
  end;
  a[n]:=vc(x1[n],y1[n],x1[1],y1[1]);
  if a[n]=0 then tt:=False;

  t:=true; vp:=true;
  for i :=1 to n do begin
    if ((a[i]<>0) and (t)) then begin
      if a[i]>0 then pol:=true;
      if a[i]<0 then pol:=false;
      t:=false;
      continue;
    end;
    if a[i]=0 then continue;
    if ( ((pol) and (a[i]<0)) or ((not(pol)) and (a[i]>0)) ) then vp:=false;
  end;

  writeln(f1,s/2:0:1);

  if ((vp) and (tt)) then write(f1,'strictly convex');
  if ((vp) and (not((tt)))) then write(f1,'nonstrictly convex');
  if not(vp) then write(f1,'not convex');

  close(f);
  close(f1);
end.
