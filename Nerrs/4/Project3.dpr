{$APPTYPE CONSOLE}

var
  x,y,r:array[0..100100] of longint;

procedure qsort(l,p:longint);
var
  q,i,j,t:LongInt;
begin
  i:=l; j:=p; q:=r[(i+j) div 2];
  repeat
    while r[i]>q do Inc(i);
    while r[j]<q do dec(j);
    if i<=j then begin
      t:=r[i]; r[i]:=r[j]; r[j]:=t; t:=x[i]; x[i]:=x[j]; x[j]:=t; t:=y[i]; y[i]:=y[j]; y[j]:=t; Inc(i); Dec(j);
    end;
  until (i>j);
  if i<p then qsort(i,p);
  if j>l then qsort(l,j);
end;

var
   i,n,j:LongInt; q:Boolean; s:extended;
begin
  assign(input,'circles.in'); assign(output,'circles.out');
  Readln(n);
  for i:=1 to n do begin
    Readln(x[i],y[i],r[i]);
  end;
  qsort(1,n);
  s:=0;
  for i:=1 to n do begin
    q:=True;
    for j:=i-1 downto 1 do begin
      if (x[j]-x[i])*(x[j]-x[i])+(y[j]-y[i])*(y[j]-y[i])<=r[j]*r[j] then begin q:=false; Break; end;
    end;
    if q then s:=s+r[i]*r[i];
  end;
  write(pi*s:0:12);
end.
