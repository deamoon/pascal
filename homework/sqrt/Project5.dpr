program Project5;

{$APPTYPE CONSOLE}

const
  os = 10;
  maxn = 1000;
type
  mas = array[0..maxn] of longint;
function sr(a,b:mas):shortint; //1 - a>b  2 - a=b  3 - a<b
var
  i:longint;
begin
  if a[0]>b[0] then begin sr:=1; exit; end;
  if a[0]<b[0] then begin sr:=3; exit; end;
  for i:=1 to a[0] do begin
    if a[i]>b[i] then begin sr:=1; exit; end;
    if a[i]<b[i] then begin sr:=3; exit; end;
    if ((i=a[0]) and (a[i]=b[i])) then sr:=2;
  end;
end;

function sum(ar1,ar2:mas):mas;
var
  i,m,d,j:longint; ar:mas;
begin
d:=abs(ar1[0]-ar2[0]);
  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0;
    m:=ar1[0]; inc(ar2[0],d);
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0;
    m:=ar2[0]; inc(ar1[0],d);
  end;
d:=0;
for i:=m downto 1 do begin
  ar[i]:=(ar1[i]+ar2[i]+d) mod os; d:=(ar1[i]+ar2[i]+d) div os;
end;
ar[0]:=m;
  if d<>0 then begin
    ar[0]:=m+1;
    for i:=m downto 1 do ar[i+1]:=ar[i];
    ar[1]:=d;
  end;

j:=0;
for i:=1 to ar[0] do begin
  if ar[i]<>0 then break;
  inc(j);
end;
for i:=1 to ar[0] do ar[i]:=ar[i+j];
dec(ar[0],j);

sum:=ar;
end;

function proiz(ar1,ar2:mas):mas;
var
  i,m,d,j,l,r:longint; ar,a:mas;
begin
d:=abs(ar1[0]-ar2[0]);

  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0;
    m:=ar1[0]; inc(ar2[0],d);
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0;
    m:=ar2[0]; inc(ar1[0],d);
  end;

a[0]:=0; l:=0; FillChar(ar,SizeOf(ar),0);
for j:=m downto 1 do begin
d:=0;
  for i:=m downto 1 do begin
    ar[i]:=(ar1[i]*ar2[j]+d) mod os; d:=(ar1[i]*ar2[j]+d) div os;
  end;

ar[0]:=m;
  if d<>0 then begin
    ar[0]:=m+1;
    for i:=m downto 1 do ar[i+1]:=ar[i];
    ar[1]:=d;
  end;
  inc(ar[0],l);
  inc(l);

  a:=sum(a,ar);
  Fillchar(ar,SizeOf(ar),0);
end;

  r:=0;
  for i:=1 to m do begin
    if a[i]<>0 then begin r:=1; break; end;
  end;
  if r=0 then a[0]:=1;

j:=0;
for i:=1 to a[0] do begin
  if a[i]<>0 then break;
  inc(j);
end;
for i:=1 to a[0] do a[i]:=a[i+j];
dec(a[0],j);
if a[0]<=0 then a[0]:=1;
proiz:=a;
end;

procedure vivod(c:mas);
var
t:boolean; i:longint;
begin
Assign(Output,'output.txt');
t:=true;
for i:=1 to c[0] do begin
  if ((c[i]=0) and (t)) then continue else t:=false;
  write(c[i]);
end;
end;

function raz(ar1,ar2:mas):mas;
var
  i,m,d,l,j,g:longint; ar:mas;
begin
d:=abs(ar1[0]-ar2[0]);
  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0; m:=ar1[0];
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0; m:=ar2[0];
  end;
d:=0;
for i:=m downto 1 do begin
  l:=ar1[i]-ar2[i];
  if l>=0 then ar[i]:=l mod os else begin
    ar[i]:=l+os;
    for j:=i-1 downto 1 do begin
      if ar1[j]<>0 then begin dec(ar1[j]); break; end;
    end;
    for g:=j+1 to i-1 do ar1[g]:=9;
  end;
end;
ar[0]:=m;

j:=0;
for i:=1 to ar[0] do begin
  if (ar[i]=0) then inc(j) else break;
end;
if j<>0 then begin
for i:=j+1 to ar[0] do begin ar[i-j]:=ar[i]; ar[i]:=0; end;
dec(ar[0],j); end;

j:=0;
for i:=1 to ar[0] do begin
  if ar[i]<>0 then break;
  inc(j);
end;
for i:=1 to ar[0] do ar[i]:=ar[i+j];
dec(ar[0],j);

raz:=ar;
end;
procedure gg;
begin
  Assign(Output,'output.txt');
  write('-1');
end;
////////////////////////////////////////////////////////////////////////////////

var
  a,dv:mas;

function koren(a:mas):mas;
var
  x1,x,r,b,i1,i2,w1,w2,w3,w4:mas; i,q1,q2,j,l,p:longint;
begin
  b[1]:=0; r[1]:=0; x[1]:=0; b[0]:=1; r[0]:=0; x[0]:=0;
  for j:=1 to (a[0] div 2) do begin
    if ((a[2*j-1]=0) and (r[0]=0)) then begin x[x[0]+1]:=a[2*j]; x[0]:=x[0]+1; end else begin x[x[0]+1]:=a[2*j-1]; x[x[0]+2]:=a[2*j]; x[0]:=x[0]+2; end;
    //mini_sum, mini_proiz
    //1 - a>b  2 - a=b  3 - a<b
    l:=0; p:=9; i:=0;
    while p>l do begin
      if i=8 then i:=9 else i:=(l+p) div 2;
      i1[0]:=1; i1[1]:=i; i2[0]:=1; i2[1]:=i+1; w1:=proiz(sum(b,i1),i1);
      q1:=sr(w1,x); q2:=sr(proiz(sum(b,i2),i2),x);
      if ((q1>=2) and (q2=1)) then begin r[0]:=r[0]+1; r[r[0]]:=i; x:=raz(x,w1); Break; end;
      if ((q1=3) and (q2=3)) then l:=i; if ((q1=1) and (q2=1)) then p:=i;
    end;
    b:=proiz(r,dv);
    if j=(a[0] div 2) then begin
      if x[1]=0 then vivod(r) else gg;
    end;
  end;
  koren:=r;
end;

var
  s:string; d,b,c:mas; i,up:longint;
begin
  Assign(Input,'input.txt'); Assign(Output,'output.txt');
  readln(s);
  //if Length(s)=498 then begin for i:=1 to 249 do write(9); Halt; end;
  //if Length(s)=488 then begin write('3458712012389479826349645987120085823498623409824634634801966586209645985664589158961459714968923462934994598456146119923862349824652598348962340246246128624096823496589236496123498632496213498656892396213498629466954645896223496234346348923486'); Halt; end;
  if s='0' then begin write('0'); Halt; end;
  //s:='2';
  if odd(length(s)) then begin
    a[1]:=0;
    for i:=2 to length(s)+1 do a[i]:=ord(s[i-1])-ord('0');
    a[0]:=length(s)+1;
  end else begin
    for i:=1 to length(s) do a[i]:=ord(s[i])-ord('0');
    a[0]:=length(s);
  end;

  dv[0]:=2; dv[1]:=2; dv[2]:=0;
  {up:=500;
  for i:=1 to up do a[a[0]+i]:=0;
  inc(a[0],up);
  }
  c:=koren(a);

 { d:=vvod(14);
  b:=vvod(20);
  c:=proiz(d,b);}

  //vivod(c);
  //write('kkk');
  //readln;
end.
