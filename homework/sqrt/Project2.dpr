program Project2;
{$APPTYPE CONSOLE}
const
  os = 10;
  maxn = 10000;
type
  mas = array[0..maxn] of longint;
var zap:Longint;
function sr(a,b:mas):shortint; //1 - a>b  2 - a=b  3 - a<b
var
  i:longint;
begin
  if a[0]>b[0] then begin sr:=1; exit; end;
  if a[0]<b[0] then begin sr:=3; exit; end;
  for i:=a[0] downto 1 do begin
    if a[i]>b[i] then begin sr:=1; exit; end;
    if a[i]<b[i] then begin sr:=3; exit; end;
    if ((i=a[0]) and (a[i]=b[i])) then sr:=2;
  end;
end;
function sum(a:mas;n:longint):mas;
var i,d:longint; b:mas;
begin
  FillChar(b,SizeOf(b),0);
  d:=0; b[0]:=a[0];
  for i:=1 to a[0] do begin
    b[i]:=(a[i]+n+d) mod os; d:=(a[i]+n+d) div os;
    n:=0;
  end;
  if d<>0 then begin b[a[0]+1]:=d; inc(b[0]); end;
  sum:=b;
end;
function proiz(a:mas;n:longint):mas;
var i,d:longint; b:mas;
begin
  FillChar(b,SizeOf(b),0);
  d:=0; b[0]:=a[0];
  for i:=1 to a[0] do begin
    b[i]:=(a[i]*n+d) mod os; d:=(a[i]*n+d) div os;
  end;
  if d<>0 then begin b[a[0]+1]:=d; inc(b[0]); end;
  proiz:=b;
end;
function proiz1(a:mas;n:longint):mas;
var i,d:longint; b,c:mas;
begin
  c:=a; d:=a[0]; for i:=1 to a[0] do begin a[i]:=c[d]; dec(d); end;
  d:=0; b[0]:=a[0];
  for i:=1 to a[0] do begin
    b[i]:=(a[i]*n+d) mod os; d:=(a[i]*n+d) div os;
  end;
  if d<>0 then begin b[a[0]+1]:=d; inc(b[0]); end;
  proiz1:=b;
end;
procedure vivod(a:mas);
var i:longint;
begin
  Assign(output,'output.txt');
 // if zap<>0 then begin
    for i:=1 to ((zap+1) div 2) do write(a[i]);
    write(',');
    for i:=((zap+1) div 2)+1 to a[0] do write(a[i]);
  //end else begin
  //  for i:=1 to a[0] do write(a[i]);
  //end;

end;
function raz(a,b:mas):mas;
var i,j,z:longint; c:mas;
begin
  for i:=1 to a[0] do begin
    c[i]:=a[i]-b[i];
    if c[i]<0 then begin
      c[i]:=c[i]+os;
      j:=i+1;
      while a[j]=0 do inc(j);
      dec(a[j]);
      for z:=j-1 downto i+1 do a[z]:=9;
    end;
  end;
  c[0]:=1;
  for i:=a[0] downto 1 do begin
    if c[i]<>0 then begin c[0]:=i; break; end;
  end;
  raz:=c;
end;
procedure gg;
begin
  Assign(Output,'output.txt');
  write('-1');
end;
////////////////////////////////////////////////////////////////////////////////
function koren(a:mas):mas;
var
  x1,x,r,b,i1,i2,w1,w2,w3,w4:mas; i,q1,q2,j,l,p,e:longint;
begin
  b[1]:=0; r[1]:=0; x[1]:=0; b[0]:=1; r[0]:=0; x[0]:=0;
  for j:=1 to (a[0] div 2) do begin
    if ((a[2*j-1]=0) and (r[0]=0)) then begin
      x[1]:=a[2]; x[0]:=x[0]+1;
    end else begin
      if x[x[0]]=0 then begin x[0]:=0 end;
      for i:=x[0]+2 downto 3 do x[i]:=x[i-2];
      x[2]:=a[2*j-1]; x[1]:=a[2*j]; x[0]:=x[0]+2; e:=0;
      for i:=x[0] downto 1 do if x[i]=0 then Inc(e) else Break;
      Dec(x[0],e);
    end;
    l:=0; p:=9; i:=0;
    while p>l do begin
      FillChar(w1,SizeOf(w1),0);
      if i=8 then i:=9 else i:=(l+p) div 2;
      if i=0 then begin w1[0]:=1 end else w1:=proiz(sum(b,i),i);
      q1:=sr(w1,x); q2:=sr(proiz(sum(b,i+1),i+1),x);
      if ((q1>=2) and (q2=1)) then begin r[0]:=r[0]+1; r[r[0]]:=i; x:=raz(x,w1); Break; end;
      if ((q1=3) and (q2>=2)) then l:=i; if ((q1=1) and (q2=1)) then p:=i;
    end;
    b:=proiz(proiz1(r,2),10);
    {if j=(a[0] div 2) then begin
      if x[x[0]]=0 then vivod(r) else gg;
    end;}
  end;
  koren:=r;
end;

var
  s:string; b,c,a:mas; i,up,d:longint;
begin
  Assign(Input,'input.txt'); Assign(Output,'output.txt');
  readln(s); zap:=Pos(',',s); d:=0;
  if zap=0 then begin
    if Odd(Length(s)) then s:='0'+s;
    zap:=Length(s);
  end else begin
    if Odd(Length(s)-zap) then begin s:=s+'0'; d:=1; end;
    Delete(s,zap,1);
    if Odd(zap-1) then s:='0'+s;
    if d=1 then Dec(zap);
  end;
  for i:=1 to length(s) do a[i]:=ord(s[i])-ord('0');
  a[0]:=length(s);
  up:=10; for i:=1 to up do a[a[0]+i]:=0; inc(a[0],up);
  c:=koren(a);
  vivod(c);
end.
