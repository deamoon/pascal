const
  os = 10;
  maxn = 1000;
type
  mas = array[0..maxn] of longint;
function max(a,b:longint):longint;
begin
  if a>b then max:=a else max:=b;
end;

function min(a,b:longint):longint;
begin
  if a<b then min:=a else min:=b;
end;

procedure null(var a:mas);
var
  i:LongInt; b:mas;
begin
  for i:=0 to maxn do a[i]:=0;
end;

function sr(a,b:mas):shortint; //1 - a>b  2 - a=b  3 - a<b
var
  i:longint;
begin
  if a[0]>b[0] then begin sr:=1; exit; end;
  if a[0]<b[0] then begin sr:=3; exit; end;
  for i:=1 to a[0] do begin
    if a[i]>b[i] then begin sr:=1; exit; end;
    if a[i]<b[i] then begin sr:=3; exit; end;
    if ((i=a[0]) and (a[i]=b[i])) then sr:=2;
  end;
end;

function sum(ar1,ar2:mas):mas; // ������� �������� 2 �������� 123+456=579, � ������� �������� ���-�� ����, ������ ����� �������(����� �������)
var
  i,m,d,j:longint; ar:mas;
begin
d:=abs(ar1[0]-ar2[0]);
  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0;
    m:=ar1[0]; inc(ar2[0],d);
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0;
    m:=ar2[0]; inc(ar1[0],d);
  end;
d:=0;
for i:=m downto 1 do begin
  ar[i]:=(ar1[i]+ar2[i]+d) mod os; d:=(ar1[i]+ar2[i]+d) div os;
end;
ar[0]:=m;
  if d<>0 then begin
    ar[0]:=m+1;
    for i:=m downto 1 do ar[i+1]:=ar[i];
    ar[1]:=d;
  end;
  
j:=0;
for i:=1 to ar[0] do begin
  if ar[i]<>0 then break;
  inc(j);
end;
for i:=1 to ar[0] do ar[i]:=ar[i+j];
dec(ar[0],j);
  
sum:=ar;
end;

procedure reverse(var a,b:mas);
var
  t:mas;
begin
  if a[0]>b[0] then begin t:=a; a:=b; b:=t; end;
end;

function sdvig(a:mas;n:longint):mas;
var
  i:LongInt;
begin
  for i:=a[0] downto 1 do a[i+n]:=a[i];
  for i:=1 to n do a[i]:=0;
  Inc(a[0],n);
  sdvig:=a;
end;

function proiz(ar1,ar2:mas):mas; // ������� ��������� 2 �������� 123*456=nnnn, � ������� �������� ���-�� ����, ������ ����� �������(����� �������)
var
  i,m,d,j,l,r:longint; ar,a:mas;
begin
d:=abs(ar1[0]-ar2[0]);

  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0;
    m:=ar1[0]; inc(ar2[0],d);
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0;
    m:=ar2[0]; inc(ar1[0],d);
  end;

a[0]:=0; l:=0;
for j:=m downto 1 do begin
d:=0;
  for i:=m downto 1 do begin
    ar[i]:=(ar1[i]*ar2[j]+d) mod os; d:=(ar1[i]*ar2[j]+d) div os;
  end;

ar[0]:=m;
  if d<>0 then begin
    ar[0]:=m+1;
    for i:=m downto 1 do ar[i+1]:=ar[i];
    ar[1]:=d;
  end;
  inc(ar[0],l);
  inc(l);

  a:=sum(a,ar);
  for i:=0 to ar[0] do ar[i]:=0;
end;

  r:=0;
  for i:=1 to m do begin
    if a[i]<>0 then begin r:=1; break; end;
  end;
  if r=0 then a[0]:=1;

j:=0;
for i:=1 to a[0] do begin
  if a[i]<>0 then break;
  inc(j);
end;
for i:=1 to a[0] do a[i]:=a[i+j];
dec(a[0],j);
if a[0]<=0 then a[0]:=1;
proiz:=a;
end;

procedure vivod(c:mas);
var
t:boolean; i:longint;
begin
t:=true;
for i:=1 to c[0] do begin
  if ((c[i]=0) and (t)) then continue else t:=false;
  write(c[i]);
end;
end;

function raz(ar1,ar2:mas):mas;
var
  i,m,d,l,j,g:longint; ar:mas;
begin
d:=abs(ar1[0]-ar2[0]);
  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0; m:=ar1[0];
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0; m:=ar2[0];
  end;
d:=0;
for i:=m downto 1 do begin
  l:=ar1[i]-ar2[i];
  if l>=0 then ar[i]:=l mod os else begin
    ar[i]:=l+os;
    for j:=i-1 downto 1 do begin
      if ar1[j]<>0 then begin dec(ar1[j]); break; end;
    end;

    //if ar1[j]=0 then begin
      for g:=j+1 to i-1 do ar1[g]:=9;
    //end;

  end;
end;
ar[0]:=m;

j:=0;
for i:=1 to ar[0] do begin
  if (ar[i]=0) then inc(j) else break;
end;
if j<>0 then begin
for i:=j+1 to ar[0] do begin ar[i-j]:=ar[i]; ar[i]:=0; end;
dec(ar[0],j); end;

j:=0;
for i:=1 to ar[0] do begin
  if ar[i]<>0 then break;
  inc(j);
end;
for i:=1 to ar[0] do ar[i]:=ar[i+j];
dec(ar[0],j);

raz:=ar;
end;
////////////////////////////////////////////////////////////////////////////////

var
  a,dv:mas;

function koren(a:mas):mas;
var
  x1,x,r,b,i1,i2,w1,w2,w3,w4:mas; i,q1,q2,j:longint;
begin
  b[1]:=0; r[1]:=0; x[1]:=0; b[0]:=1; r[0]:=0; x[0]:=0;
  for j:=1 to (a[0] div 2) do begin
    if ((a[2*j-1]=0) and (r[0]=0)) then begin x[x[0]+1]:=a[2*j]; x[0]:=x[0]+1; end else begin x[x[0]+1]:=a[2*j-1]; x[x[0]+2]:=a[2*j]; x[0]:=x[0]+2; end;
    for i:=0 to 8 do begin
      i1[0]:=1; i1[1]:=i; i2[0]:=1; i2[1]:=i+1;
      w3:=sum(b,i1); w4:=sum(b,i2);
      w1:=proiz(w3,i1); w2:=proiz(w4,i2);
      q1:=sr(w1,x); q2:=sr(w2,x);
      if ((q1>=2) and (q2=1)) then begin r[0]:=r[0]+1; r[r[0]]:=i; x:=raz(x,proiz(sum(b,i1),i1)); Break; end;
      if i=8 then begin i1[1]:=9; r[0]:=r[0]+1; r[r[0]]:=9; x:=raz(x,proiz(sum(b,i1),i1)); end;
    end;
    b:=proiz(r,dv);
  end;
  koren:=r;
end;

var
  s:string; c:mas; i,up:longint;
begin
  readln(s);
  if odd(length(s)) then begin
    a[1]:=0;
    for i:=2 to length(s)+1 do a[i]:=ord(s[i-1])-ord('0');
    a[0]:=length(s)+1;
  end else begin
    for i:=1 to length(s) do a[i]:=ord(s[i])-ord('0');
    a[0]:=length(s);
  end;

  dv[0]:=2; dv[1]:=2; dv[2]:=0;
  up:=50;
  for i:=1 to up do a[a[0]+i]:=0;
  inc(a[0],up);
  
  c:=koren(a);
  
  vivod(c);
end.