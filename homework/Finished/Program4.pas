function F(x:real):real;
begin
  if abs(x-0.5)<0.001 then F:=pi*pi/2 else F:=(1+cos(2*pi*x))/((2*x-1)*(2*x-1));
end;

var
  i:real;
begin
  i:=-1;
  writeln('-1','     ',f(-1));
  while i<=-0.11 do begin
    i:=i+0.1;
    writeln(i,'   ',f(i));
  end;
  writeln(' 0','     ',f(0));
  i:=0;
  while i<=0.39 do begin
    i:=i+0.1;
    writeln(' ',i,'   ',f(i));
  end;
  //writeln(' 0.5','   ',pi*pi/2);
  i:=0.4;
  while i<=0.89 do begin
    i:=i+0.1;
    writeln(' ',i,'   ',f(i));
  end;
  writeln(' 1','     ',f(1));
  writeln('///');
end.