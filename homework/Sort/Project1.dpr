{$APPTYPE CONSOLE}

var a:array[0..100005] of real; sr,per:Int64;

procedure qsort(l,r:longint);
var
  x,t:Real; i,j:LongInt;
begin
  i:=l; j:=r; x:=a[(i+j) div 2];
  repeat
    while a[i]<x do begin Inc(i); Inc(sr); end;
    while a[j]>x do begin Dec(j); Inc(sr); end;
    if i<=j then begin t:=a[i]; a[i]:=a[j]; a[j]:=t; Inc(per,3); Inc(i); Dec(j); end;
  until i>j;
  if i<r then qsort(i,r);
  if j>l then qsort(l,j);
end;

var
  x,i,n,j,min,l,r:longint; t:real;

begin
  Assign(Input,'5ran.txt'); Assign(Output,'1.txt'); i:=0;
  while not(Eof(Input)) do begin Inc(i); read(a[i]) end;
  n:=i; sr:=0; per:=0;
  qsort(1,n);
  //for i:=1 to n do write(a[i]:0:0,' ');
  write(n,' ',sr,' ',per);
end.
