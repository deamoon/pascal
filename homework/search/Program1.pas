procedure rangen;
var
  f1,f2:text; p,i,t:longint; s,s1:real;
begin
  //assign(f1,'input.txt'); reset(f1);
  assign(f2,'ascn.100000.txt'); rewrite(f2);
  p:=100000; t:=0; s1:=0;
  while t<>p do begin
    s:=random(500)+(t+1)*100+random(10)*0.1;
    if s>=s1 then begin write(f2,s,' '); inc(t); s1:=s; end;
  end;
  //close(f1);
  close(f2);
end;

var
  f1,f2:text; x,l,r,p,i,t,j,k:longint; s,s1,n:real; a:array[0..100001] of real; f:boolean;
begin
  assign(f1,'in.txt'); reset(f1); //assign(f2,'output.txt'); rewrite(f2);
  i:=0; a[0]:=-1; f:=false;
  repeat
    inc(i); read(f1,a[i]);
    if a[i]<a[i-1] then f:=true;
  until eof(f1);

  for j:=1 to 10 do begin
    n:=8;
    for k:=1 to i do begin
        if a[k]=n then writeln(k,' ');
    end;
    if f then writeln('Sequence is not ascending') else begin
      l:=1; r:=i;
      while r>=l do begin
        x:=(r+l) div 2;
        if a[x]>n then r:=x-1 else l:=x+1;
      end;
      if a[l-1]=n then writeln('BIN ',l-1,' ');
    end;
  end;
  
  close(f1); //close(f2);
end.