{$APPTYPE CONSOLE}
Uses
  SysUtils;
const
  n = 1 shl 21;
  p = 1000001;

var
  mas: array [1..2*n-1] of longint;
  i, j, k, x, y, sum, c: longint;

procedure insert(v : longint);
begin
  v:=v+n-1;
  While v > 0 do begin
    mas[v] := mas[v] + 1;
    v := v div 2;
  end;
end;

procedure otvet(l, r : longint);
begin
  l := l + n-1;
  r := r + n-1;
  sum := 0;
  While l <= r do begin
    If l = r then begin
      sum:=sum + mas[l];
      break;
    end;
    If l mod 2 = 1 then begin
      sum := sum + mas[l];
      inc(l);
    end;
    If r mod 2 = 0 then begin
      sum := sum + mas[r];
      dec(r);
    end;
     l := l div 2;
     r := r div 2;
   end;
    writeln(sum);
end;

begin
  reset(input,'kitten.in');
  rewrite(output, 'kitten.out');
  

  fillchar(mas, sizeof(mas), 0);
  While not eof do begin
    read(x);
    y := -p;
    If not eoln then readln(y);
    x := x + p ;
    y := y + p ;
    If y = 0 then begin
      If mas[x + n - 1] = 0 then insert (x)
    end else begin
      If x > y then begin
        c := y;
        y := x;
        x := c;
      end;
      otvet(x, y);
    end;
  end;
end.
