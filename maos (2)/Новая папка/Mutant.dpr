var
  n, i, j, m, lev, prav, l, r, sred, kol:longint;
  mas: array [1..1000000] of longint;
  cvet: array [1..200000] of longint;

begin  {levay and pravay granica}
  assign(input, 'mutants.in');
  reset(input);
  readln(n);
  For i := 1 to n do  
    read(mas[i]);
  readln(m);
  mas[n + 1] := 2000000000;
  n := n + 1;
  For i := 1 to m do 
    read(cvet[i]);
  assign(output, 'mutants.out');
  rewrite(output);
  for i := 1 to m do begin
    l := 1; 
    r := n; 
    kol := 0;
    If (mas[1] > cvet[i]) or (cvet[i] > mas[n - 1]) then 
      writeln (0)
    else  begin
      while l < r do begin 
        sred := (l + r) div 2;
          If mas[sred] > cvet[i] then  
            r := sred 
          else 
            l := sred + 1
      end;
      prav := l;
      l := 1;
      while l < r do begin
        sred := (l + r) div 2;
        If mas[sred] >= cvet[i] then  
          r := sred 
        else 
          l := sred + 1
      end;
      lev := l;   
      writeln(prav - lev); 
    end;    
  end;
end.

