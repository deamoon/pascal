program razrtabl;

{$APPTYPE CONSOLE}


uses
  SysUtils;

const
  inf = 1000000000;

var
  mas: array [1..1000001] of longint;
  min: array [0..30, 1..100001] of longint;
  i, j, k, n, m, u, v, res, lev, pr, ste, otv: longint;

function skoka(x, y: longint): longint;
begin
  if x <= y then skoka := x
  else
  skoka := y;
end;

function max(f, h: longint): longint;
begin
  if f >= h then  max := f
  else
    max := h;
end;

begin
  reset(input, 'sparse.in');
  rewrite(output, 'sparse.out');
  readln(n, m, mas[1]);
  readln(u, v);
  for i := 2 to n do mas[i] := (23 * mas[i - 1] + 21563) mod 16714589;   
  for i := 1 to n do min[0, i] := mas[i];
  res := 1; k := 0;
  while res <= n do begin
    inc(k);
    res := 2 * res;
  end;
  k := k - 1;
  for i := 1 to k do
    For j := 1 to n do
      min[i, j] := inf;
  for i := 1 to k do
    for j := 1 to n - (1 shl i) + 1  do
      min[i, j] := skoka(min[i - 1, j], min[i - 1, j + (1 shl (i - 1))]);

   for i := 1 to m  do begin
     if u = v then
       otv := mas[u] else begin
         lev := skoka(u, v);
         pr := max(u, v);
         res := 1;
         ste := 0;
         while res < pr - lev + 1 do begin
           inc(ste);
           res := 2 * res;
         end;
         dec(ste);

         otv := skoka(min[ste, lev], min[ste, pr - (1 shl ste) + 1]);
       end;
     if i <> m then begin
       u := ((17 * u + 751 + otv + 2 * i) mod n) + 1;
       v := ((13 * v + 593 + otv + 5 * i) mod n) + 1;
     end;
   end;
   writeln(u,' ', v,' ', otv);
end.
