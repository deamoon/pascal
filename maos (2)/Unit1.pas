unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, DBCtrls, StdCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, RegExpr, ExtCtrls, jpeg, GIFImg;

type

  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  html:string;
  DestEncoding:TStream;
  mm:TStringList;
  id, xxx:string;
  r : TRegExpr;
  ImgGif:TGifImage;
  ImgJpg:TJpegImage;
  posn,posk:integer;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
//�������� HTML ��� ����� � ���������� html � ���� ����
DestEncoding:= TMemoryStream.Create;
IdHTTP1.Get(edit5.Text,DestEncoding);
DestEncoding.Position :=0;
mm:=TStringList.Create;
mm.LoadFromStream(DestEncoding);
html:=mm.Text;
DestEncoding.Free;
mm.Free;
//���� ������ ��������
r.Expression:='<tr class="productListing-(odd|even)">';
//��������� ���� ���� ���� ������ � ��������?
while r.Exec (html) do
begin
if r.Exec (html) then begin
                           edit1.Text:= r.Match [0];
                           //������� ������
                           Delete(html,1,r.MatchPos[0]-1);
                           memo1.Text:=html;
//���� ��� ������
r.Expression:='<td class="productListing-data">.*?(\d{6}).*?</td>';
if r.Exec (html) then begin
                            edit2.Text:= r.Match [1];
                            id:=r.Match [1];
                            //��������� ����� ������ � ���� ������
                            ADOTable1.Append;
                            //���������� � ���� ��� ������
                            ADOTable1.FieldByName('CODE').Value:=Trim(r.Match [1]);
                       end;

//���� url ����� ����������� ������
r.Expression:='id='+Trim(id)+'.+?"><img src="(.*?)\.(.*?)"';
if r.Exec (html) then begin
                            edit3.Text:='http://price.portalkirov.ru/cat/'+ r.Match [1];
                            DestEncoding:=TMemoryStream.Create;
                            //������ � ������ ����������� ������ �� ��� url ������
                            IdHTTP1.Get('http://price.portalkirov.ru/cat/'+r.Match [1]+'.'+r.Match [2], DestEncoding);
                            DestEncoding.Position :=0;
                            //���������� ����������� ������ � BlobField ���� ���� ������
                            TBlobField(Form1.ADOTable1.FieldByName('foto')).LoadFromStream(DestEncoding);
                            DestEncoding.Free;
                            //���������� ���������� �������� � ���� ������
                            ADOTable1.FieldByName('ext').Value:=r.Match [2];
                      end;

//���� ������������ ������
r.Expression:='<td class="productListing-data">.*id='+id+'.+?">.*?</a><br>(.*?)</td>';
if r.Exec (html) then begin
                                //���� ������� '&nbsp;' ��� ��������
                                r.Expression:='&nbsp;';
                                //�������� ��������� ������� �� ''
                               xxx:= r.Replace(r.Match [1],'', True);
                                memo2.Text:=xxx;
                                //���������� ������������ ������ � ���� ������
                                ADOTable1.FieldByName('name_tovara').Value:=xxx;
                             end;

//���� ���� ������
r.Expression:='<td align="right" class="productListing-data">(.*?)���..*?</td>';
if r.Exec (html) then begin
                            posk:=r.MatchPos[0]+r.MatchLen[0];
                            //���� ������� '&nbsp;' ��� ��������
                            r.Expression:='&nbsp;';
                            //�������� ��������� ������� �� ''
                            xxx:=Trim(r.Replace(r.Match [1],'', True));
                            //���������� ���� ������ � ���� ������
                            ADOTable1.FieldByName('cost').Value:=strtofloat(xxx);
                            edit4.Text:= xxx;
                            //������� ��������� ����� �� HTML ����
                            Delete(html,1,posk);
                            beep;
                      end;

//��������� ������
ADOTable1.Post;
                      end;
//������ ��������� ��� ���������� ������
r.Expression:='<tr class="productListing-(odd|even)">';
end;
end;

//������� ����  ������
procedure TForm1.Button2Click(Sender: TObject);
begin
     ADOQuery1.SQL.Clear;
     ADOQuery1.SQL.Add('DELETE katalog.* FROM katalog;');
     ADOQuery1.ExecSQL;
     ADOQuery1.Active:=false;
     ADOTable1.Active:=False;
     ADOTable1.Active:=True;

end;


//�������� ����������� � ���� � ����������� �� ����������� ����������� ( jpg ��� gif)
procedure TForm1.DBGrid1CellClick(Column: TColumn);
begin
DestEncoding:=TMemoryStream.Create;
TBlobField(Form1.ADOTable1.FieldByName('foto')).SaveToStream(DestEncoding);
DestEncoding.Position :=0;
if (LowerCase(ADOTable1.FieldByName('ext').Value)='jpg') or (LowerCase(ADOTable1.FieldByName('ext').Value)='jpeg') then
begin
    Imgjpg:=TJpegImage.Create;
    Imgjpg.LoadFromStream(DestEncoding);
    Image1.Picture.Assign(Imgjpg);
    Imgjpg.Free;

end;
if (LowerCase(ADOTable1.FieldByName('ext').Value)='gif') then
begin
    ImgGif:=TGifImage.Create;
    ImgGif.LoadFromStream(DestEncoding);
    Image1.Picture.Assign(ImgGif);
    ImgGif.Free;

end;
DestEncoding.Free;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//����������� ������
r.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
//������� ������ ��� ������ � ����������� �����������
r := TRegExpr.Create;
end;

end.
