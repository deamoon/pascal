const
  A=['A'..'Z','a'..'z','_']; B=['0'..'9'];
var
  i,w,q,q1,f:longint; s:string;
begin
  readln(s); s:=s+chr(10);
  w:=0; q:=1;
  for i:=1 to length(s) do begin
    case q of
    
      10 : case s[i] of
            chr(10) : w:=0;
            '}' : q1:=f;
          else q:=10; end;
          
      1 : case s[i] of
            'A'..'Z','a'..'z','_' : q1:=3;
            '{' : begin q1:=10; f:=1; end;
            ' ' : q1:=1;
          else begin w:=0; break; end; end;
      3 : case s[i] of
            'A'..'Z','a'..'z','_' : q1:=3;
            '0'..'9' : q1:=3;
            ':' : q1:=5;
            '{' : begin q1:=10; f:=3; end;
            ' ' : q1:=4;
          else begin w:=0; break; end; end;
      4 : case s[i] of
            ':' : q1:=5;
            '{' : begin q1:=10; f:=4; end;
            ' ' : q1:=4;
          else begin w:=0; break; end; end;
      5 : if s[i]='=' then q1:=6 else begin w:=0; break; end;
      6 : case s[i] of
            'A'..'Z','a'..'z','_' : q1:=7;
            '0'..'9' : q1:=8;
            '-' : q1:=8;
            '{' : begin q1:=10; f:=6; end;
            ' ' : q1:=6;
          else begin w:=0; break; end; end;
      7 : case s[i] of
            'A'..'Z','a'..'z','_' : q1:=7;
            chr(10) : w:=1;
            '0'..'9' : q1:=7;
            '{' : begin q1:=10; f:=7; end;
            ' ' : q1:=9;
          else begin w:=0; break; end; end;
      8 : case s[i] of
            '0'..'9' : q1:=8;
            chr(10) : w:=1;
            '{' : begin q1:=10; f:=8; end;
            ' ' : q1:=9;
          else begin w:=0; break; end; end;
      9 : case s[i] of
            chr(10) : w:=1;
            '{' : begin q1:=10; f:=9; end;
            ' ' : q1:=9;
          else begin w:=0; break; end; end;
  end;
  q:=q1;
end;
if w=1 then write('YES') else write('NO');
end.