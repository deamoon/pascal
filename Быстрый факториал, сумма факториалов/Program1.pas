type
  mas = array[0..201] of shortint;

function sum(ar1,ar2:mas):mas;
var
  n,i:shortint;
  c:longint;
  r:mas;
begin
  if ar1[0]>ar2[0] then n:=ar1[0] else n:=ar2[0];
  for i:=ar1[0]+1 to n do ar1[i]:=0; for i:=ar2[0]+1 to n do ar2[i]:=0;
  r[0]:=n; for i:=1 to n+1 do r[i]:=0;
  
  for i:=1 to n do begin
    c:=ar1[i]+ar2[i];
    r[i]:=(r[i]+c) mod 10;
    r[i+1]:=(r[i+1]+c) div 10;
  end;
  if r[n+1]>0 then inc(r[0]);
  sum:=r;
end;



function pr(ar1:mas;n:integer):mas;
 var
 new,r:mas; i,l,j,d:integer;
begin
 r[0]:=1; r[1]:=0;
  for i:=1 to ar1[0] do begin

    d:=ar1[i]*n;
    j:=0;
    for l:=1 to i-1 do begin inc(j); new[l]:=0; end;
    while (d<>0) do begin
      inc(j);
      new[j]:=d mod 10;
      d:=d div 10;
    end;
    new[0]:=j;
    
    r:=sum(new,r);
  end;
  pr:=r;
end;



VAR
  n,i:longint;
  f,s:mas;

BEGIN
  readln(n);
  f[1]:=1; f[0]:=1;
  s[1]:=0; s[0]:=1;
  for i:=1 to n do begin
    s:=sum(s,pr(f,i)); //s:=s+f*i;
    f:=pr(f,i); //f:=f*i;
    //writeln(f,' ',s);
  end;
  
  {f[0]:=5; f[1]:=0; f[2]:=6; f[3]:=8; f[4]:=3; f[5]:=9;
  s[0]:=3; s[1]:=4; s[2]:=7; s[3]:=8;
  f:=sum(f,s);}
  
  for i:=s[0] downto 1 do write(s[i]);
  writeln;
END.