program Proj2ect2;

{$APPTYPE CONSOLE}

type
  mas = array[0..20] of Shortint;
// 4 red 5 blue 4 green
function prov(a:mas):boolean;
var
  s:Boolean; i,r,b,g:longint;
  begin
    s:=True; r:=0; b:=0; g:=0;
    for i:=1 to 13 do begin
      if a[i]=1 then Inc(r);
      if a[i]=2 then begin
        Inc(b);
        if ((a[i+1]=2) or (a[i-1]=2)) then begin s:=false; Break; end;
      end;
      if a[i]=3 then Inc(g);
    end;
    prov:=((s) and (r=3) and (b=5) and (g=5));
  end;

procedure print(a:mas);
var
  i:LongInt;
begin
  for i:=1 to 13 do write(a[i]);
  writeln;
end;

var
  a:mas; k:Int64;

procedure ball(n:Integer);
var
  j:longint;
begin
  if n=14 then begin
    if prov(a) then begin Inc(k); print(a); end;
  end else begin
    for j:=1 to 3 do begin
      a[n]:=j;
      ball(n+1);
      a[n]:=0;
    end;
  end;
end;

var
  i:longint; s1,s:string;
begin
  Assign(Output,'1.txt');
  k:=0; a[0]:=4; a[14]:=4;
  ball(1);
  Writeln(k);
end.
