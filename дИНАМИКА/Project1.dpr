program Project1;

{$APPTYPE CONSOLE}

function min(a,b:int64):Int64;
begin
  if a<b then min:=a else min:=b;
end;

function min3(a,b,c:int64):Int64;
begin
  min3:=min(min(a,b),c);
end;

var
 a,d:array[0..100] of LongInt; t,n,i,j:longint;
begin
 //Assign(Input,'1.txt'); Assign(Output,'2.txt');
 readln(n);
 for i:=1 to n do read(d[i]);

 for i:=1 to n do begin
   for j:=1 to n-1 do begin
     if d[j]>d[j+1] then begin t:=d[j]; d[j]:=d[j+1]; d[j+1]:=t; end;
   end;
 end;

 {for i:=1 to n do write(d[i],' ');
 writeln;}

 a[0]:=0; a[1]:=0; a[2]:=d[2]-d[1]; a[3]:=d[3]-d[1];
 for i:=4 to n do begin
   a[i]:=min(a[i-2],a[i-1])+d[i]-d[i-1];
 end;

 {for i:=1 to n do write(a[i],' ');
 writeln;}

 write(a[n]);

 readln; readln;
end.
