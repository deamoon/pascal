program Project4;

{$APPTYPE CONSOLE}

function ln64(n:Int64):Integer;
  var
    t:Integer;
  begin
    t:=1;
    while n mod 10<>n do begin
      n:=n div 10;
      Inc(t);
    end;
    ln64:=t;
  end;

var
 l:array[0..60] of Int64;

procedure start;
var
  i:longint;
begin
  l[1]:=1;
  for i:=2 to 60 do begin
    l[i]:=l[i-1]+l[i-1]+ln64(i);
  end;
end;

var
  n,k:Int64;
  s:string;
begin
  Readln(n,k);
  start;
  if ((n<=60) and (k>l[n])) then write('-1') else begin
    while k>ln64(n) do begin
      if n>60 then k:=k-ln64(n) else begin k:=(k-ln64(n)) mod l[n-1]; end;
      Dec(n);
    end;
    str(n,s); if k=0 then write('1') else write(s[k]);
  end;
  readln;
end.
