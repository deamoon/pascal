program Number4;

{$APPTYPE CONSOLE}

type
  mas = array[0..200] of Shortint;

var
  ax,ay:array[0..200] of mas; r,k,i,j,n:LongInt; sx,sy:array[0..200] of string; f:Boolean;

begin
  n:=100; Assign(Output,'d2ans.txt');

  {for j:=1 to n do begin
    for i:=1 to n+1 do begin
      if i<=n+1-j then ax[i][j]:=0 else ax[i][j]:=1;
      if i<=n+1-j then ay[i][j]:=0 else ay[i][j]:=1;
    end;
  end;  }

  for j:=1 to n do begin
    for i:=1 to n+1 do begin
      if i<=n+1-j then sx[i]:=sx[i]+'0' else sx[i]:=sx[i]+'1';
      if i<=n+1-j then sy[i]:=sy[i]+'0' else sy[i]:=sy[i]+'1';
    end;
  end;

  r:=0;
  for i:=1 to n+1 do begin
    for j:=1 to n+1 do begin
      f:=True;
      for k:=1 to n do begin
        if sx[i][k]>sy[j][k] then begin f:=false; Break; end;
      end;
      if f then Inc(r);
    end;
  end;
  Write(r);
end.
