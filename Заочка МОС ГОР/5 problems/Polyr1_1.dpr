{$APPTYPE CONSOLE}

var
  ar1,ar2:array[0..100070] of Extended; ar:array[0..200700] of Extended;

procedure qsort1(l,r:longint);
var
  x,t:Extended; i,j:LongInt;
begin
  i:=l; j:=r; x:=ar1[(i+j) div 2];
  repeat
    while ar1[i]<x do Inc(i);
    while ar1[j]>x do Dec(j);
    if i<=j then begin t:=ar1[i]; ar1[i]:=ar1[j]; ar1[j]:=t; Inc(i); Dec(j); end;
  until i>j;
  if i<r then qsort1(i,r);
  if j>l then qsort1(l,j);
end;
procedure qsort2(l,r:longint);
var
  x,t:Extended; i,j:LongInt;
begin
  i:=l; j:=r; x:=ar2[(i+j) div 2];
  repeat
    while ar2[i]<x do Inc(i);
    while ar2[j]>x do Dec(j);
    if i<=j then begin t:=ar2[i]; ar2[i]:=ar2[j]; ar2[j]:=t; Inc(i); Dec(j); end;
  until i>j;
  if i<r then qsort2(i,r);
  if j>l then qsort2(l,j);
end;


var
  max,min,max1,min1,r1,r2,a1,a2,s,res,k:Extended; p1,p2,i,n,kol,j,raz,c:longint; mb:array[0..200700] of Boolean;
begin
  //Assign(Input,'1.txt'); Assign(Output,'2.txt');
  Readln(n); max:=-1; min:=10000000; max1:=-1; min1:=10000000; kol:=0; p1:=0; p2:=0;
  for i:=1 to n do begin
    readln(r1,r2,a1,a2);
    if r1>max then max:=r1;
    if r2<min then min:=r2;
    if a2>a1 then begin
      if a1>max1 then max1:=a1;
      if a2<min1 then min1:=a2; p1:=1;
    end;
    if a1>a2 then begin
      Inc(kol);
      ar1[kol]:=a2; ar2[kol]:=a1;
      p2:=1;
    end;
  end;
  qsort1(1,kol); qsort2(1,kol);
  for i:=kol+1 to 100070 do begin ar1[i]:=MaxInt; ar2[i]:=MaxInt; end;
  i:=1; j:=1; raz:=0;
  while raz<kol*2 do begin
    Inc(raz);
    if ar1[i]<=ar2[j] then begin
      ar[raz]:=ar1[i]; Inc(i); mb[raz]:=True;
    end else begin
      ar[raz]:=ar2[j]; Inc(j); mb[raz]:=False;
    end;
  end;

  {for i:=1 to raz do begin
    write(ar[i]:0:0,' ');
    if mb[i] then writeln('levo') else writeln('pravo');
  end;     }

  s:=(min*min-max*max)/2; c:=0; ar[0]:=0; res:=0; k:=min1-max1;
  if ((min<=max) or (min1<=max1)) then write('0') else begin
    if ((p1=1) and (p2=1)) then begin
      for i:=1 to raz do begin
        if ((c<>0) and (((ar[i-1]>=max1) and (ar[i-1]<=min1)) or ((ar[i]>=max1) and (ar[i]<=min1)))) then begin
          if ((ar[i-1]<max1) and (ar[i]>=max1)) then res:=res+ar[i]-max1 else begin
            if ((ar[i-1]<=min1) and (ar[i]>=min1)) then res:=res+min1-ar[i-1] else begin
             res:=res+ar[i]-ar[i-1];
            end;
          end;
        end else begin
          if ((c<>0) and ((ar[i-1]<=max1) and (ar[i]>=min1))) then res:=res+min1-max1;
        end;
        if mb[i] then Inc(c) else dec(c);
      end;
      write(s*(k-res):0:8);
    end else begin
      if p1=1 then begin
        write(s*k:0:8);
      end;
      if p2=1 then begin
        for i:=1 to raz do begin
          if c<>0 then res:=res+ar[i]-ar[i-1];
          if mb[i] then Inc(c) else dec(c);
        end;
       // while True do Inc(j);
        write(s*(2*pi{6.2831853}-res):0:10);
      end;
    end;
  end;
end.
