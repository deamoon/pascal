program Project6;

{$APPTYPE CONSOLE}

function max(a,b:int64):Int64;
begin
  if a>b then max:=a else max:=b;
end;

var
  i,j,k,x,y,t,m:Int64; a,b,c:array[0..1000000] of Extended; a1:Extended; z:longint;
begin
 Readln(i,j,k,x,y);
 if i>j then begin
   t:=i; i:=j; j:=t; t:=x; x:=y; y:=t;
 end;

 m:=max(i,j); m:=max(m,k);

 b[1]:=1; b[2]:=1; a[1]:=0; a[2]:=x;
 if j-i=1 then c[i+1]:=y else begin
   for z:=3 to j do begin
     b[z]:=b[z-1]+b[z-2];
     a[z]:=a[z-1]+a[z-2];
   end;
   c[i+1]:=(y-a[j-i])/b[j-i];
 end;

 c[i]:=x;
 for z:=i+2 to m do c[z]:=c[z-1]+c[z-2];
 for z:=i-1 downto 1 do c[z]:=c[z+2]-c[z+1];

 //for z:=1 to m do write(c[z]:0:0,' ');
 //writeln;
 write(c[k]:0:0);
 readln;
end.
