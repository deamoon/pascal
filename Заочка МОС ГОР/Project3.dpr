program Project3;
{$APPTYPE CONSOLE}

const
  maxa = 10000;
  os = 10;
  maxn = 1000;
  infinity = maxlongint;

type
  mas = array[0..maxa] of LongInt;
  mas1 = array[0..1000] of LongInt;
  masb = array[0..maxn] of Boolean;

function max(a,b:int64):Int64;
begin
  if a>b then max:=a else max:=b;
end;

function min(a,b:int64):Int64;
begin
  if a<b then min:=a else min:=b;
end;

procedure null(var a:mas);
var
  i:LongInt; b:mas;
begin
  FillChar(a,SizeOf(a),0); //for i:=0 to maxa do a[i]:=0;
end;

function sum(a,b:mas):mas;
var
  i,m,t:longint; c:mas;
begin
  null(c); m:=max(a[0],b[0]); t:=0; c[0]:=m;
  for i:=1 to m do begin
    c[i]:=(a[i]+b[i]+t) mod os;
    t:=(a[i]+b[i]+t) div os;
  end;
  if t<>0 then begin
    c[i]:=(a[i]+b[i]+t) mod os; Inc(c[0]);
  end;
  sum:=c;
end;

procedure reverse(var a,b:mas);
var
  t:mas;
begin
  if a[0]>b[0] then begin t:=a; a:=b; b:=t; end;
end;

function sdvig(a:mas;n:longint):mas;
var
  i:LongInt;
begin
  for i:=a[0] downto 1 do a[i+n]:=a[i];
  for i:=1 to n do a[i]:=0;
  Inc(a[0],n);
  sdvig:=a;
end;

function proiz(a,b:mas):mas;
var
  v,c:mas; i,j,t,m:LongInt;
begin
  null(v); m:=max(a[0],b[0]); reverse(a,b);
  for i:=1 to a[0] do begin
    t:=0; null(c); c[0]:=m;
    for j:=1 to b[0] do begin
      c[j]:=(a[i]*b[j]+t) mod os;
      t:=(a[i]*b[j]+t) div os;
    end;
    if t<>0 then begin
      c[j]:=(a[i]*b[j]+t) mod os; Inc(c[0]);
    end;
    c:=sdvig(c,i-1);
    v:=sum(v,c);
  end;
  proiz:=v;
end;

function vvod(a:Int64):mas;
var
  v:mas; s:string; err,j,i:Integer;
begin
  Str(a,s);
  FillChar(v,SizeOf(v),0); //for i:=0 to maxa do v[i]:=0;
  v[0]:=Length(s); j:=0;
  for i:=Length(s) downto 1 do begin
    Inc(j);
    val(s[i],v[j],err);
  end;
  vvod:=v;
end;

procedure vivod(a:mas);
var
  i:longint;
begin
  if a[a[0]]=0 then a[0]:=1;
  for i:=a[0] downto 1 do write(a[i]);
  writeln;
end;

var
  n,s:LongInt;
  otv,umn:mas;
  d,a:array[0..maxn] of int64;
  w,br:array[0..maxn,0..maxn] of int64;

procedure mut;
var
  i:LongInt;
begin
  umn[0]:=1; umn[1]:=1;
  for i:=1 to a[0]-1 do begin
    umn:=proiz(umn,vvod(br[a[i],a[i+1]]));
    //write(a[i],'-',a[i+1],' ',br[a[i],a[i+1]],'      ');
  end;
  //vivod(umn);
  otv:=sum(otv,umn);
  //writeln;
end;

procedure rec(t:LongInt; p:masb);
var
  i:longint; r:Boolean;
begin
r:=True;
  for i:=1 to n do begin
    if ((d[i]+w[i,t]=d[t]) and (p[i])) then begin
      p[i]:=false;
      if r then begin Inc(a[0]); a[a[0]]:=t; end;
      r:=False;  //mut;
      if i=s then begin Inc(a[0]); a[a[0]]:=i; mut; end else rec(i,p);
      p[i]:=True; Dec(a[0]); //;;;;;;
    end;
  end;
end;

var
  i,j,m,u,v,c,f,h:LongInt;
  tt:Boolean;
  t,p:masb;

begin
  Assign(Input,'input.txt');  Assign(Output,'output.txt');
  //////////////////////////////////
  readln(n,m); h:=maxlongint;
  for i:=0 to n do begin for j:=0 to n do begin w[i,j]:=h; br[u,v]:=0; end; t[i]:=True; p[i]:=True; end;
  for i:=1 to m do begin
    readln(u,v,c);
    if c<w[v,u] then begin w[v,u]:=c; br[v,u]:=1; w[u,v]:=c; br[u,v]:=1; Continue; end;
    if c=w[v,u] then begin Inc(br[v,u]); Inc(br[u,v]); end;
  end;
  readln(s,f);
  for i:=1 to n do d[i]:=w[s,i];
  d[s]:=0;
  t[s]:=False; tt:=True;
  while tt do begin
    u:=-1;
    for i:=1 to n do begin
      if t[i] then begin
        if u=-1 then begin v:=d[i]; u:=i; Continue; end;
        if d[i]<v then begin v:=d[i]; u:=i; end;
      end;
    end;
    t[u]:=false;
    for i:=1 to n do begin
      if t[i] then d[i]:=min(d[i],d[u]+w[u,i]);
    end;

    tt:=False;      
    for i:=1 to n do begin
      if t[i] then begin tt:=True; Break; end;
    end;
  end;

  /////////////////////////////
  FillChar(a,SizeOf(a),0);
  otv[0]:=1; otv[0]:=0;
  p[f]:=false;
  rec(f,p);

  vivod(otv);

  {otv[0]:=1; otv[1]:=1;
  for i:=1 to 10000 do begin
    otv:=proiz(otv,vvod(3));
    vivod(otv);
  end;
  vivod(otv);

    }

   {
  h:=3;
  writeln('100 9900');
  for i:=1 to 99 do begin
    for j:=1 to 100 do writeln(i,' ',i+1,' ','100');
  end;
  Writeln('1 100');
  }
  {writeln('pps2sp');
  //vivod(otv);
  //otv:=sum(vvod(1212),vvod(21212));
  otv:=vvod(11);


  vivod(otv);   }
end.
