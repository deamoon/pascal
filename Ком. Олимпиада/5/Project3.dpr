

program H_3;

{$APPTYPE CONSOLE}

var f:text;
a,b,c,d,i,m,l,r,k,o,s:int64;

begin
  assign(f,'sums.in');
  reset(f);
  read(f,a,b,c,d);
  close(f);
  o:=0;
  if c<2*a then c:=2*a;
  if d>2*b then d:=2*b;
  if (c>=2*a) and (d<=2*b) and (c<=d) then
  begin
    s:=a+b;
    if (c<=s) and (d<=s) then
      begin
      l:=c-2*a+1;
      r:=d-2*a+1;
      o:=((l+r)*(r-l+1)) div 2;
      end;
      if (c<=s) and (d>s) then
      begin
      l:=c-2*a+1;
      r:=(2*s-d)-2*a+1;
      o:=(((l+(s-2*a+1))*((s-2*a+1)-l+1)) div 2);
      o:=o+(((r+(s-2*a))*((s-2*a)-r+1)) div 2);
      end;
      if (c>s) and (d>s) then
      begin
      l:=(2*s-c)-2*a+1;
      r:=(2*s-d)-2*a+1;
      o:=((l+r)*(r-l+1)) div 2;
      end;

  end;
  assign(f,'sums.out');
  rewrite(f);
  write(f,o);
  close(f);
end.
