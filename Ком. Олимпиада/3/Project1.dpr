program Project1;

{$APPTYPE CONSOLE}

function f(n:longint):boolean;
var
  i,s:LongInt;
  begin
    f:=false;
    for i:=0 to 6 do begin
      if i=0 then s:=1 else s:=s*2;
      if n=s then f:=true;
    end;

  end;

var
  a:array[0..10000] of longint;
  n,k,i:LongInt; f1,f2:Text;
begin
Assign(f1,'number.in'); Assign(f2,'number.out'); Reset(f1); Rewrite(f2);
Readln(f1,n); k:=0;

while not(f(n)) do begin
  Inc(k); Dec(n); a[k]:=1;
end;

while n<>64 do begin
  Inc(k); n:=n*2; a[k]:=2;
end;

a[k+1]:=2; a[k+2]:=1; a[k+3]:=1; a[k+4]:=2; a[k+5]:=1; a[k+6]:=2; a[k+7]:=1;
a[k+8]:=1; a[k+9]:=2;

Writeln(f2,k+9);
for i:=1 to k+9 do begin
  if a[i]=1 then Writeln(f2,'-') else Writeln(f2,'*');
end;

Close(f1); Close(f2);
end.
