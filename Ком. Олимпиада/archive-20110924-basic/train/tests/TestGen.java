import java.io.*;
import java.util.*;

public class TestGen {
	public static void main(String[] args) {
		new TestGen().run();
	}

	BufferedReader br;
	StringTokenizer in;
	PrintWriter out;
	int TNumber;
	Random rnd;

	int dist[][];
	int numcupe[];
	int side[];
	int up[];

	void gendist() {
		numcupe = new int[55];
		side = new int[55];
		up = new int[55];
		for (int i = 1; i <= 36; i++)
			numcupe[i] = (i - 1) / 4;
		for (int i = 37; i <= 54; i++)
			numcupe[i] = (8 - (i - 37) / 2);
		for (int i = 37; i <= 54; i++)
			side[i] = 1;
		for (int i = 2; i <= 54; i = i + 2)
			up[i] = 1;
		dist = new int[55][55];
		for (int i = 1; i < 55; i++)
			for (int j = 1; j < 55; j++) {
				dist[i][j] = Math.abs(numcupe[i] - numcupe[j]) * 100
						+ (side[i] + side[j]) * 10 + up[i] + up[j];
			}
	};

	public String nextToken() throws IOException {
		while (in == null || !in.hasMoreTokens()) {
			in = new StringTokenizer(br.readLine());
		}
		return in.nextToken();
	}

	public int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	public long nextLong() throws IOException {
		return Long.parseLong(nextToken());
	}

	public double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}

	public class Test {
		int take[] = new int[55];
		int n;

		void reset() {
			n = 0;
			Arrays.fill(take, 0);
		}

		void printTest() throws IOException {
			if (TNumber < 10) {
				out = new PrintWriter("0" + TNumber);
			} else {
				out = new PrintWriter("" + TNumber);
			}

			TNumber++;
			for (int i = 1; i <= 54; i++)
				n += take[i];
			out.println(n);
			ArrayList<Integer> test = new ArrayList<Integer>();
			for (int i = 1; i <= 54; i++)
				if (take[i] == 1)
					test.add(i);
			Collections.shuffle(test, rnd);
			// System.err.println(test.toString());
			for (int i = 0; i < n - 1; i++)
				out.print(test.get(i) + " ");
			out.println(test.get(n - 1));
			out.println();
			out.close();
		}

		void genFullTest() throws IOException {
			reset();
			for (int i = 1; i < 55; i++) {
				take[i] = 1;
			}
			printTest();
		}

		void genTestWithSt() throws IOException {
			reset();
			int st = (rnd.nextInt() % 54 + 54) % 54 + 1;
			int min = (rnd.nextInt() % 54 + 54) % 54 + 1;
			while (min == st) {
				min = (rnd.nextInt() % 54 + 54) % 54 + 1;
			}
			int max = (rnd.nextInt() % 54 + 54) % 54 + 1;
			while ((max == st) && (max == min)) {
				max = (rnd.nextInt() % 54 + 54) % 54 + 1;
			}
			take[st] = 1;
			if (dist[st][max] < dist[st][min]) {
				int t = max;
				max = min;
				min = t;
			}
			// System.err.println(min+" "+max+" "+st);
			for (int i = 1; i < 55; i++) {
				boolean good = true;
				for (int j = 1; j < 55; j++)
					if ((take[j] == 1)
							&& (!((dist[i][j] >= dist[st][min]) && (dist[i][j] <= dist[st][max]))))
						good = false;
				if (good)
					take[i] = 1;
			}
			int m=0;
			for (int i=1; i<55; i++)
				m+=take[i];
			if (m<3)
				return;
			printTest();
		}

		void genTestSimple() throws IOException {
			reset();
			int st = (rnd.nextInt() % 54 + 54) % 54 + 1;
			int en = (rnd.nextInt() % 54 + 54) % 54 + 1;
			while (en == st)
				en = (rnd.nextInt() % 54 + 54) % 54 + 1;
			take[st] = 1;
			take[en] = 1;
			printTest();
		}

		void genTestUp(int tp) throws IOException {
			reset();
			for (int i = 1; i <= 54; i++)
				if (up[i] == tp)
					take[i] = 1;
			printTest();
		}

		void genTestSide(int tp) throws IOException {
			reset();
			for (int i = 1; i < 55; i++)
				if (side[i] == tp)
					take[i] = 1;
			printTest();
		}

		void genRandomTest() throws IOException {
			reset();
			for (int i = 1; i < 55; i++)
				take[i] = (rnd.nextInt() % 2 + 6) % 2;
			printTest();
		}

		void genTestCupe(int tp) throws IOException {
			reset();
			// System.err.println(Arrays.toString(numcupe));
			for (int i = 1; i < 55; i++)
				if (numcupe[i] == tp)
					take[i] = 1;
			printTest();
		}
	}

	public void solve() throws IOException {
		rnd = new Random(321);
		TNumber = 2;
		gendist();

		Test t = new Test();

		while (TNumber < 20)
			t.genTestWithSt();

		for (int i = 0; i < 9; i++)
			t.genTestCupe(i);
		for (int i = 0; i < 2; i++)
			t.genTestSide(i);
		for (int i = 0; i < 2; i++)
			t.genTestUp(i);
		t.genFullTest();
		for (int i = 0; i < 5; i++)
			t.genTestSimple();
		for (int i = 0; i < 10; i++)
			t.genRandomTest();

	}

	private void run() {
		try {
			// br = new BufferedReader(new FileReader("TestGen.in"));
			// out = new PrintWriter("TestGen.out");
			// br = new BufferedReader(new InputStreamReader(System.in));
			// out = new PrintWriter(System.out);

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
