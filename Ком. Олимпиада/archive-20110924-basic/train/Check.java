import java.util.HashSet;

import ru.ifmo.testlib.InStream;
import ru.ifmo.testlib.Outcome;
import ru.ifmo.testlib.Outcome.Type;

public class Check implements ru.ifmo.testlib.Checker {
	int dist[][];
	int numcupe[];
	int side[];
	int up[];

	void gendist() {
		numcupe = new int[55];
		side = new int[55];
		up = new int[55];
		for (int i = 1; i <= 36; i++)
			numcupe[i] = (i - 1) / 4;
		for (int i = 37; i <= 54; i++)
			numcupe[i] = (8 - (i - 37) / 2);
		for (int i = 37; i <= 54; i++)
			side[i] = 1;
		for (int i = 2; i <= 54; i = i + 2)
			up[i] = 1;
		dist = new int[55][55];
		for (int i = 1; i < 55; i++)
			for (int j = 1; j < 55; j++) {
				dist[i][j] = Math.abs(numcupe[i] - numcupe[j]) * 100
						+ (side[i] + side[j]) * 10 + up[i] + up[j];
			}
	};

	@Override
	public Outcome test(InStream inf, InStream ouf, InStream ans) {
		gendist();
		int n = inf.nextInt();
		int free[] = new int[54 + 1];
		for (int i = 1; i <= n; i++)
			free[inf.nextInt()] = 1;
		int x = ouf.nextInt();
		int y = ouf.nextInt();
		if ((x < 1) || (x > 54))
			return new Outcome(Type.PE, "Wrong seat: " + x);
		if ((y < 1) || (y > 54))
			return new Outcome(Type.PE, "Wrong seat: " + y);
		if ((free[x] == 0) || (free[y] == 0) || (x == y))
			return new Outcome(Type.WA, "Places are not free");
		int x1 = ans.nextInt();
		int y1 = ans.nextInt();
		if ((free[x] == 0) || (free[y] == 0) || (x == y))
			return new Outcome(Type.WA,
					"Places are not free(author result wrong)");
		if (dist[x][y] > dist[x1][y1])
			return new Outcome(Type.WA, "No optimal");
		if (dist[x][y] < dist[x1][y1])
			return new Outcome(Type.WA, "Author answer wrong");
		return new Outcome(Type.OK, "Solution is correct");

	}
}