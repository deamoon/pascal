import java.io.*;
import java.util.StringTokenizer;

public class train_vd {
	BufferedReader in;
	StringTokenizer str;
	PrintWriter out;
	String SK;

	String next() throws IOException {
		while ((str == null) || (!str.hasMoreTokens())) {
			SK = in.readLine();
			if (SK == null)
				return null;
			str = new StringTokenizer(SK);
		}
		return str.nextToken();
	};

	int nextInt() throws IOException {
		return Integer.parseInt(next());
	};

	double nextDouble() throws IOException {
		return Double.parseDouble(next());
	};

	long nextLong() throws IOException {
		return Long.parseLong(next());
	};

	int dist[][];

	void gendist() {
		int numcupe[] = new int[55];
		int side[] = new int[55];
		int up[] = new int[55];
		for (int i = 1; i <= 36; i++)
			numcupe[i] = (i - 1) / 4;
		for (int i = 37; i <= 54; i++)
			numcupe[i] = (8 - (i - 37) / 2);
		for (int i = 37; i <= 54; i++)
			side[i] = 1;
		for (int i = 2; i <= 54; i = i + 2)
			up[i] = 1;
		dist = new int[55][55];
		for (int i = 1; i < 55; i++)
			for (int j = 1; j < 55; j++)
			{
				dist[i][j] = Math.abs(numcupe[i] - numcupe[j]) * 100
						+ (side[i] + side[j]) * 10 + up[i] + up[j];
			}
	};

	void solve() throws IOException {
		gendist();
		int n=nextInt();
		int a[]=new int[n];
		for (int i=0; i<n; i++)
			a[i]=nextInt();
		int optdist=10000;
		for (int i=0; i<n; i++)
			for (int j=i+1; j<n; j++)
				optdist=Math.min(optdist, dist[a[i]][a[j]]);
		for (int i=0; i<n; i++)
			for (int j=i+1; j<n; j++)
				if (dist[a[i]][a[j]]==optdist)
                             {
					   out.println(a[i]+" "+a[j]);
                                 return;
                             }
		
	};

	void run() throws IOException {

		in = new BufferedReader(new FileReader("train.in"));
		out = new PrintWriter("train.out");
		solve();
		out.close();
	}

	public static void main(String[] args) throws IOException {
		new train_vd().run();
	}

}
