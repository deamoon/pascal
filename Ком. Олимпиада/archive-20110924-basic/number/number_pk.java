import java.io.*;
import java.util.*;

public class number_pk {
	public static void main(String[] args) {
		new number_pk().run();
	}

	BufferedReader br;
	StringTokenizer in;
	PrintWriter out;

	public String nextToken() throws IOException {
		while (in == null || !in.hasMoreTokens()) {
			in = new StringTokenizer(br.readLine());
		}
		return in.nextToken();
	}

	public int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	public long nextLong() throws IOException {
		return Long.parseLong(nextToken());
	}

	public double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}

	public void solve() throws IOException {
		int n = nextInt();
		assert((1 <= n) && (n <= 100));
		int t = 1;
		while (n * t < 1000) {
			t *= 2;
		}
		ArrayList<Boolean> ans = new ArrayList<Boolean>();
		while (n != 1000) {
			if ((n - 1) * t >= 1000) {
				ans.add(true);
				n--;
			} else {
				n *= 2;
				t /= 2;
				ans.add(false);
			}
		}
		out.println(ans.size());
		for (int i = 0; i < ans.size(); i++) {
			if (ans.get(i)) 
				out.println("-");
			else
				out.println("*");
		}
	}

	public void run() {
		try {
			br = new BufferedReader(new FileReader("number.in"));
			out = new PrintWriter("number.out");
			//br = new BufferedReader(new InputStreamReader(System.in));
			//out = new PrintWriter(System.out);

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}