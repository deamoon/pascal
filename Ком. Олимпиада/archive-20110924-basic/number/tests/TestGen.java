import java.io.*;
import java.util.*;

public class TestGen {
	public static void main(String[] args) {
		new TestGen().run();
	}

	BufferedReader br;
	StringTokenizer in;
	PrintWriter out;
	int TNumber;
	

	public String nextToken() throws IOException {
		while (in == null || !in.hasMoreTokens()) {
			in = new StringTokenizer(br.readLine());
		}
		return in.nextToken();
	}

	public int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	public long nextLong() throws IOException {
		return Long.parseLong(nextToken());
	}

	public double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}

	void printTest(int k) throws IOException{
		if (TNumber < 10) {
			out = new PrintWriter("0" + TNumber);
		} else {
			out = new PrintWriter("" + TNumber);
		}
		TNumber++;
		out.println(k);
		out.close();
	}
	
	public void solve() throws IOException {
	  TNumber = 2;
	  ArrayList<Integer> al = new ArrayList<Integer>();
	  for (int i = 1; i <= 100; i++) {
	    if ((i != 8) && (i != 73)) {
	      al.add(i);
	    }
	  }
	  Collections.shuffle(al, new Random(398634));
	  for (int i : al) {
	    printTest(i);
	  }
	}

	public void run() {
		try {
			//br = new BufferedReader(new FileReader("TestGen.in"));
			//out = new PrintWriter("TestGen.out");
			//br = new BufferedReader(new InputStreamReader(System.in));
			//out = new PrintWriter(System.out);

			solve();

			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}