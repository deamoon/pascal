import ru.ifmo.testlib.InStream;
import ru.ifmo.testlib.Outcome;
import ru.ifmo.testlib.Outcome.Type;

public class Check implements ru.ifmo.testlib.Checker{

	@Override
	public Outcome test(InStream inf, InStream ouf, InStream ans) {
		long n = inf.nextInt();
		int t = ouf.nextInt();
		if ((1 > t))
			return(new Outcome(Type.WA, "Count of operation is less, then 1"));
		if (t > 50)
			return(new Outcome(Type.WA, "Count of operation is too big"));
//		String ss = ouf.nextLine();
		for (int i = 0; i < t; i++) {
			String s = ouf.nextToken();
			if (s.equals("*")) {
				n *= 2;
				continue;
			}
			if (s.equals("-")) {
				n--;
				continue;
			}
			return new Outcome(Type.PE, "Wrong operation at " + (i + 1) + " line");
		}
		if (n != 1000) 
			return new Outcome(Type.WA, "Result isn't equal to 1000");
		return new Outcome(Type.OK, "Solution is correct");
	}
	
}