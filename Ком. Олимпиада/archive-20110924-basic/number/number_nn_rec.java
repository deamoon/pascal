import java.io.*;
import java.util.*;

public class number_nn_rec {
	static void go(int x, int n, int count, PrintWriter out) {
		if (x == n) {
			out.println(count);
			return;
		}
		if (x < n || (x & 1) == 1) {
			go(x + 1, n, count + 1, out);
			out.println("-");
		} else {
			go(x >> 1, n, count + 1, out);
			out.println("*");
		}
	}

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("number.in"));
		PrintWriter out = new PrintWriter("number.out");
		int n = sc.nextInt();
		go(1000, n, 0, out);
		sc.close();
		out.close();
	}

}
