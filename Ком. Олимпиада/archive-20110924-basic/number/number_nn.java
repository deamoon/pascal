import java.io.*;
import java.util.*;

public class number_nn {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("number.in"));
		PrintWriter out = new PrintWriter("number.out");
		int n = sc.nextInt();
		int cur = 1000;
		List<String> ans = new ArrayList<String>();
		while (cur > n) {
			if ((cur & 1) == 1) {
				ans.add("-");
				cur++;
			} else {
				ans.add("*");
				cur >>= 1;
			}
		}
		while (cur < n) {
			cur++;
			ans.add("-");
		}
		Collections.reverse(ans);
		out.println(ans.size());
		for (String e : ans) {
			out.println(e);
		}
		sc.close();
		out.close();
	}

}
