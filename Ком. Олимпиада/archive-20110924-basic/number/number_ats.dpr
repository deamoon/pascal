var
    n, i, pos : longint;
    ans : array[1..50] of longint;

begin
    reset(input, 'number.in');
    rewrite(output, 'number.out');
    read(n);
    assert ((1 <= n) and (n <= 100));
    pos := 0;
    while (n <> 64) and (n <> 32) and (n <> 16) and (n <> 8) and (n <> 4) and (n <> 2) and (n <> 1) do
    begin
        dec(n);
        inc(pos);
        ans[pos] := 0;
    end;
    while(n <> 128) do
    begin
        n := n * 2;
        inc(pos);
        ans[pos] := 1;
    end;
    while (n <> 125) do
    begin
        dec(n);
        inc(pos);
        ans[pos] := 0;
    end;
    while(n <> 128) do
    begin
        n := n * 2;
        inc(pos);
        ans[pos] := 1;
    end;
    writeln(pos);
    for i := 1 to pos do
    begin
        if (ans[i] = 0) then
            writeln('-')
        else
            writeln('*');
    end;
end.