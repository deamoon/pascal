import static java.lang.Math.min;

import java.io.*;
import java.util.Scanner;

public class fence_nn_wa {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("fence.in"));
		PrintWriter out = new PrintWriter("fence.out");
		int n = sc.nextInt();
		int k = sc.nextInt();
		int s = sc.nextInt();
		int[] a = new int[n];
		int[] b = new int[k];
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt() - 1;
			b[a[i]]++;
		}
		int ans = Integer.MAX_VALUE;
		for (int i = 0; i < k; i++) {
			ans = min(ans, (n - b[i] + s - 1) / s);
		}
		out.println(ans);
		sc.close();
		out.close();
	}
}
