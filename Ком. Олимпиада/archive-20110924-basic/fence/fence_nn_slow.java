import static java.lang.Math.min;

import java.io.*;
import java.util.Scanner;

public class fence_nn_slow {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("fence.in"));
		PrintWriter out = new PrintWriter("fence.out");
		int n = sc.nextInt();
		int k = sc.nextInt();
		int s = sc.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt() - 1;
		}
		int ans = Integer.MAX_VALUE;
		for (int i = 0; i < k; i++) {
			int count = 0;
			int got = 0;
			for (int j = 0; j < n; j++) {
				if (a[j] != i) {
					count++;
				} else {
					got += (count + s - 1) / s;
					count = 0;
				}
			}
			got += (count + s - 1) / s;
			ans = min(ans, got);
		}
		out.println(ans);
		sc.close();
		out.close();
	}
}
