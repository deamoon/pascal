uses
    Math;

const
    MAXN = 100000;

var
    a, b, last : array[0 .. MAXN] of integer;
    i, n, k, j, ans, s : integer;

begin
    reset(input, 'fence.in');
    rewrite(output, 'fence.out');
    read(n, k, s);
    assert((1 <= n) and (n <= MAXN));
    assert((1 <= k) and (n <= MAXN));
    assert((1 <= s) and (n <= MAXN));
    for i := 1 to n do begin
        read(a[i]);
        assert((1 <= a[i]) and (a[i] <= k));
    end;
    i := 1;
    fillchar(last, sizeof(last), 0);
    while (i <= n) do begin
        j := i;
        while (j <= n) and (a[j] = a[i]) do 
            inc(j);
        b[a[i]] := b[a[i]] + (i - last[a[i]] - 1 + (s - 1)) div s;
        last[a[i]] := j - 1;
        i := j;
    end;
    ans := maxint;
    for i := 1 to k do begin
        b[i] := b[i] + ((n + 1) - last[i] - 1 + (s - 1)) div s;
        ans := min(ans, b[i]);
    end;
    writeln(ans);
    close(input);
    close(output);
end.