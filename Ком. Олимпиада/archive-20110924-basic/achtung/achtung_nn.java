import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class achtung_nn {
	static class Point {
		int x;
		int y;
		int num;

		public Point(int x, int y, int num) {
			this.x = x;
			this.y = y;
			this.num = num;
		}

	}

	static class Line {
		long a;
		long b;
		long c;

		Line(Point p1, Point p2) {
			a = p2.y - p1.y;
			b = p1.x - p2.x;
			c = -p1.x * a - p1.y * b;
		}

		long get(Point p) {
			return a * p.x + b * p.y + c;
		}
	}

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("achtung.in"));
		PrintWriter out = new PrintWriter("achtung.out");
		int n = sc.nextInt();
        if (n == 1) {
            out.println(1);
            out.close();
            return;
        }
		Point[] p = new Point[n];
		for (int i = 0; i < n; i++) {
			p[i] = new Point(sc.nextInt(), sc.nextInt(), i);
		}
		Point minP = null;
		Point maxP = null;
		for (int i = 0; i < n; i++) {
			if (minP == null || minP.x > p[i].x || minP.x == p[i].x
					&& minP.y > p[i].y) {
				minP = p[i];
			}
			if (maxP == null || maxP.x < p[i].x || maxP.x == p[i].x
					&& maxP.y < p[i].y) {
				maxP = p[i];
			}
		}
		Line line = new Line(minP, maxP);
		List<Point> p1 = new ArrayList<Point>();
		List<Point> p2 = new ArrayList<Point>();
		int count1 = 0;
		int count2 = 0;
		for (int i = 0; i < n; i++) {
			if (line.get(p[i]) > 0) {
				count1++;
			}
			if (line.get(p[i]) < 0) {
				count2++;
			}
		}
		if (count1 == 0 && count2 == 0) {
			out.println("No solution");
			out.close();
			return;
		}
		for (int i = 0; i < n; i++) {
			if (line.get(p[i]) > 0) {
				p1.add(p[i]);
			} else if (line.get(p[i]) < 0) {
				p2.add(p[i]);
			} else if (count1 == 0) {
				p1.add(p[i]);
			} else {
				p2.add(p[i]);
			}
		}
		Collections.sort(p1, new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				if (o1.x != o2.x) {
					return o1.x - o2.x;
				}
				return o1.y - o2.y;
			}
		});

		Collections.sort(p2, new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				if (o1.x != o2.x) {
					return o2.x - o1.x;
				}
				return o2.y - o1.y;
			}
		});
		for (Point e : p1) {
			out.print(1 + e.num + " ");
		}
		for (Point e : p2) {
			out.print(1 + e.num + " ");
		}
		out.println();
		out.close();
		sc.close();
	}
}
