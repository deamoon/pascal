var
  p : array[1..100000] of integer;
  i, n, j, t : integer;
begin
  reset(input, 'achtung.in');
  rewrite(output, 'achtung.out');
  randseed := 25566;
  read(n);
  for i := 1 to n do begin
    p[i] := i;
  end;
  for i := 1 to n do begin
    j := random(i) + 1;
    t := p[i];
    p[i] := p[j];
    p[j] := t;
  end;
  for i := 1 to n do begin
    write(p[i], ' ');
  end;
end.