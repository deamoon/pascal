import java.io.*;
import java.util.*;

public class achtung_sp implements Runnable {

	public static void main(String[] args) {
		new Thread(new achtung_sp()).run();
	}

	public void run() {
		try {
			Locale.setDefault(Locale.US);
			br = new BufferedReader(new FileReader(FILENAME + ".in"));
			out = new PrintWriter(FILENAME + ".out");
			solve();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	BufferedReader br;

	PrintWriter out;

	StringTokenizer st;

	String nextToken() throws IOException {
		while (st == null || !st.hasMoreTokens()) {
			st = new StringTokenizer(br.readLine());
		}
		return st.nextToken();
	}

	int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	void myAssert(boolean e, String s) {
		if (!e) {
			throw new Error("Assertion failure " + s);
		}
	}

	private static final String FILENAME = "achtung";
	
	class Point implements Comparable<Point> {
		int x, y, num;
		
		Point(int x, int y, int num) {
			this.x = x;
			this.y = y;
			this.num = num;
		}

		@Override
		public int compareTo(Point o) {
			if (x != o.x) return x - o.x;
			return y - o.y;
		}
	}
	
	class Line {
		long a, b, c;
		
		Line(Point p1, Point p2) {
			a = p2.y - p1.y;
			b = p1.x - p2.x;
			c = -a * p1.x - b * p1.y;
		}
		
		long side(Point p) {
			return a * p.x + b * p.y + c;
		}
	}

	public void solve() throws IOException {
		int n = nextInt();
		myAssert((1 <= n) && (n <= 100000), "Wrong n");
		Point[] p = new Point[n];
		for (int i = 0; i < n; i++) {
			int x = nextInt();
			int y = nextInt();
			myAssert(Math.abs(x) <= 1e9, "Wrong coord");
			myAssert(Math.abs(y) <= 1e9, "Wrong coord");
			p[i] = new Point(x, y, i + 1);
		}
		if (n == 1) {
			out.println(1);
			return;
		}
		Arrays.sort(p);
		for (int i = 0; i < n - 1; i++) {
			myAssert(p[i].compareTo(p[i + 1]) != 0, "Equal points");
		}
		if (p[0].x == p[n - 1].x) {
			out.println("No solution");
			return;
		}
		Line l = new Line(p[0], p[n - 1]);
		ArrayList<Point> up = new ArrayList<Point>();
		ArrayList<Point> down = new ArrayList<Point>();
		up.add(p[0]);
		down.add(p[n - 1]);
		boolean good = false;
		for (int i = 1; i < n - 1; i++) {
			if (l.side(p[i]) > 0) {
				up.add(p[i]);
				good = true;
			} else if (l.side(p[i]) < 0){
				good = true;
				down.add(p[i]);
			} else {
				down.add(p[i]);
			}
		}
		if (!good) {
			out.println("No solution");
			return;
		}
		Collections.sort(up);
		Collections.sort(down);
		Collections.reverse(down);
		for (Point pp : up) {
			out.println(pp.num);
		}
		for (Point pp : down) {
			out.println(pp.num);
		}
	}
}
 
