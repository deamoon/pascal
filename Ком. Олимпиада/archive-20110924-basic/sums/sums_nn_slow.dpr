
uses
    Math;

const 
    MAXN = 1000000000;
    MAXM = 2000000000;

var
    a, b, c, d : integer;

function f(a, b, n : integer) : int64;
var
    i, x, y : integer;
    begin
        if (n < 0) then begin
            result := 0;
            exit;
        end;
        result := 0;
        for i := a to b do begin
            y := min(b, n - i);
            result := result + max(0, y - a + 1);
        end;
    end;

begin
    reset(input, 'sums.in');
    rewrite(output, 'sums.out');
    read(a, b, c, d);
    assert((0 <= a) and (a <= MAXN));
    assert((0 <= b) and (b <= MAXN));
    assert((0 <= c) and (c <= MAXM));
    assert((0 <= d) and (d <= MAXM));
    writeln(f(a, b, d) - f(a, b, c - 1));
    close(input);
    close(output);
end.