import java.io.*;
import java.util.*;

public class sums_ats implements Runnable {
	public static void main(String[] args) {
		new Thread(new sums_ats()).start();
	}

	BufferedReader br;
	StringTokenizer in;
	PrintWriter out;

	public String nextToken() throws IOException {
		while (in == null || !in.hasMoreTokens()) {
			in = new StringTokenizer(br.readLine());
		}
		return in.nextToken();
	}

	public int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	public long nextLong() throws IOException {
		return Long.parseLong(nextToken());
	}

	public double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}

	final int MAXVAL = (int) 1e9;

	int a;
	int b;
	int c;
	int d;

	public long calc(long low, long high, long maxSum) {
		if (low * 2 > maxSum) {
			return 0;
		}
		// maxSum = Math.min(maxSum, high * 2);
		long left = maxSum - high;
		long right = maxSum - low;
		long ans = 0;
		if (left >= high) {
			ans += (high - low + 1) * (high - low + 1);
		} else if (left >= low) {
			ans += (high - low + 1) * (left - low + 1);
			if (right > b) {
				long len = right - high + 1;
				ans += (high - low + 1) * (high - low) / 2 - len * (len - 1)
						/ 2;
			} else {
				ans += (high - low + 1) * (high - low) / 2;
			}
		} else {
			long len = right - low + 1;
			ans += len * (len + 1) / 2;
		}
		return ans;
	}

	public void solve() throws IOException {
		long ans = 0;
		a = nextInt();
		b = nextInt();
		c = nextInt();
		d = nextInt();

		assert (a <= b);
		assert (c <= d);
		assert (0 <= a && b <= MAXVAL);
		assert (0 <= c && d <= MAXVAL * 2);

		out.println(calc(a, b, d) - calc(a, b, c - 1));
	}

	public void run() {
		try {
			Locale.setDefault(Locale.US);
			br = new BufferedReader(new FileReader("sums.in"));
			out = new PrintWriter("sums.out");

			solve();

			out.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
