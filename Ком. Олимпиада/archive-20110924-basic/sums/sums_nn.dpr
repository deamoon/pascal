
uses
    Math;

const 
    MAXN = 1000000000;
    MAXM = 2000000000;

var
    a, b, c, d : integer;

function g(n : integer) : int64;
    begin
        result := int64(n) * (n + 1) shr 1;
    end;

function f(a, b, n : integer) : int64;
var
    m, x, y : integer;
    begin
        if (n < 0) then begin
            result := 0;
            exit;
        end;
        m := n shr 1;
        if (m >= b) then begin
            result := int64(b - a + 1) * (b - a + 1);
            exit;
        end;
        if (m < a) then begin
            x := max(0, n - 2 * a + 1);
            y := max(0, n - b - a + 1);
            result := (g(x) - g(y - 1)) * 2;
            exit;
        end;
        result := int64(m - a + 1) * (m - a + 1);
        x := max(0, n - m - 1 - a + 1);
        y := max(0, n - b - a + 1);
        result := result + (g(x) - g(y - 1)) * 2;
    end;

begin
    reset(input, 'sums.in');
    rewrite(output, 'sums.out');
    read(a, b, c, d);
    assert((0 <= a) and (a <= MAXN));
    assert((0 <= b) and (b <= MAXN));
    assert((0 <= c) and (c <= MAXM));
    assert((0 <= d) and (d <= MAXM));
    writeln(f(a, b, d) - f(a, b, c - 1));
    close(input);
    close(output);
end.