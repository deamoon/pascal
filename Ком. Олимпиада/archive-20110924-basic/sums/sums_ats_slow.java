import java.io.*;
import java.util.*;

public class sums_ats_slow implements Runnable {
	public static void main(String[] args) {
		new Thread(new sums_ats_slow()).start();
	}

	BufferedReader br;
	StringTokenizer in;
	PrintWriter out;

	public String nextToken() throws IOException {
		while (in == null || !in.hasMoreTokens()) {
			in = new StringTokenizer(br.readLine());
		}
		return in.nextToken();
	}

	public int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}

	public long nextLong() throws IOException {
		return Long.parseLong(nextToken());
	}

	public double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}

	final int MAXVAL = (int) 1e9;

	public void solve() throws IOException {
		long ans = 0;
		int a = nextInt();
		int b = nextInt();
		int c = nextInt();
		int d = nextInt();

		assert (a <= b);
		assert (c <= d);
		assert (0 <= a && b <= MAXVAL);
		assert (0 <= c && d <= MAXVAL * 2);

		ArrayList<Integer> toSort = new ArrayList<Integer>();
		for (int first = a; first <= b; first++) {
			toSort.clear();
			int lowSum = first + a;
			int highSum = first + b;
			if (lowSum > d || highSum < c) {
				continue;
			}
			toSort.add(c);
			toSort.add(d);
			toSort.add(lowSum);
			toSort.add(highSum);
			Collections.sort(toSort);
			ans += (long) (toSort.get(2) - toSort.get(1) + 1);
		}
		out.println(ans);
	}

	public void run() {
		try {
			Locale.setDefault(Locale.US);
			br = new BufferedReader(new FileReader("sums.in"));
			out = new PrintWriter("sums.out");

			solve();

			out.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
