import java.io.*;
import java.util.*;

public class TestsGen implements Runnable {
    public static void main(String[] args) {
        new TestsGen().run();
    }

    public int done = 2;
    ArrayList<String> data = new ArrayList<String>();

    public void print() throws IOException {
        done++;
        System.err.println(done);
        PrintWriter out;
        if (done < 10)
            out = new PrintWriter("0" + String.valueOf(done));
        else
            out = new PrintWriter(String.valueOf(done));

        for (int i = 0; i < data.size(); i++)
            out.println(data.get(i));
        out.println();
        out.close();
    }

    public void gen(int minA, int maxA, int minC, int maxC, int code)
            throws IOException {
        data.clear();
        if (code == 2) {
            data.add(minA + " " + maxA + " " + minC + " " + maxC);
        } else {
            int a = nextInt(minA, maxA);
            int b = nextInt(a, maxA);
            int c, d;
            if (code == -1) {
                c = nextInt(minC, maxC);
                d = nextInt(c, maxC);
            } else if (code == 0) {
                if (rnd.nextBoolean()) {
                    c = nextInt(b * 2, maxC);
                    d = nextInt(c, maxC);
                    if (b * 2 == maxC && b != 0) {
                        b--;
                        if (a > b) {
                            a--;
                        }
                    }
                } else {
                    if (a == minC) {
                        a++;
                        if (b < a) {
                            b++;
                        }
                    }
                    d = nextInt(minC, a - 1);
                    c = nextInt(minC, d);
                }
            } else {
                c = nextInt(minC, a);
                d = nextInt(a * 2 + nextInt(1, b - a), maxC);
            }
            data.add(a + " " + b + " " + c + " " + d);
        }
        print();
    }

    public int nextInt(int low, int high) {
        int base = high - low + 1;
        int val = rnd.nextInt();
        val %= base;
        while (val < 0)
            val += base;
        val += low;
        return val;
    }

    Random rnd;

    final int MAXVAL = (int) 1e9;

    public void run() {
        try {
            rnd = new Random(346);

            gen(MAXVAL, MAXVAL, MAXVAL, MAXVAL * 2, 2);
            gen(0, MAXVAL, 0, MAXVAL * 2, 2);
            gen(MAXVAL / 2, MAXVAL, 0, MAXVAL * 2, 2);
            gen(0, 0, 0, 0, 2);
            gen(0, 1000, 1000, 2000, 2);
            gen(0, 0, 0, 100000, 2);

            gen(0, 10, 0, 20, -1);
            gen(0, 10, 0, 20, 1);
            gen(0, 10, 0, 20, 0);

            for (int i = 0; i < 3; i++) {
                gen(0, 100, 0, 200, -1);
            }
            gen(0, 100, 0, 200, 1);
            gen(0, 100, 0, 200, 0);

            for (int i = 0; i < 3; i++) {
                gen(0, 1000000, 0, 2000000, -1);
            }
            gen(0, 1000000, 0, 2000000, 1);
            gen(0, 1000000, 0, 2000000, 0);

            for (int i = 0; i < 3; i++) {
                gen(0, MAXVAL, 0, MAXVAL * 2, -1);
            }
            gen(0, MAXVAL, 0, MAXVAL * 2, 1);
            gen(0, MAXVAL, 0, MAXVAL * 2, 0);

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
