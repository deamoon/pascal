import static java.lang.Math.abs;
import static java.util.Arrays.sort;

import java.io.*;
import java.util.Scanner;

public class reserve_nn {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("reserve.in"));
		PrintWriter out = new PrintWriter("reserve.out");
		int n = sc.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		sort(a);
		for (int i = 0, j = n - 1; i <= j; i++, j--) {
			if (i == j) {
				out.println(-a[i]);
				break;
			}
			if (a[i] != -a[j]) {
				if (abs(a[i]) > abs(a[j])) {
					out.println(a[i]);
				} else {
					out.println(a[j]);
				}
				break;
			}
		}
		sc.close();
		out.close();
	}
}
