from random import *

current_test = 0

def out(a):
	global current_test
	current_test += 1
	open('%02d'%current_test, 'w').write('%d\n'%len(a) + ' '.join(str(i) for i in a) + '\n')

maxk = 1000

out([42, -1, -42, -5, 5])
out([-1])
out([1])
out([1, -1, 1])

for j in range(2):
	for i in range(0, 6):
		a = [randint(1, maxk) for i in range(10**i // 2 - 1)]
		b = a + [-i for i in a]
		b += [choice([1, -1]) * randint(1, maxk)]
		out(b)

out([1000] * 49999 + [-1000] * 49999 + [1])
out(list(range(-1000, 0)) + list(range(1, 1001)) + [-7])
out((list(range(-10, 0)) + list(range(1, 11))) * 4000 + [42])
out([1, -1] * 49999 + [789])
