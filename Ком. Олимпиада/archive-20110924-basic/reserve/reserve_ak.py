from collections import Counter
IN = open("reserve.in")

n = int(IN.readline())

assert 1 <= n <= 100000
assert n % 2 == 1

a = map(int, IN.read().split())

assert len(a) == n

assert 0 not in a

assert all(map((lambda x : -1000 <= x <= 1000), [i for i in a]))

cnt = Counter()
for i in a:
	cnt[i] += 1
	cnt[-i] = cnt[-i] # Do not delete it!

assert sum(abs(cnt[i] - cnt[-i]) for i in cnt if i) + 2 * (cnt[0] % 2) == 2


open("reserve.out", "w").write("%d\n"%-sum(a))
