uses
    Math;

const
    MAXN = 100;

var
    a : array[0 .. MAXN, 0 .. MAXN] of integer;
    got, n, m, x, y, z, i, j, k, best, ans : integer;

begin
    reset(input, 'fuse.in');
    rewrite(output, 'fuse.out');
    read(n, m);
    assert((2 <= n) and (n <= MAXN));
    assert((1 <= m) and (m <= n * (n - 1) div 2));
    for i := 1 to n do begin
        for j := 1 to n do begin
            a[i][j] := maxint;
        end;
        a[i][i] := 0;
    end;
    for i := 1 to m do begin
        read(x, y, z);
        assert((1 <= x) and (x <= n));
        assert((1 <= y) and (y <= n));
        assert(x <> y);
        assert(a[x][y] = maxint);
        assert((1 <= z) and (z <= 1000));
        a[x][y] := z;
        a[y][x] := z;
    end;
    for k := 1 to n do begin
        for i := 1 to n do begin
            for j := 1 to n do begin
                if (a[i][k] = maxint) or (a[k][j] = maxint) then
                    continue;
                a[i][j] := min(a[i][j], a[i][k] + a[k][j]);
            end;
        end;
    end;
    best := maxint;
    ans := -1;
    for i := 1 to n do begin
        got := 0;
        for j := 1 to n do begin
            got := max(got, a[i][j]);
        end;
        if (best > got) then begin
            best := got;
            ans := i;
        end;
    end;
    writeln(ans, ' ', best);
    close(input);
    close(output);
end.
