import static java.lang.Math.max;
import static java.util.Arrays.fill;

import java.io.*;
import java.util.*;

public class fuse_nn_wa {

	static class Edge {
		int from;
		int to;
		int w;

		public Edge(int from, int to, int w) {
			this.from = from;
			this.to = to;
			this.w = w;
		}

	}

	static List<Edge>[] edges;
	static boolean[] was;

	static int dfs(int v, int cur) {
		was[v] = true;
		int ret = cur;
		for (Edge e : edges[v]) {
			if (was[e.to]) {
				continue;
			}
			ret = max(ret, dfs(e.to, cur + e.w));
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("fuse.in"));
		PrintWriter out = new PrintWriter(new File("fuse.out"));
		int n = sc.nextInt();
		edges = new List[n];
		for (int i = 0; i < n; i++) {
			edges[i] = new ArrayList<Edge>();
		}
		int m = sc.nextInt();
		for (int i = 0; i < m; i++) {
			int from = sc.nextInt() - 1;
			int to = sc.nextInt() - 1;
			int w = sc.nextInt();
			edges[from].add(new Edge(from, to, w));
			edges[to].add(new Edge(to, from, w));
		}
		was = new boolean[n];
		int best = Integer.MAX_VALUE;
		int ans = -1;
		for (int i = 0; i < n; i++) {
			fill(was, false);
			int got = dfs(i, 0);
			if (got < best) {
				best = got;
				ans = i;
			}
		}
		out.println((ans + 1) + " " + best);
		sc.close();
		out.close();
	}
}
