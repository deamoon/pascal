import static java.lang.Math.max;
import static java.util.Arrays.fill;

import java.io.*;
import java.util.*;

public class fuse_nn_bfs_wa {

	static class Edge {
		int from;
		int to;
		int w;

		public Edge(int from, int to, int w) {
			this.from = from;
			this.to = to;
			this.w = w;
		}

	}

	static List<Edge>[] edges;
	static int n;

	static int bfs(int v) {
		Queue<Integer> q = new ArrayDeque<Integer>();
		q.add(v);
		int[] d = new int[n];
		boolean[] was = new boolean[n];
		fill(d, Integer.MAX_VALUE);
		d[v] = 0;
		was[v] = true;
		while (!q.isEmpty()) {
			int cur = q.poll();
			for (Edge e : edges[cur]) {
				if (was[e.to]) {
					continue;
				}
				d[e.to] = d[cur] + e.w;
				was[e.to] = true;
				q.add(e.to);
			}
		}
		int ret = 0;
		for (int i = 0; i < n; i++) {
			ret = max(ret, d[i]);
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(new File("fuse.in"));
		PrintWriter out = new PrintWriter(new File("fuse.out"));
		n = sc.nextInt();
		edges = new List[n];
		for (int i = 0; i < n; i++) {
			edges[i] = new ArrayList<Edge>();
		}
		int m = sc.nextInt();
		for (int i = 0; i < m; i++) {
			int from = sc.nextInt() - 1;
			int to = sc.nextInt() - 1;
			int w = sc.nextInt();
			edges[from].add(new Edge(from, to, w));
			edges[to].add(new Edge(to, from, w));
		}
		int best = Integer.MAX_VALUE;
		int ans = -1;
		for (int i = 0; i < n; i++) {
			int got = bfs(i);
			if (got < best) {
				best = got;
				ans = i;
			}
		}
		out.println((ans + 1) + " " + best);
		sc.close();
		out.close();
	}
}
