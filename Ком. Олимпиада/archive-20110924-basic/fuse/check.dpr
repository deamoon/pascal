uses
    testlib, SysUtils, Math;

const
    MAXN = 100;

var
    n, m, i, j, x, y, z, k, answer, got, sum, juryAns, juryGot : integer;
    a : array[0 .. MAXN, 0 .. MAXN] of integer;

begin
    n := inf.readlongint;
    m := inf.readlongint;
    for i := 1 to n do begin
        for j := 1 to n do begin
            a[i][j] := maxint;
        end;
        a[i][i] := 0;
    end;
    for i := 1 to m do begin
        x := inf.readlongint;
        y := inf.readlongint;
        z := inf.readlongint;
        a[x][y] := z;
        a[y][x] := z;
    end;
    for k := 1 to n do begin
        for i := 1 to n do begin
            for j := 1 to n do begin
                if (a[i][k] = maxint) or (a[k][j] = maxint) then
                    continue;
                a[i][j] := min(a[i][j], a[i][k] + a[k][j]);
            end;
        end;
    end;
    answer := ouf.readlongint;
    if (answer < 1) or (answer > n) then
      quit(_wa, 'Wrong number of vertex');
    got := ouf.readlongint;
    sum := -maxint;
    for i := 1 to n do begin
        sum := max(sum, a[answer][i]);
    end;
    juryAns := ans.readlongint;
    juryGot := ans.readlongint;
    if (sum <> got) then begin
        quit(_wa, 'Expected distance is ' + inttostr(sum) + ' , but found ' + inttostr(got));
    end;
    if (juryGot > got) then begin
        quit(_fail, 'Participant has got better result');
    end;
    if (juryGot < got) then begin
        quit(_wa, 'Participant couldn''t get optimal result');
    end;
    quit(_ok, 'OK: ' + IntToStr(answer) + ' ' + IntToStr(got));
end.
