import java.io.*;
import java.util.*;

public class TestsGen implements Runnable {
	public static void main(String[] args) {
		new TestsGen().run();
	}

	public int done = 1;
	ArrayList<String> data = new ArrayList<String>();

	public void print() throws IOException {
		done++;
		System.err.println(done);
		PrintWriter out;
		if (done < 10)
			out = new PrintWriter("0" + String.valueOf(done));
		else
			out = new PrintWriter(String.valueOf(done));

		for (int i = 0; i < data.size(); i++)
			out.println(data.get(i));
		out.println();
		out.close();
	}

	public void genRand(int n, int low, int high) throws IOException {
		n *= 2;
		ArrayList<Integer> cs = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			cs.add(nextInt(low, high));
		}
		gen(cs);
	}

	public void gen(ArrayList<Integer> cks) throws IOException {
		data.clear();
		data.add("" + cks.size() / 2);
		Collections.sort(cks);
		String cur = "";
		for (int i = 0; i < cks.size(); i++) {
			if (i > 0) {
				cur += ' ';
			}
			cur += cks.get(i);
		}
		data.add(cur);
		cks.clear();
		print();
	}

	public int nextInt(int low, int high) {
		int base = high - low + 1;
		int val = rnd.nextInt();
		val %= base;
		while (val < 0)
			val += base;
		val += low;
		return val;
	}

	Random rnd;

	public void run() {
		try {
			rnd = new Random(346);

			for (int i = 0; i < 5; i++) {
				genRand(6, 1, 10);
			}
			for (int i = 0; i < 5; i++) {
				genRand(7, 10, 40);
				genRand(6, 10, 40);
			}
			for (int i = 0; i < 10; i++) {
				genRand(7, 20, 100);
				genRand(6, 20, 100);
			}
			ArrayList<Integer> ck = new ArrayList<Integer>();
			for (int i = 0; i < 7; i++) {
				ck.add(1);
			}
			ck.add(100);
			gen(ck);

			ck.add(6);
			ck.add(5);
			ck.add(5);
			ck.add(3);
			gen(ck);

			ck.add(100);
			ck.add(89);
			ck.add(88);
			ck.add(87);
			ck.add(86);
			ck.add(85);
			ck.add(84);
			ck.add(3);
			gen(ck);

			for (int i = 0; i < 14; i++) {
				ck.add(100);
			}
			gen(ck);
			// queries with equal borders

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
