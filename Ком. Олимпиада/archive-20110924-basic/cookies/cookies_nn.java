import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Arrays.fill;

import java.io.*;
import java.util.Scanner;

public class cookies_nn {

    static final int MAXSUM = 701;
    static int gotSum;
    static int[][][] dp;
    static int[] a;
    static int n;

    static int go(int who, int got, int mask) {
        if (mask == (1 << 2 * n) - 1) {
            return got;
        }
        if (dp[who][got][mask] >= 0) {
            return dp[who][got][mask];
        }
        int newWho = gotSum - got > got ? 0 : gotSum - got < got ? 1 : who;
        int ret;
        if (newWho == 0) {
            ret = 0;
            for (int i = 0; i < 2 * n; i++) {
                if (((mask >> i) & 1) == 1) {
                    continue;
                }
                mask |= 1 << i;
                gotSum += a[i];
                got += a[i];
                int cur = Integer.MAX_VALUE;
                for (int j = 0; j < 2 * n; j++) {
                    if (((mask >> j) & 1) == 1) {
                        continue;
                    }
                    mask |= 1 << j;
                    gotSum += a[j];
                    cur = min(cur, go(newWho, got, mask));
                    mask &= ~(1 << j);
                    gotSum -= a[j];
                }
                ret = max(ret, cur);
                mask &= ~(1 << i);
                gotSum -= a[i];
                got -= a[i];
            }
        } else {
            ret = Integer.MAX_VALUE;
            for (int i = 0; i < 2 * n; i++) {
                if (((mask >> i) & 1) == 1) {
                    continue;
                }
                mask |= 1 << i;
                gotSum += a[i];
                int cur = 0;
                for (int j = 0; j < 2 * n; j++) {
                    if (((mask >> j) & 1) == 1) {
                        continue;
                    }
                    mask |= 1 << j;
                    gotSum += a[j];
                    got += a[j];
                    cur = max(cur, go(newWho, got, mask));
                    mask &= ~(1 << j);
                    gotSum -= a[j];
                    got -= a[j];
                }
                ret = min(ret, cur);
                mask &= ~(1 << i);
                gotSum -= a[i];
            }
        }
        return dp[who][got][mask] = ret;
    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("cookies.in"));
        PrintWriter out = new PrintWriter("cookies.out");
        n = sc.nextInt();
        assert((1 <= n) && (n <= 7));
        a = new int[2 * n];
        for (int i = 0; i < 2 * n; i++) {
            a[i] = sc.nextInt();
            assert((1 <= a[i]) && (a[i] <= 100));
        }
        dp = new int[2][MAXSUM][1 << 2 * n];
        for (int[][] d2 : dp) {
            for (int[] d : d2) {
                fill(d, -1);
            }
        }
        out.println(go(0, 0, 0));
        sc.close();
        out.close();
    }
}
