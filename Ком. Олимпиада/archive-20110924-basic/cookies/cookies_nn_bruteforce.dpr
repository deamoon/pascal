
uses
    Math;

const
    MAXN = 7;
var
    a : array[0 .. 2 * MAXN] of integer;
    was : array[0 .. 2 * MAXN] of boolean;
    n : integer;

function best(a, b, x : integer) : integer;
    begin
        if (x = 0) then
            result := max(a, b)
        else
            result := min(a, b);
    end;

function go(x, got1, got2, who : integer) : integer;
var
    i, j, cur, got : integer;
    begin
        if (x = n) then begin
            if (who = 0) then begin
                result := got1;
            end else begin
                result := got2;
            end;
            exit;
        end;
        if (who = 0) then
            result := 0
        else
            result := maxint;
        for i := 1 to 2 * n do begin
            if (was[i]) then
                continue;
            was[i] := true;
            got1 := got1 + a[i];
            if (who = 0) then begin
                cur := maxint;
            end else begin
                cur := 0;
            end;
            for j := 1 to 2 * n do begin
                if (was[j]) then
                    continue;
                was[j] := true;
                got2 := got2 + a[j];
                if (got1 > got2) then begin
                    got := go(x + 1, got2, got1, who xor 1);
                end else begin
                    got := go(x + 1, got1, got2, who);
                end;
                cur := best(cur, got, who xor 1);
                got2 := got2 - a[j];
                was[j] := false;
            end;
            was[i] := false;
            got1 := got1 - a[i];
            result := best(result, cur, who);
        end;
    end;

var
    i : integer;

begin
    reset(input, 'cookies.in');
    rewrite(output, 'cookies.out');
    read(n);
    assert((1 <= n) and (n <= MAXN));
    for i := 1 to 2 * n do begin
        read(a[i]);
        assert((1 <= a[i]) and (a[i] <= 100));
    end;
    fillchar(was, sizeof(was), 0);
    writeln(go(0, 0, 0, 0));
    close(input);
    close(output);
end.