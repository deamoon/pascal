uses
  Math;
var
  i, n, a, b : integer;
begin
  reset(input, 'lplant.in');
  rewrite(output, 'lplant.out');
  read(n);
  assert((1 <= n) and (n <= 100));
  for i := 1 to n do begin
    read(a, b);
    assert((1 <= a) and (a <= 1000000000));
    assert((1 <= b) and (b <= 1000000000));
    assert(a <> b);
    writeln(max(1, min(a div 2 * 2, b div 2 * 2)));
  end;
end.