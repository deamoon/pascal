#include<iostream>
#include<cstdio>
#include<cstdlib>

using namespace std;

static const long long m = 1e9 + 7;

long long min(long long a, long long b)
{
	return a < b ? a : b;
}

long long lca(long long u, long long v)
{
	if (u == 1 || v== 1)
		return 1;
		
	if (u % 2 == 0)
		if (v % 2 == 0)
			return min(u, v);
		else
			return min(u, v - 1);
	else
		if (v % 2 == 0)
			return min(u - 1, v);
		else 
			return min(u - 1, v - 1);
}

int main()
{
	freopen("lplant.in", "r", stdin);
	freopen("lplant.out", "w", stdout);
	
	int n;
	long long a, b, c, d;
	long long u, v, tmp_u, tmp_v;
	long long result = 0;
	
	cin >> n;
	cin >> u >> v;
	cin >> a >> b >> c >> d;
	
	for (int i = 0; i < n; i++)
	{
		result += lca(u, v);
		result %= m;
		tmp_u = (a * u + b * v + c) % d + 1;
		tmp_v = (a * v + b * u + c) % d + 1;
		u = tmp_u; v = tmp_v;
	}
	
	cout << result;
	
	return 0;
}
