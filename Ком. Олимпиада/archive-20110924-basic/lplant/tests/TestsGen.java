import java.io.*;
import java.util.*;

public class TestsGen {
	static int testcount = 0;
	
	static int MAXN = 100;
	static int MAX = 1000000000;

	static Random random = new Random(36498732);

	static void write(int[] u, int[] v) throws IOException {
		PrintWriter out = new PrintWriter(String.format("%02d", ++testcount));
		System.out.println("Generating test #" + testcount);
		out.println(u.length);
		for (int i = 0; i < v.length; i++) {
			out.println(u[i] + " " + v[i]);
		}
		out.close();
	}
	
	static void writeRand(int n, int max) throws IOException {
		int[] v = new int[n];
		int[] u = new int[n];
		for (int i = 0; i < n; i++) {
			v[i] = random.nextInt(max) + 1;
			u[i] = random.nextInt(max) + 1;
			while (u[i] == v[i]) {
				u[i] = random.nextInt(max) + 1;
			}
		}
		write(u, v);
	}
	
	static void writeAll(int max) throws IOException {
		int n = max * (max - 1) / 2;
		int[] v = new int[n];
		int[] u = new int[n];
		int c = 0;
		for (int i = 1; i <= max; i++) {
			for (int j = i + 1; j <= max; j++) {
				if (random.nextBoolean()) {
					v[c] = i;
					u[c] = j;
				} else {
					v[c] = j;
					u[c] = i;
				}
				c++;
			}
		}
		for (int i = 1; i < n; i++) {
			int j = random.nextInt(i + 1);
			int t = v[i]; v[i] = v[j]; v[j] = t;
			t = u[i]; u[i] = u[j]; u[j] = t;
		}
		write(u, v);
	}

	public static void main(String[] args) throws IOException {
		while (new File(String.format("%02d.t", testcount + 1)).exists()) {
			testcount++;
		}
		writeRand(1, 2);
		writeRand(10, 10);
		writeAll(14);
		writeRand(MAXN, 10);
		writeRand(1, MAX);
		for (int i = 0; i < 10; i++) {
			writeRand(MAXN, MAX);
		}
	}

}
