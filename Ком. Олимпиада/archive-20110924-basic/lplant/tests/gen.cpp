#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <string>
#include <cstdlib>

using namespace std;

static int current_test = 0;
static const int max_number = 1e9;
static const int max_n = 1e7;

template<typename T>
string to_string(const T& a)
{
    stringstream ss;
    string ans;
    ss << a;
    ss >> ans;
    return ans;
}

void out(int n)
{
    current_test++;
    string test = (current_test < 10 ? "0" : "") + to_string(current_test);
    ofstream out(test.c_str());
    out << n << "\n";
    
    long long a, b, c, d;
    long long u_1, v_1;
    
    a = rand() % max_number; 
    b = rand() % max_number;
    c = rand() % max_number;
    d = rand() % max_number;
    u_1 = 1;
    v_1 = rand() % max_number;
    
    out << u_1 << ' ' << v_1 << "\n";
    out << a << ' ' << b << ' ' << c << ' ' << d << "\n";
}

int main() 
{
	for(int i = 1; i < max_n; i += max_n / 20)
		out(i);
	
    return 0;
}
