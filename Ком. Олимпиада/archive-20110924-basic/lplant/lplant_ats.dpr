uses
    Math;

var
    u, v, i, n : longint;
begin
    reset(input, 'lplant.in');
    rewrite(output, 'lplant.out');
    read(n);
    assert ((1 <= n) and (n <= 100));
    for i := 1 to n do
    begin
        read(u, v);
        assert(u <> v);
        assert ((1 <= u) and (u <= 1e9));
        assert ((1 <= v) and (v <= 1e9));
        if (u = 1) or (v = 1) then
            writeln(1)
        else
        begin
            u := u - u mod 2;
            v := v - v mod 2;
            writeln(min(u, v));
        end
    end;
end.