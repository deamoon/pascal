program Project1;

{$APPTYPE CONSOLE}

const
  maxn = 100;
  p = 10;

type
  mas = array[0..maxn] of Shortint;

function max(a,b:int64):Int64;
begin
  if a>b then max:=a else max:=b;
end;

function min(a,b:int64):Int64;
begin
  if a<b then min:=a else min:=b;
end;

procedure null(var a:mas);
var
  i:LongInt; b:mas;
begin
  for i:=0 to maxn do a[i]:=0;
end;

function sum(a,b:mas):mas;
var
  i,m,t:longint; c:mas;
begin
  null(c); m:=max(a[0],b[0]); t:=0; c[0]:=m;
  for i:=1 to m do begin
    c[i]:=(a[i]+b[i]+t) mod p;
    t:=(a[i]+b[i]+t) div p;
  end;
  if t<>0 then begin
    c[i]:=(a[i]+b[i]+t) mod p; Inc(c[0]);
  end;
  sum:=c;
end;

procedure reverse(var a,b:mas);
var
  t:mas;
begin
  if a[0]>b[0] then begin t:=a; a:=b; b:=t; end;
end;

function sdvig(a:mas;n:longint):mas;
var
  i:LongInt;
begin
  for i:=a[0] downto 1 do a[i+n]:=a[i];
  for i:=1 to n do a[i]:=0;
  Inc(a[0],n);
  sdvig:=a;
end;

function proiz(a,b:mas):mas;
var
  v,c:mas; i,j,t,m:LongInt;
begin
  null(v); m:=max(a[0],b[0]); reverse(a,b);
  for i:=1 to a[0] do begin
    t:=0; null(c); c[0]:=m;
    for j:=1 to b[0] do begin
      c[j]:=(a[i]*b[j]+t) mod p;
      t:=(a[i]*b[j]+t) div p;
    end;
    if t<>0 then begin
      c[j]:=(a[i]*b[j]+t) mod p; Inc(c[0]);
    end;
    c:=sdvig(c,i-1);
    v:=sum(v,c);
  end;
  proiz:=v;
end;

function vvod(a:Int64):mas;
var
  v:mas; s:string; err,j,i:Integer;
begin
  Str(a,s);
  for i:=0 to maxn do v[i]:=0;
  v[0]:=Length(s); j:=0;
  for i:=Length(s) downto 1 do begin
    Inc(j);
    val(s[i],v[j],err);
  end;
  vvod:=v;
end;

procedure vivod(a:mas);
var
  i:Integer;
begin
  if a[a[0]]=0 then a[0]:=1;
  for i:=a[0] downto 1 do write(a[i]);
  writeln;
end;

var
  a,b:int64; c,d,e:mas;
begin
Assign(Input,'1.txt'); Assign(Output,'2.txt');

  Readln(a,b);
  //vivod(vvod(a));
  c:=vvod(a); d:=vvod(b);
  e:=proiz(c,d);
  vivod(e);

end.
 