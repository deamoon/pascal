const os=10;
type mas = array[0..1000] of longint;

function proiz(a:mas;n:longint):mas;
var i,d:longint; b:mas;
begin
  d:=0; b[0]:=a[0];
  for i:=1 to a[0] do begin
    b[i]:=(a[i]*n+d) mod os; d:=(a[i]*n+d) div os;
  end;
  if d<>0 then begin b[a[0]+1]:=d; inc(b[0]); end;
  proiz:=b;
end;

function sum(a:mas;n:longint):mas;
var i,d:longint; b:mas;
begin
  d:=0; b[0]:=a[0];
  for i:=1 to a[0] do begin
    b[i]:=(a[i]+n+d) mod os; d:=(a[i]+n+d) div os;
    n:=0;
  end;
  if d<>0 then begin b[a[0]+1]:=d; inc(b[0]); end;
  sum:=b;
end;

function raz(a,b:mas):mas;
var i,j,z:longint; c:mas;
begin
  for i:=1 to a[0] do begin
    c[i]:=a[i]-b[i];
    if c[i]<0 then begin
      c[i]:=c[i]+os;
      j:=i+1;
      while a[j]=0 do inc(j);
      dec(a[j]);
      for z:=j-1 downto i+1 do a[z]:=9;
    end;
  end;
  c[0]:=1;
  for i:=a[0] downto 1 do begin
    if c[i]<>0 then begin c[0]:=i; break; end;
  end;
  raz:=c;
end;

procedure vivod(a:mas);
var i:longint;
begin
  for i:=a[0] downto 1 do write(a[i]);
  writeln;
end;

var
  a,b:mas; n:longint;
begin
  a[0]:=3; a[1]:=0; a[2]:=0; a[3]:=1; //a[4]:=3;
  b[0]:=2; b[1]:=0; b[2]:=4; //b[3]:=9; b[4]:=2;
  n:=100;
  vivod(sum(b,2));
end.