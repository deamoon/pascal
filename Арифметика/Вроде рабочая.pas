const
  os = 10;
  maxn = 1000;
type
  mas = array[0..maxn] of longint;

function sum(ar1,ar2:mas):mas; // ������� �������� 2 �������� 123+456=579, � ������� �������� ���-�� ����, ������ ����� �������(����� �������)
var
  i,m,d:longint; ar:mas;
begin
d:=abs(ar1[0]-ar2[0]);
  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0;
    m:=ar1[0]; inc(ar2[0],d);
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0;
    m:=ar2[0]; inc(ar1[0],d);
  end;
d:=0;
for i:=m downto 1 do begin
  ar[i]:=(ar1[i]+ar2[i]+d) mod os; d:=(ar1[i]+ar2[i]+d) div os;
end;
ar[0]:=m;
  if d<>0 then begin
    ar[0]:=m+1;
    for i:=m downto 1 do ar[i+1]:=ar[i];
    ar[1]:=d;
  end;
sum:=ar;
end;

function proiz(ar1,ar2:mas):mas; // ������� ��������� 2 �������� 123*456=nnnn, � ������� �������� ���-�� ����, ������ ����� �������(����� �������)
var
  i,m,d,j,l,r:longint; ar,a:mas;
begin
d:=abs(ar1[0]-ar2[0]);

  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0;
    m:=ar1[0]; inc(ar2[0],d);
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0;
    m:=ar2[0]; inc(ar1[0],d);
  end;

a[0]:=0; l:=0;
for j:=m downto 1 do begin
d:=0;
  for i:=m downto 1 do begin
    ar[i]:=(ar1[i]*ar2[j]+d) mod os; d:=(ar1[i]*ar2[j]+d) div os;
  end;

ar[0]:=m;
  if d<>0 then begin
    ar[0]:=m+1;
    for i:=m downto 1 do ar[i+1]:=ar[i];
    ar[1]:=d;
  end;
  inc(ar[0],l);
  inc(l);

  a:=sum(a,ar);
  for i:=0 to ar[0] do ar[i]:=0;
end;

  r:=0;
  for i:=1 to m do begin
    if a[i]<>0 then begin r:=1; break; end;
  end;
  if r=0 then a[0]:=1;

  j:=0;
for i:=1 to a[0] do begin
  if a[i]<>0 then break;
  inc(j);
end;
for i:=1 to a[0] do a[i]:=a[i+j];
dec(a[0],j);

proiz:=a;
end;

function raz(ar1,ar2:mas):mas;
var
  i,m,d,l,j,g:longint; ar:mas;
begin
d:=abs(ar1[0]-ar2[0]);
  if ar1[0]>ar2[0] then begin
    for i:=ar2[0] downto 1 do ar2[i+d]:=ar2[i];
    for i:=1 to d do ar2[i]:=0; m:=ar1[0];
  end else begin
    for i:=ar1[0] downto 1 do ar1[i+d]:=ar1[i];
    for i:=1 to d do ar1[i]:=0; m:=ar2[0];
  end;
d:=0;
for i:=m downto 1 do begin
  l:=ar1[i]-ar2[i];
  if l>=0 then ar[i]:=l mod os else begin
    ar[i]:=l+os;
    for j:=i-1 downto 1 do begin
      if ar1[j]<>0 then begin dec(ar1[j]); break; end;
    end;

    //if ar1[j]=0 then begin
      for g:=j+1 to i-1 do ar1[g]:=9;
    //end;

  end;
end;
ar[0]:=m;

j:=0;
for i:=1 to ar[0] do begin
  if (ar[i]=0) then inc(j) else break;
end;
if j<>0 then begin
for i:=j+1 to ar[0] do begin ar[i-j]:=ar[i]; ar[i]:=0; end;
dec(ar[0],j); end;
//if sr(ar,ar2)<>3 then begin inc(k); ar:=raz(ar,ar3,ar3);  end;
raz:=ar;
end;

procedure vivod(c:mas);
var
t:boolean; i:longint;
begin
t:=true;
for i:=1 to c[0] do begin
  if ((c[i]=0) and (t)) then continue else t:=false;
  write(c[i]);
end;
end;

function vvod(a:longint):mas;
var
  v:mas; s:string; err,j,i:Integer;
begin
  Str(a,s);
  for i:=0 to maxn do v[i]:=0;
  v[0]:=Length(s); j:=0;
  for i:=1 to length(s) do begin
    Inc(j);
    val(s[i],v[j],err);
  end;
  vvod:=v;
end;

var
  a,b,c:mas;
begin
  a:=vvod(14);
  b:=vvod(20);
  c:=proiz(a,b);
  vivod(c);
  writeln;
end.