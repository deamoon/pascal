program Project2;

{$APPTYPE CONSOLE}

type tpoint = record
  x,y:Extended;
end;

mas = array[0..100] of longint;

var
 a:mas;

procedure qsort(l,r:longint);
var
  i,j,t,x:longint;
begin
  i:=l; j:=r; x:=a[(i+j) shr 1];
  repeat
    while a[i]>x do Inc(i);
    while a[j]<x do Dec(j);
    if i<=j then begin t:=a[i]; a[i]:=a[j]; a[j]:=t; Inc(i); Dec(j); end;
  until i>j;
  if i<r then qsort(i,r);
  if j>l then qsort(l,j);
end;

var
  b:tpoint;
  i:longint;
begin
  b.x:=4;

  for i:=1 to 5 do begin
    read(a[i]);
  end;
  qsort(1,5);
  for i:=1 to 5 do begin
    write(a[i],' ');
  end;
  readln; readln;
end.
