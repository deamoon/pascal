unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin;

type
  TForm1 = class(TForm)
    PaintBox1: TPaintBox;
    Button1: TButton;
    Button2: TButton;
    Label2: TLabel;
    Label1: TLabel;
    seRange: TSpinEdit;
    edt1: TEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

//==============================================================================
//========== ���������� �������, ������ ���� ��������� ��������� ================

Function F(x,y: real): boolean;

Var
 a,b,c,d,e:boolean;

Begin
x:=x+1;
a := y>=7/64*(x-3)*(x-3)+3;
b := x>=0;
c := y>=3/2*x+5;
d:= (x-1)*(x-1)+(y-1)*(y-1) <= 25;
e:= y<=-1/2*(x-5)*(x-5)+1;

Result:= ((not(d)) and (not(c)) and (not(b)) or (a and c and d and not(b)) or (a and not(d) and b) or (d and not(e) and b and not(a)));
// ��� �������
End;

Function F1(x,y: real): boolean;

Var
 a,b,c,d,e:boolean;
Begin
x:=x+1;
a := abs(y-(7/64*(x-3)*(x-3)+3))<=1/12;
b := abs(x-0)<=1/15;
c := abs(y-(3/2*x+5))<=1/10;
d:= abs((x-1)*(x-1)+(y-1)*(y-1) - 25)<=1/2;
e:= abs(y-(-1/2*(x-5)*(x-5)+1))<=1/9;

Result:= a or b or c or d or e; // ��� ������� ������� (������ ������), ������������� ����� ������ ��� ��������� �������
End;


//==============================================================================
//==============================================================================

procedure TForm1.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  N,                                    // =  (=half-height) ����������
  i,j,
  k: integer;                           // =   �������� �� 1.0 � �������� �����������

  x, y,                                 //   �������� ���������� ������� �����
   dx, dy: real;                        //   �� ������� �" = "� Y � ��������"

  b:     boolean;                       //      ��������� ������� �������
  R:     byte;                          //     �������� {X, Y} � {� R, � R}
begin
      //==   ������� ������� ������ (� ��������), � N = 0,5 * ��� (������, ������)
N:=PaintBox1.Width  div 2;
i:=PaintBox1.Height div 2;
if N>i then N:=i;

      //==   �������� "dx" � "dy" �������� ���, ��� "N ��������" ~ "R ������ X"
R:=seRange.Value;
k:=N div R;
if k<1 then k:=1;                       //   � = �������� �� 1.0 � �������� �����������

dx:=1/k;                                //   ��������� "�� ������� �"
dy:=dx;                                 //    ��������� "�� ������ � Y" = "� �-�������"

      //==  ������ �������
for i:=-N to N do begin
  for j:=-N to N do
    begin                               //     �������� ���� ��������
      x:=i*dx;                            //     ��������� �������� ���������� � ������������ ��������
      y:=j*dx;

      b:=F(x,y);                          //    ��������� ���������� �������� ��� (X, Y) �����

      //== ���� �������
      if not(b) then PaintBox1.Canvas.Pixels[N+i, N-j]:=clWhite else PaintBox1.Canvas.Pixels[N+i, N-j]:=clYellow;

      if f1(x,y) then begin
        PaintBox1.Canvas.Pixels[N+i, N-j]:=clBlack;
      end;

    end;
                  {
                  Sample color names:
                    clBlack   clWhite
                    clRed     clGreen     clBlue
                    clPurple  clYellow    clGray
                    clNavy    clAqua      clSilver
                    clTeal    clLime      clMaroon
                    clOlive   clCream     clFuchsia
                  }
end;
      //==    �������� 0X � 0Y ����
PaintBox1.Canvas.Pen.Color:=clFuchsia;
PaintBox1.Canvas.Pen.Style:=psSolid;    //     �������� �����

PaintBox1.Canvas.MoveTo( 0 , N);        //    �������� ��� �
PaintBox1.Canvas.LineTo(2*N, N);

PaintBox1.Canvas.MoveTo(N,  0);         // �������� ��� Y.
PaintBox1.Canvas.LineTo(N, 2*N);

      //    ��������� �������� �� ����
PaintBox1.Canvas.Pen.Color:=clPurple;
//PaintBox1.Canvas.Pen.Style:=psDot;      //   ���������� �����

for i:=1 to N div k do                  //==    ��������� �������� �� ��� X
  begin
  PaintBox1.Canvas.MoveTo(N-i*k, N*2);  //    ��� ������������� �������� X
  PaintBox1.Canvas.LineTo(N-i*k,  0 );

  PaintBox1.Canvas.MoveTo(N+i*k, N*2);  //  ��� ������������� ��������� X
  PaintBox1.Canvas.LineTo(N+i*k,  0 );
  end;

for i:=1 to N div k do                  //==   ��������� �������� �� ��� Y
  begin
  PaintBox1.Canvas.MoveTo(N*2, N-i*k);  // ��� ������������� �������� �
  PaintBox1.Canvas.LineTo( 0 , N-i*k);

  PaintBox1.Canvas.MoveTo(N*2, N+i*k);  // ��� ������������� �������� �
  PaintBox1.Canvas.LineTo( 0 , N+i*k);
  end;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  Form1.Button1Click(Form1.Button1);
end;

end.
