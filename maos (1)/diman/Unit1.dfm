object Form1: TForm1
  Left = 925
  Top = 150
  Width = 322
  Height = 249
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = Button1Click
  DesignSize = (
    306
    211)
  PixelsPerInch = 96
  TextHeight = 13
  object PaintBox1: TPaintBox
    Left = 8
    Top = 8
    Width = 201
    Height = 201
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clBlack
    ParentColor = False
  end
  object Label2: TLabel
    Left = 220
    Top = 92
    Width = 75
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Range {'#177'X, '#177'Y}'
  end
  object Label1: TLabel
    Left = 244
    Top = 112
    Width = 11
    Height = 20
    Anchors = [akTop, akRight]
    Caption = #177
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 224
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Paint'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 224
    Top = 48
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Exit'
    TabOrder = 1
    OnClick = Button2Click
  end
  object seRange: TSpinEdit
    Left = 260
    Top = 112
    Width = 45
    Height = 22
    Anchors = [akTop, akRight]
    MaxValue = 30
    MinValue = 1
    TabOrder = 2
    Value = 10
  end
  object edt1: TEdit
    Left = 56
    Top = 80
    Width = 49
    Height = 21
    TabOrder = 3
    Text = 'edt1'
  end
end
