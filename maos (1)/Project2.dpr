{$APPTYPE CONSOLE}

function prost(n:longint):boolean;
var
  i:LongInt;
begin
  result:=True;
  if ((n=0) or (n=1)) then result:=False;
  for i:=2 to Round(Sqrt(n)) do begin
    if n mod i = 0 then begin result:=false; Break; end;
  end;
end;

function sum(n:longint):LongInt;
begin
  result:=0;
  while n<>0 do begin
    Inc(Result,n mod 10); n:=n div 10;
  end;
end;

var
  i,kol:longint; a:array[0..100] of LongInt; t:Extended;

begin
  kol:=0;
  for i:=0 to 100 do a[i]:=0;
  for i:=0 to 1000000 do begin
    if prost(sum(i)) then begin Inc(a[sum(i)]); Inc(kol); end;
  end;
  t:=0;
  for i:=0 to 54 do begin
    if prost(i) then begin writeln(i,' ',a[i],' / ',kol); t:=t+(a[i]/kol)*ln(kol/a[i])/ln(2); end;
  end;
  write(t:0:2);
  Readln;
end.

