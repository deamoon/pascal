object Form1: TForm1
  Left = 26
  Top = 87
  Width = 322
  Height = 249
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    304
    204)
  PixelsPerInch = 120
  TextHeight = 16
  object PaintBox1: TPaintBox
    Left = 4
    Top = 8
    Width = 181
    Height = 249
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clBlack
    ParentColor = False
  end
  object Label2: TLabel
    Left = 188
    Top = 89
    Width = 91
    Height = 16
    Anchors = [akTop, akRight]
    Caption = 'Range {'#177'X, '#177'Y}'
  end
  object Label1: TLabel
    Left = 260
    Top = 106
    Width = 13
    Height = 24
    Anchors = [akTop, akRight]
    Caption = #177
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 188
    Top = 4
    Width = 92
    Height = 30
    Anchors = [akTop, akRight]
    Caption = 'Paint'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 188
    Top = 51
    Width = 92
    Height = 31
    Anchors = [akTop, akRight]
    Caption = 'Exit'
    TabOrder = 1
    OnClick = Button2Click
  end
  object seRange: TSpinEdit
    Left = 196
    Top = 106
    Width = 55
    Height = 26
    Anchors = [akTop, akRight]
    MaxValue = 30
    MinValue = 1
    TabOrder = 2
    Value = 10
  end
end
