unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin;

type
  TForm1 = class(TForm)
    PaintBox1: TPaintBox;
    Button1: TButton;
    Button2: TButton;
    Label2: TLabel;
    Label1: TLabel;
    seRange: TSpinEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

//==============================================================================
//========== logical function, should be customized by students ================

Function F(x,y: real): boolean;

Var
  inside_small_circle,
  outside_small_romb,
  inside_large_circle,
  outside_large_romb: boolean;

Begin

inside_small_circle := (x*x+y*y) < 5*5;
inside_large_circle := (x*x+y*y) < 10*10;

outside_small_romb  := abs(x)+abs(y) >=5;
outside_large_romb  := abs(x)+abs(y) >=10;

Result:=  (inside_small_circle and outside_small_romb)
       or (inside_large_circle and outside_large_romb);
End;

//==============================================================================
//==============================================================================

procedure TForm1.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  N,                                    // = half-width (=half-height)
  i,j,
  k: integer;                           // = pixels per 1.0 in real coordinates

  x, y,                                 // real coordinates of a current point
   dx, dy: real;                        // = "X's per pixel" = "Y's per pixel"

  b:     boolean;                       // result of boolean function
  R:     byte;                          // range for {X,Y} as {�R, �R}
begin
      //== Select area size (in pixels) as N = 0.5 * min(Height, Width)
N:=PaintBox1.Width  div 2;
i:=PaintBox1.Height div 2;
if N>i then N:=i;

      //== Select "dx" and "dy" values so as "N pixels" ~ "R units of X"
R:=seRange.Value;
k:=N div R;
if k<1 then k:=1;                       // k = pixels per 1.0 in real coordinates

dx:=1/k;                                // compute "X's per pixel"
dy:=dx;                                 // compute "Y's per pixel" = "X's per pixel"

      //== Paint the areas
for i:=-N to N do
  for j:=-N to N do
    begin                               //  iterate through all pixels
    x:=i*dx;                            // calculate real coordinates from pixel coordinates
    y:=j*dx;

    b:=F(x,y);                          // compute boolean value for the (X,Y) point

      //== colour the pixel
    if b
    then PaintBox1.Canvas.Pixels[N+i, N-j]:=clWhite
    else PaintBox1.Canvas.Pixels[N+i, N-j]:=clBlack;
    end;
                  {
                  Sample color names:
                    clBlack   clWhite
                    clRed     clGreen     clBlue
                    clPurple  clYellow    clGray
                    clNavy    clAqua      clSilver
                    clTeal    clLime      clMaroon
                    clOlive   clCream     clFuchsia
                  }

      //== draw 0X and 0Y axes
PaintBox1.Canvas.Pen.Color:=clFuchsia;
PaintBox1.Canvas.Pen.Style:=psSolid;    // solid lines

PaintBox1.Canvas.MoveTo( 0 , N);        // draw X axis
PaintBox1.Canvas.LineTo(2*N, N);

PaintBox1.Canvas.MoveTo(N,  0);         // draw Y axis
PaintBox1.Canvas.LineTo(N, 2*N);

      // draw labels on axes
PaintBox1.Canvas.Pen.Color:=clPurple;
PaintBox1.Canvas.Pen.Style:=psDot;      // dotted lines

for i:=1 to N div k do                  //== draw labels on the X axis
  begin
  PaintBox1.Canvas.MoveTo(N-i*k, N*2);  // for negative values of X
  PaintBox1.Canvas.LineTo(N-i*k,  0 );

  PaintBox1.Canvas.MoveTo(N+i*k, N*2);  // for positive values of X
  PaintBox1.Canvas.LineTo(N+i*k,  0 );
  end;

for i:=1 to N div k do                  //== draw labels on the Y axis
  begin
  PaintBox1.Canvas.MoveTo(N*2, N-i*k);  // for negative values of y
  PaintBox1.Canvas.LineTo( 0 , N-i*k);

  PaintBox1.Canvas.MoveTo(N*2, N+i*k);  // for positive values of y
  PaintBox1.Canvas.LineTo( 0 , N+i*k);
  end;

end;

end.
