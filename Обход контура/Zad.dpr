program Zad;

{$APPTYPE CONSOLE}

type
  tPoint = record x,y:Real;
end;

var
  p,v:array[0..1001] of tPoint;
  d,a:array[0..1001] of Real;
  b:array[0..1001] of Integer;
  p1,p2:tPoint; s,t,min,max,e,r,c:real;
  n,i,mx,mn,t1,j,k,z:LongInt; f1,f2:Text;
begin
  Assign(f1,'input.txt'); Assign(f2,'output.txt'); Reset(f1); Rewrite(f2);
  Readln(f1,n);
  for i:=1 to n do begin
    Readln(f1,p[i].x,p[i].y);
    if i=1 then begin min:=p[i].x; max:=min; mn:=1; mx:=1; p1:=p[i]; Continue; end;
    if p[i].x>max then begin max:=p[i].x; mx:=i; end;
    if p[i].x<min then begin min:=p[i].x; mn:=i; p1:=p[i]; end;
  end;
  //Writeln(mn);
  e:=p[mx].x-p[mn].x; r:=p[mx].y-p[mn].y; c:=Sqrt(e*e+r*r);

  for i:=1 to n do begin
    if i=mn then continue;
    v[i].x:=p[i].x-p1.x; v[i].y:=p[i].y-p1.y;
    d[i]:=Sqrt(v[i].x*v[i].x+v[i].y*v[i].y);
  end;

  for i:=1 to n do begin
    if i=mn then Continue;
    a[i]:=(v[i].x*r-v[i].y*e)/(d[i]*c);
  end;

  for i:=mn to n-1 do begin a[i]:=a[i+1]; d[i]:=d[i+1]; end;
  for i:=1 to n-1 do b[i]:=i;

  for i:=1 to n-1 do begin
    for j:=1 to n-2 do begin
      if a[j]<a[j+1] then begin t:=a[j+1]; a[j+1]:=a[j]; a[j]:=t;
                                t1:=b[j+1]; b[j+1]:=b[j]; b[j]:=t1; end;
    end;
   end;

  s:=a[1]; k:=1;
  for z:=2 to n-1 do begin
    if s=a[z] then begin inc(k); continue; end;

    if k>1 then begin
    for i:=z-k to z-1 do begin
    for j:=z-k to z-2 do begin
      if d[j]>d[j+1] then begin t:=d[j+1]; d[j+1]:=d[j]; d[j]:=t;
                                t1:=b[j+1]; b[j+1]:=b[j]; b[j]:=t1; end;
    end;
    end;

    end;

    k:=1; s:=a[z];
  end;

    if k>1 then begin
      for i:=z-k to z-1 do begin
      for j:=z-k to z-2 do begin
        if d[j]>d[j+1] then begin t:=d[j+1]; d[j+1]:=d[j]; d[j]:=t;
                                t1:=b[j+1]; b[j+1]:=b[j]; b[j]:=t1; end;
      end;
      end;
    end;


  write(f2,mn,' ');
  for i:=1 to n-1 do begin
    if b[i]<mn then write(f2,b[i],' ') else write(f2,b[i]+1,' ');
  end;

Close(f1); Close(f2);
end.
