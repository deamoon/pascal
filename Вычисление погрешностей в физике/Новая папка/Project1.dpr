program Project1;

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  f1,f2:Text;
  x1,x2,x3:real; n,i:Integer; ar:array[0..100] of real;

begin
  Assign(f1,'1.txt'); Assign(f2,'2.txt'); Reset(f1); Rewrite(f2);
  n:=0;
  write(f2,'���������� ���������: ');
  while (not(Eof(f1))) do begin
    Inc(n);
    Readln(f1,ar[n]);
    write(f2,ar[n]:0:5,' ');
  end;
  writeln(f2,' (',n,')');
  x1:=0;
  for i:=1 to n do begin
    x1:=x1+ar[i];
  end;
  x1:=x1/n;
  writeln(f2,'X_��� = ', x1:0:15); // ����� x1
  x2:=0;
  for i:=1 to n do begin
    x2:=(ar[i]-x1)*(ar[i]-x1)+x2;
  end;
  x2:=Sqrt(x2/(n*(n-1)));
  writeln(f2,'X_���� = ', x2:0:15); // ����� x2
  x3:=Sqrt(x1*x1+x2*x2);
  writeln(f2,'�X = ', x3:0:15); // ����� x3
  Close(f1); Close(f2);
end.
