program fac;

{$APPTYPE CONSOLE}

function nod(a,b:longint):longint;
begin
 while ((a<>0) and (b<>0)) do begin
   if a>b then a:=a mod b else b:=b mod a;
 end;
 nod:=a+b;
end;

var
  n,i,a,b,max,min,x,k:longint; p1,p2,p3,p4,p5,p6,g:Boolean;
begin
  k:=0; p1:=True; p2:=True; p3:=True; p4:=True; p5:=True;
  p6:=True; g:=false;

 while True do begin
   read(x);
   if x=-2000000000 then Break; Inc(k);
   if g then begin
     if x>a then begin
       p3:=False; p4:=False; p5:=false;
     end;
     if x=a then begin
       p1:=false; p3:=False;
     end;
     if x<a then begin
       p1:=false; p2:=false; p5:=false;
     end;
   end;

   a:=x;
   g:=True;
 end;

  Writeln(k);
 // if k=1 then write('CONSTANT') else begin
  if p5 then write('CONSTANT') else begin

    if p2 then write('WEAKLY ASCENDING') else begin
    if p3 then write('DESCENDING') else begin
    if p4 then write('WEAKLY DESCENDING') else begin
    if p1 then write('ASCENDING') else begin
    if p6 then write('RANDOM');
    end;
    end;
    end;
    end;
  end;// end;
  readln; readln;
end.
