program circle;

{$APPTYPE CONSOLE}

function kol(a,b:extended):Int64;
var
  t:Extended; a1,b1:Int64;
begin
  if b<a then begin t:=a; a:=b; b:=t; end;

  if a<=0 then a1:=Trunc(a) else begin
    if Int(a)=a then a1:=Trunc(a) else a1:=Trunc(a)+1;
  end;

  if b>=0 then b1:=Trunc(b) else begin
    if Int(b)=b then b1:=Trunc(b) else b1:=Trunc(b)-1;
  end;

  kol:=b1-a1+1;
end;

var
 r,x,y:Extended; p,ymin,ymax:Int64; i:longint;

begin
  Readln(r,x,y);

  if y+r>=0 then ymax:=Trunc(y+r) else begin
    if Int(y+r)=y+r then ymax:=Trunc(y+r) else ymax:=Trunc(y+r)-1;
  end;

  if y-r<=0 then ymin:=Trunc(y-r) else begin
    if Int(y-r)=y-r then ymin:=Trunc(y-r) else ymin:=Trunc(y-r)+1;
  end;

  p:=0;
  for i:=ymin to ymax do begin
    Inc(p,kol(x-sqrt(r*r-(i-y)*(i-y)),x+Sqrt(r*r-(i-y)*(i-y))));
    //write(p);
  end;
  write(p);
  readln;
end.
