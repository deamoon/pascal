uses vcl,AboutBox,Utils;

var MainForm:Form;
    FAbout:FormAboutBox;
    Editor:TextBox;
    OpenFile:OpenDialog;
    SaveFile:SaveDialog;
    Status:statusbar;

procedure MenuItemExitClick;
begin
  MainForm.close;
end;
procedure MenuItemAboutClick;
begin
  fabout.show;
end;
procedure MenuItemOpenClick;
begin
  if openfile.execute then begin
    Editor.Lines.loadfromfile(openfile.filename);
    Status.caption:=openfile.filename;
  end;
end;
procedure MenuItemSaveClick;
begin
  if savefile.execute then begin
    Editor.Lines.savetofile(openfile.filename);
    Status.caption:=openfile.filename;
  end;
end;
procedure MenuItemNewClick;
begin
  Editor.Lines.clear;
  Status.caption:='';
end;

Procedure InitComponents;
begin
  MainForm:=Form.Create(100,100,640,480,'�������');
  with MainForm do begin
    menu:=mainMenu.create;
    menu.add('����');
    menu.add('������');
    menu[1].add('�����',           MenuItemNewClick,  'NEW');
    menu[1].add('�������...',      MenuItemOpenClick, 'OPEN');
    menu[1].add('��������� ���...',MenuItemSaveClick, 'SAVE');
    menu[1].add('�����',           MenuItemExitClick, 'EXIT');
    menu[2].add('� ���������...',  MenuItemAboutClick,'HELP');
  end;
  Editor:=TextBox.Create;
  Editor.Align:=alClient;
  Editor.ScrollBars:=ssVertical;
  Editor.Font.Name:='Courier New';
  OpenFile:=OpenDialog.create('��� �����|*.*');
  SaveFile:=SaveDialog.create('��� �����|*.*');
  Status:=statusbar.create;
  FAbout:=FormAboutBox.create(MainForm.caption+' v1.2 (�) ������ �.�. 2005');
end;

begin
  InitComponents;
end.
