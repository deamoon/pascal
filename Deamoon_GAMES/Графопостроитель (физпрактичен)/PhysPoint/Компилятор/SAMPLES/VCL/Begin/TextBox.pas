//����� TextBox. �������� TextBox.Lines
//�������������� ������ ������� \PABC\Samples\Containers\StringArray.pas
uses VCL,Containers;

procedure print(s: string; var v: StringArray);
begin
  write(s);
  v.Println;
end;

var v:StringArray;
    MainForm:Form;
    Editor:TextBox;

begin
  MainForm:=Form.Create;
  Editor:=TextBox.Create;
  Editor.Align:=alClient;
  cls;
  v:=Editor.Lines;
  v.Resize(3);
  v.Fill('�����');
  v.add('������');
  v.Add('������');
  v.Add('������');
  v.Add('�������');
  v.Add('��������');
  v.Add('������');
  v.Add('��������');
  v.Add('�������');
  v.Put(7,'����������');
  v.Insert(11,'�������');
  v.Insert(1,'������������');
  print('�������� ������: ',v);
  v.Remove('������');
  print('����� Remove(''������''): ',v);
  v.Exchange(1,7);
  print('����� Exchange(1,7): ',v);
  writeln('IndexOf(''����������''): ',v.IndexOf('����������'));
  writeln('LastIndexOf(''�����''): ',v.LastIndexOf('�����'));
  writeln('Find(''����������''): ',v.Find('����������'));
  writeln('MinElement: ',v.MinElement,'  MinInd: ',v.MinInd);
  writeln('MaxElement: ',v.MaxElement,'  MaxInd: ',v.MaxInd);
  v.Sort;
  print('����� Sort: ',v);
  v.Reverse;
  print('����� Reverse: ',v);
  v.Destroy;
end.
