//������ Animate,ComboBox
//������� OnChange
uses vcl;

var MainForm:Form;
    Anim:Animate;
    AnimCase:ComboBox;

procedure ChangeAnim;
begin
  Anim.avi:=AVIType(AnimCase.ItemIndex);
  Anim.Active:=true;
end;

begin
  MainForm:=Form.Create(100,100,400,200,'Animate');
  AnimCase:=ComboBox.Create(130,20);
  AnimCase.OnChange:=ChangeAnim;
  with AnimCase.Items do begin
    Add('aviFindFolder');
    Add('aviFindFile');
    Add('aviFindComputer');
    Add('aviCopyFiles');
    Add('aviCopyFile');
    Add('aviRecycleFile');
    Add('aviEmptyRecycle');
    Add('aviDeleteFile');
  end;
  AnimCase.ItemIndex:=4;
  Anim:=Animate.Create(70,50);
  ChangeAnim;
end.
