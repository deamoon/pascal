uses VCL;

var
  F,F1: Form;
  l,l1: TextLabel;
  OK: Button;
  c: PaintBox;
  m: MainMenu;
  p: Panel;
  r1,r2,r3: RadioButton;
  color: integer;
  se: SpinEdit;
  st: StatusBar;
  bx: CheckBox;

procedure Cl;
begin
  F.Close;
end;

procedure CMouseDown(x,y,mb: integer);
begin
  c.MoveTo(x,y);
end;

procedure CMouseMove(x,y,mb: integer);
begin
  if mb=1 then
  begin
    c.Pen.Width:=se.Value;
    c.Pen.Color:=color;
    c.LineTo(x,y);
  end;
  if bx.Checked then
    st.Text:=IntToStr(x)+':'+IntToStr(y);
end;

procedure ClickExt(Sender: Component);
begin
  if Sender=r1 then color:=clRed;
  if Sender=r2 then color:=clGreen;
  if Sender=r3 then color:=clBlue;
end;

procedure AboutClick;
var res:integer;
begin
  res:=F1.ShowModal;
end;

procedure OKClick;
begin
  F1.Close;
end;

begin
  F:=Form.Create('��������� �����');
  F.SetSize(500,400);

  p:=Panel.Create;
  p.Width:=120;
  p.Align:=alLeft;

  c:=PaintBox.Create(p.width,0,screen.width,screen.height);
//  c.Align:=alClient;
  c.OnMouseDown:=CMouseDown;
  c.OnMouseMove:=CMouseMove;


  r1:=RadioButton.Create(p,10,10,80,20,'�������');
  r1.Checked:=True;
  r2:=RadioButton.Create(p,10,40,80,20,'�������');
  r3:=RadioButton.Create(p,10,70,80,20,'�����');
  r1.OnClickExt:=ClickExt;
  r2.OnClickExt:=ClickExt;
  r3.OnClickExt:=ClickExt;
  
  l1:=TextLabel.Create(p,10,100,'������ ����');

  se:=SpinEdit.Create(p,20,125,60,20);
  se.Min:=1;
  se.Max:=10;
  se.Value:=2;
  
  st:=StatusBar.Create;
  bx:=CheckBox.Create(10,170,102,20,'����������');

  m:=MainMenu.Create;
  m.Add('����');
  m.Add('������');
  m[1].Add('�����',Cl);
  m[2].Add('� ���������...',AboutClick);
  
  F1:=Form.Create(F.Left+100,F.Top+100,220,150);
  F1.BorderStyle:=bsDialog;
  F1.Caption:='� ���������';
  L:=TextLabel.Create(F1,15,30,'�����������: ������ ����');
  
  OK:=Button.Create(F1,65,70,'OK');
  OK.OnClick:=OKClick;

  color:=clRed;
end.
