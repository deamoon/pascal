//������������, �������� ����� ���������� ���������, �������� Color
//������ TrackBar,Panel
uses vcl,utils;

type colorsTrackBar=class(TrackBar)
       name:TextLabel;
       constructor create(parent:ContainerControl;l,t,w,h:integer;lc:string;lt:integer;tbchange:procedure);
       begin
         inherited create(parent,l+lt+10,t);
         name:=TextLabel.Create(parent,l,t,lc);
         setSize(w-lt-l-10,h);
         TickMarks:=tmBoth;
         Max:=255;
         Frequency:=15;
         OnChange:=tbChange;
       end;
     end;

var MainForm:Form;
    tbRed,tbGreen,tbBlue,tbGray:colorsTrackBar;
    Colors:Panel;
    
procedure RGBChange;
begin
  with Colors do begin
    color:=RGB(tbRed.Position,tbGreen.Position,tbBlue.Position);
    caption:=ColorToString(color);
    Font.Color:=$FFFFFF xor Color;
  end;
end;

procedure GrayChange;
begin
  with tbGray do begin
    tbRed.Position:=Position;
    tbGreen.Position:=Position;
    tbBlue.Position:=Position;
  end;
  RGBChange;
end;

begin
  MainForm:=Form.Create(100,100,400,370,'�����');
  MainForm.BorderIcons:=[biSystemMenu];
  MainForm.BorderStyle:=bsSingle;
  Colors:=Panel.Create(80,260,300,50);
  tbRed:=  colorsTrackBar.create(MainForm,10,10, 390,50,'�������',50,RGBChange);
  tbGreen:=colorsTrackBar.create(MainForm,10,70, 390,50,'�������',50,RGBChange);
  tbBlue:= colorsTrackBar.create(MainForm,10,130,390,50,'�����',  50,RGBChange);
  tbGray:= colorsTrackBar.create(MainForm,10,190,390,50,'�����',  50,GrayChange);
  RGBChange;
end.
