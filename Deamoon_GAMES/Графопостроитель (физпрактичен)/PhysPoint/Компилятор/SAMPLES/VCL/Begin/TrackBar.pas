//�������� Color, ������ TrackBar,Panel
//�������� Position,Max,Frequency
//c������ OnChange
uses vcl,utils;

var MainForm:Form;
    LabelRed,LabelGreen,LabelBlue,LabelGray:TextLabel;
    tbRed,tbGreen,tbBlue,tbGray:TrackBar;
    Colors:Panel;

procedure RGBChange;
begin
  with Colors do begin
    color:=RGB(tbRed.Position,tbGreen.Position,tbBlue.Position);
    caption:=ColorToString(color);
    Font.Color:=$FFFFFF xor Color;
  end;
end;

procedure GrayChange;
begin
  with tbGray do begin
    tbRed.Position:=Position;
    tbGreen.Position:=Position;
    tbBlue.Position:=Position;
  end;
  RGBChange;
end;

begin
  MainForm:=Form.Create(100,100,400,370,'�����');
  MainForm.BorderIcons:=[biSystemMenu];
  MainForm.BorderStyle:=bsSingle;

  Colors:=Panel.Create(80,260,300,50);

  LabelRed:=TextLabel.Create(10,20,'�������');
  tbRed:=TrackBar.Create(80,20,Colors.Width,Colors.Height);
  with tbRed do begin
    TickMarks:=tmBoth;
    Max:=255;
    Frequency:=15;
    OnChange:=RGBChange;
  end;
  
  LabelGreen:=TextLabel.Create(10,80,'�������');
  tbGreen:=TrackBar.Create(80,80,Colors.Width,Colors.Height);
  with tbGreen do begin
    TickMarks:=tmBoth;
    Max:=255;
    Frequency:=15;
    OnChange:=RGBChange;
  end;

  LabelBlue:=TextLabel.Create(10,140,'�����');
  tbBlue:=TrackBar.Create(80,140,Colors.Width,Colors.Height);
  with tbBlue do begin
    TickMarks:=tmBoth;
    Max:=255;
    Frequency:=15;
    OnChange:=RGBChange;
  end;

  LabelGray:=TextLabel.Create(10,200,'�����');
  tbGray:=TrackBar.Create(80,200,Colors.Width,Colors.Height);
  with tbGray do begin
    TickMarks:=tmBoth;
    Max:=255;
    Frequency:=15;
    OnChange:=GrayChange;
  end;

  RGBChange;
end.
