//����� ScrollBox,Splitter
uses vcl;

var MainForm:Form;
    ImageBox:ScrollBox;
    Image:PaintBox;
    Spl:Splitter;
begin
  MainForm:=Form.Create;

  ImageBox:=ScrollBox.Create;
  ImageBox.Align:=alLeft;

  Spl:=Splitter.Create(ImageBox.Width,0);
  Spl.Align:=alLeft;

  Image:=PaintBox.Create(ImageBox);
  Image.LoadFromFile('..\earth.bmp');
end.
