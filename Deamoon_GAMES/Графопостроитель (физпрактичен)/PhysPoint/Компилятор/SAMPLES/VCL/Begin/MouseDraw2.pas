uses vcl;

var MainForm:Form;
    PaintBox1:PaintBox;

procedure MouseDown(x,y,button: integer);
begin
  PaintBox1.MoveTo(x,y);
end;

procedure MouseMove(x,y,button: integer);
begin
  MainForm.Caption:='('+IntToStr(x)+','+IntToStr(y)+')';
  if button=btLeft then PaintBox1.LineTo(x,y);
end;

begin
  MainForm:=Form.Create;
  PaintBox1:=PaintBox.Create(0,0,Screen.Width,Screen.Height);
  PaintBox1.OnMouseDown:=MouseDown;
  PaintBox1.OnMouseMove:=MouseMove
end.
