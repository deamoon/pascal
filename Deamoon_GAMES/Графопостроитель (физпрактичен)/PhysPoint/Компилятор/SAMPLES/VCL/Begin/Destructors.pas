//�����������
//������������, Parent, onClickM
uses vcl;

type
  myform=class(Form)
    destructor destroy;
    begin
      writeln('����������� '+caption);
      inherited destroy;
    end;
  end;
  myButton=class(Button)
    destructor destroy;
    begin
      writeln('����������� '+caption);
      inherited destroy;
    end;
  end;

var MainForm,f2:myForm;
    b1,b2,bd1,bd2:myButton;

procedure bClick(sender:component);
begin
  if control(sender).parent=MainForm then
    control(sender).parent:=f2
  else
    control(sender).parent:=MainForm;
end;

procedure bdClick(sender:component);
begin
  control(sender).parent.destroy;
  //������ � Parent ����������� ��� ��� ��������
end;

begin
  cls;// application.enablevcldebug;
  MainForm:=myform.create(100,100,200,200);
  f2:=myform.create(300,100,200,200);
  b1:=mybutton.create(10,30);  //�� ��������� � �������� Parent ��������� MainForm
  b2:=mybutton.create(f2,60,60);
  bd1:=mybutton.create(0,0,100,25,'���������1');
  bd2:=mybutton.create(f2,0,0,100,25,'���������2');
  b1.onClickExt:=bClick;
  b2.onClickExt:=bClick;
  bd1.onclickExt:=bdClick;
  bd2.onclickExt:=bdClick;
  f2.Show;
  //������� ����� ������������ �� ���������
end.
