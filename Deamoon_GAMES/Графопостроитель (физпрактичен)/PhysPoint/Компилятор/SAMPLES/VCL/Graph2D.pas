uses VCL;

var MainForm:Form;
    Paint:PaintBox;
    BMP1,BMP2:BitMap;
    R1,R2:Rect;
    a:array[1..5] of Point;
begin
//  application.enablevcldebug;
  MainForm:=Form.Create(100,100,640,480);
  Paint:=PaintBox.Create(0,0,MainForm.ClientWidth,MainForm.ClientHeight);
  BMP1:=BitMap.Create(1,1000);
  bmp1.Width:=200;
  BMP2:=BitMap.Create(100,200);

  bmp1.canvas.pen.color:=clred;
  bmp1.canvas.rectangle(0,0,100,100);

  Paint.rectangle(20,20,50,50);

  R1:=ToRect(0,0,110,110);
  BMP2.canvas.CopyRect(0,0,110,110,Paint.Canvas,0,0,110,110);
  bmp2.Canvas.font.name:='Courier New';
  bmp2.Canvas.Font.Size:=8;
  bmp2.Canvas.font.color:=clGreen;
  bmp2.Canvas.TextOut(20,20,'BMP2');

  Paint.CopyRect(0,0,110,110,BMP1.Canvas,0,0,110,110);
  Paint.CopyRect(100,100,210,210,BMP2.Canvas,0,0,110,110);
  Paint.CopyRect(20,20,40,40,BMP2.Canvas,20,20,50,50);
  BMP1.LoadFromFile('earth.bmp');
  Paint.CopyRect(100,10,300,90,BMP1.Canvas,0,0,BMP1.Width,BMP1.Height);
  bmp1.TransparentMode:=tmAuto;
  Paint.Draw(200,200,bmp1);
  
  paint.penpos:=ToPoint(10,10);
  paint.lineto(30,20);
  
  a[1]:=ToPoint(10,20);
  a[2]:=ToPoint(20,40);
  a[3]:=ToPoint(60,80);
  a[4]:=ToPoint(10,10);
  a[5]:=ToPoint(100,100);
  paint.PolyLine(a,5);
end.
