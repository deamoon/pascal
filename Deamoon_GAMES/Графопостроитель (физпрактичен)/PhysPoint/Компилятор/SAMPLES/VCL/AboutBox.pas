unit AboutBox;

uses vcl,Utils;


procedure FormAboutBoxClose(Sender:Component);forward;
procedure CloseAboutBoxForm(Sender:Component);forward;
procedure imgshow(sender:component);forward;
procedure imgclick(sender:Component);forward;
procedure abiOnMouseUpm(Sender:Component;Shift:ShiftState;x,y:Integer);forward;
procedure abiOnMouseDownm(Sender:Component;Shift:ShiftState;x,y:Integer);forward;
procedure abiOnMouseMovem(Sender:Component;Shift:ShiftState;x,y:Integer);forward;


procedure NaturalFunc(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=0;
end;
{procedure ParamFunc(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
function GradToRad(grad:real):real;
const pi=3.1415926535897932385;
begin
  result:=grad*pi/180;
end;
const r=40;
begin
  x:=cos(GradToRad(t1))*cos(GradToRad(t2))*r;
  y:=sin(GradToRad(t1))*cos(GradToRad(t2))*r;
  z:=sin(GradToRad(t2))*r;
  c:=abs(round(z));c:=rgb(0,c*6,220);
end;{}
procedure ParamFunc(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
function GradToRad(grad:real):real;
const pi=3.1415926535897932385;
begin
  result:=grad*pi/180;
end;
var a,b,d,r:real;
begin
  r:=35;b:=r;a:=r/3;d:=r/2;
  x:=(b+a*cos(GradToRad(t1)))*cos(GradToRad(t2));
  y:=(b+a*cos(GradToRad(t1)))*sin(GradToRad(t2));
  z:=d*sin(GradToRad(t1));
  c:=abs(round(z));c:=rgb(0,c*10,220);
end;{}


type
     AboutBoxImage=class(PaintBox3D)
       figures:list3D;
       da,db,dg,dx,dy,dd:real;
       moveleft,moveright:boolean;
       procedure setfunctocenter;
       begin
         centerx:=ClientWidth/2;centery:=ClientHeight/2;
       end;
       constructor create(own:ContainerControl;L,T,W,H:integer);
       var r:real;
       begin
         inherited create(own,L,T,W,H);
         figures:=list3d.create;
         onmousedownExt:=abionmousedownm;
         onmouseupExt:=abionmouseupm;
         OnMouseMoveExt:=abiOnMouseMovem;
         da:=0;db:=0;dg:=0;
         alpha:=0;beta:=0;gamma:=0;dist:=1000;
         setfunctocenter;
         setlist3d(figures);
         r:=31;
{         figures.addfigure3d(NaturalFunction3D.Create(rgb(100,50,200),-r,-r,r,r,10,true,NaturalFunc));
         figures.addfigure3d(Line3D.create(clred  ,0,0,0,r,0,0));
         figures.addfigure3d(line3d.create(clgreen,0,0,0,0,r,0));
         figures.addfigure3d(line3d.create(clblue ,0,0,0,0,0,r));{}    {19}
         figures.addfigure3d(ParamFunction3D.Create(cllime,0,0,360,360,13,true,ParamFunc));
         moveleft:=false;moveright:=false;
         dx:=0;dy:=0;dd:=0;
       end;
       procedure randomd;
       begin
         da:=2-random*4;db:=2-random*4;dg:=2-random*4;
       end;
     end;
     simpletimer=class(timer)
       img:AboutBoxImage;
       constructor create(_img:AboutBoxImage);begin
         inherited create;
         img:=_img;
       end;
     end;
     FormAboutBox=class(form)
       c:control;p:panel;tm:simpletimer;
       img:AboutBoxImage;
//       imgform:form;
       constructor create(s:string);
       begin
         inherited Create(200,200,430,210);
         OnCloseExt:=FormAboutBoxClose;
         caption:='� ���������...';
         borderstyle:=bssingle;
         bordericons:=[biSystemMenu];
         p:=Panel.Create(self,10,10,clientwidth-20,clientHeight-60);p.caption:='';p.bevelinner:=bvLowered;
         c:=TextLabel.Create(p,10,10,50,50);c.caption:=vcl_info;
         c:=TextLabel.Create(p,10,25,50,50);c.caption:='������ '+vcl_version;c.font.style:=[fsBold];
         c:=TextLabel.Create(p,10,40,50,50);c.caption:=vcl_copyright;
         c:=TextLabel.Create(p,10,55,50,50);c.caption:=vcl_site;
         c:=TextLabel.Create(p,10,80,50,50);c.caption:=s;
         c:=TextLabel.Create(p,10,95,50,50);c.caption:=vcl_dsemail;
         c:=Button.Create(self,180,140,80,30,'OK');c.onclickExt:=CloseAboutBoxForm;
         img:=AboutBoxImage.create(p,300,5,100,100);
         img.Brush.Color:=color;{}
{         imgforExt:=form.create(left+width,top+50);
         imgform.borderstyle:=bsnone;
         imgform.clientwidth:=100;
         imgform.clientheight:=100;
         imgform.color:=clblack;
         imgform.TransparentColor:=true;
         imgform.TransparentColorValue:=imgform.color;
         img:=AboutBoxImage.create(imgform,0,0,100,100);
         img.Brush.Color:=imgform.color;{}
//         img.align:=alright;
//         img.setfunctocenter;
         tm:=simpletimer.create(img);tm.ontimerExt:=imgshow;tm.interval:=30;{}
       end;
       procedure show;
       var i:integer;
       begin
         img.randomd;
         tm.start;
//         imgform.show;
         i:=showmodal;
//         inherited show;
       end;
     end;

procedure FormAboutBoxClose(Sender:Component);
begin
  FormAboutBox(sender).tm.stop;
//  FormAboutBox(sender).imgform.close;
end;
procedure CloseAboutBoxForm(Sender:Component);
begin
  Form(control(sender).parent).close;
end;



procedure imgshow(sender:component);
begin
  with simpletimer(sender).img do begin
    alpha:=alpha+da;
    beta:=beta+db;
    gamma:=gamma+dg;
    show;
  end;
end;

procedure imgclick(sender:Component);
begin
  AboutBoxImage(sender).randomd;
end;

procedure abiOnMouseDownm(Sender:Component;Shift:ShiftState;x,y:Integer);
var tmp:AboutBoxImage;{!!}
begin
  tmp:=AboutBoxImage(sender);
  with tmp do begin
  dx:=x;dy:=y;
  moveleft:=ssLeft in Shift;
  moveright:=ssRight in Shift;
  if (ssLeft in Shift)and(ssRight in Shift) then begin
    da:=0;db:=0;dg:=0;
  end;
  end;
end;
procedure abiOnMouseMovem(Sender:Component;Shift:ShiftState;x,y:Integer);
var tmp:AboutBoxImage; {!!!}
begin
  tmp:=AboutBoxImage(sender);
  with tmp do begin
 if moveleft  then begin
   da:=x-dx;db:=y-dy;
 end;
 if moveright  then begin
   dd:=y-dy;
   dg:=-x+dx;
 end;
 dx:=x;dy:=y;
 end;
end;
procedure abiOnMouseUpm(Sender:Component;Shift:ShiftState;x,y:Integer);
var tmp:AboutBoxImage;
begin
  tmp:=AboutBoxImage(sender);
  tmp.moveleft:=false;
  tmp.moveright:=false;
end;

begin
end.
