unit functions;

uses vcl;

const pi=3.1415926535897932385;
      eps=1.0e-20;
      fieldsize=400;
      max_normf=10;
      max_paramf=6;

var func_norm:array [1..max_normf] of record
        name:string;
        func:procedure(x,y:real;var z:real;var show:boolean;var c:integer);
    end;
    func_param:array [1..max_paramf] of record
        name:string;
        func:procedure(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
    end;
    t:real;

function GradToRad(grad:real):real;
begin
  result:=grad*pi/180;
end;

function Sign(AValue: real): integer;
begin
  Result := 0;
  if AValue < 0 then
    Result := -1
  else if AValue > 0 then
    Result := 1;
end;

function colorfromz(z:real):integer;
var c:Integer;
begin
  c:=abs(round(z));
  if c>125 then c:=125;
  result:=rgb(c*2,0,220);
end;


procedure polushar(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=150*150-x*x-y*y;
  if z>=0 then z:=sqrt(z)
          else z:=0;
  c:=colorfromz(z);
end;
procedure tochki(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=200-random(400);
  c:=colorfromz(z);
end;
procedure polushar2(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=100*100-x*x-y*y;
  if z>=0 then z:=sqrt(z)
          else z:=-sqrt(-z);
  c:=colorfromz(z);
end;
procedure polusharminus(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=150*150-x*x-y*y;
  if z>=0 then z:=-sqrt(z)
          else z:=0;
  c:=colorfromz(z);
end;

procedure kluv(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=(x*x*x*x/50+y*y*y+x*x*y+y*y+x)/10000;{}
  if abs(z)>1000 then show:=false;
  c:=colorfromz(z);
end;
procedure porabola(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=(x*x+y*y)/100;{}
  if abs(z)>1000 then show:=false;
  c:=colorfromz(z);
end;
procedure ploskost(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=0;
  c:=colorfromz(z);
end;
procedure rosa(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  z:=x*x*y*y/10000000000;{}
  if abs(z)>eps then z:=sqrt(1/z)
                else show:=false;
  if abs(z)>1000 then show:=false;
  c:=colorfromz(z);
end;

procedure polutor2(x,y:real;var z:real;var show:boolean;var c:integer);
var a,b:real;
begin
  a:=200;b:=a/2;
  z:=sqr(b/(a+b))-sqr(sqrt((x*x+y*y)/10000)-a/(a+b));{}
  if z<0 then z:=-sqrt(-z)*b
         else z:=sqrt(z)*b;
  c:=abs(round(z));
  if c>125 then c:=125;
  c:=rgb(0,c*2,220);
end;


procedure polutor(x,y:real;var z:real;var show:boolean;var c:integer);
var a,b:real;
begin
  a:=300;b:=200;
  z:=sqr(b/(a+b))-sqr(sqrt(x*x/10000+y*y/10000)-a/(a+b));{}
  if z<0 then z:=-sqrt(-z)
         else z:=sqrt(z)*b;
  c:=abs(round(z));
  if c>125 then c:=125;
  c:=rgb(0,c*2,220);
end;

procedure umf(x,y:real;var z:real;var show:boolean;var c:integer);
begin
  x:=(x+fieldsize/2)/fieldsize;
  y:=(y+fieldsize/2)/fieldsize;
  z:={sin(pi/2*x)*cos(t)+}0.176*sin(0.176*t)*sin(3*pi/2*x*t)*cos(pi*y)*cos(x*y*t);{}
  z:=z*100;
  c:=abs(round(z));
  if c>125 then c:=125;
  c:=rgb(0,c*10,220);
end;
//  z:=sin(pi/2*x)*cos(t)+2/(pi*sqrt(13))*sin(2/(pi*sqrt(13))*t)*sin(3*pi/2*x*t)*cos(pi*y);{}

procedure kumir(x,y:real;var z:real;var show:boolean;var c:integer);
var r,k:real;
begin
  k:=1/fieldsize*pi*fieldsize/150;
  x:=x*k;
  y:=y*k;
  r:=x*x+y*y+1;
  z:=100*cos(r)/r;
  c:=abs(round(z));
  if c>125 then c:=125;
  c:=rgb(0,c*3,220);
end;



procedure shar(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
begin
  x:=cos(GradToRad(t1))*cos(GradToRad(t2))*100;
  y:=sin(GradToRad(t1))*cos(GradToRad(t2))*100;
  z:=sin(GradToRad(t2))*100;{}
  c:=colorfromz(z);
end;
{procedure tor(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
var a,b,d:real;
begin
  b:=100;a:=40;d:=80;
  x:=(b+a*cos(GradToRad(t1)))*cos(GradToRad(t2));
  y:=(b+a*cos(GradToRad(t1)))*sin(GradToRad(t2));
  z:=d*sin(GradToRad(t1));
  c:=colorfromz(z);
end;                   {}
procedure tor(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
var a,b,d:real;
begin
  b:=100;a:=40;d:=80;
  x:=(b+a*cos(GradToRad(t1)))*cos(GradToRad(t2));
  y:=(b+a*cos(GradToRad(t1)))*sin(GradToRad(t2));
  z:=d*sin(GradToRad(t1));{}
  c:=colorfromz(z);
end;
procedure antena(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
var a,b,d:real;
begin
  b:=100;a:=40;d:=100;
  x:=(t1+a*cos(GradToRad(t1)))*cos(GradToRad(t2));
  y:=(t1+a*cos(GradToRad(t1)))*sin(GradToRad(t2));
  z:=d*sin(GradToRad(t1));
  c:=colorfromz(z);
end;                  {}
procedure vintpl(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
begin
  x:=cos(GradToRad(t1))*cos(GradToRad(t2))*100;
  y:=sin(GradToRad(t1))*cos(GradToRad(t2))*100;
  z:=t1;{����}
  c:=colorfromz(z);
end;
procedure koleso(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
begin
  t2:=t2-180;
  x:=cos(GradToRad(t1))*(t2*t2*t2*t2/10000+1000)/100;
  y:=sin(GradToRad(t1))*(t2*t2*t2*t2/10000+1000)/100;
//  if (abs(y)>500)or(abs(x)>500) then badcord:=true;
  z:=t2;{������}
  c:=colorfromz(z);
end;
procedure paramtest(t1,t2:real;var x,y,z:real;var show:boolean;var c:integer);
begin
end;                  {}


initialization //BEGIN!!!
  func_norm[1].name:='���������';  func_norm[1].func:=ploskost;
  func_norm[2].name:='�������';    func_norm[2].func:=polushar;
  func_norm[3].name:='������� +/-';func_norm[3].func:=polushar2;
  func_norm[4].name:='�������';    func_norm[4].func:=polutor;
  func_norm[5].name:='������� +/-';func_norm[5].func:=polutor2;
  func_norm[6].name:='��������';   func_norm[6].func:=porabola;
  func_norm[7].name:='����';       func_norm[7].func:=kluv;
  func_norm[8].name:='����';       func_norm[8].func:=rosa;
  func_norm[9].name:='�����';      func_norm[9].func:=kumir;
  func_norm[10].name:='�����';     func_norm[10].func:=tochki;

  func_param[1].name:='���';                func_param[1].func:=shar;
  func_param[2].name:='�������� ���������'; func_param[2].func:=vintpl;
  func_param[3].name:='������';             func_param[3].func:=koleso;
  func_param[4].name:='���';                func_param[4].func:=tor;
  func_param[5].name:='�������';            func_param[5].func:=antena;
  func_param[6].name:='����';               func_param[6].func:=paramtest;
end.
