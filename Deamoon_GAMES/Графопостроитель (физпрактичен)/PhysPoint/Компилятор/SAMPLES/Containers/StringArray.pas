uses Containers;

procedure print(s: string; var v: StringArray);
begin
  write(s);
  v.Println;
end;


var
  v: StringArray;
begin
  cls;
  v:=StringArray.Create(3);
  v.Fill('�����');
  v.add('������');
  v.Add('������');
  v.Add('������');
  v.Add('�������');
  v.Add('��������');
  v.Add('������');
  v.Add('��������');
  v.Add('�������');
  v.Put(7,'����������');
  v.Insert(11,'�������');
  v.Insert(1,'������������');
  print('�������� ������: ',v);
  v.Remove('������');
  print('����� Remove(''������''): ',v);
  v.Exchange(1,7);
  print('����� Exchange(1,7): ',v);
  writeln('IndexOf(''����������''): ',v.IndexOf('����������'));
  writeln('LastIndexOf(''�����''): ',v.LastIndexOf('�����'));
  writeln('Find(''����������''): ',v.Find('����������'));
  writeln('MinElement: ',v.MinElement,'  MinInd: ',v.MinInd);
  writeln('MaxElement: ',v.MaxElement,'  MaxInd: ',v.MaxInd);
  v.Sort;
  print('����� Sort: ',v);
  v.Reverse;
  print('����� Reverse: ',v);
  v.Destroy;
end.
