uses Containers;

procedure print(str: string; s: RealSet);
begin
  write(str+':(');
  s.Print(', ');
  writeln(') Count=',s.Count);
end;

function Sum(s: RealSet): real;
begin
  Result:=0;
  s.Reset;
  while not s.eos do
  begin
    Result:=Result+s.Current;
    s.Next;
  end;
end;

procedure printReverse(str: string; s: RealSet);
begin
  write(str+': ');
  s.MoveLast;
  while not s.eos do
  begin
    write(s.Current,' ');
    s.Prev;
  end;
  writeln('Count=',s.Count);
end;

procedure FillRandom(s: RealSet);
var
  i: integer;
begin
  for i:=1 to 5 do
    s.Include(Random(30)/10);
end;

var
  s,s1,s2: RealSet;

begin
  cls;
  s:=RealSet.Create;

  s.Include(5.2);
  s.Include(3.4);
  s.Include(6.1);
  print('s',s);
  if s.Find(3.4) then; // ���� ������� ������, �� Current ��������������� �� ����
  write(s.Current,' ');
  if s.Find(5.2) then;
  write(s.Current,' ');
  if s.Find(6.1) then;
  write(s.Current,' ');
  write(s.Find(7));
  if not s.Eos then // ���� ������� �� ������, �� Current �� ��������� � Eos ���������� False
    write(s.Current,' ');
  writeln;
  s.Delete(5);
  print('����� s.Delete(5)',s);
  writeln('First=',s.First,'  Last=',s.Last);

  s1:=RealSet.Create;
  s2:=RealSet.Create;

  FillRandom(s1);
  FillRandom(s2);

  print('s1',s1);
  print('s2',s2);

  s.Union(s1,s2);
  print('����������� s1 � s2',s);

  s.Intersect(s1,s2);
  print('����������� s1 � s2',s);

  s.Difference(s1,s2);
  print('�������� s1 � s2',s);

  print('s: ',s);
  writeln('s ���������� � s1: ',s.Embed(s1));
  writeln('s ���������� � s2: ',s.Embed(s2));

  s.Destroy;
  s1.Destroy;
  s2.Destroy;
end.
