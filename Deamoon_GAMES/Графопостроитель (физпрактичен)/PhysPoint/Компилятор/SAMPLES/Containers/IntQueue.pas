uses Containers;

var
  v: IntQueue;

begin
  cls;
  v:=IntQueue.Create;
  v.Push(3); v.Push(2); v.Push(4); v.Push(7); v.Push(1);
  writeln('First: ',v.First);
  writeln('Last: ',v.Last);
  while not v.IsEmpty do
    write(v.Pop,' ');
  v.Destroy;
end.
