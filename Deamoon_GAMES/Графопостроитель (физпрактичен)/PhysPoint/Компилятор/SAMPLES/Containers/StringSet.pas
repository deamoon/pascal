uses Containers;

procedure print(str: string; s: StringSet);
begin
  write(str+':(');
  s.Print(',');
  writeln(') Count=',s.Count);
end;

function CountLetters(s: StringSet): integer;
begin
  Result:=0;
  s.Reset;
  while not s.eos do
  begin
    Result:=Result+Length(s.Current);
    s.Next;
  end;
end;

procedure printReverse(str: string; s: StringSet);
begin
  write(str+': ');
  s.MoveLast;
  while not s.eos do
  begin
    write(s.Current,' ');
    s.Prev;
  end;
  writeln('Count=',s.Count);
end;

var
  s,s1,s2: StringSet;

begin
  cls;
  s:=StringSet.Create;
  s1:=StringSet.Create;
  s2:=StringSet.Create;

  s.Include('������');
  s.Include('�������');
  s.Include('������');
  print('s',s);
  printReverse('Reverse s',s);

  if s.Find('������') then; // ���� ������� ������, �� Current ��������������� �� ����
  write(s.Current,' ');
  if s.Find('�������') then;
  write(s.Current,' ');
  if s.Find('������') then;
  write(s.Current,' ');
  write(s.Find('�����'));
  if not s.Eos then // ���� ������� �� ������, �� Current �� ��������� � Eos ���������� False
    write(s.Current,' ');
  writeln;
  s.Delete('������');
  print('����� s.Delete(''������'')',s);
  writeln('First=',s.First,'  Last=',s.Last);

  s1.Include('aa');
  s1.Include('bbb');
  s1.Include('cccc');

  s2.Include('dddd');
  s2.Include('bbb');
  s2.Include('cccc');
  s2.Include('ee');

  print('s1',s1);
  print('s2',s2);

  s.Union(s1,s2);
  print('����������� s1 � s2',s);

  s.Intersect(s1,s2);
  print('����������� s1 � s2',s);

  s.Difference(s1,s2);
  print('�������� s1 � s2',s);

  print('s: ',s);
  writeln('s ���������� � s1: ',s.Embed(s1));
  writeln('s ���������� � s2: ',s.Embed(s2));

  s.Destroy;
  s1.Destroy;
  s2.Destroy;
end.
