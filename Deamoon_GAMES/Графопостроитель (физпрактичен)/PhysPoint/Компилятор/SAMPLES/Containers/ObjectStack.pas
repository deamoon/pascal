uses Containers;

type
  A=class
    r: real;
    constructor Create(r: real);
    begin
      Self.r:=r;
    end;
  end;

var
  v: ObjectStack;

begin
  cls;
  v:=ObjectStack.Create;
  v.Push(A.Create(3));
  v.Push(A.Create(2));
  v.Push(A.Create(4));
  v.Push(A.Create(7));
  v.Push(A.Create(1));
  writeln('Top: ',A(v.Top).r);
  while not v.IsEmpty do
    write(A(v.Pop).r,' ');
  v.Destroy;
end.
