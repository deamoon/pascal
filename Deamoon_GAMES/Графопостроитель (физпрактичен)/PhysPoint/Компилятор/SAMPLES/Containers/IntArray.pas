uses Containers;

procedure fill(var v: IntArray);
var i: integer;
begin
  v.Fill(-1);
  for i:=1 to 10 do
    v.add(2*i);
end;

procedure print(s: string; var v: IntArray);
begin
  write(s);
  v.Println;
end;

var
  v: IntArray;
  
begin
  cls;
  v:=IntArray.Create(3);
  fill(v);
  v[7]:=53;
  v.Insert(11,666);
  v.Insert(1,18);
  v.Insert(6,53);
  v.Delete(5);
  v.Resize(18);
  print('�������� ������: ',v);
  v.Remove(53);
  print('����� Remove(53): ',v);
  v.Exchange(1,7);
  print('����� Exchange(1,7): ',v);
  writeln('IndexOf(-1): ',v.IndexOf(-1));
  writeln('LastIndexOf(-1): ',v.LastIndexOf(-1));
  writeln('Find(4): ',v.Find(4));
  writeln('MinElement: ',v.MinElement,'  MinInd: ',v.MinInd);
  writeln('MaxElement: ',v.MaxElement,'  MaxInd: ',v.MaxInd);
  writeln('Sum: ',v.Sum,'  Average: ',v.Average);
  v.Sort;
  print('����� Sort: ',v);
  v.Reverse;
  print('����� Reverse: ',v);
  v.Destroy;
end.
