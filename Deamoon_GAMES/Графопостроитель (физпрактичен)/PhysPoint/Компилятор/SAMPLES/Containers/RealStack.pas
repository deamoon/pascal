uses Containers;

var
  v: RealStack;

begin
  cls;
  v:=RealStack.Create;
  v.Push(3.1); v.Push(2.2); v.Push(4.5); v.Push(7.3); v.Push(1.4);
  writeln('Top: ',v.Top);
  while not v.IsEmpty do
    write(v.Pop,' ');
  v.Destroy;
end.
