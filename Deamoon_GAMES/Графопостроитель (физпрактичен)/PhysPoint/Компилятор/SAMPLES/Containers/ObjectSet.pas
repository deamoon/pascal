uses Containers;

type
  Pupil=class
    age: integer;
    name: string;
    constructor Create(n: string; a: integer);
    begin
      age:=a; name:=n;
    end;
    function ToString: string;
    begin
      Result:=name+' '+IntToStr(age);
    end;
    destructor Destroy;
    begin
      write('d('+ToString+') ');
    end;
  end;

procedure PrintPupils(s: string; d: ObjectSet);
begin
  writeln(s);
  write(' ');
  d.Print('  ');
  writeln;
end;

var
  d,d1,d2: ObjectSet;
  a: ObjectArray;

begin
  cls;
  a:=ObjectArray.Create;
  a.OwnsObjects:=True;
// a ������� ������ ����������
  a.Add(Pupil.Create('������',15));
  a.Add(Pupil.Create('�����',14));
  a.Add(Pupil.Create('������',12));
  a.Add(Pupil.Create('�������',16));
  a.Add(Pupil.Create('�������',12));
  a.Add(Pupil.Create('������',13));
  a.Add(Pupil.Create('������',22));
  a.Add(Pupil.Create('���������',11));

  d:=ObjectSet.Create;
  d.OwnsObjects:=False;
  d1:=ObjectSet.Create;
  d1.OwnsObjects:=False;
  d2:=ObjectSet.Create;
  d2.OwnsObjects:=False;
// d,d1,d2 �� ������� ������ ����������

  d.Include(a.get(1));
  d.Include(a.get(2));
  d.Include(a.get(3));
  d.Include(a.get(4));
  d.Include(a.get(5));
  d.Include(a.get(6));
  PrintPupils('d:',d);

  d1.Include(a.get(2));
  d1.Include(a.get(7));
  d1.Include(a.get(5));
  d1.Include(a.get(8));
  PrintPupils('d1:',d1);
  
  d2.Union(d,d1);
  PrintPupils('Union:',d2);
// ��� ��������� �������� ������� ������ ��������� ��������� - ���������
// ������� ������� � ���, ��� �������� ��������� �������� ��������������� �� �������

  d2.Intersect(d,d1);
  PrintPupils('Intersect:',d2);

  d2.Difference(d,d1);
  PrintPupils('Difference:',d2);
  
  PrintPupils('d2: ',d2);
  writeln('d2 ���������� � d: ',d2.Embed(d));
  writeln('d2 ���������� � d1: ',d2.Embed(d1));

  writeln('����������� ���������: ');
  write(' ');

  a.Destroy;
  d.Clear;
  d.Destroy;
  d1.Destroy;
  d2.Destroy;
end.
