uses Containers;

Type
  A=class
    i: real;
    constructor Create(i: real);
    begin
      Self.i:=i;
    end;
    destructor Destroy;
    begin
      write('D',i,' ');
    end;
    function ToString: string;
    begin
      Result:=FloatToStr(i);
    end;
  end;

procedure print(v: ObjectArray);
var i: integer;
begin
  for i:=1 to v.size do
  begin
    if v.get(i)<>nil then write(A(v.get(i)).ToString,' ')
    else write('nil ');
  end;
  writeln;
end;

var
  v: ObjectArray;
  t3,t5: A;

begin
  cls;
  v:=ObjectArray.Create;
//  v.OwnsObjects:=False;
  v.Add(A.Create(5));
  v.Add(A.Create(2));
  t3:=A.Create(3);
  t5:=A.Create(5);
  v.Add(t3);
  v.Add(nil);
  v.Add(t5);
  v.Add(nil);
  v.Add(A.Create(8));
  v.Add(A.Create(9));
  write('�������� ������: ');
  print(v);
  write('Put (���������� ����������): ');
  v.put(3,A.Create(11));
  write(#10+'Remove (���������� ����������): ');
  v.Remove(t5);
  write(#10+'Delete (���������� ����������): ');
  v.Delete(6);
  write(#10+'Resize (���������� ����������): ');
  v.Resize(5);
  writeln;
  print(v);
  write('����������� Print (��� nil): ');
  v.println;
  write('Destroy: ');
  v.Destroy;
end.
