uses Containers,Utils;

procedure FillRandom(s: IntSet);
var i: integer;
begin
  for i:=1 to 1000000 do
    s.Include(Random(10000000));
end;

var
  s,s1,s2: IntSet;
  m0: integer;

begin
  cls;
  s:=IntSet.Create;
  s1:=IntSet.Create;
  s2:=IntSet.Create;
  m0:=Milliseconds;
  writeln('���������� ���� �������� �� 1000000 ���������. ���������...');
  FillRandom(s1);
  FillRandom(s2);
  writeln('FillRandom: ����� ������=',(Milliseconds-m0)/1000);

  m0:=Milliseconds;
  s.Union(s1,s2);
  writeln('Union:':11,' ���������� ���������=',s.Count:7,'  ����� ������=',(Milliseconds-m0)/1000);

  m0:=Milliseconds;
  s.Intersect(s1,s2);
  writeln('Intersect:':11,' ���������� ���������=',s.Count:7,'  ����� ������=',(Milliseconds-m0)/1000);

  m0:=Milliseconds;
  s.Difference(s1,s2);
  writeln('Difference:':11,' ���������� ���������=',s.Count:7,'  ����� ������=',(Milliseconds-m0)/1000);

  m0:=Milliseconds;
  s.Destroy;
  s1.Destroy;
  s2.Destroy;
  writeln('Destroy   : ����� ������=',(Milliseconds-m0)/1000);
end.
