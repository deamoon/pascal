uses Containers;

procedure print(str: string; s: IntSet);
begin
  write(str+':(');
  s.Print(',');
  writeln(') Count=',s.Count);
end;

function Sum(s: IntSet): integer;
begin
  Result:=0;
  s.Reset;
  while not s.eos do
  begin
    Result:=Result+s.Current;
    s.Next;
  end;
end;

procedure printReverse(str: string; s: IntSet);
begin
  write(str+': ');
  s.MoveLast;
  while not s.eos do
  begin
    write(s.Current,' ');
    s.Prev;
  end;
  writeln('Count=',s.Count);
end;

procedure FillRandom(s: IntSet);
var i: integer;
begin
  for i:=1 to 10 do
    s.Include(Random(20));
end;

var
  s,s1,s2: IntSet;

begin
  cls;
  s:=IntSet.Create;
  
  s.Include(5);
  s.Include(3);
  s.Include(6);
  print('s',s);
  if s.Find(3) then; // ���� ������� ������, �� Current ��������������� �� ����
  write(s.Current,' ');
  if s.Find(5) then;
  write(s.Current,' ');
  if s.Find(6) then;
  write(s.Current,' ');
  write(s.Find(7));
  if not s.Eos then // ���� ������� �� ������, �� Current �� ��������� � Eos ���������� False
    write(s.Current,' ');
  writeln;
  s.Delete(5);
  print('����� s.Delete(5)',s);
  writeln('First=',s.First,'  Last=',s.Last);

  s1:=IntSet.Create;
  s2:=IntSet.Create;

  FillRandom(s1);
  FillRandom(s2);

  print('s1',s1);
  printreverse('s1 reverse',s1);
  print('s2',s2);
  writeln('����� ��������� s1=',Sum(s1));

  s.Union(s1,s2);
  print('����������� s1 � s2',s);

  s.Intersect(s1,s2);
  print('����������� s1 � s2',s);

  s.Difference(s1,s2);
  print('�������� s1 � s2',s);
  
  print('s: ',s);
  writeln('s ���������� � s1: ',s.Embed(s1));
  writeln('s ���������� � s2: ',s.Embed(s2));

  s.Destroy;
  s1.Destroy;
  s2.Destroy;
end.
