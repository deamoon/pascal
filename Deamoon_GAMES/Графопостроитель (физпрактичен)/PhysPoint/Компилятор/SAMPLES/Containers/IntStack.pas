uses Containers;

var
  v: IntStack;

begin
  cls;
  v:=IntStack.Create;
  v.Push(3); v.Push(2); v.Push(4); v.Push(7); v.Push(1);
  writeln('Top: ',v.Top);
  while not v.IsEmpty do
    write(v.Pop,' ');
  v.Destroy;
end.
