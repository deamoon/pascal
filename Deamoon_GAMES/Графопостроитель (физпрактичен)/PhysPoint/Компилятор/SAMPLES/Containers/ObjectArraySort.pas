uses Containers;

type
  Pupil=class
    age: integer;
    name: string;
    constructor Create(n: string; a: integer);
    begin
      age:=a; name:=n;
    end;
    function ToString: string; // ���������������� ����� ToString ���������� ObjectDynArray.Print
    begin
      Result:=name+' '+IntToStr(age);
    end;
  end;

function LessAge(o1,o2: Object): boolean;
begin
  Result:=Pupil(o1).age<Pupil(o2).age
end;

function LessName(o1,o2: Object): boolean;
begin
  Result:=Pupil(o1).name<Pupil(o2).name
end;

procedure PrintPupils(s: string; d: ObjectArray);
begin
  writeln(s);
  d.Println(#10);
  writeln;
end;

var
  d: ObjectArray;

begin
  cls;
  d:=ObjectArray.Create;
  d.Add(Pupil.Create('������',15));
  d.Add(Pupil.Create('�����',14));
  d.Add(Pupil.Create('������',12));
  d.Add(Pupil.Create('�������',16));
  d.Add(Pupil.Create('�������',12));
  d.Add(Pupil.Create('������',13));
  d.Sort(LessName);
  PrintPupils('���������� �� �����:',d);
  d.Sort(LessAge);
  PrintPupils('���������� �� ��������:',d);
  d.Destroy;
end.
