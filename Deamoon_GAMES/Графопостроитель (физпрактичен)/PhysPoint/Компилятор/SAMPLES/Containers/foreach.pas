uses Containers;

procedure p(var i: integer);
begin
  i:=2*i;
end;

procedure p1(var o: Object);
begin
  write(o.TypeName,' ');
end;

var
  a: IntArray;
  b: ObjectArray;

begin
  cls;
  a:=IntArray.Create;
  a.add(1);
  a.add(5);
  a.add(3);
  a.Println;
  a.foreach(p);
  a.Println;
  b:=ObjectArray.Create;
  b.Add(Object.Create);
  b.Add(IntArray.Create);
  b.ForEach(p1);

end.
