uses Containers;

var
  v: RealQueue;

begin
  cls;
  v:=RealQueue.Create;
  v.Push(3); v.Push(2); v.Push(4); v.Push(7); v.Push(1);
  writeln('First: ',v.First);
  writeln('Last: ',v.Last);
  while not v.IsEmpty do
    write(v.Pop,' ');
  v.Destroy;
end.
