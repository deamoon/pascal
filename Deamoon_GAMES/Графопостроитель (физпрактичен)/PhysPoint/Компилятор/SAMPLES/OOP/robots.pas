// ������������ �������� ����� ������� Robot-Field.
// ������ ����� ������ ��������� �� ����, �� �������� �����
uses GraphABC;

const
  delay=50;

type
  Field=class
    m,n,sz,x0,y0: integer;
    procedure draw;
    var cx,cy,w,h: integer;
    begin
      SetPenColor(clBlack);
      w:=sz*n;
      h:=sz*m;
      for cy:=0 to m do
        line(x0,y0+cy*sz,x0+w,y0+cy*sz);
      for cx:=0 to n do
        line(x0+cx*sz,y0,x0+cx*sz,y0+h)
    end;
    constructor init(mm,nn,ssz,x,y: integer);
    begin
      m:=mm; n:=nn; sz:=ssz; x0:=x; y0:=y;
      draw;
    end;
    procedure drawRobot(cx,cy: integer; c: integer);
    const zz=3;
    begin
      SetPenColor(clWhite);
      SetBrushColor(c);
      Rectangle(x0+(cx-1)*sz+zz,y0+(cy-1)*sz+zz,x0+cx*sz-zz+1,y0+cy*sz-zz+1);
    end;
  end;

  Robot=class
    x,y,c: integer;
    f: Field;
    procedure show;
    begin
      f.drawRobot(x,y,c);
    end;
    procedure hide;
    begin
      f.drawRobot(x,y,clWhite);
    end;
    constructor init(xx,yy,col: integer; ff: Field);
    begin
      x:=xx; y:=yy; c:=col; f:=ff;
      show;
    end;
    procedure moveon(dx,dy: integer);
    begin
      hide;
      x:=x+dx; y:=y+dy;
      show;
    end;
    procedure up;
    begin
      moveon(0,-1);
    end;
    procedure down;
    begin
      moveon(0,1);
    end;
    procedure left;
    begin
      moveon(-1,0);
    end;
    procedure right;
    begin
      moveon(1,0);
    end;
  end;
  
const
  x0=40;
  y0=40;
  n=10;
  m=12;
  sz=40;

var
  F: Field;
  r,r1: Robot;
  i: integer;
  
begin
  cls;
  SetWindowCaption('������ �� ��������� ����');
  SetWindowSize(2*x0+m*sz,2*y0+n*sz);
  F:=Field.init(n,m,sz,x0,y0);
  r:=Robot.init(1,1,clGreen,F);
  r1:=Robot.init(F.n,F.m,clRed,F);
  while true do
  begin
    for i:=1 to F.n-1 do
    begin
      sleep(delay);
      r.right;
      r1.left;
    end;
    for i:=1 to F.m-1 do
    begin
      sleep(delay);
      r.down;
      r1.up;
    end;
    for i:=1 to F.n-1 do
    begin
      sleep(delay);
      r.left;
      r1.right;
    end;
    for i:=1 to F.m-1 do
    begin
      sleep(delay);
      r.up;
      r1.down;
    end;
  end;
end.
