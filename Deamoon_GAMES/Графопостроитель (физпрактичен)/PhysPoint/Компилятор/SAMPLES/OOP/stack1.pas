// ������������ �������� �������� ������ ����� �� ���� �������
const maxsz=10;

type
  stack=class
    a: array [1..maxsz] of integer;
    last: integer;
    Constructor Create;
    begin
      last:=0;
    end;
    procedure push(i: integer);
    begin
      Inc(last);
      a[last]:=i;
    end;
    function pop: integer;
    begin
      pop:=a[last];
      Dec(last);
    end;
    function top: integer;
    begin
      top:=a[last];
    end;
    function empty: boolean;
    begin
      empty:=(last=0);
    end;
    procedure print;
    var i: integer;
    begin
      for i:=1 to last do
        write(a[i],' ');
    end;
  end;
  
var s: stack;

begin
  cls;
  s:=stack.Create;
  s.push(7);
  s.push(2);
  s.push(5);
  while not s.empty do
    write(s.pop,' ');
  s.Destroy;
end.


