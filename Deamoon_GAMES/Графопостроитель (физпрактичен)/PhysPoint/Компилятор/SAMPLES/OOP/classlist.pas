// ������������ ������ ������������ ������ ����� � ���������� ����������
type
  IntListNode=class
    i: integer;
    next: IntListNode;
    constructor Create(ii: integer; nxt: IntListNode);
    begin
      i:=ii;
      next:=nxt;
    end;
  end;

  IntList=class
  private
    first,last,current: IntListNode;
  public
    constructor Create;
    function IsEmpty: boolean;
      begin Result:=first=nil; end;
    procedure Reset;
      begin current:=first; end;
    procedure Next;
      begin current:=current.next; end;
    function CurrentValue: integer;
      begin Result:=current.i; end;
    function EndOfList: boolean;
      begin Result:=current=nil; end;
    procedure Clear;
    destructor Destroy;
      begin Clear; end;
    procedure Add(i: integer);
    procedure Print;
  end;
  
constructor IntList.Create;
begin
  first:=nil;
  last:=nil;
  current:=nil;
end;

procedure IntList.Clear;
begin
  while first<>nil do
  begin
    current:=first;
    first:=first.next;
    current.Destroy;
  end;
  current:=nil;
  last:=nil;
end;

procedure IntList.Add(i: integer);
begin
  if last<>nil then
  begin
    last.next:=IntListNode.Create(i,nil);
    last:=last.next
  end
    else
  begin
    last:=IntListNode.Create(i,nil);
    first:=last;
    current:=first;
  end;
end;
    
procedure IntList.Print;
var f: IntListNode;
begin
  f:=first;
  while f<>nil do
  begin
    write(f.i,' ');
    f:=f.next;
  end;
end;

var l: IntList;

begin
  cls;
  l:=IntList.Create;
  l.Add(1);
  l.Add(2);
  l.Add(3);
  l.Add(7);
  l.Add(5);
  l.Add(6);

  l.Print;
  writeln;

  l.Reset;
  while not l.EndOfList do
  begin
    write(l.CurrentValue,' ');
    l.Next
  end;
  writeln;

  l.Destroy;
end.
