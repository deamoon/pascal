// ���������� ��������� ������� ��� �������� �������������� �������
uses Containers;

type
  AssocArray=class
  private
    words: StringArray;
    nums: IntArray;
    procedure setProp(ind: string; value: integer);
    function getProp(ind: string): integer;
  public
    constructor Create;
    begin
      words:=StringArray.Create;
      nums:=IntArray.Create;
    end;
    destructor Destroy;
    begin
      words.Destroy;
      nums.Destroy;
    end;
    function getSize: integer;
      begin Result:=words.Size; end;
    property Size: integer read getSize;
    property Items[ind: string]: integer read getProp write setProp;
    property Keys: StringArray read words;
    property Values: IntArray read nums;
  end;

procedure AssocArray.setProp(ind: string; value: integer);
var i: integer;
begin
  i:=words.IndexOf(ind);
  if i<>0 then
    nums[i]:=value
  else
  begin
    words.add(ind);
    nums.add(value);
  end;
end;

function AssocArray.getProp(ind: string): integer;
var i: integer;
begin
  i:=words.IndexOf(ind);
  if i<>0 then
    Result:=nums[i]
  else
  begin
    words.add(ind);
    nums.add(0);
    Result:=0;
  end;
end;

var
  CountNames: AssocArray;
  i: integer;
  
begin
  cls;
  CountNames:=AssocArray.Create;
  CountNames['��������']:=3;
  CountNames['�������']:=5;
  CountNames['�������']:=CountNames['�������']+2;
  CountNames['�����']:=CountNames['�����']+1;
  for i:=1 to CountNames.Size do
    writeln(CountNames.Keys[i],' ',CountNames.Values[i]);
end.

