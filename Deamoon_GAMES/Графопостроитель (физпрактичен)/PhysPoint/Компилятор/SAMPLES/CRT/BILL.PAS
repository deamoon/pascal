uses CRT,GraphABC;

var
  ax,ay,vx,vy: integer;
  A: array [1..80,1..24] of char;

procedure CleanA;
var i,j: integer;
begin
  for j := 1 to 24 do
  for i := 1 to 80 do
    A[i,j] := ' '
end;

procedure Horiz(x,y,L: integer);
var i: integer;
begin
  for i := x to x+L-1 do
    A[i,y] := '*'
end;

procedure Vert(x,y,L: integer);
var j: integer;
begin
  for j := y to y+L-1 do
    A[x,j] := '*'
end;

procedure MinimalFill;
begin
  CleanA;
  Horiz(1,1,80);
  Horiz(1,24,80);
  Vert(1,2,22);
  Vert(80,2,22);
end;

procedure Fill;
begin
  Horiz(49,9,31);
  Horiz(49,14,31);
end;

procedure DrawScreen;
var i,j: integer;
begin
  MinimalFill;
  Fill;
  ClrScr;
  for j := 1 to 24 do
  for i := 1 to 80 do
    write(A[i,j])
end;

function NaPutiPrepiatstvie: boolean;
begin
  if (A[ax+vx,ay]<>' ') or (A[ax,ay+vy]<>' ') or (A[ax+vx,ay+vy]<>' ')
    then NaPutiPrepiatstvie := True
    else NaPutiPrepiatstvie := False
end;

procedure ChangeDirection;
begin
  if A[ax+vx,ay]<>' ' then vx := -vx;
  if A[ax,ay+vy]<>' ' then vy := -vy;
  if (A[ax+vx,ay]=' ') and (A[ax,ay+vy]=' ') and (A[ax+vx,ay+vy]<>' ') then
  begin
    vx := -vx;
    vy := -vy
  end;
  Sleep(10);
end;

procedure Show;
begin
  GotoXY(ax,ay);
  write('B');
end;

procedure Hide;
begin
  GotoXY(ax,ay);
  write(' ');
end;

procedure SetCoords (x,y: integer);
begin
  ax := x;
  ay := y
end;

procedure SetVeloc (vx0,vy0: integer);
begin
  vx := vx0;
  vy := vy0
end;

procedure MoveTo (x,y: integer);
begin
  Hide;
  SetCoords(x,y);
  Show
end;

procedure MoveOn (dx,dy: integer);
begin
  MoveTo(ax+dx,ay+dy);
end;

BEGIN
  SetWindowCaption('�������� (���������� �� CRT)');
  textattr := White;
  clrscr;
  HideCursor;
  DrawScreen;

  SetCoords(70,13);
  SetVeloc(1,1);

  TextColor(Yellow);
  Show;
  repeat
    Delay(20);
    if NaPutiPrepiatstvie then ChangeDirection;
    MoveOn(vx,vy);
  until False;
END.
