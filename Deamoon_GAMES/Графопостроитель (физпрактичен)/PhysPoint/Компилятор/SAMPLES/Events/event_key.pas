// ������������ ��������� ������� OnKeyDown, OnKeyPress
uses GraphABC,Events;

const
  r=10;
  d=3;

var x,y: integer;

procedure Show;
begin
  SetBrushColor(clBlack);
  Ellipse(x-r,y-r,x+r,y+r);
end;

procedure Hide;
begin
  SetBrushColor(clWhite);
  Ellipse(x-r,y-r,x+r,y+r);
end;

procedure KeyDown(Key: integer);
begin
  Hide;
  case Key of
   VK_Left: x:=x-d;
   VK_Up: y:=y-d;
   VK_Right: x:=x+d;
   VK_Down: y:=y+d;
  end;
  Show
end;

procedure KeyPress(Ch: char);
begin
end;

begin
  SetWindowCaption('��������� ������� ��� ����������� �������');
  x:=WindowWidth div 2;
  y:=WindowHeight div 2;
  
  SetPenColor(clWhite);
  
  OnKeyDown:=KeyDown;
  OnKeyPress:=KeyPress;

  Show;
end.
