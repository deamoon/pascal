// ��������� �����
uses GraphABC, Events;

procedure MouseDown(x,y,mb: integer);
begin
  MoveTo(x,y);
end;

procedure MouseMove(x,y,mb: integer);
begin
  SetWindowCaption('('+IntToStr(x)+','+IntToStr(y)+')');
  if mb=1 then LineTo(x,y);
end;

begin
  OnMouseDown:=MouseDown;
  OnMouseMove:=MouseMove
end.
