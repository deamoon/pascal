// ������������ ��������� ������� OnMouseDown, OnMouseUp, OnMouseMove
uses GraphABC,Events;

const r=10;

procedure MouseDown(x,y,mousebutton: integer);
begin
  if mousebutton=1 then
    TextOut(10,10,'������ ����� ����     ');
  if mousebutton=2 then
    TextOut(10,10,'������ ������ ����  ');
  Rectangle(x-r,y-r,x+r,y+r);
  MoveTo(x,y);
end;

procedure MouseMove(x,y,mousebutton: integer);
begin
  if mousebutton=1 then
    LineTo(x,y);
  if mousebutton=2 then
    SetPixel(x,y,clBlack);
  TextOut(10,WindowHeight-32,'���������� ������� ����: '+IntToStr(x)+' '+IntToStr(y)+'      ');
  MoveTo(x,y);
end;

procedure MouseUp(x,y,mousebutton: integer);
begin
  FillRect(10,10,150,26);
  Ellipse(x-r,y-r,x+r,y+r);
end;

begin
  OnMouseDown := MouseDown;
  OnMouseUp := MouseUp;
  OnMouseMove := MouseMove;
end.
