 // Демонстрация элементарной анимации. Перемещение картинки
uses GraphABC;

const speed=1;

var
  w,h,i,j,pic: integer;

begin
  SetWindowCaption('Перемещение картинки');
  pic:=LoadPicture('demo.bmp');
  w:=PictureWidth(pic);
  h:=PictureHeight(pic);
  SetBrushColor(clWhite);
  for i:=0 to WindowWidth-w do
  begin
    DrawPicture(pic,i,0);
    if i mod speed = 0 then Sleep(1);
    FillRect(i,0,i+1,0+h);
  end;
  DestroyPicture(pic);
end.
