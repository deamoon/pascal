uses GraphABC;

const
  x0=16;
  y0=16;
  z=16;
  w=96;
  h=64;

var
  a: array [1..8] of integer;
  x,y,i: integer;

begin
  a[1]:=bsSolid;
  a[2]:=bsClear;
  a[3]:=bsHorizontal;
  a[4]:=bsVertical;
  a[5]:=bsBDiagonal;
  a[6]:=bsFDiagonal;
  a[7]:=bsCross;
  a[8]:=bsDiagCross;
  x:=x0;
  y:=y0;
  SetPenStyle(psClear);
  for i:=1 to 8 do
  begin
    SetBrushColor(clBlack);
    SetBrushStyle(a[i]);
    Rectangle(x,y,x+w,y+h);
    x:=x+w+z;
    if i=4 then
    begin
      x:=x0;
      y:=y+h+z;
    end;
  end;
end.
