uses GraphABC;

const
  x0=16;
  x1=416;
  y0=16;
  z=32;

var
  a: array [1..6] of integer;
  y,i: integer;

begin
  a[1]:=psSolid;
  a[2]:=psClear;
  a[3]:=psDash;
  a[4]:=psDot;
  a[5]:=psDashDot;
  a[6]:=psDashDotDot;
  y:=y0;
  for i:=1 to 6 do
  begin
    SetPenStyle(a[i]);
    Line(x0,y,x1,y);
    y:=y+z;
  end;
end.
