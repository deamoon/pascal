uses GraphABC, Events;

const n=3;

var
  m: array [0..n] of integer;
  x,y,h,w,i: integer;

procedure DrawAll;
var i,j,x0,y0: integer;
begin
  x0 := x;
  y0 := y;
  for i := 0 to 5 do
    for j := 0 to 5 do
      DrawPicture(m[Random(4)],x0 + i*w, y0 + j*h);
end;

procedure KeyDown(key: integer);
begin
  case key of
  VK_Up:    y := y - 1;
  VK_Down:  y := y + 1;
  VK_Right: x := x + 1;
  VK_Left:  x := x - 1;
  end;
  DrawAll;
end;

begin
  for i := 0 to n do
    m[i] := LoadPicture('a'+intToStr(i+1)+'.bmp');
  x := 10;
  y := 10;
  h := PictureHeight(m[0]);
  w := PictureWidth(m[0]);
  OnKeyDown := KeyDown;
  while true do
  begin
    DrawAll;
    Sleep(100);
  end;
end.
