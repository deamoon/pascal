uses GraphABC, Events;

const n=3;

var
  m: array [0..n] of integer;
  x,y,k,h,w,i: integer;

procedure KeyDown(key: integer);
var i,j: integer;
begin
  inc(k);
  case key of
  VK_Up:    y := y - 2;
  VK_Down:  y := y + 2;
  VK_Right: x := x + 2;
  VK_Left:  x := x - 2;
  end;
  for i := 0 to 5 do
    for j := 0 to 5 do
      DrawPicture(m[k mod 3],x + i*w, y + j*h);
end;

begin
  for i := 0 to n do
    m[i] := LoadPicture('a'+intToStr(i+1)+'.bmp');
  x := 10;
  y := 10;
  k := -1;
  h := PictureHeight(m[0]);
  w := PictureWidth(m[0]);
  KeyDown(0);
  OnKeyDown := KeyDown;
end.
