// ����������� �������. ��������� ������������� �� ������ ������������ �����.
uses GraphABC;

var i,j,r1,r2,r3,g1,g2,g3,b1,b2,b3,x,y,z,brightness: integer;
    r: real; 

function LinearInterp(a,b,fa,fb,x: integer): integer;
var t: real;
begin
  t:=(x-a)/(b-a);
  LinearInterp:=round(fa*(1-t)+fb*t);
end;

begin
  SetWindowCaption('��������');
  brightness:=512;
  SetWindowSize(256*3,400);
  r1:=255;
  g1:=0;
  b1:=255;
  r2:=255;
  g2:=255;
  b2:=0;
  r3:=0;
  g3:=255;
  b3:=255;
  
  for i:=0 to 255 do
    begin
      x:=LinearInterp(0,255,r1,r2,i);
      y:=LinearInterp(0,255,g1,g2,i);
      z:=LinearInterp(0,255,b1,b2,i);
      SetPenColor(RGB(x,y,z));
      Line(i,0,i,WindowHeight);
    end;  
  for i:=0 to 255 do
    begin
      x:=LinearInterp(0,255,r2,r3,i);
      y:=LinearInterp(0,255,g2,g3,i);
      z:=LinearInterp(0,255,b2,b3,i);
      SetPenColor(RGB(x,y,z));
      Line(i+256,0,i+256,WindowHeight);
    end;  
  for i:=0 to 255 do
    begin
      x:=LinearInterp(0,255,r3,r1,i);
      y:=LinearInterp(0,255,g3,g1,i);
      z:=LinearInterp(0,255,b3,b1,i);
      SetPenColor(RGB(x,y,z));
      Line(i+256*2,0,i+256*2,WindowHeight);
    end;  
end.
