// ��������� drawGraph ��������� ������� ������� � ������ ����
uses GraphABC;

type FUN = function (x: real): real;

function f(x: real): real;
begin
  f:=x*sin(x);
end;

// l (logical) - ���������� ����������
// s (screen) - ���������� ����������
procedure drawGraph(x1,x2,y1,y2: real; f: FUN);
 var
  xl,xl0,wl,yl,yl0,hl: real;
  xs0,ws,ys0,hs: integer;
 function LtoSx(xl: real): integer;
 begin
   Result:=round(ws/wl*(xl-xl0)+xs0);
 end;
 function LtoSy(yl: real): integer;
 begin
   Result:=round(hs/hl*(yl-yl0)+ys0);
 end;
 function StoLx(xs: integer): real;
 begin
   Result:=wl/ws*(xs-xs0)+xl0;
 end;
 var xi: integer;
begin // drawGraph
  xs0:=0; ys0:=WindowHeight;
  ws:=WindowWidth;
  hs:=WindowHeight;
  xl0:=x1;
  yl0:=y1;
  wl:=x2-x1;
  hl:=-(y2-y1);
  MoveTo(xs0,LtoSy(f(StoLx(xs0))));
  for xi:=xs0+1 to xs0+ws do
    LineTo(xi,LtoSy(f(StoLx(xi))));
end;

begin // program
  SetWindowCaption('������ �������');
  drawGraph(0,4*6.28,-23,23,f);
end.
