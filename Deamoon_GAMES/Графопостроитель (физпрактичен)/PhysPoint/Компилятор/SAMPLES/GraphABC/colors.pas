 // ������������ ����������� ������
uses GraphABC;

const n=22;

var 
  x,y,y1,w,ww,i: integer;
  Colors: array [1..n] of integer;

begin
  SetWindowCaption('����������� �����');
  SetPenStyle(psClear);
  Colors[1]:=clWhite;
  Colors[2]:=clLightGray;
  Colors[3]:=clGray;
  Colors[4]:=clDarkGray;
  Colors[5]:=clBlack;
  Colors[6]:=clRed;
  Colors[7]:=clGreen;
  Colors[8]:=clBlue;
  Colors[9]:=clYellow;
  Colors[10]:=clAqua;
  Colors[11]:=clFuchsia;
  Colors[12]:=clPurple;
  Colors[13]:=clBrown;
  Colors[14]:=clMaroon;
  Colors[15]:=clMoneyGreen;
  Colors[16]:=clSkyBlue;
  Colors[17]:=clCream;
  Colors[18]:=clOlive;
  Colors[19]:=clTeal;
  Colors[20]:=clLime;
  Colors[21]:=clSilver;
  Colors[22]:=clNavy;

  x:=10; y:=20;
  y1:=400;
  w:=30;
  ww:=9;

  SetWindowSize(x+n*(w+ww),y1+y);

  for i:=1 to n do
  begin
    SetBrushColor(Colors[i]);
    Rectangle(x,y,x+w,y1);
    x:=x+w+ww;
  end;
end.
