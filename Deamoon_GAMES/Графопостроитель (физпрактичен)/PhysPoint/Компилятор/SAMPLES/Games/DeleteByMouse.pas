uses ABCObjects,GraphABC,Events;

var
  i,current,mistakes,x,y: integer;
  ob: ObjectABC;
  txt: TextABC;

procedure MyMouseDown(x,y,mb: integer);
var ob: ObjectABC;
begin
  ob:=ObjectUnderPoint(x,y);
  if (ob<>nil) and (ob is RectangleABC) then
    if ob.Number=current then
    begin
      ob.Destroy;
      Inc(current);
    end
    else
    begin
      ob.Color:=clRed;
      Inc(mistakes);
      txt.Text:='������: '+IntToStr(mistakes);
    end;
end;

begin
  SetWindowTitle('����: ����� ��� �������� �� �������');
  for i:=1 to 16 do
  begin
    x:=Random(WindowWidth-50);
    y:=Random(WindowHeight-50);
    ob:=RectangleABC.Create(x,y,50,50,clMoneyGreen);
    ob.Number:=i;
  end;
  txt:=TextABC.Create(10,WindowHeight-30,14,clRed,'������: 0');
  current:=1;
  mistakes:=0;
  OnMouseDown:=MyMouseDown;
end.