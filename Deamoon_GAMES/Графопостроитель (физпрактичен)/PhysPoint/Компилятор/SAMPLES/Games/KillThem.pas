uses ABCObjects,Events,GraphABC,Timers,Utils;

var
  kLeftKey,kRightKey: boolean;
  kSpaceKey: integer;
  Player: RectangleABC;
  t: integer;
  EndOfGame: boolean;
  Enemies: TextABC;
  StaticObjectsCount: integer;
  NewGame: TextABC;
  Wins,Falls: integer;
  WinsABC,FallsABC: TextABC;
  r1: RectangleABC;

type
  KeysType=(kLeft,kRight);
  Pulya=class(CircleABC)
    constructor Create(x,y: integer);
    begin
      inherited Create(x,y,5,clRed);
      dx:=0; dy:=-5;
    end;
    procedure Move;
    var j: integer;
    begin
      inherited Move;
      if Top<0 then
        Visible:=False;
      for j:=StaticObjectsCount+1 to ObjectsCount do
        if (Objects[j]<>Self) and Intersect(Objects[j]) then
        begin
          Objects[j].Visible:=False;
          Visible:=False;
        end;
    end;
  end;
  Enemy=class(RectangleABC)
    constructor Create(x,y,w: integer);
    begin
      inherited Create(x,y,w,20,clRandom);
      if Random(2)=0 then
        dx:=5
      else dx:=-5;
      dy:=0;
    end;
    procedure Move;
    begin
      if Random(2)<>0 then Exit;
      if Random(10)=0 then dy:=5;
      if (Left<0) or (Left+Width>WindowWidth) or (Random(30)=0) then
        dx:=-dx;
      inherited Move;
      if dy<>0 then dy:=0;
      if Top>WindowHeight-50 then
        EndOfGame:=True;
    end;
  end;

function NumberOfEnemies: integer;
var i: integer;
begin
  Result:=0;
  for i:=1 to ObjectsCount do
    if Objects[i] is Enemy then
      Inc(Result);
end;

procedure CreateObjects;
var
  i: integer;
  r1: RectangleABC;
begin
  Player:=RectangleABC.Create(280,WindowHeight-30,100,20,clTeal);
  for i:=1 to 100 do
  begin
    r1:=Enemy.Create(Random(WindowWidth-50),40+Random(10),50);
    r1.TextVisible:=True;
    r1.Number:=i;
  end;
end;

procedure DestroyObjects;
var i: integer;
begin
  for i:=ObjectsCount downto StaticObjectsCount+1 do
    Objects[i].Destroy;
end;

procedure MoveObjects;
var i: integer;
begin
  for i:=StaticObjectsCount+2 to ObjectsCount do
    Objects[i].Move;
end;

procedure DestroyKilledObjects;
var i: integer;
begin
  for i:=ObjectsCount downto StaticObjectsCount+2 do
    if not Objects[i].Visible then
      Objects[i].Destroy;
end;

procedure KeyDown(Key: integer);
begin
  case Key of
vk_Left:  kLeftKey:=True;
vk_Right: kRightKey:=True;
vk_Space: if kSpaceKey=2 then kSpaceKey:=1;
  end;
end;

procedure KeyUp(Key: integer);
begin
  case Key of
vk_Left:  kLeftKey:=False;
vk_Right: kRightKey:=False;
vk_Space: kSpaceKey:=2;
  end;
end;

procedure Timer1;
var
  i,j,n: integer;
  p: Pulya;
begin
  if kLeftKey and (Player.Left>0) then
    Player.MoveOn(-10,0);
  if kRightKey and (Player.Left+Player.Width<WindowWidth) then
    Player.MoveOn(10,0);
  if kSpaceKey=1 then
  begin
    p:=Pulya.Create(Player.Left+Player.Width div 2,Player.Top-10);
    kSpaceKey:=0;
  end;
  MoveObjects;
  DestroyKilledObjects;
  RedrawObjects;
  n:=NumberOfEnemies;
  Enemies.Text:='������: '+IntToStr(n);
  if n=0 then
    EndOfGame:=True;
  if EndOfGame then
  begin
    StopTimer(t);
    if n>0 then
    begin
      Inc(Falls);
      FallsABC.Text:='���������: '+IntToStr(Falls);
      RedrawObjects;
      ShowMessage('�� ���������!');
      DestroyObjects;
      Enemies.Text:='������: '+IntToStr(NumberOfEnemies);
      RedrawObjects;
    end
    else
    begin
      Inc(Wins);
      WinsABC.Text:='�����: '+IntToStr(Wins);
      RedrawObjects;
      ShowMessage('�� ��������!');
      DestroyObjects;
      Enemies.Text:='������: '+IntToStr(NumberOfEnemies);
      RedrawObjects;
    end;
  end;
end;

procedure MouseUp(x,y,mb: integer);
begin
  if NewGame.PTInside(x,y) then
    StartTimer(t);
end;

procedure KeyPress(Key: char);
begin
  if ((UpCase(Key)='G') or (UpCase(Key)='�')) and EndOfGame then
  begin
    EndOfGame:=False;
    StartTimer(t);
    CreateObjects;
    kSpaceKey:=2;
    kLeftKey:=False;
    kRightKey:=False;
  end;
end;

begin
  LockDrawingObjects;
  EndOfGame:=True;
  r1:=RectangleABC.Create(0,0,WindowWidth,38,clWhite);
  NewGame:=TextABC.Create(WindowWidth-180,5,14,clBlack,'G - ����� ����');
  Enemies:=TextABC.Create(10,5,14,clRed,'������: '+IntToStr(NumberOfEnemies));
  WinsABC:=TextABC.Create(150,5,14,clRed,'�����: '+IntToStr(Wins));
  FallsABC:=TextABC.Create(280,5,14,clRed,'���������: '+IntToStr(Falls));
  StaticObjectsCount:=ObjectsCount;
  Enemies.Text:='������: '+IntToStr(NumberOfEnemies);
  OnKeyDown:=KeyDown;
  OnKeyPress:=KeyPress;
  OnKeyUp:=KeyUp;
  OnMouseUp:=MouseUp;
  t:=CreateTimer(10,Timer1);
  StopTimer(t);
  RedrawObjects;
end.
