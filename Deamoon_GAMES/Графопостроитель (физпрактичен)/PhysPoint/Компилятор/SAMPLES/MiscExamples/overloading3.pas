// ���������� - ���� 3. ���������� � inherited
type
  a=class
    constructor create(ii: integer);
    begin
      writeln('create(ii: integer)');
    end;
    constructor create;
    begin
      writeln('create');
    end;
  end;
  b=class(a)
    constructor create(ii: integer);
    begin
      inherited create(ii);
    end;
    constructor create;
    begin
      inherited create;
    end;
  end;
  
var
  bb: b;

begin
  cls;
  bb:=b.create;
  bb:=b.create(2);
end.

