var f: text;
begin
  try
    assign(f,'aa.txt');
    reset(f);
  except
    on e: Exception do
      writeln(e.TypeName, ' ', e.Message);
  end
end.