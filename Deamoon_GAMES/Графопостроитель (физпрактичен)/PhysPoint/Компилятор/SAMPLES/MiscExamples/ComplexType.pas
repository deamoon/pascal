// ���, ��� ����� ������ � ������������ �������
const c=(3,4);

var c1,c2: complex;
  
begin
  cls;
  writeln(c,' ',c.Re,' ',c.Im);
  c1:=(1,2); c2:=(3,-1);
  writeln(c1+c2,' ',c1-c2,' ',c1*c2,' ',c1/c2,' ',-c);
  writeln(c,' ',conj(c)); // conj - ����������� ����������
  writeln(abs(c),' ',carg(c));
  writeln(sin(c),' ',cos(c));
  writeln(exp(c),' ',ln(c));
  writeln(sqrt(c)); // ������� ����� �����
end.
