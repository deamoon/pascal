// ���� ���� ��������� ����� �������������� �����
procedure proc;
begin
end;

function fun: integer;
begin
  Result:=1;
end;

type
  MyClass=class end;
  MyClass1=class end;
  MyInheritedClass=class(MyClass) end;
  intervinteger=0..9;
  intervchar='a'..'z';
  enum1=(w1,w2,w3,w4,w5);
  enum2=(z1,z2,z3,z4,z5);
  intervenum=w2..w4;
  pinteger=^integer;
  proctype=procedure;
  functype=function: integer;
  strng=string;
  strng40=string[40];

var
  b: byte;
  i: integer;
  r: real;
  c: complex;
  ii: intervinteger;
  ich: intervchar;
  ch: char;
  s: string;
  s40: string[40];
  e: enum1;
  e2: enum2;
  ie: intervenum;
  pi: pinteger;
  pr: ^real;
  p: pointer;
  cl: MyClass;
  clh: MyInheritedClass;
  pp: procedure;
  fp: function: integer;

begin
  cls;
// byte
  i:=integer(b);
  r:=real(b);
  ch:=char(b);
//  b:=5;
  e:=enum1(b); // ������ �����
  c:=complex(b);
// b:=11;
  ii:=intervinteger(b); // ������ �����
    b:=Ord('a');
  ich:=intervchar(b); // ������ �����
    b:=1;
  ie:=intervenum(b); // ������ �����

// integer
// i:=256;
  b:=byte(i);
  r:=real(i);
  c:=complex(i);
// i:=11;
  ii:=intervinteger(i);
  i:=Ord('a');
  ich:=intervchar(i); // ������ �����
  i:=1;
  ie:=intervenum(i); // ������ �����
  ch:=char(i);
  e:=enum1(i); // ������ �����

// real
  c:=complex(r);
  
// char
  b:=byte(ch); // ������ �����
  i:=integer(ch); // ������ �����
  r:=real(ch); // ������ �����
  c:=complex(ch); // ������ �����
  e:=enum1(ch); // ������ �����
  ch:='a';
  s:=strng(ch); // ������ ������������ ��������� ����� string ��� ���������� ����
  ich:=intervchar(ch);
  ch:=#1;
  ii:=intervinteger(ch); // ������ �����
  ie:=intervenum(ch); // ������ �����

// enum
  i:=integer(e); // ������ �����
  b:=byte(e); // ������ �����
  ch:=char(e); // ������ �����
  e2:=z4;
  e2:=enum2(e); // ������ �����
//  ich:=intervchar(e);
  ii:=intervinteger(e); // ������ �����
  ie:=intervenum(e); // ������ �����
  r:=real(e); // ������ �����
  c:=complex(e); // ������ �����

// interv
  r:=real(ii);
  b:=byte(ii);
  c:=complex(ii);
  ch:=char(ii); // ������ �����
  ie:=intervenum(ii); // ������ �����
//  ich:=intervchar(ii);
  ii:=intervinteger(ii);
  e:=enum1(ii); // ������ �����

// nil
  pi:=pinteger(nil);
  cl:=MyClass(nil);
  pp:=proctype(nil);
  fp:=functype(nil);

// class
  cl:=MyClass(clh);
  clh:=MyInheritedClass(cl); // ������ ����� !
//  cl:=MyClass1(cl);

// procname
  pp:=proctype(proc);

// funcname
  fp:=functype(fun);
  
// string
  s:=s40;
  s40:=s;

// pointer
  p:=pi;
  pi:=pinteger(p); // ������ �����
  pi:=pinteger(pr); // ������ �����
end.

