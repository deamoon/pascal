// ���� �������� in - ��� ���������� ���������� �����
type
  enum=(w1,w2,w3);

var
  b: byte;
  i: integer;
  c: char;
  j: 2..4;
  k: '1'..'3';
  e: enum;
  ei: w1..w2;
  
  s: set of byte;
  s1: set of 3..7;
  s2: set of char;
  s3: set of '2'..'4';
  s4: set of enum;
  s5: set of w2..w3;
begin
  cls;
  writeln(b in s,i in s,j in s);
  writeln(b in s1,i in s1,j in s1);
  writeln(c in s2,k in s2);
  writeln(c in s3,k in s3);
  writeln(c in s3,k in s3);
  writeln(e in s4,ei in s4);
  writeln(e in s5,ei in s5);
end.

