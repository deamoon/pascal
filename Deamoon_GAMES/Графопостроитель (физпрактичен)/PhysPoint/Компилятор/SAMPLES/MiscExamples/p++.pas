// ������� GetMem � FreeMem. �������� � ��������������� ����������� (��� � C)
const n=20;

type
  pinteger=^integer;
  ArrN=^array [1..MaxInt] of integer;
var
  p,pi: ^integer;
  pa: ArrN;
  i: integer;
begin
  cls;
  GetMem(p,n*sizeof(integer));
  pi:=p;
  for i:=1 to n do
  begin
    pi^:=2*i;
    pi:=pi+1;
  end;
  pa:=ArrN(p);
  for i:=1 to n do
    write(pa^[i]:3);
  writeln;
  FreeMem(p);
end.

