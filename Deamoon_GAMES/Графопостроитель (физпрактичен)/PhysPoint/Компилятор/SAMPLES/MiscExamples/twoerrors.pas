type
  A=class
    procedure pp;
    begin
    end;
  end;

procedure q;
type // error
  B=class
    procedure pp;
    begin
    end;
  end;
begin

end;

var
  p: procedure;
  a1: A;

begin
  p:=a1.pp; // error
end.
