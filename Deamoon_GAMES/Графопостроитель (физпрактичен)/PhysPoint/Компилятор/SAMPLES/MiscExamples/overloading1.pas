// ���������� - ���� 1 (� Delphi - ��� ���������� ������������ �������� ����� overload)
type
  A=class
    constructor Create;
    begin
      write('c0');
    end;
    constructor Create(i: integer);
    begin
      write('c1');
    end;
    procedure q;
    begin
      write('q0');
    end;
    procedure q(i: integer); // ���������� � ������
    begin
      write('q1');
    end;
//    function q(i: real): integer; begin end; // ���������������! ������� �� ����� ����������� ��������� � ��������
  end;

procedure p;
begin
  write(0);
end;

procedure p(i: integer);
begin
  write(1);
end;

procedure p(i: real);
begin
  write(2);
end;

procedure p2(i: integer; r: real);
begin
  write('ir');
end;

procedure p2(i: real; r: integer);
begin
  write('ri');
end;

var a1: A;

begin
  cls;
  p;
  p(1);
  p(1.0);
  a1:=A.Create;
  a1:=A.Create(1);
  a1.q(1);
  a1.q;
//  p2(1,2); // ���������������! ��������������� ������!
end.
