type
  A=class
  private
    i: integer;
    procedure r;
    begin
      write(1);
    end;
  protected
    procedure p;
    begin
      write(2);
    end;
  public
    procedure q;
    begin
      write(3);
    end;
  end;
  B=class(A)
  private
    j: integer;
    procedure r1;
    begin
      Self.p; // ��, � Pascal ABC ���� p ����������, ���� ����� Self!
      write(4);
    end;
  protected
    procedure p1;
    begin
//      r;
      write(5);
    end;
  public
    procedure q1;
    begin
      inherited q;
      write(6);
    end;
  end;
  
var
  a1: A;
  b1: B;
  
begin
  a1:=A.Create;
  b1:=B.Create;
//  a1.p;
  a1.q;
//  a1.r;
//  b1.p;
  b1.q;
//  b1.r;
end.
