// ���� ������� � ��������� �������
type
  A=class
    aa: real;
    bb: array[1..10] of real;
    procedure SetB(ind: integer; v: real);
    begin
      bb[ind]:=v;
    end;
    function GetB(ind: integer): real;
    begin
      Result:=bb[ind];
    end;
    procedure SetA(v: real);
    begin
      aa:=v;
    end;
    function GetA: real;
    begin
      Result:=aa;
    end;
    property B[i: integer]: real read GetB write SetB;
    procedure p;
    begin
      b[3]:=b[2]+1;
    end;
    property A: real read GetA write SetA;
  end;

var a1: A;

begin
  cls;
  a1:=A.Create;
  a1[2]:=44;
  writeln(a1[2]);
  a1.p;
  writeln(a1[3]);
  a1.A:=3.5;
  writeln(a1.A);
end.
