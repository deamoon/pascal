{procedure q;
begin
end;}

procedure q(i: integer);
begin
  write(3);
end;

procedure Create(r: real);
begin
  write(r);
end;

type
  A=class
    constructor Create(s: string);
    begin
      write(6);
    end;
    constructor Create(i: integer);
    begin
      Create(2.1);
    end;
    procedure p;
    begin
      write(1);
    end;
    procedure q(s: string);
    begin
      write(1);
    end;
    procedure p(s: string);
    begin
      write(2);
//      q(2.0);
    end;
  end;

var a1: A;

begin
  cls;
//  p(2.1);
  a1:=A.Create(2);
//  a1.Create('dfh');
//  a1.f('1');
//  a1.q(1)
end.
