// ���� ���� ��������� ������� �������������� �����
procedure proc;
begin
end;

function fun: integer;
begin
  Result:=1;
end;

type
  MyClass=class end;
  MyInheritedClass=class(MyClass) end;

var
  b: byte;
  i: integer;
  r: real;
  c: complex;
  ii: 0..10;
  ich: 'a'..'z';
  ch: char;
  s: string;
  s40: string[40];
  e: (w1,w2,w3,w4,w5);
  ie: w2..w4;
  pi: ^integer;
  cl: MyClass;
  clh: MyInheritedClass;
  pp: procedure;
  fp: function: integer;
  p: pointer;

begin
// byte
  i:=b;
  r:=b;
  c:=b;
// b:=11;
  ii:=b;

// integer
// i:=256;
  b:=i;
  r:=i;
  c:=i;
// i:=11;
  ii:=i;

// real
  c:=r;
  
// char
  ch:='a';
  s:=ch;
  ich:=ch;
  
// enum
  e:=w4;
  ie:=e;
  
// interv
  r:=ii;
  b:=ii;
  c:=ii;
  s:=ich;
  e:=ie;
  
// nil
  p:=nil;
  pi:=nil;
  cl:=nil;
  pp:=nil;
  fp:=nil;

// class
  cl:=clh;

// procname
  pp:=proc;

// funcname
  fp:=fun;
  
// string
  s:=s40;
  s40:=s;
  
// pointer
  p:=pi;
//  pi:=p;
end.

