// ���� ������������� �������
type
  A=class
  private
    i: integer;
  protected
    j: integer;
  public
    k: integer;
    procedure p;
    begin
      i:=2;
    end;
  end;

  B=class(A)
    procedure q;
    begin
//      i:=3; // ���� i ����������
      j:=3;
    end;
  end;

var
  a1: B;


begin
  a1:=B.Create;
// writeln(a1.i); // - ������ - ���� i ����������
// writeln(a1.j); // - ������ - ���� j ����������
  writeln(a1.k);
end.


