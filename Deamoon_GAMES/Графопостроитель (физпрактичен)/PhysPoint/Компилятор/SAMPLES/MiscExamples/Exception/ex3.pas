// ����������� ������ exit � ������ except/on

type
  MyEx=class(Exception)
    destructor Destroy;
    begin
      write(666,' ');
    end;
  end;

procedure p;
var i: integer;
begin
  for i:=1 to 5 do
  begin
    try
      raise MyEx.Create('��-��!');
    except
      on d: MyEx do
      begin
        write(1);
        if i=3 then
          exit;
        write(2);
      end;
    end;
  end;
end;

procedure p1;
begin
  write(3);
  exit;
  write(3);
end;


begin
  cls;
  p;
  p1();
  writeln('end');
end.

