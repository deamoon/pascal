// ����������� ������ exception �� ���������� �������

procedure p;
begin
  try
    raise EZeroDivide.Create;
  except
    on e: EOutOfMemory do // ����� �� ������������
      write(1);
  end;
end;

begin
  try
    p;
  except
    on e: EZeroDivide do // � ����� ������������ !
      write(2);
  end
end.
