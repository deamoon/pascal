// �������� ����� for - ��� ���������� ���������� �����
var
  i: integer;
  c: char;
  b: byte;
  it: 2..6;
  en: (w1,w2,w3,w4);
  itc: 'a'..'z';
  ite: w2..w3;

begin
  cls;
  for b:=2 to 5 do
    write(b);
  for c:='7' to '9' do
    write(c);
  writeln;
  for it:=6 downto 2 do
    write(it);
  for en:=w1 to w4 do
    write('e');
  writeln;
  for itc:='a' to 'k' do
    write(itc);
  writeln;
  for ite:=w2 to w3 do
    write('e');
end.

