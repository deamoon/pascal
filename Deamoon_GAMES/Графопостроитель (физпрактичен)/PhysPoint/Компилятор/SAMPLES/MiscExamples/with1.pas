uses PointRect;

type
  A=class
    i,j,x: integer;
    procedure p;
    begin
      Inc(i);
    end;
  end;

var
  pp: Point;
  r: Rect;
  a1: A;
  q: array [1..2] of Point;
  
procedure pr(var b: A);
begin
  with b do
    Inc(i);
end;

begin
  cls;
  pp:=PointF(2,3);
  a1:=A.Create;
  
  with pp,a1 do
  begin
    x:=x+2;
    Inc(a1.i);
    i:=i+1;
    j:=j+1;
    pr(a1);
    p;
  end;
  write(a1.i,' ',pp.x);
end.

