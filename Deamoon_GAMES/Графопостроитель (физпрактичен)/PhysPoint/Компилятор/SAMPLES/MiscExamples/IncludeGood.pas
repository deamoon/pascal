type
  enum = (a,b,c);
  dyapenum = a..b;

var
  s1: set of byte;
  s2: set of char;
  s3: set of enum;
  s4: set of 1..4;
  s5: set of 'a'..'z';
  s6: set of dyapenum;
  de: dyapenum;

begin
  Include(s1,1);
  Include(s2,'1');
  Include(s3,a);
  Include(s3,de);
  Include(s4,2);
  Include(s5,'2');
  Include(s6,c);
  Exclude(s1,1);
  Exclude(s2,'1');
  Exclude(s3,a);
  Include(s3,de);
  Exclude(s4,2);
  Exclude(s5,'2');
  Exclude(s6,c);
end.

