// ������������ �������, ������������ ��������� ����
type
  Point=record
    x,y:integer;
  end;
  A=class
    i: integer;
    function g: integer;
    begin
      g:=2;
    end;
    procedure p(r: real);
    begin
      write(r);
    end;
  end;

function PointF(x,y: integer): Point;
begin
  Result.x:=x;
  Result.y:=y;
end;

function f(i: integer): Point;
begin
  Result:=PointF(i,i);
end;

function st: string;
begin
  st:='Hello!';
end;

function co: complex;
begin
  co:=(4,5);
end;

var
  a1: A;
  i,j: integer;
  pp: ^integer;
  arr: array[1..10] of integer;
  
function ra: array[1..10] of integer;
begin
  Result:=arr;
end;
  
function g(j: integer): ^integer;
begin
  Result:=@i;
end;

function g1: ^integer;
begin
  Result:=@i;
end;

function cl: Object;
begin
  Result:=A.Create;
end;

procedure pf(var i: integer);
begin
  i:=i+1;
end;

function aa(i,j: integer): record r,i: real end;
begin
  Result.r:=i;
  Result.i:=j;
end;

begin
  cls;
  i:=3;
  Inc(i,5);
  writeln(i);
  g(2)^:=5;
  g1^:=7;
  pf(g1^);
  writeln(i);
  arr[2]:=5;
  writeln(f(3).x,' ',ra[2],' ',st[2]);
  writeln(co.re);
  writeln(cl.TypeName);
  writeln(aa(5,6).r,' ',aa(5,6).i);
end.
