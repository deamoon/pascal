// �� ���� �� Includ�� �� ������ ��������������� - ��� ���������!
type
  enum = (a,b,c);
  enum1 = (d,e,f);
  dyapenum = a..b;

var
  s1: set of byte;
  s2: set of char;
  s3: set of enum;
  s4: set of 1..4;
  s5: set of 'a'..'z';
  s6: set of dyapenum;

begin
//  Include(s1,'1');
//  Include(s2,1);
//  Include(s3,1);
//  Include(s3,d);
//  Include(s4,'2');
//  Include(s5,e);
  Include(s6,d);
end.

