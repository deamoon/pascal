function a(i,j: integer): record r,i: real end;
begin
  Result.r:=i;
  Result.i:=j;
end;

begin
  cls;
  writeln(a(5,6).r,' ',a(5,6).i);
end.

