type
  D=class
    x,y: real;
    procedure p(i: integer);
    begin
      write(i);
    end;
  end;

var
  a: record
    r,i: integer;
  end;
  b,i: real;
  c: complex;
  d1: D;

begin
  cls;
  d1:=D.Create;
  with a,d1 do
  begin
    y:=4;
    i:=1;
    b:=2;
  end;
  writeln(a.i,b);
  with d1 do
  begin
    x:=3;
    p(5);
  end;
  writeln(d1.x,d1.y);
  with c do
    re:=7;
  writeln(c.re);
end.
