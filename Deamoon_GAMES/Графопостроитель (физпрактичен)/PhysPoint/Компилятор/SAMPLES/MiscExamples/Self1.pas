type
  A=class
    k: integer;
    constructor Create(k: integer);
    begin
      Self.k:=k;
    end;
    destructor Destroy;
    begin
      Self.k:=0;
      writeln(Self.k);
    end;
    function f: integer;
    var r: real;
    begin
      Result:=Self.k;
    end;
    procedure p;
    var r: real;
    begin
      k:=2;
      Self.k:=3;
    end;
  end;

var a1: A;

begin
  cls;
  a1:=A.Create(5);
//  a1.p;
  writeln(a1.f);
  a1.Destroy;
end.
