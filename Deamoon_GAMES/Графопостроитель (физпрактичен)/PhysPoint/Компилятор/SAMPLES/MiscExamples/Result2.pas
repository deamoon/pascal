type A=class
  i: integer;
  procedure p;
  var i: integer;
  begin
    Self.i:=2;
  end;
  function f: integer;
  var j: integer;
  begin
    Result:=5
  end;
end;

  function f1: record r,i:integer end;
  var j: integer;
  begin
    Result.r:=78
  end;

var
  a1: A;
  r: record r,i:integer end;
begin
  cls;
  a1:=A.Create;
  a1.p;
//  write(a1.f,a1.i);
  
  r:=f1;
  write(r.r);
end.
