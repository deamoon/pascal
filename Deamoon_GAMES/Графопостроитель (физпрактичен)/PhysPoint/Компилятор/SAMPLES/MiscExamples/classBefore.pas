// ���� ������������ ������
type
  A=class;
  B=class
    h: integer;
    procedure f;
    begin
      write(h);
    end;
  end;
  A=class(B)
    g: integer;
    procedure q;
    begin
      write(g);
    end;
  end;
  C=class
    h: integer;
    procedure f;
    begin
      write(h);
    end;
  end;


var
  y: B;
  x: A;
  z: C;

begin
  cls;
  y:=B.Create;
//  y.f;
  x:=A.Create;
  x.h:=9;
  x.g:=3;
  write(x.h);
  x.q;
  y.h:=2;
  y.f;
  z:=C.Create;
  z.h:=7;
  z.f;
end.
