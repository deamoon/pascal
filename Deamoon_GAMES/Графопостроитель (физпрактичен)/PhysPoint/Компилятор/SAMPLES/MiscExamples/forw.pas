// ������������ forward-����������
procedure f(i: integer); forward;

procedure f;
begin
end;

procedure f(i: integer);
// forward-���������� ��� ��������� ����������� ���������
// procedure g(i: integer); forward; // - ������!
// procedure l(i: integer); forward; // - ������!
 procedure g(i: integer);
 begin
   write(i);
 end;
 procedure l(i: integer);
 begin
   write(i);
 end;
begin
  g(i); l(i);
end;

begin
  cls;
  f(7);
end.
