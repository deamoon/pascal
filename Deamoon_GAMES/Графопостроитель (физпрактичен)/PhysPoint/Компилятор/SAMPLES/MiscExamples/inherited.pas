// ���� inherited. inherited Create - ������� (�����������) ���������� ��� ���������
type
  A=class
    constructor Create;
    begin
      writeln('constructor A');
    end;
    destructor Destroy;
    begin
      writeln('destructor A');
    end;
    procedure p;
    begin
      write('A');
    end;
    function f: integer;
    begin
      f:=5;
    end;
  end;
  B=class(A)
    constructor Create;
    begin
      inherited Create;
      writeln('constructor B');
    end;
    destructor Destroy;
    begin
      writeln('destructor B');
      inherited Destroy;
    end;
    procedure p;
    begin
      write('B');
      inherited p;
    end;
    function f: integer;
    begin
      f:=inherited f + 1
    end;
  end;

var b1: B;

begin
  cls;
  b1:=B.Create;
  writeln(b1.f);
  b1.Destroy;
end.

