unit Field3;

interface

uses GraphABC;

const
  StandardDelay=30;
  MaxDim=40;

procedure Pause;

type
  Field=class
    DimX,DimY,sz,x0,y0: integer;
    constructor Create(dx,dy,ssz,x,y: integer);
    procedure Draw;
    procedure DrawCircle(x,y,zaz,color: integer);
    procedure DrawRect(x,y,zaz,color: integer);
    procedure ClearCell(x,y: integer);
    procedure SetDim(dx,dy,ssz: integer);
    function CellIsFree(x,y: integer): boolean;
    function CellIsOccupied(x,y: integer): boolean;
    function CellInField(x,y: integer): boolean;
  end;
  
  Ball=class; // !! �������� ������ ���� � ��� �� ��������� type
  FieldWithBalls=class(Field)
  private
    A: array [1..MaxDim,1..MaxDim] of Ball;
  public
    procedure ClearBallArray;
    constructor Create(dx,dy,ssz,x,y: integer);
    procedure DrawObjects;
    procedure Draw;
    procedure AddBall(B: Ball; x,y: integer);
    function CellIsFree(x,y: integer): boolean;
    procedure MoveObject(x,y,x1,y1: integer);
    procedure DeleteObject(x,y: integer);
    procedure Go;
  end;
  
  Ball=class
  protected
    x,y,color: integer;
    F: FieldWithBalls;
    dx,dy: integer;
    goflag: boolean;
  public
    constructor Create(ddx,ddy,c: integer; F: FieldWithBalls);
    constructor Create(cc: integer; FF: FieldWithBalls);
    procedure Draw;
    procedure Hide;
    procedure SetCoords(xx,yy: integer);
    procedure MoveTo(xx,yy: integer);
    procedure MoveOn(dx,dy: integer);
    function CellIsFree(dx,dy: integer): boolean;
    function CellIsOccupied(dx,dy: integer): boolean;
    procedure Reset;
    procedure Go;
  end;

  BrownBall=class(Ball)
    constructor Create(c: integer; F: FieldWithBalls);
    procedure Go;
    procedure Draw;
  end;

  BrownEatingBall=class(BrownBall)
    procedure Go;
    procedure Draw;
  end;

  Pulya=class(Ball)
    procedure Go;
    procedure Draw;
  end;

  ShootingBall=class(BrownBall)
  private
    period: integer;
  public
    procedure Go;
    procedure Draw;
  end;

implementation

procedure Pause;
begin
  Sleep(StandardDelay);
end;

// Field
constructor Field.Create(dx,dy,ssz,x,y: integer);
begin
  DimX:=dx; DimY:=dy; sz:=ssz; x0:=x; y0:=y;
end;

procedure Field.Draw;
var cx,cy,w,h: integer;
begin
  SetPenColor(clBlack);
  w:=sz*DimX;
  h:=sz*DimY;
  for cy:=0 to DimY do
    line(x0,y0+cy*sz,x0+w+1,y0+cy*sz);
  for cx:=0 to DimX do
    line(x0+cx*sz,y0,x0+cx*sz,y0+h);
end;

procedure Field.DrawCircle(x,y,zaz,color: integer);
begin
  SetBrushColor(color);
  Ellipse(x0+(x-1)*sz+zaz,y0+(y-1)*sz+zaz,x0+x*sz-zaz+1,y0+y*sz-zaz+1);
end;

procedure Field.DrawRect(x,y,zaz,color: integer);
begin
  SetBrushColor(color);
  Rectangle(x0+(x-1)*sz+zaz,y0+(y-1)*sz+zaz,x0+x*sz-zaz+1,y0+y*sz-zaz+1);
end;

procedure Field.ClearCell(x,y: integer);
begin
  SetBrushColor(clWhite);
  FillRect(x0+(x-1)*sz+1,y0+(y-1)*sz+1,x0+x*sz-1+1,y0+y*sz-1+1);
end;

procedure Field.SetDim(dx,dy,ssz: integer);
begin
  DimX:=dx; DimY:=dy; sz:=ssz;
end;

function Field.CellIsFree(x,y: integer): boolean;
begin
  Result:=True; // ���� ���� ������� ��� ���� ������ ����������
end;

function Field.CellIsOccupied(x,y: integer): boolean;
begin
  Result:=not CellIsFree(x,y);
end;

function Field.CellInField(x,y: integer): boolean;
begin
  Result:=(x>=1) and (y>=1) and (x<=DimX) and (y<=DimY)
end;

// FieldWithBalls
procedure FieldWithBalls.ClearBallArray;
var ix,iy: integer;
begin
  for ix:=1 to DimX do
  for iy:=1 to DimY do
    A[ix,iy]:=nil;
end;

constructor FieldWithBalls.Create(dx,dy,ssz,x,y: integer);
begin
  inherited Create(dx,dy,ssz,x,y);
  ClearBallArray;
end;

procedure FieldWithBalls.DrawObjects;
var ix,iy: integer;
begin
  for ix:=1 to DimX do
  for iy:=1 to DimY do
    if A[ix,iy]<>nil then A[ix,iy].Draw;
end;

procedure FieldWithBalls.Draw;
begin
  inherited Draw;
  DrawObjects;
end;

procedure FieldWithBalls.AddBall(B: Ball; x,y: integer);
begin
  if not CellIsFree(x,y) then Exit;
  A[x,y]:=B;
  B.SetCoords(x,y);
  B.Draw;
end;

function FieldWithBalls.CellIsFree(x,y: integer): boolean;
begin
  Result:=CellInField(x,y) and (A[x,y]=nil)
end;

procedure FieldWithBalls.MoveObject(x,y,x1,y1: integer);
begin
  if CellIsFree(x,y) then exit;
  if not CellIsFree(x1,y1) then exit;
  A[x,y].Hide;
  A[x1,y1]:=A[x,y];
  A[x,y]:=nil;
  A[x1,y1].SetCoords(x1,y1);
  A[x1,y1].Draw;
end;

procedure FieldWithBalls.DeleteObject(x,y: integer);
begin
  if A[x,y]=nil then exit;
  A[x,y].Destroy;
  A[x,y]:=nil;
  ClearCell(x,y);
end;

procedure FieldWithBalls.Go;
var ix,iy:integer;
begin
  for ix:=1 to DimX do
  for iy:=1 to DimY do
    if A[ix,iy]<>nil then
      A[ix,iy].Reset;
  for ix:=1 to DimX do
  for iy:=1 to DimY do
    if A[ix,iy]<>nil then
      A[ix,iy].Go;
end;
    
// Ball
constructor Ball.Create(ddx,ddy,c: integer; F: FieldWithBalls);
begin
  Create(c,F);
  goflag:=false;
  dx:=ddx; dy:=ddy;
end;

constructor Ball.Create(cc: integer; FF: FieldWithBalls);
begin
  x:=1; y:=1;
  color:=cc;
  F:=FF;
end;

procedure Ball.Draw;
begin
  F.DrawCircle(x,y,3,color);
end;

procedure Ball.Hide;
begin
  F.ClearCell(x,y);
end;

procedure Ball.SetCoords(xx,yy: integer);
begin
  x:=xx;
  y:=yy;
end;

procedure Ball.MoveTo(xx,yy: integer);
begin
  F.MoveObject(x,y,xx,yy)
end;

procedure Ball.MoveOn(dx,dy: integer);
begin
  MoveTo(x+dx,y+dy)
end;

function Ball.CellIsFree(dx,dy: integer): boolean;
begin
  Result:=F.CellIsFree(x+dx,y+dy);
end;

function Ball.CellIsOccupied(dx,dy: integer): boolean;
begin
  Result:=F.CellIsOccupied(x+dx,y+dy);
end;

procedure Ball.Reset;
begin
  goflag:=false;
end;

procedure Ball.Go;
begin
  if goflag=true then exit;
  if (CellIsOccupied(dx,0) and CellIsOccupied(0,dy)) or (CellIsFree(dx,0) and CellIsFree(0,dy) and CellIsOccupied(dx,dy)) then
  begin
    dx:=-dx;
    dy:=-dy;
  end
  else if (dx<>0) and CellIsOccupied(dx,0) then
    dx:=-dx
  else if (dy<>0) and CellIsOccupied(0,dy) then
    dy:=-dy;
  MoveOn(dx,dy);
  goflag:=True;
end;

// BrownBall
constructor BrownBall.Create(c: integer; F: FieldWithBalls);
begin
  inherited Create(0,0,c,F);
end;

procedure BrownBall.Go;
begin
  if goflag=true then exit;
  dx:=Random(3)-1;
  dy:=Random(3)-1;
  MoveOn(dx,dy);
  goflag:=True;
end;
    
procedure BrownBall.Draw;
begin
  F.DrawCircle(x,y,4,color);
end;

// BrownEatingBall
procedure BrownEatingBall.Go;
var FF: FieldWithBalls;
begin
  FF:=FieldWithBalls(F);
  if goflag=true then exit;
  repeat
    dx:=Random(3)-1;
    dy:=Random(3)-1;
  until (dx<>0) or (dy<>0);
  if FF.CellInField(x+dx,y+dy) and CellIsOccupied(dx,dy) then
    FF.DeleteObject(x+dx,y+dy);
  MoveOn(dx,dy);
  goflag:=True;
end;
    
procedure BrownEatingBall.Draw;
begin
  F.DrawRect(x,y,3,color);
end;

// Pulya
procedure Pulya.Go;
var FF: FieldWithBalls;
begin
  FF:=FieldWithBalls(F);
  if goflag=true then exit;
  if FF.CellInField(x+dx,y+dy) and CellIsOccupied(dx,dy) then
  begin
    FF.DeleteObject(x+dx,y+dy);
    FF.DeleteObject(x,y);
    exit;
  end;
  if not FF.CellInField(x+dx,y+dy) then
  begin
    FF.DeleteObject(x,y);
    exit;
  end;
  MoveOn(dx,dy);
  goflag:=True;
end;
    
procedure Pulya.Draw;
begin
  F.DrawCircle(x,y,6,color);
end;
    
// ShootingBall
procedure ShootingBall.Go;
var FF: FieldWithBalls;
begin
  FF:=FieldWithBalls(F);
  if goflag=true then exit;
  repeat
    dx:=Random(3)-1;
    dy:=Random(3)-1;
  until (dx<>0) or (dy<>0);
  Inc(period);
  if period>10 then
  begin
    period:=0;
    if CellIsFree(1,0) then FF.AddBall(Pulya.Create(1,0,clBlack,F),x+1,y);
    if CellIsFree(-1,0) then FF.AddBall(Pulya.Create(-1,0,clBlack,F),x-1,y);
    if CellIsFree(0,1) then FF.AddBall(Pulya.Create(0,1,clBlack,F),x,y+1);
    if CellIsFree(0,-1) then FF.AddBall(Pulya.Create(0,-1,clBlack,F),x,y-1);
  end else
    MoveOn(dx,dy);
  goflag:=True;
end;
    
procedure ShootingBall.Draw;
begin
  F.DrawCircle(x,y,0,color);
end;

end.
