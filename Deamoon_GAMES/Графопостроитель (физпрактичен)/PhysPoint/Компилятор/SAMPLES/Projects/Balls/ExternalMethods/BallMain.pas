uses GraphABC,FieldBall,Timers,Events,Utils;

var
  F: FieldWithBalls;
  t: integer;
  
procedure MouseDown(x,y,mb: integer);
var fx,fy,dx,dy: integer;
begin
  if (x<F.x0) or (y<F.y0) then exit;
  fx:=(x-F.x0) div F.sz + 1;
  fy:=(y-F.y0) div F.sz + 1;
  if (fx>F.DimX) or (fy>F.DimY) then exit;
  repeat
    dx:=Random(3)-1;
    dy:=Random(3)-1;
  until (dx<>0) or (dy<>0);
  if F.CellIsFree(fx,fy) then
    F.AddBall(Ball.Create(dx,dy,RGB(Random(256),Random(256),Random(256)),F),fx,fy)
  else
    F.DeleteObject(fx,fy);
end;
  
procedure OnTimer;
begin
  F.Go;
end;

  
begin
  cls;
  SetWindowCaption('������');
  OnMouseDown:=MouseDown;
  F:=FieldWithBalls.Create(30,20,22,20,20);
  SetWindowSize(F.DimX*F.sz+2*F.x0,F.DimY*F.sz+2*F.y0);
  F.Draw;
  F.AddBall(BrownBall.Create(RGB(Random(256),Random(256),Random(256)),F),15,5);
  F.AddBall(BrownEatingBall.Create(RGB(Random(256),Random(256),Random(256)),F),12,12);
  F.AddBall(Pulya.Create(1,0,clBlack,F),7,3);
  F.AddBall(ShootingBall.Create(clGreen,F),10,8);
  t:=CreateTimer(70,OnTimer);
end.
