uses ABCButtons,GraphABC,Sounds,Events,Containers,Utils;

var path: string;

procedure FillFileNameSizeArrays(Names: StringArray);
var sr: SearchRec;
begin
  if FindFirstFile(path+'*.*',sr) then
  begin
    if Uppercase(ExtractFileExt(sr.Name))='.WAV' then
      Names.Add(sr.Name);
    while FindNext(sr) do
      if Uppercase(ExtractFileExt(sr.Name))='.WAV' then
        Names.Add(sr.Name);
  end;
  FindClose(sr);
end;

procedure ButtonClick(Sender: ButtonABC);
var s: Sound;
begin
  Sender.Color:=clYellow;
  s:=Sound.Create(Sender.Text);
  s.Play;
  while s.IsPlaying do
    Sleep(10);
  s.Destroy;
  Sender.Color:=clLtGray;
end;

var Names: StringArray;
    i,x,y: integer;
    b: ButtonABC;
begin
  SetWindowCaption('����������� �����');
  CenterWindow;
  path:=PascalABCPath+'Media\Sounds\';
  Names:=StringArray.Create;
  FillFileNameSizeArrays(Names);
  Names.Sort;
  SetWindowSize(550,((Names.Count+1) div 2)*30+40);

  x:=50; y:=20;
  for i:=1 to Names.Count do
  begin
    if (i>(Names.Count+1) div 2) and (x=50) then
    begin
      x:=300;
      y:=20;
    end;
    b:=ButtonABC.Create(x,y,200,25,Names[i],clLtGray);
    b.OnClickExt:=ButtonClick;
    y:=y+30;
  end
end.