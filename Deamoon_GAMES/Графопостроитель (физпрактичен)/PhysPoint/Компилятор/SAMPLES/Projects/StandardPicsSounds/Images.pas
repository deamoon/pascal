// ������������ ����������� ������� (�������� ����� �� ��������� Open Office)
// ��� ��������� ��������� �������

uses Utils,GraphABC,Events,Containers,ABCObjects;

var path: string;

procedure FillFileNameSizeArrays(Names: StringArray; Sizes: IntArray);
var
  sr: SearchRec;
  ext: string;
  
procedure AddCurrentNameSize;
begin
  ext:=Uppercase(ExtractFileExt(sr.Name));
  if (ext='.JPG') or (ext='.GIF') or (ext='.PNG') or (ext='.BMP') then
  begin
    Names.Add(sr.Name);
    Sizes.Add(sr.Size);
  end;
end;

begin
  if FindFirstFile(path+'*.*',sr) then
  begin
    AddCurrentNameSize;
    while FindNext(sr) do
      AddCurrentNameSize;
  end;
  FindClose(sr);
end;

var
  Names: StringArray;
  Sizes: IntArray;
  ind: integer;
  r: RectangleABC;

procedure OutputTexture(Name: string; Size: integer);
var n: integer;
begin
//  n:=LoadPicture(path+Name);
//  DrawPicture(n,0,0);
  FillWindow(path+Name);
  r.Text:=IntToStr(ind)+'('+IntToStr(Names.Count)+'): '+Name+'  '+IntToStr(Size)+' ����';
end;

procedure MyKeyDown(key: integer);
var
  b: boolean;
  oldind: integer;
begin
  oldind:=ind;
  case key of
vk_Left,vk_Up,vk_PageUp: if ind>1 then Dec(ind);
vk_Right,vk_Down,vk_PageDown: if ind<Names.Count then Inc(ind);
vk_Home: ind:=1;
vk_End: ind:=Names.Count;
vk_Delete:
  begin
    b:=DeleteFile(path+Names[ind]);
    Names.Delete(ind);
    Sizes.Delete(ind);
    if ind>Names.Count then ind:=Names.Count;
    OutputTexture(Names[ind],Sizes[ind]);
  end
  end;
  if (oldind<>ind) and (ind>0) then
    OutputTexture(Names[ind],Sizes[ind]);
end;

procedure MyMouseDown(x,y,mb: integer);
begin
  if mb=1 then MyKeyDown(vk_Down)
  else if mb=2 then MyKeyDown(vk_Up)
end;

procedure MyResize;
begin
  OutputTexture(Names[ind],Sizes[ind]);
  r.MoveTo(30,WindowHeight-70);
  r.Width:=WindowWidth-60;
end;

begin
  SetWindowCaption('����������� ��������');
  SetWindowSize(800,600);
  CenterWindow;
  path:=PascalABCPath+'Media\Images\';
  Names:=StringArray.Create;
  Sizes:=IntArray.Create;
  ind:=1;
  FillFileNameSizeArrays(Names,Sizes);
  r:=RectangleABC.Create(30,WindowHeight-70,WindowWidth-60,40,clWhite);
  r.TextScale:=0.6;
  
  if Names.Count=0 then
  begin
    r.Text:='�������� �����������';
    Halt;
  end;
  OutputTexture(Names[1],Sizes[1]);
  OnKeyDown:=MyKeyDown;
  OnMouseDown:=MyMouseDown;
  OnResize:=MyResize;
end.


