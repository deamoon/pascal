type
  PNode=^TNode;
  TNode=record
    s: string;
    left,right: PNode;
  end;

var
  f: text;
  p,p1,p2,root: PNode;
  x: integer;
  s,q: string;

procedure InitRoot;
begin
  New(root);
  root^.s:='������';
  root^.left:=nil;
  root^.right:=nil;
end;

procedure SaveToFile(p: PNode);
begin
  if p=nil then
  begin
    writeln(f,'');
    exit
  end;
  writeln(f,p^.s);
  SaveToFile(p^.left);
  SaveToFile(p^.right);
end;

function LoadFromFile: PNode;
var
  s: string;
  p: PNode;
begin
  readln(f,s);
  if s='' then
  begin
    Result:=nil;
    exit
  end;
  New(p);
  p^.s:=s;
  p^.left:=LoadFromFile;
  p^.right:=LoadFromFile;
  Result:=p;
end;

begin
  cls;
  writeln('��������� ��������');
  assign(f,'data.txt');
  if not FileExists('data.txt') then
    InitRoot
  else
  begin
    reset(f);
    root:=loadfromfile;
    close(f);
  end;
  
  p:=root;
  while p^.left<>nil do
  begin
    write(p^.s+'? ');
    readln(x);
    if x=1 then p:=p^.left
    else p:=p^.right
  end;
  
  write('��� '+p^.s+'? ');
  readln(x);
  if x=1 then
    writeln('� �������!')
  else
  begin
    write('� ���������. ��� ��� �� ��������? ');
    readln(s);
    write('������� ������, ���������� ��� �������� �� '+p^.s+': ');
    readln(q);
    New(p1);
    New(p2);
    p1^.s:=s;
    p1^.left:=nil;
    p1^.right:=nil;
    p2^.s:=p^.s;
    p2^.left:=nil;
    p2^.right:=nil;
    p^.s:=q;
    p^.left:=p1;
    p^.right:=p2;
  end;
  rewrite(f);
  SaveToFile(root);
  close(f);
end.
