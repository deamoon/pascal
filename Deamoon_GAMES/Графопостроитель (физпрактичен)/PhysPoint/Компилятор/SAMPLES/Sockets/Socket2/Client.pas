uses Sockets;

const delay=200;

var
  sock: integer;
  b: boolean;
  
procedure SendCommands(s: string);
var i: integer;
begin
  for i:=1 to Length(s) do
  begin
    Sleep(delay);
    case s[i] of
      'L': SendString(sock,'Left');
      'R': SendString(sock,'Right');
      'U': SendString(sock,'Up');
      'D': SendString(sock,'Down');
      'P': SendString(sock,'Paint');
      'E': SendString(sock,'End');
    end;
  end;
end;

begin
  sock:=CreateClientSocket;
  b:=Connect(sock,'127.0.0.1',1024);
  if not b then
  begin
    writeln('���������� ����������� ��������');
    exit;
  end;
  SendCommands('RRRRULPLPLPLPLE');
  CloseSocket(sock);
end.
