uses RBZad,uRobot,Sockets;
var
  s: string;
  sock,clsock: integer;
begin
  cls;
  c1;
  Speed(5);
  
  sock:=CreateServerSocket(1024);
  clsock:=Accept(sock);
  writeln('Connected');
  while s<>'End' do
  begin
    Sleep(100);
    ReceiveString(clsock,s);
    if s='Up' then Up;
    if s='Right' then Right;
    if s='Down' then Down;
    if s='Left' then Left;
    if s='Paint' then Paint;
    if s<>'' then
      write(s+' ');
  end;
  CloseSocket(sock);
  CloseSocket(clsock);
end.
