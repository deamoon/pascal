uses ABCObjects;

var
  jadro: CircleABC;
  r: RectangleABC;
  i: integer;

procedure CheckPulyaIntersects;
var i: integer;
begin
  for i:=ObjectsCount-1 downto 1 do
    if jadro.Intersect(Objects[i]) then
      Objects[i].Destroy;
end;

begin
  for i:=1 to 300 do
    r:=CreateRectangleABC(Random(WindowWidth-200)+100,Random(WindowHeight-100),Random(200),Random(200),clRandom);
  jadro:=CircleABC.Create(10,WindowHeight div 2,100,clBlack);
  for i:=1 to 700 do
  begin
    jadro.MoveOn(1,0);
    CheckPulyaIntersects;
  end;
end.
