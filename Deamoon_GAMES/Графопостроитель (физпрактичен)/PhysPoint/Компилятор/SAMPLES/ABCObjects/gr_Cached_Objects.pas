uses ABCObjects,ABCAdditionalObjects,GraphABC;

type
  CachedProba=class(CachedPicABC)
    procedure Draw0(b: integer);
    var i,j: integer;
    begin
      for i:=0 to Width-1 do
      for j:=0 to Height-1 do
        SetPixel(i,j,clRandom);
    end;
  end;

var
  j: integer;
  c: CachedProba;

begin
  cls;
  LockDrawingObjects;

  for j:=1 to 10 do
    c:=CachedProba.Create(200,170,50,60);

  while True do
  begin
    for j:=1 to ObjectsCount do
      Objects[j].moveOn(Random(3)-1,Random(3)-1);
    RedrawObjects;
  end;
end.
