uses GraphABC,ABCSprites,Events,Containers;

var s: SpriteABC;

procedure KeyDown(key: integer);
begin
  case key of
    vk_Left: s.MoveOn(-3,0);
    vk_Right: s.MoveOn(3,0);
    vk_Up: s.MoveOn(0,-3);
    vk_Down: s.MoveOn(0,3);
  end;
end;

var i: integer;

begin
  FillWindow('Textures\mint.gif');

  for i:=1 to 20 do
  begin
    s:=SpriteABC.CreateFromInfo(Random(500),Random(300),'Sprites\spr.png');
    s.State:=Random(s.StateCount)+1;
    s.Speed:=Random(5)+6;
  end;
  s.State:=1;
  s.Speed:=10;
  StartSprites;
  
  OnKeyDown:=KeyDown;
end.