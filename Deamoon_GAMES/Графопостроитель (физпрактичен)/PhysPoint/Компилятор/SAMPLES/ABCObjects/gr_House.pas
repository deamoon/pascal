uses ABCObjects,ABCHouse,Events;
// ��������� ��������� ��������� ������ ��� ��������� ���������
// ������������ �� ��������

var
  r: RectangleABC;
  z: StarABC;
  pp: RegularPolygonABC;
  c: CircleABC;
  sq: SquareABC;
  el: EllipseABC;
  p: PictureABC;
  br: BoardABC;
  f: ContainerABC;
  h,h1: HouseABC;
  cp: RegularPolygonABC;
  i,j,dx,dy: integer;
  g: ObjectABC;

begin
  cls;
  h:=HouseABC.Create(100,70,255,150,clGray);
  h.Wall.BorderWidth:=3;
  h.Window.Color:=clYellow;
  h.Door.Color:=clAqua;
  h.Window.BorderWidth:=2;
  h.Door.BorderWidth:=2;
  h.Window.Width2:=5;
  h.moveon(10,0);
  h.Redraw;
  f:=ContainerABC.Create(100,270);
  f.Add(RectangleABC.Create(20,0,300,80,clCream));
  g:=h.Door.Clone;
  g.moveon(80,10);
  g.Owner:=f;
  z:=StarABC.Create(300,300,70,35,5,clRed);
  g:=z.Clone;

  OnMouseDown:=ABCMouseDown;
  OnMouseMove:=ABCMouseMove;
  OnMouseUp:=ABCMouseUp;
end.
