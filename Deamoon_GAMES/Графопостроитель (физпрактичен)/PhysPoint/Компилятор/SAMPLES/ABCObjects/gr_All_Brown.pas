uses ABCObjects;

var
  r: RectangleABC;
  rr: RoundRectABC;
  z: RegularPolygonABC;
  c: CircleABC;
  sq: SquareABC;
  rsq: RoundSquareABC;
  el: EllipseABC;
  p: PictureABC;
  br: BoardABC;
  t: TextABC;
  g: ObjectABC;
  i,j: integer;

procedure MoveAll(a,b: integer);
var j: integer;
begin
  for j:=1 to Objects.Count do
    Objects[j].moveOn(a,b);
end;

begin
//  LockDrawingObjects;
  sq:=CreateSquareABC(30,5,90,clSkyBlue);
  r:=CreateRectangleABC(10,10,100,180,RGB(255,100,100));
  rr:=CreateRoundRectABC(200,180,180,50,20,clRandom);
  rsq:=CreateRoundSquareABC(20,180,80,40,clRandom);
  c:=CreateCircleABC(160,55,70,clGreen);
  z:=CreateStarABC(200,150,70,135,5,clRandom);
  z.Filled:=False;
  el:=CreateEllipseABC(5,55,65,50,clRandom);
  el.Bordered:=False;
  t:=TextABC.Create(100,170,15,clBlack,'Hello, ABCObjects!');
  br:=CreateBoardABC(200,20,7,9,20,clBlack);
  p:=PictureABC.Create(80,30,'girl.bmp');
  z.Height:=200;
  z.Radius:=70;
  sq.Width:=120;
  t.TransparentBackground:=False;
  t.BackgroundColor:=clYellow;
  t.FontName:='Times New Roman';
  t.FontSize:=20;
  c.Height:=50;
  c.Scale(2);
  MoveAll(160,110);
  
  LockDrawingObjects;
  RedrawObjects;
  while True do
  begin
    for j:=1 to Objects.Count do
      Objects[j].moveOn(Random(3)-1,Random(3)-1);
    RedrawObjects;
  end;
end.
