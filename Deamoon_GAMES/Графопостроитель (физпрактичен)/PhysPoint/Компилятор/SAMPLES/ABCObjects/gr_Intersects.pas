uses ABCObjects;

var
  r: RectangleABC;
  r1: EllipseABC;

begin
  cls;
  r:=CreateRectangleABC(10,10,103,180,clRed);
  r1:=CreateEllipseABC(110,145,100,180,clGreen);

  while True do
  begin
    r1.MoveOn(-1,1);
    Sleep(100);
    if r1.Intersect(r) then
      r1.Color:=clWhite
    else r1.Color:=clGreen
  end;
end.
