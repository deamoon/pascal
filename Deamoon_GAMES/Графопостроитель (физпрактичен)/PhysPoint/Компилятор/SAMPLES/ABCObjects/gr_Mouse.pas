uses ABCObjects,Events;

var
  r: RectangleABC;
  z: StarABC;
  el: EllipseABC;
  br: BoardABC;
  f: ContainerABC;
  cp: RegularPolygonABC;

begin
  cls;
  SetWindowCaption('�������������� �������� �����');
  r:=CreateRectangleABC(100,70,120,180,clCream);
  r.BorderWidth:=3;
  r.BorderColor:=clBrown;
  br:=CreateBoardABC(290,290,5,6,20,clBlack);
  z:=CreateStarABC(200,100,70,35,5,clRed);
  el:=CreateEllipseABC(5,55,65,50,clRandom);
  el:=el.Clone;
  el.Color:=clRandom;

  cp:=CreateRegularPolygonABC(250,220,60,6,clGreen);

  f:=CreateContainerABC(370,80);
  f.Add(CreateRectangleABC(80,0,20,100,clGray));
  f.Add(CreateRectangleABC(0,80,100,20,clGray));
  f.Scale(1.3);
  
  f:=f.Clone;
  f.MoveOn(50,-40);

  z.ToBack;

  OnMouseDown:=ABCMouseDown;
  OnMouseMove:=ABCMouseMove;
  OnMouseUp:=ABCMouseUp;
end.
