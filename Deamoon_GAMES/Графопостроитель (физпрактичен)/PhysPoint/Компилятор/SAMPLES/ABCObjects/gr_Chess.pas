uses ABCObjects,ABCChessObjects,Events,Utils;

var
  br: ChessBoardABC;
  ChessFigures: ChessSetABC;
  wk,wq,wb1,wkn1,wr1,wb2,wkn2,wr2: ChessFigureABC;
  wp: array [1..8] of ChessFigureABC;
  bk,bq,bb1,bkn1,br1,bb2,bkn2,br2: ChessFigureABC;
  bp: array [1..8] of ChessFigureABC;

procedure CreateFigures;
var i: integer;
begin
  ChessFigures:=ChessSetABC.Create(PascalABCPath+'Media\Images\Chess.wmf',45,br);

  wk:=ChessFigures.CreateWhiteKing(5,1);
  wq:=ChessFigures.CreateWhiteQueen(4,1);
  wb1:=ChessFigures.CreateWhiteBishop(3,1);
  wkn1:=ChessFigures.CreateWhiteKnight(2,1);
  wr1:=ChessFigures.CreateWhiteRook(1,1);
  wb2:=ChessFigures.CreateWhiteBishop(6,1);
  wkn2:=ChessFigures.CreateWhiteKnight(7,1);
  wr2:=ChessFigures.CreateWhiteRook(8,1);
  for i:=1 to 8 do
    wp[i]:=ChessFigures.CreateWhitePown(i,2);

  bk:=ChessFigures.CreateBlackKing(5,8);
  bq:=ChessFigures.CreateBlackQueen(4,8);
  bb1:=ChessFigures.CreateBlackBishop(3,8);
  bkn1:=ChessFigures.CreateBlackKnight(2,8);
  br1:=ChessFigures.CreateBlackRook(1,8);
  bb2:=ChessFigures.CreateBlackBishop(6,8);
  bkn2:=ChessFigures.CreateBlackKnight(7,8);
  br2:=ChessFigures.CreateBlackRook(8,8);
  for i:=1 to 8 do
    bp[i]:=ChessFigures.CreateBlackPown(i,7);
end;

begin
  br:=CreateChessBoardABC(20,20,8,8,50,clBlack);
  br.MouseIndifferent:=True;
  br.Delay:=50;
  
  CreateFigures;

  wp[5].move('e4');
  bp[5].move('e5');
  wkn2.move('f3');
  bkn1.move('c6');
  wp[4].move('d4');
  bkn1.move('d4');
  wp[4].Destroy;
  wkn2.move('d4');
  bkn1.Destroy;
  bp[5].move('d4');
  wkn2.Destroy;
  wq.move('d4');
  bp[5].Destroy;

  OnMouseDown:=ABCMouseDown;
  OnMouseMove:=ABCMouseMove;
  OnMouseUp:=ABCMouseUp;
end.
