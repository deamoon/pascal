uses ABCObjects;

var
  bt: TextABC;
  x: integer;
  
begin
  x:=224;
  bt:=CreateTextABC(60,110,110,RGB(x,x,x),'Hello!');
  while x>32 do
  begin
    Sleep(40);
    Dec(x,32);
    bt:=TextABC(bt.Clone);
    bt.Color:=RGB(x,x,x);
    bt.MoveOn(7,7);
  end;
end.
