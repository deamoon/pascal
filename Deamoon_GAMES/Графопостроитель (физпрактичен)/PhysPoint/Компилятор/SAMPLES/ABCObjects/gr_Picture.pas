uses ABCObjects;

var
  p: PictureABC;
  i: integer;
  
begin
  SetWindowCaption('Демонстрация свойства прозрачности фона картинки');
  for i:=0 to 15 do
    p:=CreatePictureABC(60+30*i,110,'girl.bmp');
  for i:=0 to 15 do
  begin
    p:=CreatePictureABC(60+30*i,210,'girl.bmp');
    p.TransparentBackground:=False;
  end;
end.
