uses ABCObjects;

type
  MovingBall=class(CircleABC)
    vx,vy: integer;
    constructor Create(x,y,r,dx,dy,color: integer);
    begin
      inherited Create(x,y,r,color);
      vx:=dx; vy:=dy;
    end;
    procedure Move;
    begin
      MoveOn(vx,vy);
      if (Left<0) or (Left+Width>WindowWidth) then
        vx:=-vx;
      if (Top<0) or (Top+Height>WindowHeight) then
        vy:=-vy;
    end;
  end;

const n=100;

var
  m: MovingBall;
  i: integer;

begin
  SetWindowCaption('������ - ABC �������');
  LockDrawingObjects;
  for i:=1 to n do
    m:=MovingBall.Create(Random(WindowWidth-30),Random(WindowHeight-30),Random(20)+10,Random(7)-3,Random(7)-3,clRandom);
  while True do
  begin
    Sleep(1);
    for i:=1 to ObjectsCount do
      MovingBall(Objects[i]).Move;
    RedrawObjects;
  end;
end.
