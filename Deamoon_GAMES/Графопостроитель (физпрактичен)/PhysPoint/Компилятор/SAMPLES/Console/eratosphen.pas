// "������ ����������" - ���������� ������� �����
const
  n=255;

var
  primes: set of byte;
  i,x,c: integer;
  
begin
  cls;

  primes:=[2..n];
  
  for i:=2 to round(sqrt(n)) do
  begin
    if not (i in primes) then
      continue;
    x:=2*i;
    while x<=n do
    begin
      primes:=primes-[x];
      x:=x+i;
    end;
  end;

  for i:=0 to n do
    if i in primes then
      write(i,' ');
end.
