const n=9;

var i,j: integer;

begin
  cls;
  writeln('Таблица умножения');
  for i:=1 to n do
  begin
    for j:=1 to n do
      write(i*j:4);
    writeln;
  end;
end.
