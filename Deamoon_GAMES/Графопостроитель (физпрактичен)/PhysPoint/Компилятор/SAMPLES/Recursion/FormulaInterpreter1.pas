// ���������� ������������� ������ (����� ������ ��������� 1 ������)
// E::=T{+T|-T}  T::=F{*F}  F::=(E)|num

function Calc(s: string): integer;
var
  ns: integer;
  CurSym: char;
 procedure MoveNext;
 begin
   Inc(ns);
   if ns<=Length(s) then
     CurSym:=s[ns]
   else CurSym:=#0;
 end;
 function Num: integer;
 begin
   Result:=Ord(CurSym)-Ord('0');
 end;
 function T: integer; forward;
 function F: integer; forward;
 function E: integer;
 begin
   Result:=T;
   while CurSym in ['+','-'] do
   begin
     if CurSym='+' then
     begin
       MoveNext;
       Result:=Result+T
     end
     else
     begin
       MoveNext;
       Result:=Result-T;
     end
   end;
 end;
 function T: integer;
 begin
   Result:=F;
   while CurSym='*' do
   begin
     MoveNext;
     Result:=Result*F;
   end;
 end;
 function F: integer;
 begin
   if CurSym='(' then
   begin
     MoveNext;
     Result:=E
   end
   else Result:=Num; // CurSym in ['0'..'9']
   MoveNext;
 end;
begin // Calc
  MoveNext;
  Result:=E;
end;
begin
  writeln(Calc('1+2*4'));
end.

