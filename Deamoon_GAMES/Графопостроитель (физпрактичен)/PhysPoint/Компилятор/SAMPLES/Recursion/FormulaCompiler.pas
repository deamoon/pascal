// ���������� ���������� ������ � ���������� ����� (����� ������ ��������� 1 ������)
// E::=T{+T|-T}  T::=F{*F}  F::=(E)|num

function Prefix(s: string): string;
var
  ns: integer;
  CurSym: char;
 procedure error;
 begin
   writeln('error: ',ns,' ',CurSym);
   Halt(1);
 end;
 procedure MoveNext;
 begin
   Inc(ns);
   if ns<=Length(s) then
     CurSym:=s[ns]
   else CurSym:=#0;
 end;

 function T: string; forward;
 function F: string; forward;

 function E: string;
 begin
   Result:=T;
   while CurSym in ['+','-'] do
   begin
     Result:=CurSym+Result;
     MoveNext;
     Result:=Result+T;
   end;
 end;

 function T: string;
 begin
   Result:=F;
   while CurSym='*' do
   begin
     Result:='*'+Result;
     MoveNext;
     Result:=Result+F;
   end;
 end;

 function F: string;
 begin
   if CurSym='(' then
   begin
     MoveNext;
     Result:=E;
     if CurSym=')' then MoveNext
     else error;
   end
   else if CurSym in ['0'..'9'] then
   begin
     Result:=CurSym;
     MoveNext
   end
   else error
 end;
begin // Prefix
  MoveNext;
  Result:=E;
end;

function Calc(s: string): integer;
var i: integer;
 function Calc0: integer;
 var o1,o2: integer;
 begin
   Inc(i);
   case s[i] of
     '+': begin o1:=Calc0; o2:=Calc0; Result:=o1+o2; end;
     '-': begin o1:=Calc0; o2:=Calc0; Result:=o1-o2; end;
     '*': begin o1:=Calc0; o2:=Calc0; Result:=o1*o2; end;
     '0'..'9': Result:=Ord(s[i])-Ord('0');
   end;
 end;
begin
  i:=0;
  Result:=Calc0;
end;

var res: string;

begin
  cls;
  res:=Prefix('(1+2)*3');
  writeln(res);
  writeln(Calc(res));
end.
