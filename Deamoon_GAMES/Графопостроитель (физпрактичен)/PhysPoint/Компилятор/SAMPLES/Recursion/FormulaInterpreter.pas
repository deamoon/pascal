// ���������� ������������� ������ (����� ������ ��������� 1 ������)
// E::=T{+T|-T}  T::=F{*F}  F::=(E)|num
var
  s: string;
  i: integer;
  ch: char;

procedure error;
begin
  writeln('������: ������� ',i,' ',ch);
  Halt(1);
end;

function getsym: char;
begin
  Inc(i);
  if i<=Length(s) then
    Result:=s[i]
  else Result:=#0;
end;

function T: integer; forward;
function F: integer; forward;

function E: integer;
begin
  Result:=T;
  while ch in ['+','-'] do
    if ch='+' then
    begin
      ch:=getsym;
      Result:=Result+T
    end
    else
    begin
      ch:=getsym;
      Result:=Result-T;
    end;
end;

function T: integer;
begin
  Result:=F;
  while ch='*' do
  begin
    ch:=getsym;
    Result:=Result*F;
  end;
end;

function F: integer;
begin
  if ch='(' then
  begin
    ch:=getsym;
    Result:=E;
    if ch=')' then ch:=getsym
    else error;
  end
  else if ch in ['0'..'9'] then
  begin
    Result:=Ord(ch)-Ord('0');
    ch:=getsym
  end
  else error
end;

begin
  cls;
  s:='(1+2-4)';
  ch:=getsym;
  writeln(E);
end.
