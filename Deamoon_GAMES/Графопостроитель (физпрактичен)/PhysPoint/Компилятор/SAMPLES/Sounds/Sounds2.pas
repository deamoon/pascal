uses Sounds;

var
  i: integer;
  snd1,snd2: Sound;

begin
  snd1:=Sound.Create('kuku.wav');
  snd2:=Sound.Create('goodbye.wav');
  snd1.Play;
  for i:=1 to Random(3)+3 do
  begin
    snd1.Play;
    Sleep(500);
  end;
  snd2.Play;
  while snd2.IsPlaying do
    Sleep(100);
    
  snd1.Destroy;
  snd2.Destroy;
end.
