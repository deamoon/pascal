uses Sounds;

var snd1,snd2,i: integer;

begin
  snd1:=LoadSound('kuku.wav');
  snd2:=LoadSound('goodbye.wav');
  PlaySound(snd1);
  for i:=1 to Random(3)+3 do
  begin
    PlaySound(snd1);
    Sleep(500);
  end;
  PlaySound(snd2);
  while SoundIsPlaying(snd2) do
    Sleep(100);
  DestroySound(snd1);
  DestroySound(snd2);
end.
