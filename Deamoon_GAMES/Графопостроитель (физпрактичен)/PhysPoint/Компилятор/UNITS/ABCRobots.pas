unit ABCRobots;

interface

uses GraphABC, ABCObjects;

type
  RobotField=class(ContainerABC)
  private
    constructor Create(x,y,w,h,c: integer); begin end;
    function GetCellSize: integer;
    begin
      Result:=BoardABC(Objects[1]).CellSize
    end;
    function GetDimX: integer; begin Result:=BoardABC(Objects[1]).DimX end;
    function GetDimY: integer; begin Result:=BoardABC(Objects[1]).DimY end;
  public
    constructor Create(x,y,nn,mm,ssz: integer; cl: ColorType);
    begin
      inherited Create(x,y);
      Add(BoardABC.Create(0,0,nn,mm,ssz,cl));
    end;
    property CellSize: integer read GetCellSize;
    property DimX: integer read GetDimX;
    property DimY: integer read GetDimY;
    function Clone0: ObjectABC; begin Result:=BoardABC.CreateBy(Self) end;
    function Clone: RobotField; begin Result:=RobotField(Clone0) end;
  end;

  Robot=class(BoundedObjectABC)
  private
    b: RobotField;
    zz: integer;
    fx,fy: integer;
    procedure moveon(x,y: integer); begin inherited moveon(x,y); end; // ����� ������ ��
  public
    constructor Create(x,y,c: integer; bb: RobotField);
    procedure Draw(x,y: integer);
    procedure move(x,y: integer);
    procedure Up; begin move(fx,fy-1); end;
    procedure Down; begin move(fx,fy+1); end;
    procedure Left; begin move(fx+1,fy); end;
    procedure Right; begin move(fx-1,fy); end;
    function Clone0: ObjectABC; begin Result:=Robot.CreateBy(Self) end;
    function Clone: Robot; begin Result:=Robot(Clone0) end;
  end;

implementation

//------ Robot ------
constructor Robot.Create(x,y,c: integer; bb: RobotField);
begin
  b:=bb;
  zz:=4;
  fx:=x; fy:=y;
  inherited Create((x-1)*b.CellSize+zz,(y-1)*b.CellSize+zz,b.CellSize-2*zz+1,b.CellSize-2*zz+1,c);
  Owner:=b;
end;

procedure Robot.Draw(x,y: integer);
begin
  SetBrushColor(Color);
  Rectangle(x,y,x+Width,y+Height);
end;

procedure Robot.move(x,y: integer);
var sz: integer;
begin
  if (x<1) or (x>b.DimX) or (y<1) or (y>b.DimY) then Exit;
  sz:=b.CellSize;
  fx:=x; fy:=y;
  SetX((x-1)*sz+zz);
  SetY((y-1)*sz+zz);
end;

end.
