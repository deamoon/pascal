unit ABCChessObjects;

interface

uses GraphABC, ABCObjects, PointRect;

type
  ChessBoardABC=class(BoardABC)
    Delay: integer;
    constructor Create(x,y,nn,mm,ssz: integer; cl: ColorType); begin Delay:=20; inherited Create(x,y,nn,mm,ssz,cl) end;
    procedure Draw(x,y: integer);
  end;

  ChessFigureABC=class(ObjectABC)
  private
    b: integer;
    board: ChessBoardABC;
  public
    constructor Create(x,y,bb: integer; brd: ChessBoardABC);
    procedure Draw(x,y: integer); begin DrawPicture(b,x,y); end;
    procedure Move(newx,newy: integer);
    procedure Move(s: string);
    function Clone0: ObjectABC; begin Result:=ChessFigureABC.CreateBy(Self) end;
    function Clone: ChessFigureABC; begin Result:=ChessFigureABC(Clone0) end;
  end;

  ChessSetABC=class
  private
    m,b,sz: integer;
    board: ChessBoardABC;
  public
    constructor Create(fname: string; size: integer; brd: ChessBoardABC);
    function CreateContainerABC(x,y,n: integer): ChessFigureABC;
    function CreateWhiteKing(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,1); end;
    function CreateWhiteQueen(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,2); end;
    function CreateWhiteBishop(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,3); end;
    function CreateWhiteKnight(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,4); end;
    function CreateWhiteRook(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,5); end;
    function CreateWhitePown(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,6); end;
    function CreateBlackKing(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,7); end;
    function CreateBlackQueen(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,8); end;
    function CreateBlackBishop(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,9); end;
    function CreateBlackKnight(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,10); end;
    function CreateBlackRook(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,11); end;
    function CreateBlackPown(x,y: integer): ChessFigureABC; begin Result:=CreateContainerABC(x,y,12); end;
    procedure SetSize(w: integer); begin SetPictureSize(m,w*6,w); end;
  end;

function CreateChessBoardABC(x,y,nn,mm,ssz: integer; cl: ColorType): ChessBoardABC;

implementation

//------ ChessBoardABC ------
procedure ChessBoardABC.Draw(x,y: integer);
var i,j: integer;
begin
  inherited Draw(x,y);
  SetBrushColor(clLightGray);
  for i:=1 to DimX do
  for j:=1 to DimY do
    if (i+j) mod 2 = 1 then
      FillRect(x+(i-1)*CellSize+1,y+(j-1)*CellSize+1,x+i*CellSize,y+j*CellSize);
end;

//------ ChessSetABC ------
constructor ChessSetABC.Create(fname: string; size: integer; brd: ChessBoardABC);
begin
  board:=brd;
  m:=LoadPicture(fname);
  SetPictureSize(m,size*12,size);
  sz:=PictureHeight(m);
  b:=CreatePicture(12*sz,sz);
  SetDrawingSurface(b);
  DrawPicture(m,0,0);
  RestoreDrawingSurface;
end;

function ChessSetABC.CreateContainerABC(x,y,n: integer): ChessFigureABC;
var k: integer;
begin
  SetDrawingSurface(b);
  k:=CreatePictureFromRect(RectF((n-1)*sz,0,n*sz,sz));
  RestoreDrawingSurface;
  SetPictureTransparent(k,True);
  Result:=ChessFigureABC.Create(x,y,k,board);
end;

//------ ChessFigureABC ------
constructor ChessFigureABC.Create(x,y,bb: integer; brd: ChessBoardABC);
begin
  b:=bb;
  board:=brd;
  inherited Create(brd.Left+(x-1)*brd.CellSize+3,brd.Top+(8-y)*brd.CellSize+3,PictureWidth(b),PictureHeight(b),clWhite);
end;

procedure ChessFigureABC.Move(newx,newy: integer);
var
  i,x,y,num: integer;
  t: real;
begin
  newx:=board.Left+(newx-1)*board.CellSize+3;
  newy:=board.Top+(8-newy)*board.CellSize+3;
  x:=Left;
  y:=Top;
  t:=0;
  num:=board.Delay;
  for i:=1 to num do
  begin
    t:=t+1/num;
    Sleep(10);
    moveto(round(t*newx+(1-t)*x),round(t*newy+(1-t)*y));
  end;
end;

procedure ChessFigureABC.Move(s: string);
begin
  Move(Ord(s[1])-Ord('a')+1,StrToInt(s[2]));
end;

function CreateChessBoardABC(x,y,nn,mm,ssz: integer; cl: ColorType): ChessBoardABC;
begin
  Result:=ChessBoardABC.Create(x,y,nn,mm,ssz,cl);
end;

end.
