//PT4LoadInfo//DM==mydm|��� ������� ��� ����������|2
unit DMTasks;

interface

uses DrawManMake;

implementation

procedure FirstDM;
var i,a: integer;
begin
  TaskText('���������, ��������� ����');
  DrawManField(14,8);
  DoToPoint(7,7);
  a:=6;
  for i:=1 to 6 do
  begin
    DoPenDown;
    DoOnVector(a,-a);
    DoOnVector(-a,a);
    DoOnVector(-a,-a);
    DoOnVector(a,a);
    Dec(a);
    DoPenUp;
    DoOnVector(0,-1);
  end;
end;

procedure SecondDM;
 procedure Cross;
 begin
   DoPenDown;
   DoOnVector(0,1); DoOnVector(1,0); DoOnVector(0,1); DoOnVector(1,0);
   DoOnVector(0,-1); DoOnVector(1,0); DoOnVector(0,-1); DoOnVector(-1,0);
   DoOnVector(0,-1); DoOnVector(-1,0); DoOnVector(0,1); DoOnVector(-1,0);
   DoPenUp;
 end;
begin
  TaskText('���������, ��������� ��������� Cross');
  DrawManField(18,12);
  DoToPoint(2,6); Cross;
  DoToPoint(11,9); Cross;
  DoToPoint(14,4); Cross;
  DoToPoint(7,2); Cross;
end;

procedure InitTask(num: integer);
begin
  case num of
1: FirstDM;
2: SecondDM;
  end;
end;

begin
  AddGroup('MyDM', 2, InitTask);
end.
