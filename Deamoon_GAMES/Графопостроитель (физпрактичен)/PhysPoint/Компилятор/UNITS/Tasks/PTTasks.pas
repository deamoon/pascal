//PT4LoadInfo//==PTTasks|��� ����������� ������� PT4|999
unit PTTasks;
interface
uses pt4make;

implementation



procedure InitTask(num: integer);
begin
  case num of
  1..40: useTask('Begin',num);
  41..70: useTask('Integer',num-40);
  71..110: useTask('Boolean',num-70);
  111..140: useTask('If',num-110);
  141..160: useTask('Case',num-140);
  161..200: useTask('For',num-160);
  201..230: useTask('While',num-200);
  231..270: useTask('Series',num-230);
  271..330: useTask('Proc',num-270);
  331..360: useTask('Minmax',num-330);
  361..500: useTask('Array',num-360);
  501..600: useTask('Matrix',num-500);
  601..670: useTask('String',num-600);
  671..760: useTask('File',num-670);
  761..820: useTask('Text',num-760);
  821..890: useTask('Param',num-820);
  891..920: useTask('Recur',num-890);
  921..999: useTask('Dynamic',num-920);
  end;

end;

begin
  AddGroup('PTTasks', '��� ����������� ������� ��������� Programming Taskbook 4', '�. �. �������, 2006', 'sgrqawqdfsdfsdf35345', 999, InitTask);

  AddCommentText('������ ������ ������� �������� ��� �������, �������� �\ 18~������� ����� ��������� Programming');
  AddCommentText('Taskbook~4, ��\ ����������� ���������� ������� ������ Dynamic (�\ ������, Dynamic80).');
  AddCommentText('������ ����������� ������ �\ ���, ��� ������ ������� ����� ��������� \I��\ ����� 999~�������\i.');
  SubgroupComment('���� � ����� ������, �������� ������������');  UseCommentText('Begin','');
  SubgroupComment('����� �����');  UseCommentText('Integer','');
  SubgroupComment('���������� ���������');  UseCommentText('Boolean','');
  SubgroupComment('�������� ��������');  UseCommentText('If','');
  SubgroupComment('�������� ������');  UseCommentText('Case','');
  SubgroupComment('���� � ����������');  UseCommentText('For','');
  SubgroupComment('���� � ��������');  UseCommentText('While','');
  SubgroupComment('������������������');  UseCommentText('Series','');
  SubgroupComment('��������� � �������');  UseCommentText('Proc','');
  SubgroupComment('�������� � ���������');  UseCommentText('Minmax','');
  SubgroupComment('���������� �������: ������������');  UseCommentText('Array',''); UseCommentText('Array','���������� �������: ������������');
  SubgroupComment('���������� �������: ������ ���������');  UseCommentText('Array','���������� �������: ������ ���������');
  SubgroupComment('���������� �������: ��������������');  UseCommentText('Array','���������� �������: ��������������');
  SubgroupComment('���������� �������: ��������� ����� �� ���������');  UseCommentText('Array','���������� �������: ��������� ����� �� ���������');
  SubgroupComment('��������� ������� (�������): ������������');  UseCommentText('Matrix',''); UseCommentText('Matrix','��������� ������� (�������): ������������');
  SubgroupComment('��������� ������� (�������): ��������������');  UseCommentText('Matrix','��������� ������� (�������): ��������������');
  SubgroupComment('������� � ������: �������� ��������');  UseCommentText('String',''); UseCommentText('String','������� � ������: �������� ��������');
  SubgroupComment('������ � �������������� ���� � ������');  UseCommentText('String','������ � �������������� ���� � ������');
  SubgroupComment('�������� �����: �������� ��������');  UseCommentText('File','');
  SubgroupComment('���������� � ��������� �����');  UseCommentText('File','���������� � ��������� �����');
  SubgroupComment('������������� ������ ��� ������ � ���������');  UseCommentText('File','������������� ������ ��� ������ � ���������');
  SubgroupComment('��������� �����: �������� ��������');  UseCommentText('Text','');
  SubgroupComment('��������� ����� � �������� �����������');  UseCommentText('Text','��������� ����� � �������� �����������');
  SubgroupComment('��������� � �������: ������ � ���������');  UseCommentText('Param',''); UseCommentText('Param','��������� � �������: ������ � ���������');
  SubgroupComment('��������� � �������: ������ � ��������');  UseCommentText('Param','��������� � �������: ������ � ��������');
  SubgroupComment('��������: ���������� ���������');  UseCommentText('Recur','��������: ���������� ���������');
  SubgroupComment('��������: ������ ���������');  UseCommentText('Recur','��������: ������ ���������');
  SubgroupComment('������������ ��������� ������: ���� � ������� �����');  UseCommentText('Dynamic','');
  SubgroupComment('������������ ��������� ������: ����');  UseCommentText('Dynamic','������������ ��������� ������: ����');
  SubgroupComment('������������ ��������� ������: �������');  UseCommentText('Dynamic','������������ ��������� ������: �������');
  SubgroupComment('������������ ��������� ������: ���������� ������');  UseCommentText('Dynamic','������������ ��������� ������: ���������� ������');
  SubgroupComment('������������ ��������� ������: ������ � ��������� ���������');  UseCommentText('Dynamic','������������ ��������� ������: ������ � ��������� ���������');

CloseGroup;


end.
