//PT4LoadInfo//==MyTasks|��� �������|247
unit MyTasks;
interface
uses pt4make,BegMini;

implementation

procedure sdem65an56;
var
  a: array [1..10] of real;
  i, n: integer;
begin
  StartTask;
taskText('��� ������ �������~{N}.',0,2);
taskText('������� ���\ �������� �\ �������� �������.',0,4);
n := Random(9)+2;
for i:=1 to n do
    a[i] := 9.98*Random;
dataN('N = ',n,0,2,1);
for i := 1 to n do
  dataR('',a[i],center(i,n,5,2),4,5);
for i := 1 to n do
  resultR('',a[n+1-i],center(i,n,5,2),3,5);
setNumberOfTests(3);
  Pause;
  Pause;
end;        

procedure sdem65an57;
var
  b, b1: array [1..10] of integer;
  i, j, n, n0, k: integer;
begin
  StartTask;
  TaskText('��� ������������� ������ �������~{N}.', 0, 2);
  TaskText('����� ���������� ��������� ��������� �\ ������ �������.', 0, 4);
  n := 5 + Random(6);
  for i := 1 to n do
  begin
    b[i] := 2 + Random(8);
    b1[i] := b[i];
  end;
  if Random(2)=0 then k := 1
  else k := -1;
  for j := 1 to n do
    for i := 1 to n-j do
      if b[i]*k<b[i+1]*k then
      begin
        n0 := b[i];
        b[i] := b[i+1];
        b[i+1] := n0;
      end;
  k := 1;
  for i := 2 to n do
    if b[i] <> b[i-1] then inc(k);
dataN('N = ',n,0,2,1);
for i := 1 to n do
  dataN('',b1[i],center(i,n,1,6),4,1);
resultN('',k,0,3,2);
  SetNumberOfTests(5);
//  Pause;
end;

procedure sdem65an58;
var
  b, b1, b2: array [1..10] of integer;
  a0, b0, i, n, m, l: integer;
begin
  StartTask('����� ����� �����');
  TaskText('��� ������������� ������~{A} �������~{N}. ������� \I������\i ������ ������ ������', 0, 1);
  TaskText('���������� ���������, �\ \I������ �����\i\ \= ���������� ���� ��������� (����� �����', 0, 2);
  TaskText('����� ���� �����~1). ������������ ��� ����� ������������� �������~{B} �~{C}', 0, 3);
  TaskText('����������� �������, ������� �\ ������~{B} ����� ���� ����� ��������� �������,', 0, 4);
  TaskText('�\ �\ ������~{C}\ \= �������� ���������, ���������� ���\ �����.', 0, 5);
a0:=1 + Random(5); b0:=1+Random(3);
n := 5 + Random(6);
for i:=1 to n do
 begin
  b[i] := Random(b0)+a0;
 end;
m := 0;
l := 1;
for i:=2 to n do
  if b[i] =b[i-1] then Inc(l)
  else
    begin
      Inc(m);
      b1[m] := l;
      b2[m] := b[i-1];
      l := 1;
    end;
Inc(m);
b1[m] := l;
b2[m] := b[n];
dataN('N = ',n,0,2,1);
dataComment('A:',center(1,n+1,1,6),4);
for i := 1 to n do
  dataN('',b[i],center(i+1,n+1,2,5),4,2);
resultComment('B:',center(1,m+1,1,6),2);
for i := 1 to m do
  resultN('',b1[i],center(i+1,m+1,2,5),2,2);
resultComment('C:',center(1,m+1,1,6),4);
for i := 1 to m do
  resultN('',b2[i],center(i+1,m+1,2,5),4,2);
  SetNumberOfTests(5);
//  Pause;
end;


procedure InitTask(num: integer);
begin
  case num of
  1..140: useTask('Array',num);
  141..240: useTask('Matrix',num-140);
  241: sdem65an56;
  242: sdem65an57;
  243: sdem65an58;
  244..247: useTask('BegMini',num-243);
  end;
end;

begin
  AddGroup('MyTasks', '��� �������', '�. �. �������, 2006', 'sgrqawqdfsdfsdf35345', 247, InitTask);
end.
