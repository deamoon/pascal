//PT4LoadInfo//RB==myrob|��� ������� ��� ������|2
unit RobTasks;

interface

uses RobotMake;

implementation

procedure FirstRob;
begin
  TaskText('��������� ���������� ������');
  RobotField(10,6);
  HorizontalWall(0,3,4);
  VerticalWall(4,3,2);
  RobotBegin(1,4);
  VerticalWall(5,1,5);
  HorizontalWall(5,1,4);
  RobotEnd(6,2);
  Tag(6,2);
end;

procedure SecondRob;
begin
  TaskText('��������� ���������� ������');
  RobotField(10,8);
  RobotBeginEnd(1,8,10,1);
  HorizontalWall(1,1,8);
  HorizontalWall(1,7,8);
  VerticalWall(1,2,5);
  VerticalWall(9,1,5);
  TagRect(2,2,9,7);
end;

procedure ThirdRob;
var n,i: integer;
begin
  TaskText('��������� ������ ��� ������������');
  n:=Random(4)+7;
  RobotField(n,4);
  RobotBeginEnd(1,3,n,3);
  MarkPainted(n,2);
  Tag(n,3);
  for i:=2 to n-1 do
    if Random(3)=1 then
    begin
      MarkPainted(i,2);
      Tag(i,3);
    end;
end;

procedure InitTask(num: integer);
begin
  case num of
1: FirstRob;
2: SecondRob;
3: ThirdRob;
  end;
end;

begin
  AddGroup('MyRob', 3, InitTask);
end.
