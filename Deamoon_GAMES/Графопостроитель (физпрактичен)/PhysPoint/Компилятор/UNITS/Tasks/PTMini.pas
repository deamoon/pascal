//PT4LoadInfo//==PTMini|Mini: ������ ������|2
unit PTMini;
interface
uses pt4make;

implementation

procedure sdem65an1;
var
  a, b: real;
begin
startTask;
startTask;
startTask;
startTask;
taskText('���� ������� ��������������~{a} �~{b}. ',0,2);
taskText('����� ��� �������~{S}~=~{a}\*{b} �\ ��������~{P}~=~2\*({a}\;+\;{b}).',0,4);
a := (1+Random(100))/10;
b := (1+Random(100))/10;
dataR('a = ',a,xLeft,3,4);
dataR('b = ',b,xRight,3,4);
resultR('S = ',a*b,0,2,4);
resultR('P = ',2*(a+b),0,4,4);
setNumberOfTests(3);
//pause;
end;

procedure sdem65an2;
var
  x: real;
begin
startTask;
taskText('����� ����� ����������~{L} �\ ������� �����~{S} ��������� �������~{R}:',0,2);
taskText('\[{L}~=~2\*\p\*{R},\Q    {S}~=~\p\*{R}^2.\]',0,3);
taskText('�\ �������� ��������~\p ������������~3.14.',0,4);
x := 1+Random(9);
if Random(2)=0 then x := x + 0.5;
dataR('R = ',x,0,3,4);
setprecision(3);
resultR('L = ',2*Pi*x,xLeft,3,7);
resultR('S = ',Pi*x*x,xRight,3,7);
setNumberOfTests(3);
//pause;
end;

procedure sdem65an3;
var
  a, b: real;
begin
startTask;
taskText('���� ��� ��������� �����.',0,2);
taskText('����� �����, ��������, ������������ �\ ������� ��\ ���������.',0,4);
a := (Random(200) - 90.0)/10;
b := (Random(200) - 90.0)/10;
if a = 0 then a := 0.1;
if b = 0 then b := 0.1;
dataR('A = ',a,xLeft,3,5);
dataR('B = ',b,xRight,3,5);
resultR('A^2 + B^2 = ',a*a+b*b,xLeft,2,7);
resultR('A^2 - B^2 = ',a*a-b*b,xRight,2,7);
resultR('A^2 \* B^2 = ',a*a*b*b,xLeft,4,7);                     
resultR('A^2 / B^2 = ',a*a/b/b,xRight,4,7);
setNumberOfTests(3);
//pause;
end;

procedure sdem65an56;
var
  a: array [1..10] of real;
  i, n: integer;
begin
  StartTask;
taskText('��� ������ �������~{N}.',0,2);
taskText('������� ���\ �������� �\ �������� �������.',0,4);
n := Random(9)+2;
for i:=1 to n do
    a[i] := 9.98*Random;
dataN('N = ',n,0,2,1);
for i := 1 to n do
  dataR('',a[i],center(i,n,5,2),4,5);
for i := 1 to n do
  resultR('',a[n+1-i],center(i,n,5,2),3,5);
setNumberOfTests(3);
  Pause;
  Pause;
end;        


procedure InitTask(num: integer);
begin
  case num of                                             
  1: sdem65an1;
  2: sdem65an56;
  3: sdem65an3;

  end;
end;
                                         
begin
  AddGroup('PTMini', 'Mini: ������ ������',
    '�. �. �������, 2006', 'qwqfsdf13dfttd', 3, InitTask);
end.
