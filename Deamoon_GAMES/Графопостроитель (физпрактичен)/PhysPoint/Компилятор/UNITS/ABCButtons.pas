unit ABCButtons;

interface

uses GraphABC, ABCObjects, Events, PointRect;

type
  ButtonABC=class(UIElementABC)
  public
    OnClick: procedure;
    OnClickExt: procedure(Sender: ButtonABC);
  private
    constructor Create(x,y,w,h: integer; cl: ColorType); begin end;
  public
    constructor Create(x,y,w,h: integer; txt: string; cl: ColorType);
    begin
      inherited Create(x,y,w,h,cl);
      TextScale:=0.7;
      TextVisible:=True;
      Text:=txt;
      OnClick:=nil;
      OnClickExt:=nil;
    end;
    constructor Create(x,y,w: integer; txt: string; cl: ColorType);
    begin
      Create(x,y,w,30,txt,cl);
    end;
    procedure Draw(x,y: integer);
    var z,z1: integer;
    begin
      z:=BorderWidth div 2;
      z1:=(BorderWidth-1) div 2;
      SetBrushColor(Color);
      SetPenColor(BorderColor);
      SetPenWidth(BorderWidth);
      RoundRect(x+z,y+z,x+Width-z1,y+Height-z1,10,10);
      DrawText(x,y);
    end;
  end;

implementation

var __pressedButton: ButtonABC;

type MouseProc = procedure(x,y,mb: integer);

var OldOnMouseDown,OldOnMouseUp: MouseProc;

procedure ButtonsMouseDown(x,y,mb:integer);
begin
  if mb<>1 then exit;
  __pressedButton:=ButtonABC(UIElementUnderPoint(x,y));
  if __pressedButton<>nil then
    __pressedButton.MoveOn(1,1)
  else if OldOnMouseDown<>nil then
    OldOnMouseDown(x,y,mb);
end;

procedure ButtonsMouseUp(x,y,mb:integer);
var b: ButtonABC;
begin
  if __pressedButton<>nil then
  begin
    __pressedButton.MoveOn(-1,-1);
    __pressedButton:=nil;
    b:=ButtonABC(UIElementUnderPoint(x,y));
    if (b<>nil) and (b.OnClick<>nil) then
      b.OnClick;
    if (b<>nil) and (b.OnClickExt<>nil) then
      b.OnClickExt(b);
    if (b=nil) and (OldOnMouseDown<>nil) then
      OldOnMouseUp(x,y,mb);
  end;
end;

initialization
  __pressedButton:=nil;
finalization
  OldOnMouseDown:=OnMouseDown;
  OnMouseDown:=ButtonsMouseDown;
  OldOnMouseUp:=OnMouseUp;
  OnMouseUp:=ButtonsMouseUp;
end.
