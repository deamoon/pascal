unit ABCObjects;

interface

uses GraphABC, Containers, PointRect, Utils, Events;

const
  clLightGray=GraphABC.clLightGray;
  clDarkGray=GraphABC.clDarkGray;
  clBrown=GraphABC.clBrown;
  clBlack=GraphABC.clBlack;
  clMaroon=GraphABC.clMaroon;
  clGreen=GraphABC.clGreen;
  clOlive=GraphABC.clOlive;
  clNavy=GraphABC.clNavy;
  clPurple=GraphABC.clPurple;
  clTeal=GraphABC.clTeal;
  clGray=GraphABC.clGray;
  clSilver=GraphABC.clSilver;
  clRed=GraphABC.clRed;
  clLime=GraphABC.clLime;
  clYellow=GraphABC.clYellow;
  clBlue=GraphABC.clBlue;
  clFuchsia=GraphABC.clFuchsia;
  clAqua=GraphABC.clAqua;
  clLtGray=GraphABC.clLtGray;
  clDkGray=GraphABC.clDkGray;
  clWhite=GraphABC.clWhite;
  clMoneyGreen=GraphABC.clMoneyGreen;
  clSkyBlue=GraphABC.clSkyBlue;
  clCream=GraphABC.clCream;
  clMedGray=GraphABC.clMedGray;
  
type
  pstring=^string;
  dynstring=record
    p: pstring;
    sz: integer; // ������, ���������� ��� ������ + ���� �����
  end;

  ColorType=integer;
  CanvasType=integer;

  Point = PointRect.Point;
  Rect = PointRect.Rect;

  ContainerABC=class; // ������������ !

  ObjectABC=class
  public
    Ow: ContainerABC;
    MouseIndifferent: boolean;
    dx,dy: integer;
  protected
    fx,fy: integer;
    fw,fh: integer;
  private
    txt: dynstring;
    col: ColorType;
    vis,vistxt: boolean;
    txtscale: real;
    c: CanvasType; // ���������� Picture, �� ������� ������
  protected
    procedure SetVis(v: boolean);
    procedure SetColor(cl: ColorType);
    procedure SetOwner(o: ContainerABC);
    procedure SetText(t: string);
    function GetText: string;
    procedure SetVisTxt(b: boolean);
    procedure SetTextScale(r: real);
    procedure SetNum(n: integer);
    function GetNum: integer;
    procedure SetRealNum(r: real);
    function GetRealNum: real;
    procedure SetCenter(p: Point);
    function GetCenter: Point;
    procedure SetPosition(p: Point);
    function GetPosition: Point;
  protected
    // InternalDraw: �������������� ��������� (�� ���������� �������������). ������ �������� Draw(fx,fy)
    procedure InternalDraw;
    procedure SetX(x: integer);
    procedure SetY(y: integer);
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
    procedure CalcOwnerOffset(var l,t: integer);
    procedure DrawAfterChangeBounds(oldBounds,newBounds: Rect);
    procedure DrawText(x,y: integer);
  public
    constructor Create(x,y,w,h: integer; cl: ColorType);
    constructor CreateBy(g: ObjectABC);
    procedure Draw(x,y: integer); begin end;
      // Draw: ���������������� � ����������. ������ ������ �� ������� �����
    procedure Redraw;
      // Redraw: �������������� ��������� (�� ���������� �������������).
      // ��������� ����������� ������� �� ������, ������� drawRect(Bounds).
      // ���������� � ���������, ����� ������� ������� �� ���������� (����., ����� �����)
      // ��� ������������� (���������� ������ ������� � ������)
    procedure SetCoords(x,y: integer); begin fx:=x; fy:=y; end;
    procedure MoveTo(x,y: integer);
    procedure MoveTo(p: Point); begin MoveTo(p.x,p.y); end;
    procedure MoveOn(dx,dy: integer); begin MoveTo(fx+dx,fy+dy); end;
    procedure Move; begin MoveOn(dx,dy); end;
    procedure Scale(f: real);
    procedure ToFront;
    procedure ToBack;
    function Bounds: Rect;
    function IntersectRect(r: Rect): boolean;
    function Intersect(g: ObjectABC): boolean;
    function PtInside(x,y: integer): boolean;
    function PtInside(p: Point): boolean; begin Result:=PtInside(p.x,p.y); end;
    property Left: integer read fx write SetX;
    property Top: integer read fy write SetY;
    property Center: Point read GetCenter write SetCenter;
    property Position: Point read GetPosition write SetPosition;
    property Width: integer read fw write SetWidth;
    property Height: integer read fh write SetHeight;
    property Visible: boolean read vis write SetVis;
    property Color: ColorType read col write SetColor;
    property Owner: ContainerABC read ow write SetOwner;
    
    property Text: string read GetText write SetText;
    property TextVisible: boolean read vistxt write SetVisTxt;
    property TextScale: real read txtscale write SetTextScale;
    property Number: integer read GetNum write SetNum;
    property RealNumber: real read GetRealNum write SetRealNum;

    function Clone0: ObjectABC;
    function Clone: ObjectABC; begin Result:=Clone0 end;
    destructor Destroy;
  end;

  BoundedObjectABC=class(ObjectABC)
  private
    bcol: ColorType;
    bw: integer;
    fil,bor: boolean;
    procedure SetBColor(cl: ColorType);
    procedure SetBW(w: integer);
    procedure SetFilled(f: boolean);
    procedure SetBordered(b: boolean);
  protected
    procedure SetDrawSettings;
  public
    constructor Create(x,y,w,h: integer);
    constructor Create(x,y,w,h: integer; cl: ColorType);
    property BorderColor: ColorType read bcol write SetBColor;
    property BorderWidth: integer read bw write SetBW; // ������ ������� ��� ��������� ������ ������, �.�. ��� �� ��������� �� �������� ������� �������
    property Filled: boolean read fil write SetFilled;
    property Bordered: boolean read bor write SetBordered;
    constructor CreateBy(g: BoundedObjectABC);
  end;

  RectangleABC=class(BoundedObjectABC)
    procedure Draw(x,y: integer);
    function Clone0: ObjectABC; begin Result:=RectangleABC.CreateBy(Self); end;
    function Clone: RectangleABC; begin Result:=RectangleABC(Clone0) end;
  end;

  SquareABC=class(RectangleABC)
  protected
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
    constructor Create(x,y,w,h: integer; cl: ColorType); begin end;
  public
    constructor Create(x,y,w: integer; cl: ColorType); begin inherited Create(x,y,w,w,cl); end;
    procedure Scale(f: real); begin Width:=round(Width*f); end;
    function Clone: SquareABC; begin Result:=SquareABC(Clone0) end;
  end;

  RoundRectABC=class(BoundedObjectABC)
  private
    r: integer;
    procedure SetRadius(rr: integer);
//    constructor Create(x,y,w,h: integer; cl: ColorType); begin end;
  public
    constructor Create(x,y,w,h,rr: integer; cl: ColorType); begin r:=rr; inherited Create(x,y,w,h,cl); end;
    constructor CreateBy(r: RoundRectABC);
    procedure Draw(x,y: integer);
    property Radius: integer read r write SetRadius;
    function Clone0: ObjectABC; begin Result:=RoundRectABC.CreateBy(Self); end;
    function Clone: RoundRectABC; begin Result:=RoundRectABC(Clone0) end;
  end;

  RoundSquareABC=class(RoundRectABC)
  protected
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
  public
    constructor Create(x,y,w,r: integer; cl: ColorType); begin inherited Create(x,y,w,w,r,cl); end;
    procedure Scale(f: real); begin Width:=round(Width*f); end;
    function Clone: RoundSquareABC; begin Result:=RoundSquareABC(Clone0) end;
  end;
  
  EllipseABC=class(BoundedObjectABC)
    function PtInside(x,y:integer): boolean;
    procedure Draw(x,y: integer);
    function Clone0: ObjectABC; begin Result:=EllipseABC.CreateBy(Self) end;
    function Clone: EllipseABC; begin Result:=EllipseABC(Clone0) end;
  end;

  CircleABC=class(EllipseABC)
  public
    constructor Create(x,y,w: integer; cl: ColorType); begin inherited Create(x,y,w,w,cl); end;
    function Clone0: ObjectABC; begin Result:=CircleABC.CreateBy(Self) end;
    procedure scale(f: real); begin Width:=round(Width*f); end;
    function Clone: CircleABC; begin Result:=CircleABC(Clone0) end;
  private
    constructor Create(x,y,w,h: integer; cl: ColorType); begin end; // �������� ����� ������ ���� ������������
  protected
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
  end;
  
  UIElementABC=class(RectangleABC)
  end;

  TextABC=class(ObjectABC)
  private
    fname: string;
    pointsz: integer;
    tb: boolean;
    bc: integer;
  protected
    function GetText: string;
    procedure SetFName(fn: string);
    procedure SetFSize(sz: integer);
    procedure SetTB(b: boolean);
    procedure SetBC(c: integer);
  public
    procedure SetText(t: string);
    constructor Create(x,y,pt: integer; cl: ColorType; tx: string);
    procedure Draw(x,y: integer);
    property Text: string read GetText write SetText;
    property FontName: string read fname write SetFName;
    property FontSize: integer read pointsz write SetFSize;
    property TransparentBackground: boolean read tb write SetTB;
    property BackgroundColor: integer read bc write SetBC;
    constructor CreateBy(c: TextABC);
    function Clone0: ObjectABC; begin Result:=TextABC.CreateBy(Self) end;
    function Clone: TextABC; begin Result:=TextABC(Clone0) end;
  end;

  RegularPolygonABC=class(BoundedObjectABC)
  private
    n: integer;
    angl: real;
  protected
    a: ^array [1..MaxInt] of Point;
  protected
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
    procedure SetR(r: integer);
    procedure SetCount(c: integer);
    procedure SetAngle(a: real);
    function GetR: integer;
    function GetCount: integer; begin Result:=n; end;
  public
    constructor Create(x,y,r,nn: integer; cl: ColorType);
    destructor Destroy;
    begin
      inherited Destroy;
      FreeMem(a);
    end;
    procedure Draw(x,y: integer);
    function PtInside(x,y: integer): boolean;
    property Count: integer read GetCount write SetCount;
    property Radius: integer read GetR write SetR;
    property Angle: real read angl write SetAngle;
    procedure Scale(f: real); begin Width:=round(Width*f); end;
    constructor CreateBy(c: RegularPolygonABC);
    function Clone0: ObjectABC; begin Result:=RegularPolygonABC.CreateBy(Self); end;
    function Clone: RegularPolygonABC; begin Result:=RegularPolygonABC(Clone0) end;
  end;

  StarABC=class(RegularPolygonABC)
  private
    r_rr: real;
  protected
    function GetRR: integer;
    procedure SetRR(r1: integer);
    constructor Create(x,y,r,nn: integer; cl: ColorType); begin end;
    function GetCount: integer; begin Result:=inherited GetCount div 2; end;
    procedure SetCount(c: integer); begin inherited SetCount(c*2); end;
  public
    constructor Create(x,y,r,r1,nn: integer; cl: ColorType);
    procedure Draw(x,y: integer);
    property InternalRadius: integer read GetRR write SetRR;
    function PtInside(x,y: integer): boolean;
    procedure scale(f: real); begin Width:=round(Width*f); end;
    constructor CreateBy(z: StarABC); begin r_rr:=z.Radius/z.InternalRadius; inherited CreateBy(z); end;
    function Clone0: ObjectABC; begin Result:=StarABC.CreateBy(Self) end;
    function Clone: StarABC; begin Result:=StarABC(Clone0) end;
  end;

  PictureABC=class(ObjectABC)
  protected
    p: Picture;
    OwnsPic: boolean;
//    t: boolean;      // ������������
    sx,sy: real;     // ������� �������� �� ���� OX � OY; 1 - �������� ������; -1 - ���������� �����������
  private
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
    procedure SetTransparent(tt: boolean);
    function GetTransparent: boolean;
    procedure SetScaleX(ssx: real);
    procedure SetScaleY(ssy: real);
  protected
    procedure DrawAfterChangePicture(var oldRect: Rect);
  public
    constructor Create(x,y,w,h: integer; fname: string); // ��� wmf
    constructor Create(x,y: integer; fname: string);
    constructor CreateFromPic(x,y: integer; p: Picture);
    destructor Destroy;
    procedure Draw(x,y: integer);
    procedure ChangePicture(fname: string);
    procedure Save(fname: string);
    procedure FlipVertical;
    procedure FlipHorizontal;
    function Clone0: ObjectABC; begin Result:=PictureABC.CreateBy(Self) end;
    property Transparent: boolean read GetTransparent write SetTransparent;
    property ScaleX: real read sx write SetScaleX;
    property ScaleY: real read sy write SetScaleY;
    function Clone: PictureABC; begin Result:=PictureABC(Clone0) end;
  end;
  
  MultiPictureABC=class(PictureABC)
  protected
    cur,cnt: integer;
    procedure SetCurrentPicture(i: integer);
  public
    constructor Create(x,y: integer; fname: string);
    constructor Create(x,y,w: integer; fname: string);
    constructor CreateFromPic(x,y,w: integer; p1: Picture);
    procedure Add(fname: string);
    procedure Draw(x,y: integer);
    procedure ChangePicture(fname: string);
    procedure ChangePicture(w: integer; fname: string);
    procedure NextPicture;
    procedure PrevPicture;
    property CurrentPicture: integer read cur write SetCurrentPicture;
    property Count: integer read cnt;
  end;

  BoardABC=class(BoundedObjectABC)
  private
    n,m,sz: integer;
  protected
    constructor Create(x,y,w,h,c: integer); begin end;
  public
    constructor Create(x,y,nn,mm,ssz: integer; cl: ColorType);
    procedure Draw(x,y: integer);
    property DimY: integer read n;
    property DimX: integer read m;
    property CellSize: integer read sz;
    function Clone0: ObjectABC; begin Result:=BoardABC.CreateBy(Self) end;
    function Clone: BoardABC; begin Result:=BoardABC(Clone0) end;
  end;

  ContainerABC=class(ObjectABC)
  protected
    l: ObjectArray;
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
    function GetCount: integer; begin Result:=l.Count; end;
  private
    procedure SetItem(i: integer; o: ObjectABC);
    begin
      l[i]:=o;
    end;
    function GetItem(i: integer): ObjectABC;
    begin
      Result:=ObjectABC(l[i])
    end;
  public
    constructor Create(x,y: integer); begin l:=ObjectArray.Create; inherited Create(x,y,0,0,clWhite); end;
    destructor Destroy; begin inherited Destroy; l.Destroy; end;
    constructor CreateBy(f: ContainerABC);
    procedure Add(g: ObjectABC);
    procedure UnLink(g: ObjectABC);
    procedure Draw(x,y: integer);
    procedure RecalcBounds;
    function PtInside(x,y:integer): boolean;
    property Count: integer read GetCount;
    property Objects[i: integer]: ObjectABC read GetItem write SetItem;
    function Clone0: ObjectABC; begin Result:=ContainerABC.CreateBy(Self) end;
    function Clone: ContainerABC; begin Result:=ContainerABC(Clone0) end;
  end;

  ObjectsABCArray=class
    l: ObjectArray;
    function GetCount: integer;
    begin
      Result:=l.Count;
    end;
    constructor Create(ll: ObjectArray);
    begin
      l:=ll;
    end;
    procedure SetItem(i: integer; o: ObjectABC);
    begin
      l[i]:=o;
    end;
    function GetItem(i: integer): ObjectABC;
    begin
      Result:=ObjectABC(l[i])
    end;
    property Items[i: integer]: ObjectABC read GetItem write SetItem;
    property Count: integer read GetCount;
  end;

function CreateRectangleABC(x,y,w,h: integer; cl: ColorType): RectangleABC;
function CreateRoundRectABC(x,y,w,h,r: integer; cl: ColorType): RoundRectABC;
function CreateSquareABC(x,y,w: integer; cl: ColorType): SquareABC;
function CreateRoundSquareABC(x,y,w,r: integer; cl: ColorType): RoundSquareABC;
function CreateEllipseABC(x,y,w,h: integer; cl: ColorType): EllipseABC;
function CreateCircleABC(x,y,w: integer; cl: ColorType): CircleABC;
function CreateTextABC(x,y,pt: integer; cl: ColorType; tx: string): TextABC;
function CreateRegularPolygonABC(x,y,r,nn: integer; cl: ColorType): RegularPolygonABC;
function CreateStarABC(x,y,r,r1,nn: integer; cl: ColorType): StarABC;
function CreatePictureABC(x,y: integer; fname: string): PictureABC;
function CreateMultiPictureABC(x,y,w: integer; fname: string): MultiPictureABC;
function CreateMultiPictureABC(x,y: integer; fname: string): MultiPictureABC;
function CreateBoardABC(x,y,nn,mm,ssz: integer; cl: ColorType): BoardABC;
function CreateContainerABC(x,y: integer): ContainerABC;

function ObjectsCount: integer;
function ObjectUnderPoint(x,y: integer): ObjectABC;
function ObjectUnderPoint(p: Point): ObjectABC;
function ObjectsIntersects(g1,g2: ObjectABC): boolean;
procedure SwapPositions(o1,o2: ObjectABC);

procedure ToFront(grobj: ObjectABC);
procedure ToBack(grobj: ObjectABC);

procedure LockDrawingObjects;
procedure UnLockDrawingObjects;
procedure RedrawObjects;

procedure SetMouseDragABCObjects;

// ������������ �� GraphABC
function RGB(r,g,b: integer): integer;
function clRandom: integer;
function WindowWidth: integer;
function WindowHeight: integer;
procedure SetWindowWidth(w: integer);
procedure SetWindowHeight(h: integer);
procedure SetWindowCaption(s: string);
procedure SetWindowSize(w,h: integer);

// ������������ �� PointRect
function PointF(x,y: integer): Point;
function RectF(l,t,r,b: integer): Rect;

implementation

var
  __l: ObjectArray;
  __lUI: ObjectArray; // �������� ����������������� ����������
  __LockDrawingObjects: boolean;
  Objects: ObjectsABCArray;
  movingObject: ObjectABC;
  ABC_dx,ABC_dy: integer;
  CurrentDrawingBitmap: integer;
  
//------ ������������ ������
procedure InitString(var d: dynstring);
begin
  d.p:=nil;
  d.sz:=0;
end;

procedure AssignString(var d: dynstring; s: string);
var l: integer;
begin
  l:=Length(s);
  if l+1>d.sz then
  begin
    if d.p<>nil then
      FreeMem(d.p);
    GetMem(d.p,l+1);
    d.sz:=l+1;
  end;
  CopyMem(@s,d.p,l+1);
end;

procedure DisposeString(var d: dynstring);
begin
  if d.p<>nil then
    FreeMem(d.p);
  d.sz:=0;
end;

function StringValue(var d: dynstring): string;
begin
  if d.p=nil then
    Result:=''
  else Result:=d.p^;
end;

//------ ���������� ���������  � �������
procedure SwapPositions(o1,o2: ObjectABC);
var p: Point;
begin
  p:=o1.Position;
  o1.Position:=o2.Position;
  o2.Position:=p;
end;

function RGB(r,g,b: integer): integer;
begin
  Result:=GraphABC.RGB(r,g,b);
end;

function clRandom: integer;
begin
  Result:=GraphABC.clRandom;
end;

function WindowWidth: integer;
begin
  Result:=GraphABC.WindowWidth;
end;

function WindowHeight: integer;
begin
  Result:=GraphABC.WindowHeight;
end;

procedure SetWindowWidth(w: integer);
begin
  GraphABC.SetWindowWidth(w)
end;

procedure SetWindowHeight(h: integer);
begin
  GraphABC.SetWindowHeight(h)
end;

procedure SetWindowCaption(s: string);
begin
  GraphABC.SetWindowCaption(s)
end;

procedure SetWindowSize(w,h: integer);
begin
  GraphABC.SetWindowSize(w,h)
end;

function min(a,b: integer): integer;
begin
  if a<b then Result:=a
  else Result:=b
end;

function max(a,b: integer): integer;
begin
  if a>b then Result:=a
  else Result:=b
end;

function min(a,b: real): real;
begin
  if a<b then Result:=a
  else Result:=b
end;

function max(a,b: real): real;
begin
  if a>b then Result:=a
  else Result:=b
end;

function UnionRect(r1,r2: Rect): Rect;
begin
  Result.Left:=min(r1.Left,r2.Left);
  Result.Right:=max(r1.Right,r2.Right);
  Result.Top:=min(r1.Top,r2.Top);
  Result.Bottom:=max(r1.Bottom,r2.Bottom);
end;

function IntRect(r1,r2: Rect): Rect;
begin
  Result.Left:=max(r1.Left,r2.Left);
  Result.Right:=min(r1.Right,r2.Right);
  Result.Top:=max(r1.Top,r2.Top);
  Result.Bottom:=min(r1.Bottom,r2.Bottom);
end;

procedure drawRect(r: Rect);
// �������������� ��������� (�� ���������� �������������). �������� ����������� ����������� ��������������.
var
  b: integer;
  i: integer;
  g: ObjectABC;
begin
  b:=CreatePictureFromScreenBufferRect(r);
  SetDrawingSurface(b);
  CurrentDrawingBitmap:=b;
//  CopyScreenBufferToPicture(b,r);
  for i:=1 to __l.Size do
  begin
    g:=ObjectABC(__l[i]);
    if g.Visible and g.IntersectRect(r) then
      g.Draw(g.Left-r.Left,g.Top-r.Top);
  end;
  for i:=1 to __lUI.Size do
  begin
    g:=ObjectABC(__lUI[i]);
    if g.Visible and g.IntersectRect(r) then
      g.Draw(g.Left-r.Left,g.Top-r.Top);
  end;
  RestoreDrawingSurface;
  CurrentDrawingBitmap:=0;
  LockScreenBuffer;
  DrawPicture(b,r.Left,r.Top);
  UnLockScreenBuffer;
  DestroyPicture(b);
end;

function ObjectsIntersects(g1,g2: ObjectABC): boolean;
var
  b1,b2: integer;
  r: Rect;
begin
  Result:=g1.IntersectRect(g2.Bounds);
  if Result=False then exit;
  r:=IntRect(g1.Bounds,g2.Bounds);

  b1:=CreatePicture(r.Right-r.Left,r.Bottom-r.Top);
  SetDrawingSurface(b1);
  g1.Draw(g1.Left-r.Left,g1.Top-r.Top);

  b2:=CreatePicture(r.Right-r.Left,r.Bottom-r.Top);
  SetDrawingSurface(b2);
  g2.Draw(g2.Left-r.Left,g2.Top-r.Top);

  Result:=ImageIntersect(b1,b2);

  DestroyPicture(b1);
  DestroyPicture(b2);
  RestoreDrawingSurface;
end;

//------ ObjectABC ------
constructor ObjectABC.Create(x,y,w,h: integer; cl: ColorType);
begin
  MouseIndifferent:=False;
  ow:=nil;
  fx:=x; fy:=y;
  fw:=w; fh:=h;
  col:=cl;
  vis:=True;
  vistxt:=true;
  InitString(txt);
  txtscale:=0.8;
  if Self is UIElementABC then
    __lUI.Add(Self)
  else __l.Add(Self);
  InternalDraw;
end;

constructor ObjectABC.CreateBy(g: ObjectABC);
begin
  InitString(txt);
  Left:=g.Left;
  Top:=g.Top;
  Width:=g.Width;
  Height:=g.Height;
  Color:=g.Color;
  TextVisible:=g.TextVisible;
  Text:=g.Text;
  TextScale:=g.TextScale;
  if Self is UIElementABC then
    __lUI.Add(Self)
  else __l.Add(Self);
  Visible:=g.Visible;
  Owner:=g.Owner;
end;

procedure ObjectABC.DrawText(x,y: integer);
var
  bs,tw,th,fs,d: integer;
  m: real;
begin
  if not TextVisible or (txt.p=nil) or (txt.p^='') then
    exit;
  bs:=BrushStyle;
  SetBrushStyle(bsClear);
  SetFontColor(clBlack);
  SetFontName('Arial');
  SetFontSize(100);
  tw:=TextWidth(txt.p^);
  th:=TextHeight(txt.p^);
//  m:=max(tw/Width,th/Height);
  m:=th/Height;
  fs:=round(txtscale/m*100);
  if fs<1 then fs:=1;
  SetFontSize(fs);
  
  tw:=TextWidth(txt.p^);
  th:=TextHeight(txt.p^);
  if fw>50 then
    d:=10
  else if fw>20 then
    d:=5
  else d:=0;
  if tw>Width-d then
  begin
    fs:=round(fs*(Width-(Height-th)*2)/tw);
    SetFontSize(fs);
    tw:=TextWidth(txt.p^);
    th:=TextHeight(txt.p^);
  end;
  TextOut(x+(Width-tw) div 2,y+(Height-th) div 2,txt.p^);
  SetBrushStyle(bs);
end;

procedure ObjectABC.DrawAfterChangeBounds(oldBounds,newBounds: Rect);
// �������������� ��������� (�� ���������� �������������). �������� ����������� ����� ��������� �������� ����� ������.
var l,t: integer;
begin
  if __LockDrawingObjects then Exit;
  l:=0; t:=0;
  if Owner<>nil then
  begin
    CalcOwnerOffset(l,t);
    Owner.RecalcBounds;
  end;
  OffsetRect(oldBounds,l,t);
  OffsetRect(newBounds,l,t);
  if PointRect.IntersectRect(oldBounds,newBounds) then
    drawRect(UnionRect(oldBounds,newBounds))
  else
  begin
    drawRect(newBounds);
    drawRect(oldBounds)
  end;
end;

procedure ObjectABC.SetCenter(p: Point);
begin
  MoveTo(p.x-fw div 2,p.y-fh div 2);
end;

function ObjectABC.GetCenter: Point;
begin
  Result.x := fx + fw div 2;
  Result.y := fy + fh div 2;
end;

procedure ObjectABC.SetPosition(p: Point);
begin
  MoveTo(p.x,p.y);
end;

function ObjectABC.GetPosition: Point;
begin
  Result.x := fx;
  Result.y := fy;
end;

procedure ObjectABC.SetX(x: integer);
begin
  moveto(x,fy);
end;

procedure ObjectABC.SetY(y: integer);
begin
  moveto(fx,y);
end;

procedure ObjectABC.SetVis(v: boolean);
begin
  if vis=v then Exit;
  vis:=v;
  Redraw;
end;

procedure ObjectABC.SetVisTxt(b: boolean);
begin
  if vistxt=b then Exit;
  vistxt:=b;
  Redraw;
end;

procedure ObjectABC.SetWidth(Width: integer);
var r: Rect;
begin
  if Width<1 then Exit;
  if fw=Width then Exit;
  r:=Bounds;
  fw:=Width;
  if not __LockDrawingObjects then
    DrawAfterChangeBounds(r,Bounds)
end;

procedure ObjectABC.SetHeight(Height: integer);
var r: Rect;
begin
  if Height<1 then Exit;
  if fh=Height then Exit;
  r:=Bounds;
  fh:=Height;
  if not __LockDrawingObjects then
    DrawAfterChangeBounds(r,Bounds)
end;

procedure ObjectABC.SetColor(cl: ColorType);
begin
  if col=cl then Exit;
  col:=cl;
  Redraw;
end;

procedure ObjectABC.SetNum(n: integer);
begin
  Text:=IntToStr(n);
end;

procedure ObjectABC.SetRealNum(r: real);
var s: string;
begin
  Str(r:0:1,s);
  Text:=s;
end;

function ObjectABC.GetNum: integer;
var err: integer;
begin
  Val(Text,Result,err);
  if err<>0 then
    Result:=0;
end;

function ObjectABC.GetRealNum: real;
var err: integer;
begin
  Val(Text,Result,err);
  if err<>0 then
    Result:=0;
end;

procedure ObjectABC.SetTextScale(r: real);
begin
  if txtscale=r then Exit;
  if r<0.01 then r:=0.01 else
  if r>1 then r:=1;
  txtscale:=r;
  Redraw;
end;

procedure ObjectABC.SetOwner(o: ContainerABC);
begin
  if ow<>nil then
    ow.Unlink(Self);
  ow:=nil;
  if o<>nil then
    o.Add(Self)
end;

procedure ObjectABC.InternalDraw;
begin
  LockScreenBuffer;
  if not __LockDrawingObjects then
    Draw(fx,fy);
  UnLockScreenBuffer;
end;

procedure ObjectABC.CalcOwnerOffset(var l,t: integer);
var
  o: ContainerABC;
begin
  l:=0; t:=0;
  o:=Owner;
  while o<>nil do
  begin
    l:=l+o.Left;
    t:=t+o.Top;
    o:=o.Owner;
  end;
end;

procedure ObjectABC.Redraw;
var
  l,t: integer;
  r: Rect;
begin
  if not __LockDrawingObjects then
  begin
    CalcOwnerOffset(l,t);
    r:=Bounds;
    OffsetRect(r,l,t);
    drawRect(r);
  end;
end;

procedure ObjectABC.Moveto(x,y: integer);
var r,r1: Rect;
begin
  if (fx=x) and (fy=y) then Exit;
  r:=Bounds;
  setCoords(x,y);
  r1:=Bounds;
  DrawAfterChangeBounds(r,r1)
end;

procedure ObjectABC.Scale(f: real);
begin
  Assert(f>0,'���������� �����������<0');
  Width:=round(Width*f);
  Height:=round(Height*f);
end;

procedure ObjectABC.ToFront;
begin
  ABCObjects.ToFront(Self);
end;

procedure ObjectABC.ToBack;
begin
  ABCObjects.ToBack(Self);
end;

function ObjectABC.Bounds: Rect;
begin
  Result:=RectF(fx,fy,fx+fw,fy+fh);
end;

function ObjectABC.IntersectRect(r: Rect): boolean;
begin
  Result:=PointRect.IntersectRect(Bounds,r);
end;

function ObjectABC.PtInside(x,y:integer): boolean;
begin
  Result:=PtInRect(Bounds,PointF(x,y));
end;

function ObjectABC.Clone0: ObjectABC;
begin
  Result:=nil;
  writeln('������ - ������� ObjectABC.Clone0');
end;

function ObjectABC.Intersect(g: ObjectABC): boolean;
begin
  Result:=ObjectsIntersects(Self,g);
end;

destructor ObjectABC.Destroy;
begin
  Visible:=False;
  DisposeString(txt);
  if Owner<>nil then Exit;
//  Owner:=nil;
  if Self is UIElementABC then
  begin
    __lUI.OwnsObjects:=False;
    __lUI.Remove(Self);
    __lUI.OwnsObjects:=True;
  end
  else
  begin
    __l.OwnsObjects:=False;
    __l.Remove(Self);
    __l.OwnsObjects:=True;
  end;
end;

procedure ObjectABC.SetText(t: string);
begin
  if (txt.p<>nil) and (txt.p^=t) then
    exit;
  AssignString(txt,t);
  Redraw;
end;

function ObjectABC.GetText: string;
begin
  if txt.p=nil then
    Result:=''
  else Result:=txt.p^;
// else CopyMem(txt.p,@Result,Length(txt.p^)+1);
end;

//------ BoundedObjectABC ------
constructor BoundedObjectABC.Create(x,y,w,h: integer);
begin
  Create(x,y,w,h,clWhite);
end;

constructor BoundedObjectABC.Create(x,y,w,h: integer; cl: ColorType);
begin
  bcol:=clBlack;
  bw:=1;
  fil:=True;
  bor:=True;
  inherited Create(x,y,w,h,cl);
end;

constructor BoundedObjectABC.CreateBy(g: BoundedObjectABC);
begin
  BorderColor:=g.BorderColor;
  BorderWidth:=g.BorderWidth;
  Filled:=g.Filled;
  Bordered:=g.Bordered;
  inherited CreateBy(g);
end;

procedure BoundedObjectABC.SetBColor(cl: ColorType);
begin
  if bcol=cl then Exit;
  bcol:=cl;
  Redraw;
end;

procedure BoundedObjectABC.SetBW(w: integer);
begin
  if bw=w then Exit;
  bw:=w;
  Redraw;
end;

procedure BoundedObjectABC.SetFilled(f: boolean);
begin
  if fil=f then Exit;
  fil:=f;
  Redraw;
end;

procedure BoundedObjectABC.SetBordered(b: boolean);
begin
  if bor=b then Exit;
  bor:=b;
  Redraw;
end;

procedure BoundedObjectABC.SetDrawSettings;
begin
  SetBrushColor(Color);
  if Filled then
    SetBrushStyle(bsSolid)
  else SetBrushStyle(bsClear);
  if Bordered then
    SetPenStyle(psSolid)
  else SetPenStyle(psClear);
  SetPenColor(BorderColor);
  SetPenWidth(BorderWidth);
end;


//------ RectangleABC ------
procedure RectangleABC.Draw(x,y: integer);
var z,z1: integer;
begin
  SetDrawSettings;
  z:=BorderWidth div 2;
  z1:=(BorderWidth-1) div 2;
  Rectangle(x+z,y+z,x+Width-z1,y+Height-z1);
  DrawText(x,y);
end;

//------ RoundRectABC ------
constructor RoundRectABC.CreateBy(r: RoundRectABC);
begin
  Radius:=r.Radius;
  inherited CreateBy(r);
end;

procedure RoundRectABC.SetRadius(rr: integer);
begin
  if rr=r then Exit;
  r:=rr;
  Redraw;
end;

procedure RoundRectABC.Draw(x,y: integer);
var z,z1: integer;
begin
  z:=BorderWidth div 2;
  z1:=(BorderWidth-1) div 2;
  SetDrawSettings;
  RoundRect(x+z,y+z,x+Width-z1,y+Height-z1,r,r);
  DrawText(x,y);
end;

//------ SquareABC ------
procedure SquareABC.SetWidth(Width: integer);
begin
  inherited SetWidth(Width);
  inherited SetHeight(Width);
end;

procedure SquareABC.SetHeight(Height: integer);
begin
  SetWidth(Height);
end;

//------ RoundSquareABC ------
procedure RoundSquareABC.SetWidth(Width: integer);
begin
  inherited SetWidth(Width);
  inherited SetHeight(Width);
end;

procedure RoundSquareABC.SetHeight(Height: integer);
begin
  SetWidth(Height);
end;

//------ EllipseABC ------
function EllipseABC.PtInside(x,y:integer): boolean;
var a,b,x0,y0,dx,dy: integer;
begin
  a:=Width div 2;
  b:=Height div 2;
  x0:=Left+a;
  y0:=Top+b;
  dx:=x-x0;
  dy:=y-y0;
  Result:=(dx*dx/a/a+dy*dy/b/b)<=1;
end;

procedure EllipseABC.Draw(x,y: integer);
var z,z1: integer;
begin
  z:=BorderWidth div 2;
  z1:=(BorderWidth-1) div 2;
  SetDrawSettings;
  Ellipse(x+z,y+z,x+Width-z1,y+Height-z1);
  DrawText(x,y);
end;

//------ CircleABC ------
procedure CircleABC.SetWidth(Width: integer);
begin
  inherited SetWidth(Width);
  inherited SetHeight(Width);
end;

procedure CircleABC.SetHeight(Height: integer);
begin
  SetWidth(Height);
end;

//------ TextABC ------
constructor TextABC.Create(x,y,pt: integer; cl: ColorType; tx: string);
var w,h: integer;
begin
  tb:=True;
  bc:=clWhite;
//  txt:=tx;
  pointsz:=pt;
  fname:='Arial';
  SetFontName(fname);
  SetFontSize(pt);
  w:=TextWidth(tx);
  h:=TextHeight(tx);
  inherited Create(x,y,w,h,cl);
  Text:=tx;
end;

constructor TextABC.CreateBy(c: TextABC);
begin
  text:=c.Text;
  fname:=c.FontName;
  pointsz:=c.FontSize;
  tb:=c.TransparentBackground;
  bc:=c.BackgroundColor;
  inherited CreateBy(c);
  InternalDraw;
end;

procedure TextABC.Draw(x,y: integer);
var bs: integer;
begin
  SetBrushColor(bc);
  if tb then
  begin
    bs:=BrushStyle;
    SetBrushStyle(bsClear);
  end;
  SetFontColor(Color);
  SetFontName(fname);
  SetFontSize(pointsz);
  TextOut(x,y,Text);
  if tb then
    SetBrushStyle(bs);
end;

function TextABC.GetText: string;
begin
  Result:=inherited GetText;
end;

procedure TextABC.SetText(t: string);
var r: Rect;
begin
  if Text=t then
    exit;
  r:=Bounds;
  Width:=TextWidth(t);
  Height:=TextHeight(t);
  inherited SetText(t);
  if not __LockDrawingObjects then
    DrawAfterChangeBounds(r,Bounds)
end;

procedure TextABC.SetFName(fn: string);
begin
  if fname=fn then
    exit;
  fname:=fn;
  SetFontName(fname);
  SetFontSize(pointsz);
  Width:=TextWidth(Text);
  Height:=TextHeight(Text);
  Redraw;
end;

procedure TextABC.SetFSize(sz: integer);
begin
  if pointsz=sz then Exit;
  pointsz:=sz;
  SetFontName(fname);
  SetFontSize(pointsz);
  Width:=TextWidth(Text);
  Height:=TextHeight(Text);
  Redraw;
end;

procedure TextABC.SetTB(b: boolean);
begin
  if tb=b then Exit;
  tb:=b;
  Redraw;
end;

procedure TextABC.SetBC(c: integer);
begin
  if bc=c then Exit;
  bc:=c;
  Redraw;
end;

//------ RegularPolygonABC ------
constructor RegularPolygonABC.Create(x,y,r,nn: integer; cl: ColorType);
begin
  n:=nn;                     // ����������� ����� ������� ������������ ������!
  GetMem(a,nn*sizeof(Point)); // ����������� ����� ������� ������������ ������!
  angl:=0;
  inherited Create(x-r,y-r,2*r+1,2*r+1,cl);
end;

constructor RegularPolygonABC.CreateBy(c: RegularPolygonABC);
begin
  Count:=c.Count;
  Angle:=c.Angle;
  GetMem(a,n*sizeof(Point));
  inherited CreateBy(c);
end;

procedure RegularPolygonABC.Draw(x,y: integer);
var
  i,r,z,x0,y0: integer;
  phi: real;
begin
  SetDrawSettings;
  phi:=-90+Angle;
  z:=BorderWidth div 2;
  r:=Width div 2;
  r:=r-z;
  x0:=x + Width div 2;
  y0:=y + Width div 2;
  for i:=1 to n do
  begin
    a^[i].x:=round(r*cos(phi*Pi/180))+x0;
    a^[i].y:=round(r*sin(phi*Pi/180))+y0;
    phi:=phi+360/n;
  end;
  Polygon(a^,n);
  DrawText(x,y);
end;

function RegularPolygonABC.PtInside(x,y:integer): boolean;
var
  a,x0,y0,dx,dy,pz: integer;
  phi: real;
  part: real;
begin
  part:=360/n;
  a:=Width div 2 + 1;
  x0:=Left+Width div 2;
  y0:=Top+Width div 2;
  dx:=x-x0;
  dy:=y-y0;
  if dx=0 then
  begin
    if dy<0 then phi:=0
    else phi:=180;
  end
  else
  begin
    phi:=90+arctan(dy/dx)*180/Pi;
    if dx<0 then phi:=phi+180;
  end;
  pz:=trunc(phi/part);
  phi:=phi-pz*part;
  if phi>part/2 then phi:=part-phi;
  Phi:=part/2-Phi;
  Result:=sqrt(dx*dx+dy*dy)<a*cos(part/2/180*Pi)/cos(phi/180*Pi);
end;

procedure RegularPolygonABC.SetAngle(a: real);
begin
  if angl=a then Exit;
  angl:=a;
  Redraw;
end;

procedure RegularPolygonABC.SetWidth(Width: integer);
begin
  if Width mod 2=0 then Width:=Width-1; // ������ � ������ ������ ���� ���������
  inherited SetWidth(Width);
  inherited SetHeight(Width);
end;

procedure RegularPolygonABC.SetHeight(Height: integer);
begin
  SetWidth(Height);
end;

procedure RegularPolygonABC.SetCount(c: integer);
var r: Rect;
begin
  if c<3 then c:=3;
  if c>500 then c:=500;
  if n=c then Exit;
  FreeMem(a);
  n:=c;
  GetMem(a,n*sizeof(Point));
  Redraw;
end;

procedure RegularPolygonABC.SetR(r: integer);
begin
  if Width=r*2+1 then Exit;
  Width:=r*2+1;
end;

function RegularPolygonABC.GetR: integer;
begin
  Result:=Width div 2;
end;

//------ StarABC ------
constructor StarABC.Create(x,y,r,r1,nn: integer; cl: ColorType);
var rr: integer;
begin
  if r<r1 then
  begin
    rr:=r;
    r:=r1;
    r1:=rr;
  end;
  r_rr:=r/r1;
  inherited Create(x,y,r,2*nn,cl);
end;

function StarABC.GetRR: integer;
begin
  Result:=round(Radius/r_rr);
end;

procedure StarABC.SetRR(r1: integer);
begin
  if r1>Radius then Exit;
  if r_rr=Radius/r1 then Exit;
  r_rr:=Radius/r1;
  Redraw;
end;

procedure StarABC.Draw(x,y: integer);
var
  i,r,rr,z,x0,y0: integer;
  phi: real;
begin
  SetDrawSettings;
  phi:=-90+Angle;
  z:=BorderWidth;
  r:=Width div 2;
  r:=r-z;
  x0:=x + Width div 2;
  y0:=y + Width div 2;
  rr:=round(r/r_rr);
  for i:=1 to Count*2 do
  begin
    if i mod 2 = 1 then
    begin
      a^[i].x:=round(r*cos(phi*Pi/180))+x0;
      a^[i].y:=round(r*sin(phi*Pi/180))+y0;
    end
    else
    begin
      a^[i].x:=round(rr*cos(phi*Pi/180))+x0;
      a^[i].y:=round(rr*sin(phi*Pi/180))+y0;
    end;
    phi:=phi+360/Count/2;
  end;
  Polygon(a^,Count*2);
  DrawText(x,y);
end;

function StarABC.PtInside(x,y: integer): boolean;
var
  a,x0,y0,pz: integer;
  phi: real;
  part,dx,dy,b,c,r: real;
begin
  part:=360/Count;
  a:=Width div 2 + 1;
  x0:=Left+Width div 2;
  y0:=Top+Width div 2;
  dx:=x-x0;
  dy:=y-y0;
  if dx=0 then
  begin
    if dy<0 then phi:=0
    else phi:=180;
  end
  else
  begin
    phi:=90+arctan(dy/dx)*180/Pi;
    if dx<0 then phi:=phi+180;
  end;
  pz:=trunc(phi/part);
  phi:=phi-pz*part;
  if phi>part/2 then phi:=part-phi;
  b:=(InternalRadius+1)*cos(part/2*Pi/180);
  c:=(InternalRadius+1)*sin(part/2*Pi/180);
  r:=sqrt(dx*dx+dy*dy);
  dx:=r*cos(phi*Pi/180);
  dy:=r*sin(phi*Pi/180);
  Result:=c*(dx-a)<dy*(b-a);
end;

//------ BoardABC ------
constructor BoardABC.Create(x,y,nn,mm,ssz: integer; cl: ColorType);
begin
  n:=nn;
  m:=mm;
  sz:=ssz;
  BorderColor:=cl;
  inherited Create(x,y,m*sz+1,n*sz+1,clWhite);
end;

procedure BoardABC.Draw(x,y: integer);
var i: integer;
begin
  SetDrawSettings;
  FillRect(x,y,x+Width,y+Height);
  for i:=0 to m do
    Line(x+i*sz,y,x+i*sz,y+Height);
  for i:=0 to n do
    Line(x,y+i*sz,x+Width,y+i*sz);
end;

//------ PictureABC ------
constructor PictureABC.Create(x,y: integer; fname: string);
begin
  OwnsPic:=True;
  sx:=1; sy:=1;
  p:=Picture.Create(fname);
  p.Transparent:=True;
  inherited Create(x,y,p.Width,p.Height,clBlack);
end;

constructor PictureABC.Create(x,y,w,h: integer; fname: string);
begin
  OwnsPic:=True;
  sx:=1; sy:=1;
  p:=Picture.Create(fname);
  p.Transparent:=True;
  p.SetSize(w,h);
  inherited Create(x,y,w,h,clBlack);
end;

constructor PictureABC.CreateFromPic(x,y: integer; p: Picture);
begin
  OwnsPic:=False;
  sx:=1; sy:=1;
  Self.p:=p;
  inherited Create(x,y,p.Width,p.Height,clBlack);
end;


procedure PictureABC.DrawAfterChangePicture(var oldRect: Rect);
begin
  fw:=round(abs(sx)*p.Width)+1;
  fh:=round(abs(sy)*p.Height)+1;
  if not __LockDrawingObjects then
    DrawAfterChangeBounds(oldRect,Bounds)
end;

procedure PictureABC.ChangePicture(fname: string);
var r: Rect;
begin
  r:=Bounds;
  p.Load(fname);
  p.Transparent:=True;
  DrawAfterChangePicture(r);
end;

destructor PictureABC.Destroy;
begin
  inherited Destroy;
  if OwnsPic then
    p.Destroy;
end;

procedure PictureABC.Draw(x,y: integer);
var bw,bh,x1,y1,x2,y2: integer;
begin
  if (sx=1) and (sy=1) then
    p.Draw(x,y)
  else
  begin
    bw:=p.Width;
    bh:=p.Height;
    if sx<0 then
      x:=x-round(bw*sx);
    if sy<0 then
      y:=y-round(bh*sy);
    p.Draw(x,y,round(bw*sx),round(bh*sy));
  end;
  DrawText(x,y);
end;

procedure PictureABC.SetWidth(Width: integer);
begin
  if Width<=0 then exit;
  if sx>0 then
    sx:=Width/p.Width
  else sx:=-Width/p.Width;
  inherited SetWidth(Width);
end;

procedure PictureABC.SetHeight(Height: integer);
begin
  if Height<=0 then exit;
  if sy>0 then
    sy:=Height/p.Height
  else sy:=-Height/p.Height;
  inherited SetHeight(Height);
end;

procedure PictureABC.SetTransparent(tt: boolean);
begin
  if p.Transparent=tt then Exit;
  p.Transparent:=tt;
  Redraw;
end;

function PictureABC.GetTransparent: boolean;
begin
  Result:=p.Transparent;
end;

procedure PictureABC.SetScaleX(ssx: real);
var r: Rect;
begin
  if sx=ssx then exit;
  r:=Bounds;
  fw:=round(abs(ssx)*p.Width)+1;
  sx:=ssx;
  if not __LockDrawingObjects then
    DrawAfterChangeBounds(r,Bounds)
end;

procedure PictureABC.SetScaleY(ssy: real);
var r: Rect;
begin
  if sy=ssy then exit;
  r:=Bounds;
  fh:=round(abs(ssy)*p.Height)+1;
  sy:=ssy;
  if not __LockDrawingObjects then
    DrawAfterChangeBounds(r,Bounds)
end;

procedure PictureABC.Save(fname: string);
begin
  p.Save(fname);
end;

procedure PictureABC.FlipVertical;
begin
  p.FlipVertical;
  Redraw;
end;

procedure PictureABC.FlipHorizontal;
begin
  p.FlipHorizontal;
  Redraw;
end;

//------ MultiPictureABC ------
constructor MultiPictureABC.Create(x,y: integer; fname: string);
begin
  cur:=1;
  cnt:=1;
  inherited Create(x,y,fname);
end;

constructor MultiPictureABC.Create(x,y,w: integer; fname: string);
begin
  OwnsPic:=True;
  sx:=1; sy:=1;
  p:=Picture.Create(fname);
  p.Transparent:=True;
  if p.Width mod w <> 0 then
    raise Exception.Create('������� ������ ������ ��������');
  cur:=1;
  cnt:=p.Width div w;
  inherited Create(x,y,w,p.Height,clBlack);
end;

constructor MultiPictureABC.CreateFromPic(x,y,w: integer; p1: Picture);
begin
  OwnsPic:=False;
  sx:=1; sy:=1;
  p:=p1;
  if p.Width mod w <> 0 then
    raise Exception.Create('������� ������ ������ ��������');
  cur:=1;
  cnt:=p.Width div w;
  inherited Create(x,y,w,p.Height,clBlack);
end;

procedure MultiPictureABC.Draw(x,y: integer);
begin
  p.Draw(x,y,RectF((cur-1)*Width,0,cur*Width,Height));
end;

procedure MultiPictureABC.ChangePicture(fname: string);
begin
  p.Load(fname);
  cur:=1;
  cnt:=1;
  Width:=p.Width;
  Height:=p.Height;
  Redraw;
end;

procedure MultiPictureABC.ChangePicture(w: integer; fname: string);
begin
  p.Load(fname);
  if p.Width mod w <> 0 then
    raise Exception.Create('������� ������ ������ ��������');
  cur:=1;
  cnt:=p.Width div w;
  Width:=w;
  Height:=p.Height;
  Redraw;
end;

procedure MultiPictureABC.Add(fname: string);
var
  p1: Picture;
  r,r1: Rect;
begin
  Inc(cnt);
  p1:=Picture.Create(fname);
  if (p1.Width<>Width) or (p1.Height<>Height) then
    raise Exception.Create('������� �������� � ����� '+fname+' ���������� �� �������� MultiPictureABC');
  p1.Transparent:=Transparent;
  p.Width:=p.Width+Width;
  r1:=RectF(0,0,Width,Height);
  r:=RectF((cnt-1)*Width,0,cnt*Width,Height);
  p.CopyRect(r,p1,r1);
  p1.Destroy;
end;

procedure MultiPictureABC.SetCurrentPicture(i: integer);
begin
  if i<1 then
    i:=1;
  if i>Count then
    i:=Count;
  cur:=i;
  Redraw;
end;

procedure MultiPictureABC.NextPicture;
begin
  if cur>=cnt then
    CurrentPicture:=1
  else CurrentPicture:=cur+1
end;

procedure MultiPictureABC.PrevPicture;
begin
  if cur=1 then
    CurrentPicture:=cnt
  else CurrentPicture:=cur-1
end;

//------ ContainerABC ------
constructor ContainerABC.CreateBy(f: ContainerABC);
var
  i: integer;
  g: ObjectABC;
begin
  l:=ObjectArray.Create;
  inherited CreateBy(f);
  for i:=1 to f.Count do
  begin
    g:=f[i].Clone0;
    g.Owner:=Self;
  end;
end;

procedure ContainerABC.Add(g: ObjectABC);
var
  ind: integer;
  b: boolean;
begin
  if Self is UIElementABC then
    exit;
  __l.OwnsObjects:=False;
  b:=g.Visible;
  g.Visible:=False;
  __l.Remove(g);
  __l.OwnsObjects:=True;
  l.Add(g);
  g.Ow:=Self;
  g.Visible:=b;
  RecalcBounds;
  Redraw;
end;

procedure ContainerABC.Draw(x,y: integer);
var
  i: integer;
  g: ObjectABC;
begin
  for i:=1 to l.Count do
  begin
    g:=ObjectABC(l[i]);
    g.Draw(x+g.Left,y+g.Top);
  end;
end;

function ContainerABC.PtInside(x,y:integer): boolean;
var
  i: integer;
  g: ObjectABC;
begin
  Result:=False;
  for i:=1 to l.Count do
  begin
    g:=ObjectABC(l[i]);
    if g.PtInside(x-Left,y-Top) then
    begin
      Result:=True;
      Exit;
    end;
  end;
end;

procedure ContainerABC.SetWidth(Width: integer);
var
  i: integer;
  scale: real;
  g: ObjectABC;
begin
  if Self.Width=0 then Exit;
  scale:=Width/Self.Width;
  for i:=1 to l.Count do
  begin
    g:=ObjectABC(l[i]);
    g.Left:=round(g.Left*scale);
    g.Width:=round(g.Width*scale);
  end;
  inherited SetWidth(Width);
end;

procedure ContainerABC.SetHeight(Height: integer);
var
  i: integer;
  scale: real;
  g: ObjectABC;
begin
  if Self.Height=0 then Exit;
  scale:=Height/Self.Height;
  for i:=1 to l.Count do
  begin
    g:=ObjectABC(l[i]);
    g.Top:=round(g.Top*scale);
    g.Height:=round(g.Height*scale);
  end;
  inherited SetHeight(Height);
end;

procedure ContainerABC.RecalcBounds;
var
  i: integer;
  r: Rect;
begin
  if Count=0 then Exit;
  r:=ObjectABC(l[1]).Bounds;
  for i:=2 to l.Count do
    r:=UnionRect(r,ObjectABC(l[i]).Bounds);
  fx:=fx+r.Left;
  fy:=fy+r.Top;
  fw:=r.Right-r.Left;
  fh:=r.Bottom-r.Top;
  for i:=1 to l.Count do
    ObjectABC(l[i]).SetCoords(ObjectABC(l[i]).Left-r.Left,ObjectABC(l[i]).Top-r.Top);
end;

procedure ContainerABC.UnLink(g: ObjectABC);
begin
  l.OwnsObjects:=False;
  l.Remove(g);
  l.OwnsObjects:=True;
  Redraw;
  RecalcBounds;
end;

function CreateRectangleABC(x,y,w,h: integer; cl: ColorType): RectangleABC;
begin
  Result:=RectangleABC.Create(x,y,w,h,cl);
end;

function CreateRoundRectABC(x,y,w,h,r: integer; cl: ColorType): RoundRectABC;
begin
  Result:=RoundRectABC.Create(x,y,w,h,r,cl);
end;

function CreateSquareABC(x,y,w: integer; cl: ColorType): SquareABC;
begin
  Result:=SquareABC.Create(x,y,w,cl);
end;

function CreateRoundSquareABC(x,y,w,r: integer; cl: ColorType): RoundSquareABC;
begin
  Result:=RoundSquareABC.Create(x,y,w,r,cl);
end;

function CreateEllipseABC(x,y,w,h: integer; cl: ColorType): EllipseABC;
begin
  Result:=EllipseABC.Create(x,y,w,h,cl);
end;

function CreateCircleABC(x,y,w: integer; cl: ColorType): CircleABC;
begin
  Result:=CircleABC.Create(x,y,w,cl);
end;

function CreateTextABC(x,y,pt: integer; cl: ColorType; tx: string): TextABC;
begin
  Result:=TextABC.Create(x,y,pt,cl,tx);
end;

function CreateRegularPolygonABC(x,y,r,nn: integer; cl: ColorType): RegularPolygonABC;
begin
  Result:=RegularPolygonABC.Create(x,y,r,nn,cl);
end;

function CreateStarABC(x,y,r,r1,nn: integer; cl: ColorType): StarABC;
begin
  Result:=StarABC.Create(x,y,r,r1,nn,cl);
end;

function CreatePictureABC(x,y: integer; fname: string): PictureABC;
begin
  Result:=PictureABC.Create(x,y,fname);
end;

function CreateMultiPictureABC(x,y: integer; fname: string): MultiPictureABC;
begin
  Result:=MultiPictureABC.Create(x,y,fname);
end;

function CreateMultiPictureABC(x,y,w: integer; fname: string): MultiPictureABC;
begin
  Result:=MultiPictureABC.Create(x,y,w,fname);
end;

function CreateBoardABC(x,y,nn,mm,ssz: integer; cl: ColorType): BoardABC;
begin
  Result:=BoardABC.Create(x,y,nn,mm,ssz,cl);
end;

function CreateContainerABC(x,y: integer): ContainerABC;
begin
  Result:=ContainerABC.Create(x,y);
end;

procedure RedrawObjects;
begin
  drawRect(RectF(0,0,WindowWidth,WindowHeight));
end;

procedure LockDrawingObjects;
begin
  __LockDrawingObjects:=True;
end;

procedure UnLockDrawingObjects;
begin
  __LockDrawingObjects:=False;
  RedrawObjects;
end;

function ObjectsCount: integer;
begin
  Result:=__l.Count;
end;

function ObjectUnderPoint(x,y: integer): ObjectABC;
var
  i: integer;
  g: ObjectABC;
begin
  Result:=nil;
  for i:=ObjectsCount downto 1 do
  begin
    g:=Objects[i];
    if g.PtInside(x,y) then
    begin
      Result:=g;
      Exit
    end;
  end;
end;

function ObjectUnderPoint(p: Point): ObjectABC;
begin
  Result:=ObjectUnderPoint(p.x,p.y);
end;

function UIElementUnderPoint(x,y: integer): UIElementABC;
var
  p: Point;
  i: integer;
  g: UIElementABC;
begin
  p:=PointF(x,y);
  Result:=nil;
  for i:=__lUI.Count downto 1 do
  begin
    g:=UIElementABC(__lUI[i]);
    if g.PtInside(x,y) then
    begin
      Result:=g;
      Exit
    end;
  end;
end;

procedure ToFront(grobj: ObjectABC);
var ind: integer;
begin
  if grobj is UIElementABC then exit;
  if grobj=nil then exit;
  ind:=__l.IndexOf(grobj);
  if ind=__l.Count then Exit;
  if ind=0 then Exit;
  __l.OwnsObjects:=False;
  __l.Delete(ind);
  __l.Add(grobj);
  __l.OwnsObjects:=True;
  grobj.Redraw;
end;

procedure ToBack(grobj: ObjectABC);
var ind: integer;
begin
  if grobj is UIElementABC then exit;
  if grobj=nil then exit;
  ind:=__l.IndexOf(grobj);
  if ind=1 then Exit;
  if ind=0 then Exit;
  __l.OwnsObjects:=False;
  __l.Delete(ind);
  __l.Insert(1,grobj);
  __l.OwnsObjects:=True;
  grobj.Redraw;
end;

procedure MyClose0;
begin
  __l.Destroy;
  __lUI.Destroy
end;

procedure ABCMouseDown(x,y,mb:integer);
begin
  movingObject:=ObjectUnderPoint(x,y);
  if (movingObject<>nil) and not movingObject.MouseIndifferent then
  begin
    ABC_dx:=x-movingObject.Left;
    ABC_dy:=y-movingObject.Top;
    ToFront(movingObject);
  end
  else
  begin
    ABC_dx:=0;
    ABC_dy:=0;
  end
end;

procedure ABCMouseMove(x,y,mb:integer);
begin
  if mb<>1 then Exit;
  if (movingObject<>nil) and not movingObject.MouseIndifferent then
    movingObject.moveto(x-ABC_dx,y-ABC_dy);
end;

procedure ABCMouseUp(x,y,mb:integer);
begin
  movingObject:=nil;
end;

procedure SetMouseDragABCObjects;
begin
  OnMouseDown:=ABCMouseDown;
  OnMouseUp:=ABCMouseUp;
  OnMouseMove:=ABCMouseMove;
end;

procedure ABCRedrawProc;
begin
  drawrect(RectF(0,0,WindowWidth,WindowHeight));
end;

function PointF(x,y: integer): Point;
begin
  Result:=PointRect.PointF(x,y);
end;

function RectF(l,t,r,b: integer): Rect;
begin
  Result:=PointRect.RectF(l,t,r,b);
end;

initialization
  __LockDrawingObjects:=False;
  __l:=ObjectArray.Create;
  __lUI:=ObjectArray.Create;
  Objects:=ObjectsABCArray.Create(__l);
  SetWindowCaption('����������� ������� ABC');
  SetWindowSize(640,480);
  CenterWindow;
  CurrentDrawingBitmap:=0;
  movingobject:=nil;
  SetRedrawProc(ABCRedrawProc);
finalization

end.
