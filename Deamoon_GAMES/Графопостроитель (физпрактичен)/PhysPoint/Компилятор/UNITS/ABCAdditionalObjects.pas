unit ABCAdditionalObjects;

interface

uses GraphABC, ABCObjects;

type
  CachedPicABC=class(ObjectABC)
  private
    b: integer;
    changed: boolean;
  public
    constructor Create(x,y,w,h: integer);
    destructor Destroy;
    procedure Draw0(b: integer); begin end;
    procedure Draw(x,y: integer);
    procedure SetWidth(Width: integer);
    procedure SetHeight(Height: integer);
    function Clone0: ObjectABC; begin Result:=CachedPicABC.CreateBy(Self) end;
    function Clone: CachedPicABC; begin Result:=CachedPicABC(Clone0) end;
  end;

implementation

//------ CachedPicABC ------
constructor CachedPicABC.Create(x,y,w,h: integer);
begin
  changed:=True;
  b:=CreatePicture(w,h);
  SetPictureTransparent(b,True);
  inherited Create(x,y,w,h,clBlack);
end;

destructor CachedPicABC.Destroy;
begin
  DestroyPicture(b);
end;

procedure CachedPicABC.Draw(x,y: integer);
begin
  if changed then
  begin
    changed:=False;
    SetDrawingSurface(b);
    Draw0(b);
    if CurrentDrawingBitmap=0 then
      RestoreDrawingSurface
    else
      SetDrawingSurface(CurrentDrawingBitmap);
  end;
  DrawPicture(b,x,y);
end;

procedure CachedPicABC.SetWidth(Width: integer);
begin
  changed:=True;
  SetPictureSize(b,Width,Height);
  inherited SetWidth(Width);
end;

procedure CachedPicABC.SetHeight(Height: integer);
begin
  changed:=True;
  SetPictureSize(b,Width,Height);
  inherited SetHeight(Height);
end;

end.
