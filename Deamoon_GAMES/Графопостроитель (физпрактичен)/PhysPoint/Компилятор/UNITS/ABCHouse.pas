unit ABCHouse;

interface

uses GraphABC, ABCObjects;

type
  DoorABC=class(RectangleABC)
    procedure Draw(x,y: integer);
    begin
      inherited Draw(x,y);
      SetPenWidth(1);
      SetBrushColor(clWhite);
      Ellipse(x+Width-20,y+Height div 2 - 5,x+Width-10,y+Height div 2 + 5);
    end;
    function Clone0: ObjectABC; begin Result:=DoorABC.CreateBy(Self) end;
    function Clone: DoorABC; begin Result:=DoorABC(Clone0) end;
  end;

  WindowABC=class(RectangleABC)
    ww: integer;
  private
    constructor Create(x,y,w,h: integer; cl: ColorType); begin  end;
    procedure SetWidth2(w: integer);
  public
    constructor Create(x,y,w,h,www: integer; cl: ColorType);
    begin
      ww:=www+1;
      inherited Create(x,y,w,h,cl);
    end;
    procedure Draw(x,y: integer);
    var v: integer;
    begin
      v:=(Width-ww*3) div 2;
      inherited Draw(x,y);
      SetBrushColor(clWhite);
      SetPenWidth(1);
      Rectangle(x+ww,y+ww,x+ww+v+1,y+Height-ww);
      Rectangle(x+ww*2+v,y+ww,x+Width-ww,y+ww+v+1);
      Rectangle(x+ww*2+v,y+ww*2+v,x+Width-ww,y+Height-ww);
    end;
    property Width2:integer read ww write SetWidth2;
    function Clone0: ObjectABC; begin Result:=DoorABC.CreateBy(Self) end;
    function Clone: WindowABC; begin Result:=WindowABC(Clone0) end;
  end;

  HouseABC=class(ContainerABC)
  private
    function GetWindow: WindowABC; begin Result:=WindowABC(l[3]); end;
    function GetDoor: DoorABC; begin Result:=DoorABC(l[2]); end;
    function GetWall: RectangleABC; begin Result:=RectangleABC(l[1]); end;
  public
    constructor Create(x,y,w,h: integer; cl: ColorType);
    property Door: DoorABC read GetDoor;
    property Window: WindowABC read GetWindow;
    property Wall: RectangleABC read GetWall;
    function Clone0: ObjectABC; begin Result:=HouseABC.CreateBy(Self) end;
    function Clone: HouseABC; begin Result:=HouseABC(Clone0) end;
  end;

implementation

//------ HouseABC ------
constructor HouseABC.Create(x,y,w,h: integer; cl: ColorType);
var zazw,doorw,doorh,winw,winh: integer;
begin
  zazw:=w div 6;
  doorw:=w div 4;
  doorh:=2*h div 3;
  winw:=w div 4;
  winh:=h div 2;
  inherited Create(x,y);
  Add(RectangleABC.Create(0,0,w,h,cl));
  Add(DoorABC.Create(zazw,h-doorh,doorw,doorh,clWhite));
  Add(WindowABC.Create(w-zazw-winw,h div 4,winw,winh,2,clWhite));
end;

//------ WindowABC ------
procedure WindowABC.SetWidth2(w: integer);
begin
  if ww=w+1 then Exit;
  ww:=w+1;
  Redraw;
end;

end.
