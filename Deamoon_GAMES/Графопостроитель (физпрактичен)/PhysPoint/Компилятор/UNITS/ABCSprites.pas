unit ABCSprites;

interface

uses ABCObjects,Containers,Timers,Events,GraphABC,Utils;

type
  SpriteABC = class(MultiPictureABC)
  private
    StateNames: StringArray; // ����� ���������
    StateBegs: IntArray;     // ������ ��������, ���������� �������� ���������
    StateCounts: IntArray;   // ����� ���������
    curst: integer;          // ����� �������� ���������
    ticks: integer;          // ������� ����� �������� �� ����� �����, ������� �������������� ��������: 0..10
    act: boolean;            // ������� �� ������
    curtick: integer;        // ������� ���, ����� �� ���������� ������
    procedure SetStateName(n: string);
    function GetStateName: string;
    procedure SetState(n: integer);
    function GetStateCount: integer;
    procedure SetSpeed(n: integer);
    function GetSpeed: integer;
    procedure SetActive(b: boolean);
    procedure SetFrame(n: integer);
    function GetFrame: integer;
    function FrameCount: integer;
    function FrameBeg: integer;
  public
    constructor Create(x,y: integer; fname: string);   // ����� ����� �������� �������� Add � AddState, ����� CheckStates
    constructor Create(x,y,w: integer; fname: string); // ����� ����� �������� �������� AddState, ����� CheckStates
    constructor CreateFromPic(x,y,w: integer; p: Picture);
    constructor CreateFromInfo(x,y: integer; fname: string);
      // CreateFromInfo - ��������� ������ �������� ������� - �� ��������������� �����
      // ����� ���� ����� ��������, ����������� ��������� �������� AddState, ����� ���� ������� SaveWithInfo
    destructor Destroy;
    procedure AddState(name: string; count: integer);
    procedure CheckStates;
    procedure Add(fname: string);
    procedure SaveWithInfo(fname: string); // ����������� ����������� � �������������� �����
    procedure NextFrame;     // ������� � ���������� ������ � ������� ���������
    procedure NextTick;      // ������� � ���������� ���� �������; ���� �� ����� ticks, �� �� ������������ � 1 � ���������� NextFrame
    property StateName: string read GetStateName write SetStateName;
    property State: integer read curst write SetState;
    property StateCount: integer read GetStateCount;
    property Speed: integer read GetSpeed write SetSpeed;
    property Active: boolean read act write SetActive;
    property Frame: integer read GetFrame write SetFrame;
    function FrameCount: integer;
    function FrameBeg: integer;
  end;

procedure StartSprites;
procedure StopSprites;

implementation

const infoext='.spinf';
  
var
  _Sprites: ObjectArray; // ������ ��������
  _t: integer;          // ��������� �������
  timerMs: integer;
  
constructor SpriteABC.Create(x,y: integer; fname: string);
begin
  StateNames:=StringArray.Create;
  StateBegs:=IntArray.Create;
  StateCounts:=IntArray.Create;
  StateBegs.Add(1);
  StateCounts.Add(1);
  StateNames.Add('');
  curst:=1;
  curtick:=1;
  Speed:=5;
  act:=True;
  _Sprites.Add(Self);
  inherited Create(x,y,fname);
end;

constructor SpriteABC.Create(x,y,w: integer; fname: string);
begin
  StateNames:=StringArray.Create;
  StateBegs:=IntArray.Create;
  StateCounts:=IntArray.Create;
  StateBegs.Add(1);
  StateCounts.Add(1);
  StateNames.Add('');
  curst:=1;
  curtick:=1;
  Speed:=5;
  act:=True;
  _Sprites.Add(Self);
  inherited Create(x,y,w,fname);
  StateCounts[1]:=Count;
end;

constructor SpriteABC.CreateFromPic(x,y,w: integer; p: Picture);
begin
  StateNames:=StringArray.Create;
  StateBegs:=IntArray.Create;
  StateCounts:=IntArray.Create;
  StateBegs.Add(1);
  StateCounts.Add(1);
  StateNames.Add('');
  curst:=1;
  curtick:=1;
  Speed:=5;
  act:=True;
  _Sprites.Add(Self);
  inherited CreateFromPic(x,y,w,p);
  StateCounts[1]:=Count;
end;

constructor SpriteABC.CreateFromInfo(x,y: integer; fname: string);
var
  s,vs,sname: string;
  f: System.text;
  i,j,w,sp,num: integer;
begin
  if not FileExists(fname) then
    fname:=StandardImageFolder+fname;
  if not FileExists(fname) then
    raise Exception.Create('���� '+ExtractFileName(fname)+' �� ������');

  StateNames:=StringArray.Create;
  StateBegs:=IntArray.Create;
  StateCounts:=IntArray.Create;
  StateBegs.Add(1);
  StateCounts.Add(1);
  StateNames.Add('');
  curst:=1;
  curtick:=1;
  Speed:=5;
  act:=True;

  s:=LowerCase(ExtractFileExt(fname));
  i:=Pos(s,fname);
  s:=fname;
  Delete(s,i,Length(s));
  s:=s+infoext;
  if not FileExists(s) then
    raise Exception.Create('�������������� ���� ������� '+ExtractFileName(s)+' �� ������');

  try
    assign(f,s);
    reset(f);
    readln(f,w);
    readln(f,sp);
    Speed:=sp;
    readln(f,num);
    for i:=1 to num do
    begin
      readln(f,vs);
      j:=Pos(' ',vs);
      sname:=Copy(vs,1,j-1);
      Delete(vs,1,j);
      vs:=TrimLeft(vs);
      j:=Pos(' ',vs);
      if j>0 then
        vs:=Copy(vs,1,j-1);
      AddState(sname,StrToInt(vs));
    end;
    close(f);
  except
    raise Exception.Create('������ ���������� �� ��������������� ����� �������');
  end;
  _Sprites.Add(Self);
  inherited Create(x,y,w,fname);
  CheckStates;
end;

destructor SpriteABC.Destroy;
begin
  _Sprites.OwnsObjects:=False;
  _Sprites.Remove(Self);
  _Sprites.OwnsObjects:=True;
  StateNames.Destroy;
  StateBegs.Destroy;
  StateCounts.Destroy;
  inherited Destroy;
end;

procedure SpriteABC.SaveWithInfo(fname: string);
var
  s: string;
  f: System.text;
  i: integer;
begin
  CheckStates;
  s:=LowerCase(ExtractFileExt(fname));
  if (s<>'.bmp') and (s<>'.jpg') and (s<>'.gif') and (s<>'.png') then
    raise Exception.Create('����� �������� ������ ������������ �����');
  Save(fname);
  i:=Pos(s,fname);
  Delete(fname,i,Length(s));
  fname:=fname+infoext;
  assign(f,fname);
  rewrite(f);
  writeln(f,width,' // ������ �����');
//  writeln(f,count,' // ���������� ������');
  writeln(f,Speed,' // ��������');
  writeln(f,StateCount,' // ���������� ���������');
  for i:=1 to StateCount do
    if i=1 then
      writeln(f,StateNames[i],' ',StateCounts[i],' // ����� ��������� � ���������� ������ � ���')
    else writeln(f,StateNames[i],' ',StateCounts[i]);
  close(f);
end;

procedure SpriteABC.CheckStates;
var
  s: integer;
  i: integer;
begin
  s:=0;
  for i:=1 to StateCount do
    s:=s+StateCounts[i];
  if s<>Count then
    raise Exception.Create('����� ������ � ���������� ������� ���������� �� ������ ���������� ������');
end;

procedure SpriteABC.Add(fname: string);
begin
  Assert(StateCount=1,'��� ���������� ������ ���������� ��������� ������ ���� ����� 1');
  inherited Add(fname);
  StateCounts[1]:=StateCounts[1]+1;
end;

procedure SpriteABC.NextTick;
begin
  Inc(curtick);
  if curtick>ticks then
  begin
    NextFrame;
    curtick:=1;
  end;
end;

procedure SpriteABC.SetStateName(n: string);
var i: integer;
begin
  i:=StateNames.IndexOf(n);
  if i<>0 then
    State:=i;
end;

function SpriteABC.GetStateName: string;
begin
  Result:=StateNames[curst];
end;

procedure SpriteABC.SetState(n: integer);
begin
  if curst=n then
    exit;
  if n<1 then
    n:=1;
  if n>StateCount then
    n:=StateCount;
  curst:=n;
  CurrentPicture:=StateBegs[curst];
  Redraw;
end;

function SpriteABC.GetStateCount: integer;
begin
  Result:=StateCounts.Count;
end;

procedure SpriteABC.SetSpeed(n: integer);
begin
  // ���� ��� ������� ��������
  // ������� �������� �� ��������� ���� ��� ���������� ��������!
  case n of
1: ticks:=30;
2: ticks:=20;
3: ticks:=14;
4: ticks:=10;
5: ticks:=8;
6: ticks:=6;
7: ticks:=4;
8: ticks:=3;
9: ticks:=2;
10: ticks:=1;
  end;
end;

function SpriteABC.GetSpeed: integer;
begin
  case ticks of
30: Result:=1;
20: Result:=2;
14: Result:=3;
10: Result:=4;
8:  Result:=5;
6:  Result:=6;
4:  Result:=7;
3:  Result:=8;
2:  Result:=9;
1:  Result:=10;
  end;
end;

procedure SpriteABC.SetActive(b: boolean);
begin
  act:=b;
end;

procedure SpriteABC.NextFrame;
var n: integer;
begin
  n:=Frame+1;
  if n>StateCounts[curst] then
    n:=1;
  Frame:=n;
//  Redraw; // ��� ���� ��������� �� �����!
end;

procedure SpriteABC.SetFrame(n: integer);
begin
  if Frame=n then
    exit;
  if n<1 then
    n:=1;
  if n>StateCounts[curst] then
    n:=StateCounts[curst];
  CurrentPicture:=StateBegs[curst]+n-1;
end;

function SpriteABC.GetFrame: integer;
begin
  Result:=CurrentPicture-StateBegs[curst]+1;
end;

function SpriteABC.FrameCount: integer;
begin
  Result:=StateCounts[curst];
end;

function SpriteABC.FrameBeg: integer;
begin
  Result:=StateBegs[curst];
end;

procedure SpriteABC.AddState(name: string; count: integer);
begin
  if (StateNames[1]='') then
  begin
//    StateBegs[1]:=1;
    StateCounts[1]:=count;
    StateNames[1]:=name;
  end
  else
  begin
    StateBegs.Add(StateBegs[StateCount]+StateCounts[StateCount]);
    StateCounts.Add(count);
    StateNames.Add(name);
  end;
end;

procedure StartSprites;
begin
  StartTimer(_t);
end;

procedure StopSprites;
begin
  StopTimer(_t);
end;

var k: integer;

procedure TimerProc;
var i: integer;
    s: SpriteABC;
begin
//  LockDrawingObjects;
  i:=1;
  while i<=_Sprites.Count do
  begin
    SpriteABC(_Sprites[i]).NextTick;
    Inc(i);
  end;
  Inc(k);
  //SetWindowCaption(IntToStr(round(k*1000/Milliseconds)));
  //RedrawObjects;
end;


initialization
  timerMs:=50; // ������������� �������. ����� ��������� ��������������
  _Sprites:=ObjectArray.Create;
  _t := CreateTimer(timerMs,TimerProc);
  StopTimer(_t);
finalization
  StartSprites;
end.