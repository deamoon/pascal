uses
  graphabc;

type
  vector_real = record
    x, y: real;
  end;

type
  vector_int = record
    x, y: integer;
  end;

procedure sv(var v: vector_real; a, b: real);
begin
  v.x := a; v.y := b;
end;

function nerav(a, b: vector_int): boolean;
begin
  nerav := ((a.x <> b.x) or (a.y <> b.y));
end;

var
  r, v: vector_real;
  t: real;
  i, k, st1, n1, leng, kol, n_bez1: longint;
  q1: array[0..1000, 0..660] of boolean;
  a1: array[0..200000] of vector_int;
  shar: vector_int;
  s:string;

procedure KeyDown(Key: integer);
begin
  case Key of
    vk_Left: if v.y <> 0 then sv(v,-k,0);
    vk_Right: if v.y <> 0 then sv(v,k,0);
    vk_Up: if v.x <> 0 then sv(v,0,-k);
    vk_Down: if v.x <> 0 then sv(v,0,k);
  end;
end;

procedure cir;
begin
  SetPenColor(clWhite);
  circle(shar.x, shar.y, 5);
  SetPenColor(clRed);
  shar.x := random(900) + 30; shar.y := random(600) + 30;
  circle(shar.x, shar.y, 5); FloodFill(shar.x, shar.y, clRed);
end;

procedure KeyPress(Key: char);
begin
  case key of
        'a': if v.y <> 0 then sv(v, -k, 0); 'A': if v.y <> 0 then sv(v, -k, 0);
        '�': if v.y <> 0 then sv(v, -k, 0); '�': if v.y <> 0 then sv(v, -k, 0);
        '�': if v.x <> 0 then sv(v, 0, -k); '�': if v.x <> 0 then sv(v, 0, -k);
        'w': if v.x <> 0 then sv(v, 0, -k); 'W': if v.x <> 0 then sv(v, 0, -k);
        '�': if v.y <> 0 then sv(v, k, 0); '�': if v.y <> 0 then sv(v, k, 0);
        'd': if v.y <> 0 then sv(v, k, 0); 'D': if v.y <> 0 then sv(v, k, 0);
        '�': if v.x <> 0 then sv(v, 0, k); '�': if v.x <> 0 then sv(v, 0, k);
        's': if v.x <> 0 then sv(v, 0, k); 'S': if v.x <> 0 then sv(v, 0, k);
        'q': k := k + 5; '�': k := k + 5; '�': k := k + 5; 'Q': k := k + 5;
        'e': if k>5 then k := k - 5; '�': if k>5 then  k := k - 5; '�': if k>5 then k := k - 5; 'E': if k>5 then k := k - 5;
  end;
end;

begin
  SetWindowWidth(1000); SetWindowHeight(660);
  OnKeyPress := KeyPress;
  OnKeyDown := KeyDown;
  SetWindowTitle('Snake 1.0'); CenterWindow; 
  
  r.x := 300; r.y := 150;
  v.x := 5; v.y := 0; k := 20;
  t := 0.01; i := 0; st1 := 1; cir; leng := 1000; kol := 0;
  while true DO 
  begin
    
    SetPenColor(clBlue);
    circle(round(r.x), round(r.y), 5);    n1 := (n1 + 1) mod 200000;
    a1[n1].x := round(r.x); a1[n1].y := round(r.y); inc(i); if i > 1000000000 then i := leng + 1;
    if ((abs(round(r.x) - shar.x) <= 7) and (abs(round(r.y) - shar.y) <= 7)) then begin
      cir; inc(kol);leng := leng + 50; if kol mod 10 = 0 then inc(k, 10);
      if kol mod 30 = 0 then FloodFill(999, 659, clBlack);
      if kol mod 40 = 0 then FloodFill(999, 659, clGreen);
      SetPenColor(clBlack);
//      moveto(1, 1);
      s:=' �������: ' + inttostr(kol);
      TextOut(5,5,s);
    end;
    
    if n1 = 0 then n_bez1 := 200000 - 1 else n_bez1 := n1 - 1;
    if ((q1[round(r.x), round(r.y)]) and (nerav(a1[n1], a1[n_bez1]))) then begin TextOut(5,30,'�� ��������'); v.x := 0; v.y := 0; break; end;
    q1[round(r.x), round(r.y)] := true;
    
    if i > leng then begin
      dec(i);
      SetPenColor(clWhite);
      q1[a1[st1].x, a1[st1].y] := false;
      circle(a1[st1].x, a1[st1].y, 5);
      st1 := (st1 + 1) mod 200000;
    end else begin
      SetPenColor(clWhite);
      circle(5, 5, 5);
    end;
    
    r.x := r.x + v.x * t; r.y := r.y + v.y * t;
    

    
  end;
  
end.