uses graphabc,crt,sounds;

type vector_real = record;
  x,y:real;
end;
type vector_int = record;
  x,y:integer;
end;

procedure sv(var v:vector_real; a,b:real);
begin
  v.x:=a; v.y:=b;
end;
function nerav(a,b:vector_int):boolean;
begin
  nerav:=((a.x<>b.x) or (a.y<>b.y));
end;

var
  r,v:vector_real;
  t:real;
  i,k,st1,n1,leng,kol,n_bez1:longint;
  c:char;
  mus,music1,music2,music3,music4,music5:integer;
  q1:array[0..1000,0..660] of boolean;
  a1:array[0..200000] of vector_int;
  shar:vector_int;

procedure cir;
begin
  SetPenColor(clWhite);
  circle(shar.x,shar.y,5);
  SetPenColor(clRed);
  shar.x:=random(900)+30; shar.y:=random(600)+30;
  circle(shar.x,shar.y,5); FloodFill(shar.x,shar.y,clRed);
end;

begin
SetWindowWidth(1000); SetWindowHeight(660);

music1:=LoadSound('3.wma'); music2:=LoadSound('4.wma'); music3:=LoadSound('5.wma'); music4:=LoadSound('6.wma'); music5:=LoadSound('7.wma');
r.x:=300; r.y:=150;
v.x:=5; v.y:=0; k:=20;
t:=0.001; i:=0; st1:=1; cir; leng:=10000; kol:=0;

  WHILE true DO BEGIN

    SetPenColor(clBlue);
    circle(round(r.x),round(r.y),5);    n1:=(n1+1) mod 200000;
    a1[n1].x:=round(r.x); a1[n1].y:=round(r.y); inc(i); if i>1000000000 then i:=leng+1;
    if ((abs(round(r.x)-shar.x)<=7) and (abs(round(r.y)-shar.y)<=7)) then begin
      mus:=random(5);
      case mus of
        0: PlaySound(music1);
        1: PlaySound(music2);
        2: PlaySound(music3);
        3: PlaySound(music4);
        4: PlaySound(music5);
      end;
      cir; inc(kol);leng:=leng+500; if kol mod 10 = 0 then inc(k,10);
      if kol mod 30 = 0 then FloodFill(999,659,clBlack);
      SetPenColor(clBlack);
      FloodFill(2,2,clWhite);
      Rectangle(0,0,100,20);
      GotoXY(1,1);
      write(' �������: ',kol);
    end;
    
    if n1=0 then n_bez1:=200000-1 else n_bez1:=n1-1;
    if ((q1[round(r.x),round(r.y)]) and (nerav(a1[n1],a1[n_bez1]))) then begin write(' �� ��������'); v.x:=0; v.y:=0; break; end;
    q1[round(r.x),round(r.y)]:=true;
    
    if i>leng then begin
      dec(i);
      SetPenColor(clWhite);
      q1[a1[st1].x,a1[st1].y]:=false;
      circle(a1[st1].x,a1[st1].y,5);
      st1:=(st1+1) mod 200000;
    end else begin
      SetPenColor(clWhite);
      circle(5,5,5);
    end;

    r.x:=r.x+v.x*t; r.y:=r.y+v.y*t;

    if KeyPressed then begin
      c:=ReadKey;
      case c of
        'M':if v.y<>0 then sv(v,-k,0);
        'H':if v.x<>0 then sv(v,0,-k);
        'K':if v.y<>0 then sv(v,k,0);
        'P':if v.x<>0 then sv(v,0,k);
        'q':k:=k+10;
        'e':k:=k-10;
      end;
    end;

  END;

end.