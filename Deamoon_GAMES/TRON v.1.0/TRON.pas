uses graphabc,crt;

type vector_real = record;
  x,y:real;
end;
type vector_int = record;
  x,y:integer;
end;

procedure sv(var v:vector_real; a,b:real);
begin
  v.x:=a; v.y:=b;
end;

var
  r,v,r1,b:vector_real;
  t:real;
  i:longint;
  c:char;
  k:integer;
  q1,q2:array[0..2000,0..2000] of boolean;
  
begin
SetWindowWidth(1280); SetWindowHeight(860); SetPenColor(clBlue);

r.x:=300; r.y:=150;
r1.x:=350; r1.y:=300;
v.x:=0; v.y:=0; b.x:=0; b.y:=0; k:=20;
t:=0.001;

  WHILE true DO BEGIN

    SetPenColor(clBlue);
    circle(round(r.x),round(r.y),5);
    if q2[round(r.x),round(r.y)] then begin write('����� ���'); v.x:=0; v.y:=0; b.x:=0; b.y:=0; break; end;
    q1[round(r.x),round(r.y)]:=true;

    SetPenColor(clRed);
    circle(round(r1.x),round(r1.y),5);
    if q1[round(r1.x),round(r1.y)] then begin write('������� ���'); v.x:=0; v.y:=0; b.x:=0; b.y:=0; break; end;
    q2[round(r1.x),round(r1.y)]:=true;
    
    r.x:=r.x+v.x*t; r.y:=r.y+v.y*t;
    r1.x:=r1.x+b.x*t; r1.y:=r1.y+b.y*t;

    if KeyPressed then begin
      c:=ReadKey;
      case c of
        'M':sv(v,-k,0);
        'H':sv(v,0,-k);
        'K':sv(v,k,0);
        'P':sv(v,0,k);
        'w':sv(b,0,-k);
        'd':sv(b,k,0);
        's':sv(b,0,k);
        'a':sv(b,-k,0);
      end;
    end;
    
  END;
  
end.