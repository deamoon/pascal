//COPYRIGHT DEAMOON

uses graphabc,crt;

type vector_real = record;
  x,y:real;
  end;
type vector_int = record;
  x,y:longint;
  end;
  
var
  r,r0,r2,v,s,dr,a,dr2,a2,a3,r3,dr3:vector_real; t,p:real; rad,k,w,k2,rad2,k3:longint;
begin
  k:=3000; k2:=3000; k3:=3000; rad:=10; rad2:=30;

  SetWindowWidth(1280); SetWindowHeight(860); SetPenColor(clBlue);
  r0.x:=200; r0.y:=300; r2.x:=800; r2.y:=300; r3.x:=500; r3.y:=300;
  circle(round(r0.x),round(r0.y),rad2); FloodFill(round(r0.x),round(r0.y),clBlue);
  circle(round(r2.x),round(r2.y),rad2); FloodFill(round(r2.x),round(r2.y),clBlue);
  //circle(round(r3.x),round(r3.y),rad2); FloodFill(round(r3.x),round(r3.y),clBlue);
  
  r.x:=400; r.y:=200; v.x:=8; v.y:=0; t:=0.1; //w:=0;
  while true do begin
    ///////////////////////////////////
    dr.x:=r0.x-r.x; dr.y:=r0.y-r.y; dr2.x:=r2.x-r.x; dr2.y:=r2.y-r.y; dr3.x:=r3.x-r.x; dr3.y:=r3.y-r.y;
    a.x:=k*dr.x/(sqrt((dr.x*dr.x+dr.y*dr.y)*(dr.x*dr.x+dr.y*dr.y)*(dr.x*dr.x+dr.y*dr.y)));
    a.y:=k*dr.y/(sqrt((dr.x*dr.x+dr.y*dr.y)*(dr.x*dr.x+dr.y*dr.y)*(dr.x*dr.x+dr.y*dr.y)));
    a2.x:=k2*dr2.x/(sqrt((dr2.x*dr2.x+dr2.y*dr2.y)*(dr2.x*dr2.x+dr2.y*dr2.y)*(dr2.x*dr2.x+dr2.y*dr2.y)));
    a2.y:=k2*dr2.y/(sqrt((dr2.x*dr2.x+dr2.y*dr2.y)*(dr2.x*dr2.x+dr2.y*dr2.y)*(dr2.x*dr2.x+dr2.y*dr2.y)));
a3.x:=0;    //a3.x:=k3*dr3.x/(sqrt((dr3.x*dr3.x+dr3.y*dr3.y)*(dr3.x*dr3.x+dr3.y*dr3.y)*(dr3.x*dr3.x+dr3.y*dr3.y)));
a3.y:=0;    //a3.y:=k3*dr3.y/(sqrt((dr3.x*dr3.x+dr3.y*dr3.y)*(dr3.x*dr3.x+dr3.y*dr3.y)*(dr3.x*dr3.x+dr3.y*dr3.y)));
    a.x:=a.x+a2.x+a3.x; a.y:=a.y+a2.y+a3.y;

    v.x:=v.x+a.x*t; v.y:=v.y+a.y*t;
    s.x:=a.x*t*t/2; s.y:=a.y*t*t/2;
    ///////////////////////////////////

    r.x:=r.x+v.x*t+s.x; r.y:=r.y+v.y*t+s.y;
    SetPenColor(clRed); circle(round(r.x),round(r.y),rad); FloodFill(round(r.x),round(r.y),clRed);
    //delay(10);
    {SetPenColor(clWhite);} circle(round(r.x),round(r.y),rad); //FloodFill(round(r.x),round(r.y),clWhite);
    //Setpixel(round(r.x),round(r.y),ClRed);
  end;
  
end.