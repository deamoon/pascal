program Zad_2;

{$APPTYPE CONSOLE}

var
  a,b,c:array[0..50] of string; i,n,j,z,x,err,t:longint; s:string;

begin
  Assign(Input,'input.txt'); Assign(Output,'output.txt');
  Readln(n);
  for i:=1 to 50 do a[i]:='';
  for i:=1 to n do begin
    Readln(c[i]); s:=c[i];
    t:=0;
    for j:=1 to Length(s) do begin
      if t=1 then begin t:=0; Continue; end;
      if ((s[j]>='0') and (s[j]<='9')) then begin
        Val(s[j],x,err);
        for z:=1 to x do a[i]:=a[i]+s[j+1];
        t:=1;
      end else a[i]:=a[i]+s[j];
    end;
  end;
  readln(s); t:=0;
  for i:=1 to n do begin
    x:=Pos(s,a[i]);
    if x<>0 then begin
      Inc(t); b[t]:=c[i];
    end;
  end;
  Writeln(t);
  for i:=1 to t do Writeln(b[i]);
end.
