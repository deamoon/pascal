program ZAD_1;

{$APPTYPE CONSOLE}

var
  a,b:array[0..10000] of LongInt; i,c,n,j:longint;

begin
  Assign(Input,'input.txt'); Assign(Output,'output.txt');
  Readln(n); FillChar(a,SizeOf(a),0); FillChar(b,SizeOf(b),0); c:=0;
  for i:=1 to n do begin
    Readln(a[i],b[i]);
    Inc(c);
    for j:=1 to c-1 do begin
      if ((a[i]=a[j]) and (b[i]-b[j]<5)) then begin write('YES'); Halt; end;
    end;
  end;
  write('NO');
end.
