var
  d,e:longint;

function pr(a,b:longint):boolean;
  var
    t1,t2,d1,x1,x2,y1,y2:real;
  begin
  if ((a<d) and (b<e)) then begin pr:=true; exit; end;
    t1:=a*e/b;
    t2:=a/b;
    d1:=t1*t2*t1*t2-(t2*t2+1)*(t1*t1-a*a);
    if d1<0 then begin pr:=false; exit; end;
    x1:=(t1*t2+sqrt(d1))/(t2*t2+1);
    x2:=(t1*t2-sqrt(d1))/(t2*t2+1);
    
    if ((a*a-x1*x1<0) or (b*b-(e-x1)*(e-x1)<0)) then y1:=-1 else y1:=sqrt(a*a-x1*x1)+sqrt(b*b-(e-x1)*(e-x1));
    
    if ((x2>0) and ((a*a-x2*x2<0) or (b*b-(e-x2)*(e-x2)<0))) then y2:=-1 else y2:=sqrt(a*a-x2*x2)+sqrt(b*b-(e-x2)*(e-x2));
    
    pr:=(((x1>=0) and (x1<=e) and (y1>=0) and (y1<=d)) or ((x2>=0) and (x2<=e) and (y2>=0) and (y2<=d)))
  end;

var
  a,b,c:longint; p:boolean;
begin
  readln(a,b,c,d,e);
  pr(c,a);
  p:=((pr(a,b)) or (pr(b,a)) or (pr(a,c)) or (pr(c,a)) or (pr(b,c)) or (pr(c,b)));
  if p then write('YES') else write('NO');
end.