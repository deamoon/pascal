var
  d,a,b,c,t,x1,x2:real;
begin
readln(a,b,c);
  if ((a=0) and (b=0) and (c=0)) then write('-1') else begin
    if ((a=0) and (b=0) and (c<>0)) then write('0') else begin
      if (a=0) then write('1 ',-c/b) else begin
        d:=b*b-4*a*c;
        if d<0 then write('0');
        if d>0 then begin write('2 '); x1:=(-b-sqrt(d))/(2*a); x2:=(-b+sqrt(d))/(2*a); if x1>x2 then write(x2:0:2,' ',x1:0:2) else write(x1:0:2,' ',x2:0:2); end;
        if d=0 then write('3 ',-b/(2*a):0:2);
      end;
    end;
  end;
end.