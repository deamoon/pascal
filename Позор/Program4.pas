var
  a,b,c,d,e,f,x,y:real;
begin
readln(a,b,c,d,e,f);

 if (((a=0) and (b=0) and (e<>0)) or ((c=0) and (d=0) and (f<>0))) then write('0') else begin
  if ((a=0) and (b=0) and (c=0) and (d=0)) then begin
    if ((e=0) and (f=0)) then write('2XY') else write('0');
  end;
  ////
  if ((a=0) and (b=0) and (c=0) and (d<>0)) then begin
    write('1X ',f/d:0:2);
  end;
  if ((a=0) and (b=0) and (c<>0) and (d=0)) then begin
    write('1Y ',f/c:0:2);
  end;
  if ((a=0) and (b<>0) and (c=0) and (d=0)) then begin
    write('1X ',e/b:0:2);
  end;
  if ((a<>0) and (b=0) and (c=0) and (d=0)) then begin
    write('1Y ',e/a:0:2);
  end;
  ////
  if ((a=0) and (b=0) and (c<>0) and (d<>0)) then begin
    write('1');
  end;
  if ((a=0) and (b<>0) and (c=0) and (d<>0)) then begin
    if e/b<>f/d then write('0') else write('1X ',e/b:0:2);
  end;
  if ((a=0) and (b<>0) and (c<>0) and (d=0)) then begin
    write('2 ',f/c:0:2,' ',e/b:0:2);
  end;
  if ((a<>0) and (b=0) and (c=0) and (d<>0)) then begin
    write('2 ',e/a:0:2,' ',f/d:0:2);
  end;
  if ((a<>0) and (b=0) and (c<>0) and (d=0)) then begin
    if e/a<>f/c then write('0') else write('1Y ',f/c:0:2);
  end;
  if ((a<>0) and (b<>0) and (c=0) and (d=0)) then begin
    write('1');
  end;
  ////
  if ((a=0) and (b<>0) and (c<>0) and (d<>0)) then begin
    write('2 ',(f-d*e/b)/c:0:2,' ',e/b:0:2);
  end;
  if ((a<>0) and (b=0) and (c<>0) and (d<>0)) then begin
    write('2 ',e/a:0:2,' ',(f-c*e/a)/d:0:2);
  end;
  if ((a<>0) and (b<>0) and (c=0) and (d<>0)) then begin
    write('2 ',(e-b*f/d)/a:0:2,' ',f/d:0:2);
  end;
  if ((a<>0) and (b<>0) and (c<>0) and (d=0)) then begin
    write('2 ',f/c:0:2,' ',(e-a*f/c)/b:0:2);
  end;
  ////
  if ((a<>0) and (b<>0) and (c<>0) and (d<>0)) then begin
    if ((a/c=b/d) and (a/c=e/f)) then write('1');
    if ((a/c=b/d) and (a/c<>e/f)) then write('0');
    if (a/c<>b/d) then begin
      y:=(e*c-a*f)/(b*c-a*d); x:=(e*d-b*f)/(a*d-b*c); write('2 ',x:0:2,' ',y:0:2);
    end;
  end;
 end;
end.