var
h,m,t:longint;
begin
  readln(h,m);
  t:=11*(60*h+m) mod 720;
  if t<>0 then t:=720 - t;
  write(t div 11);
end.